//
//  TabbarController.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/06/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class TabbarController: UITabBarController {
    
    //MARK:- Properties
    //  var isAppMenuLoaded : Bool = false
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.backgroundColor = UIColor.white
        
        self.viewControllers?.forEach({ (aController) in
            
            if aController.isKind(of: DashBoardVC().classForCoder){
                aController.tabBarItem.title = Localized("Title_DashBoard")
            } else if aController.isKind(of: TarrifsVC().classForCoder) {
                aController.tabBarItem.title = Localized("Title_Tariffs")
            } else if aController.isKind(of: ServicesVC().classForCoder) {
                aController.tabBarItem.title = Localized("Title_Services")
            } else if aController.isKind(of: UsersVC().classForCoder) {
                aController.tabBarItem.title = Localized("Title_Users")
            }
        })
        
        /* Check if connected to internet then call AppResume api */
        if Connectivity.isConnectedToInternet == true {
            
            /* Calling AppResume */
            getAppResume()
            
        } else {
            
            /* Calling AppMenu in case not connected to internet */
            self.getAppMenu()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /* isLanguageChanged will be true in case use change his current language */
        if MBPICUserSession.shared.isLanguageChanged == true {
            
            /* Calling AppResume and load information of new selected language */
            getAppResume()
            
            /* Reset changed language check*/
            MBPICUserSession.shared.isLanguageChanged = false
        }
    }
    
    //MARK:- Functios
    
    /**
     Sort AppMenu data and update PIC session and load tabbar options.
     
     - parameter appMenuData: information of AppMenu.
     
     - returns: void.
     */
    func sortAndLoadAppMenu(_ appMenuData: AppMenus) {
        
        /* Setting appMenu information */
        MBPICUserSession.shared.appMenu = appMenuData
        
        /* Sort AppMenus */
        MBPICUserSession.shared.appMenu?.menuHorizontal?.sort { $0.sortOrder.toInt < $1.sortOrder.toInt }
        MBPICUserSession.shared.appMenu?.menuVertical?.sort { $0.sortOrder.toInt < $1.sortOrder.toInt }
        
        /* Load options accourding to response */
        self.setTabBarMenuLayout()
        
        /* Fire notification when App menu updated */
        NotificationCenter.default.post(name: NSNotification.Name(Constants.KN_ReloadHomePage), object: nil, userInfo: nil)
    }
    
    /**
     update Tabbar layout setting accourding to new Data.
     
     - returns: void
     */
    func setTabBarMenuLayout() {
        
        var viewControllers: [UIViewController] = []
        
        // Loop throught each item in new tabbar data
        MBPICUserSession.shared.appMenu?.menuHorizontal?.forEach { (aMenuListItem) in
            
            // Getting object for View controller in tabBar against identifier
            if let aViewControllerObject = self.getObjectOfViewControllerOfType(Identifier: aMenuListItem.identifier, title: aMenuListItem.title) {
                
                viewControllers.append(aViewControllerObject)
            }
        }
        
        /* IN CASE there is no information exist create Dashboard object. */
        if viewControllers.count <= 0 {
            
            // Get instance first add into tabbar
            if let aViewController : UIViewController = getObjectOfViewControllerOfType(Identifier: "dashboard", title: Localized("Title_DashBoard")) {
                
                viewControllers.append(aViewController)
            } else {
                
                // if no instance found then create and add Dashboard object
                if let aViewController :DashBoardVC = DashBoardVC.instantiateViewControllerFromStoryboard() {
                    aViewController.tabBarItem = UITabBarItem(title: Localized("Title_DashBoard"), image: UIImage.imageFor(name:  "dashboard-selected"), selectedImage: UIImage.imageFor(name:  "dashboard-unselected"))
                    // Add EdgeInsets to align image.
                    //aViewController.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
                    viewControllers.append(aViewController)
                }
            }
        }
        // Replace tabbar controllers with new controller.
        self.setViewControllers(viewControllers, animated: true)
    }
    
    /**
     Get or Create object against identifier and return UIViewController object.
     
     - parameter identifier: ViewController identifier.
     
     - returns: UIViewController object againt identifier.
     */
    func getObjectOfViewControllerOfType(Identifier identifier : String, title :String?) -> UIViewController? {
        
        var aViewController : UIViewController?
        
        // Find viewController against identifier from tabbar and
        self.viewControllers?.forEach({ (aController) in
            
            if aController.isKind(of: DashBoardVC().classForCoder) && identifier == "dashboard" {
                aViewController = aController
                return
            } else if aController.isKind(of: TarrifsVC().classForCoder) && identifier == "tariffs" {
                aViewController = aController
                return
            } else if aController.isKind(of: ServicesVC().classForCoder) && identifier == "services" {
                aViewController = aController
                return
            } else if aController.isKind(of: UsersVC().classForCoder) && identifier == "user_group" {
                aViewController = aController
                return
            }
        })
        
        // If not able to find a viewController then create a new ViewController object and add
        if aViewController == nil {
            aViewController = self.createInstanceOfViewControllerFor(Identifier: identifier, title: title)
        } else {
            aViewController?.tabBarItem = createTabBarItem(forIdentifier: identifier, title: title)
        }
        //  aViewController?.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        return aViewController
    }
    
    /**
     Create object against identifier and return UIViewController object.
     
     - parameter identifier: ViewController identifier.
     
     - returns: create UIViewController object againt identifier.
     */
    func createInstanceOfViewControllerFor(Identifier identifier : String, title :String?) -> UIViewController? {
        
        if identifier == "users_group" {
            
            if let aViewController :UsersVC = UsersVC.instantiateViewControllerFromStoryboard() {
                aViewController.tabBarItem = self.createTabBarItem(forIdentifier: identifier, title: title)
                return aViewController
            }
        } else if identifier == "tarrifs" {
            
            if let aViewController :TarrifsVC = TarrifsVC.instantiateViewControllerFromStoryboard() {
                aViewController.tabBarItem = self.createTabBarItem(forIdentifier: identifier, title: title)
                return aViewController
            }
        } else if identifier == "services" {
            
            if let aViewController :ServicesVC = ServicesVC.instantiateViewControllerFromStoryboard() {
                aViewController.tabBarItem = self.createTabBarItem(forIdentifier: identifier, title: title)
                return aViewController
            }
        } else if identifier == "dashboard" {
            
            if let aViewController :DashBoardVC = DashBoardVC.instantiateViewControllerFromStoryboard() {
                aViewController.tabBarItem = self.createTabBarItem(forIdentifier: identifier, title: title)
                return aViewController
            }
        }
        
        return nil
    }
    
    func createTabBarItem(forIdentifier identifier:String, title :String?) -> UITabBarItem {
        
        var itemTitle = ""
        var itemIconName = ""
        var selectedItemIcon = ""

        if identifier == "users_group" {
            itemTitle = Localized("Title_Users")
            itemIconName = "users-selected"
            selectedItemIcon = "users-unselected"
        
        } else if identifier == "tarrifs" {
            
            itemTitle = Localized("Title_Tariffs")
            itemIconName = "tarrifs-selected"
            selectedItemIcon = "tarrifs-unselected"
        
        } else if identifier == "services" {
            
            itemTitle = Localized("Title_Services")
            itemIconName = "services-selected"
            selectedItemIcon = "services-unselected"
        
        } else if identifier == "dashboard" {
            
            itemTitle = Localized("Title_DashBoard")
            itemIconName = "dashboard-selected"
            selectedItemIcon = "dashboard-unselected"
        }
        
        let item = UITabBarItem(title: itemTitle,
                                image: UIImage.imageFor(name: itemIconName),
                                selectedImage: UIImage.imageFor(name: selectedItemIcon))
        
        return item
        
    }
    
    //MARK: - API Calls
    
    /**
     call to get appResume data.
     
     - returns: void.
     */
    func getAppResume() {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.gethomepageResume({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            //            _ = MBAPIClient.sharedClient.getUserGroupData(customerID: MBPICUserSession.shared.picUserInfo?.customerID ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            //
            //                if let userGroupHandler = Mapper<UserGroupData>().map(JSONObject: resultData) {
            //                    MBUtilities.saveUserGroupData(userGroupData: userGroupHandler)
            //                }
            //            })
            
            self.hideActivityIndicator()
            if error != nil {
                if resultCode != Constants.MBAPIStatusCode.sessionExpired.rawValue &&
                    MBUtilities.loadCustomerDataFromUserDefaults() == true {
                    // getting AppMenu Detail
                    self.getAppMenu()
                    
                } else if MBUtilities.loadCustomerDataFromUserDefaults() == false  {
                    
                    error?.showServerErrorInViewController(self, {
                        BaseVC.logout()
                    })
                }
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let appResumeHandler = Mapper<AppResumeData>().map(JSONObject: resultData) {
                        
                        /* Save RateUS information */
                        RateUsAlertVC.saveTitle(title: appResumeHandler.predefinedData?.rateUsPopupTitle)
                        RateUsAlertVC.saveMessage(message: appResumeHandler.predefinedData?.rateUsPopupContent)
                      
                        /* check if user login for the first time then ask to change password */
                        if appResumeHandler.picUserInformation?.picNewReset?.isEqual("1", ignorCase: true) ?? false {
                            if let changePasswordVC :ChangePasswordVC = ChangePasswordVC.instantiateViewControllerFromStoryboard() {
                                changePasswordVC.canUserNavigate = false
                                changePasswordVC.picUserInformation = appResumeHandler.picUserInformation
                                self.presentPOPUP(changePasswordVC, animated: true)
                            }
                        } else  if appResumeHandler.picUserInformation?.picTNC?.isEqual("0", ignorCase: true) ?? false {
                            /* check if terms and condition are updated, If yes, then ask user to accept new terms and conditions. */
                            if let termsConditionVC :TermsConditionsVC = TermsConditionsVC.instantiateViewControllerFromStoryboard() {
                                termsConditionVC.isPopUp = true
                                self.presentPOPUP(termsConditionVC, animated: true)
                            }
                        } else  if appResumeHandler.picUserInformation?.rateUsIOS?.isEqual("0", ignorCase: true) ?? false {
                            
                            /* Check is Rate Us is presented to user before*/
                            if MBUtilities.isRateUsShownBefore() == false {
                                
                                let timeDiffranceInHours = MBDateUtilities.calculateTotalHoursBetweenTwoDates(startingDate: MBUtilities.getLoginTime(), endingDate: Date().todayDateString(), dateFormate: Constants.kHomeDateFormate)
                                
                                if timeDiffranceInHours >= (appResumeHandler.predefinedData?.firstPopup?.toInt ?? 0) {
                                    if let rateUsAlert : RateUsAlertVC = RateUsAlertVC.fromNib() {
                                        rateUsAlert.setRateUs({
                                            self.redirectUserToAppStore()
                                        })
                                        self.presentPOPUP(rateUsAlert, animated: true, completion: nil)
                                    }
                                }
                                
                            } else {
                                let timeDiffranceInDays = MBDateUtilities.calculateTotalDaysBetween(startingDate: MBUtilities.getRateUsLaterTime(), endingDate: Date().todayDateString(), dateFormate: Constants.kHomeDateFormate)
                                
                                if timeDiffranceInDays >= (appResumeHandler.predefinedData?.lateOnPopup?.toInt ?? 0) {
                                    if let rateUsAlert : RateUsAlertVC = RateUsAlertVC.fromNib() {
                                        rateUsAlert.setRateUs({
                                            self.redirectUserToAppStore()
                                        })
                                        self.presentPOPUP(rateUsAlert, animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                        /* Saving AppResume information in User Defaults */
                        MBUtilities.saveAppResumeInfo(loggedInUserMSISDN: MBPICUserSession.shared.msisdn, appResumeData: appResumeHandler)
                    }
                    
                    // getting AppMenu Detail
                    self.getAppMenu()
                    
                } else if resultCode != Constants.MBAPIStatusCode.sessionExpired.rawValue &&
                    MBUtilities.loadCustomerDataFromUserDefaults() == true {
                    // getting AppMenu Detail
                    self.getAppMenu()
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc, {
                        BaseVC.logout()
                    })
                }
            }
            
        })
    }
    
    
    /**
     call to get AppMenu data.
     
     - returns: void.
     */
    func getAppMenu() {
        
        var foundAppMenuData : Bool = false
        /* Loading initial data from UserDefaults */
        // Parssing response data
        if let appMenuHandler :AppMenus = AppMenus.loadFromUserDefaults(key: APIsType.appMenu.localizedAPIKey()) {
            
            /* If found information in userDefaults then hide loader and call in background and load. */
            if (appMenuHandler.menuVertical?.count ?? 0) <= 0 &&
                (appMenuHandler.menuHorizontal?.count ?? 0) <= 0 {
                
                foundAppMenuData = false
            } else {
                foundAppMenuData = true
            }
            
            /* load appMenu information */
            self.sortAndLoadAppMenu(appMenuHandler)
        }
        if foundAppMenuData == false {
            self.showActivityIndicator()
        }
        /*API call tpo get data*/
        _ = MBAPIClient.sharedClient.getAppMenu({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if foundAppMenuData != true {
                self.hideActivityIndicator()
            }
            
            if error != nil && foundAppMenuData == false { /* If error occur Durring AppMenu API call then logout */
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"), {
                    
                    BaseVC.logout()
                })
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let appMenuHandler = Mapper<AppMenus>().map(JSONObject: resultData){
                        
                        /*Save data into user defaults*/
                        appMenuHandler.saveInUserDefaults(key: APIsType.appMenu.localizedAPIKey())
                        
                        /* load appMenu information */
                        self.sortAndLoadAppMenu(appMenuHandler)
                        
                        /* Get unread notification count */
                        self.getNotificationsCount()
                        
                    }
                } else { /* If error occur Durring AppMenu API call then logout */
                    self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"), {
                        
                        if foundAppMenuData != true {
                            BaseVC.logout()
                        }
                    })
                }
            }
        })
    }
    
    /**
     call to get unread Notifications count Information.
     */
    func getNotificationsCount() {
        
        /*API call to get data*/
        _ = MBAPIClient.sharedClient.getNotificationsCount({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            // handling data from API response.
            if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                
                // Parssing response data
                if let notificationsCountResponse = Mapper<NotificationsCount>().map(JSONObject:resultData) {
                    
                    /* Saving notification unread count */
                    MBUtilities.saveJSONStringInToUserDefaults(jsonString: notificationsCountResponse.notificationUnreadCount?.toInt.toString(), key: APIsType.notificationCount.keyValue())
                    
                    /* Fire notification when App menu updated */
                    NotificationCenter.default.post(name: NSNotification.Name(Constants.KN_ReloadHomePage), object: nil, userInfo: nil)
                }
            }
        })
    }
}
