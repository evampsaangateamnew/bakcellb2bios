//
//  ActionHistoryTableViewHeader.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/27/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class ActionHistoryTableViewHeader: MBAccordionTableViewHeaderView {

    //MARK: - IBOutlet
    @IBOutlet var dateTimeTitleLabel: MBLabel!
    @IBOutlet var actionTitleLabel : MBLabel!
    @IBOutlet var usersTitleLabel: MBMarqueeLabel!
//    @IBOutlet var actionButton : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    //MARK: -  Set Action History Info
    func setActionHistoryHeader(actionHistory : OrderList?)  {
        
        dateTimeTitleLabel.text = actionHistory?.date ?? ""
        actionTitleLabel.text = actionHistory?.orderType ?? ""
        usersTitleLabel.text = actionHistory?.orderStatusN ?? ""
//        usersTitleLabel.text = actionHistory?.orderStatus.localizedString() ?? ""
    }
    
}
