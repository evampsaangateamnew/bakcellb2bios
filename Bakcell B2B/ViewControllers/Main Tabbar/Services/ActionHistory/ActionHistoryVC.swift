//
//  ActionHistoryMainVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/27/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ActionHistoryVC: BaseVC {
    
    enum ActionHistoryType {
        case failedActionHistory
        case pendingActionHistory
    }
    
    //MARK: - Properties
    var actionsHistoryList : [OrderList]?
    var filterdActionsHistoryList : [OrderList]?
    var isStartDate : Bool!
    var selectedActionType: String = "All"
    var isReloading : Bool = false
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var actionHistoryTableView : MBAccordionTableView!
    @IBOutlet var callsDropDown: UIDropDown!
    @IBOutlet var daysDropDown: UIDropDown!
    @IBOutlet var pageCount: MBLabel!
    @IBOutlet var spacificBtnView: UIView!
    @IBOutlet var endDateButton: UIButton!
    @IBOutlet var startDateButton: UIButton!
    @IBOutlet var startDateLabel: MBLabel!
    @IBOutlet var endDateLabel: MBLabel!
    @IBOutlet var dateTimeTitleLabel: MBLabel!
    @IBOutlet var actionTitleLabel : MBLabel!
    @IBOutlet var usersTitleLabel: MBLabel!
    
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        actionHistoryTableView.estimatedSectionHeaderHeight = 70
        actionHistoryTableView.sectionHeaderHeight = UITableView.automaticDimension
        actionHistoryTableView.estimatedRowHeight = 160
        actionHistoryTableView.rowHeight = UITableView.automaticDimension
        actionHistoryTableView.allowMultipleSectionsOpen = true
        
        spacificBtnView.isHidden = true
        let todayDateString = Date().todayDateString(dateFormate: Constants.kAPIFormat)
        let tomorrowDateString = Date().todayDateString(dateFormate: Constants.kAPIFormat)
        self.getActionHistory(startDate: todayDateString, endDate: tomorrowDateString)
        
        setUpDropDown()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    //Mark: -  Functions
    
    ///Set Localized labels
    func loadViewLayout()  {
        titleLabel.text = Localized("Title_ActionHistory")
        dateTimeTitleLabel.text = Localized("Title_DateTime")
        actionTitleLabel.text = Localized("Title_Action")
        usersTitleLabel.text = Localized("Title_Effect")
        
        dateTimeTitleLabel.textColor = .mbTextLighGray
        actionTitleLabel.textColor = .mbTextLighGray
        usersTitleLabel.textColor = .mbTextLighGray
    }
    
    ///Initialze / Setup dropdown
    func setUpDropDown() {
        
        //Calls Drop Down
        let callsDropDownTitles : [String] = [Localized("DropDown_All"),
                                              Localized("DropDown_Broadcast_SMS"),
                                              Localized("DropDown_Process_core_services"),
                                              Localized("DropDown_Change_supplementary"),
                                              Localized("DropDown_Change_SIM"),
                                              Localized("DropDown_Change_Group"),
                                              Localized("DropDown_Change_Limit"),
                                              Localized("DropDown_Change_Tariff"),
                                              Localized("DropDown_Retry_failed")]
        callsDropDown.placeholder = Localized("DropDown_All")
        
        callsDropDown.dataSource = callsDropDownTitles
        callsDropDown.didSelectOption { (index, option) in
            self.setSelectedCategory(category: option)
        }
        
        //Date Drop Down
        let dates : [String] = [Localized("DropDown_CurrentDay"),
                                Localized("DropDown_Last7Days"),
                                Localized("DropDown_Last30Days"),
                                Localized("DropDown_PreviousMonth"),
                                Localized("DropDown_SelectPeriod")]
        
        daysDropDown.placeholder = Localized("DropDown_CurrentDay")
        
        daysDropDown.dataSource = dates
        daysDropDown.didSelectOption { (index, option) in
            self.dateSelected(selectedDateIndex: index)
        }
    }
    
    func getCurrentSelectedDateIndex() -> Int {
        
        return daysDropDown.dataSource.firstIndex(of: daysDropDown.text ?? "") ?? 0
    }
    
    /**
     Date dropdown selection.
     
     - parameter selectedDateIndex: Selected dropdown indx.
     
     - returns: void
     */
    func dateSelected(selectedDateIndex:Int) {
        
        let todayDateString = Date().todayDateString(dateFormate: Constants.kAPIFormat)
        
        switch selectedDateIndex {
        case 0:
            print("default")
            self.spacificBtnView.isHidden = true
            
            self.getActionHistory(startDate: todayDateString, endDate: todayDateString)
            
        case 1:
            print("Last 7 days")
            self.spacificBtnView.isHidden = true
            
            // Get data of previous seven days
            let sevenDaysAgo = Date().todayDateString(withAdditionalValue: -7)
            self.getActionHistory(startDate: sevenDaysAgo, endDate: todayDateString)
            
            
        case 2:
            print("Last 30 days")
            self.spacificBtnView.isHidden = true
            
            // Get data of previous thirty days
            let thirtyDaysAgo = Date().todayDateString(withAdditionalValue: -30)
            self.getActionHistory(startDate: thirtyDaysAgo, endDate: todayDateString)
            
        case 3:
            print("Previous month")
            self.spacificBtnView.isHidden = true
            
            // Getting previous month start and end date
            let startOfPreviousMonth = Date().todayDate().getPreviousMonth()?.startOfMonth() ?? Date().todayDate()
            let endOfPreviousMonth =  Date().todayDate().getPreviousMonth()?.endOfMonth() ?? Date().todayDate()
            
            
            self.getActionHistory(startDate: MBPICUserSession.shared.dateFormatter.createString(from: startOfPreviousMonth, dateFormate: Constants.kAPIFormat),
                                  endDate: MBPICUserSession.shared.dateFormatter.createString(from: endOfPreviousMonth, dateFormate: Constants.kAPIFormat))
            
        case 4:
            self.spacificBtnView.isHidden = false
            
            // For specific period of time
            if isReloading{
                reloadActionHistory()
            } else{
                self.startDateLabel.text =  Date().todayDateString(dateFormate: Constants.kDisplayFormat)
                self.endDateLabel.text =  Date().todayDateString(dateFormate: Constants.kDisplayFormat)
                
                self.startDateLabel.textColor = UIColor.mbTextGray
                self.endDateLabel.textColor = UIColor.mbTextGray
                
                self.getActionHistory(startDate: todayDateString, endDate: todayDateString)
            }
            
            
        default:
            print("destructive")
            
        }
    }
    ///Rload Action History
    func reloadActionHistory()  {
        
        isReloading = false
        let todayDate = Date().todayDate()
        
        let startDate = MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        let endDate = MBPICUserSession.shared.dateFormatter.createDate(from: endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        //API Call
        self.getActionHistory(startDate: MBPICUserSession.shared.dateFormatter.createString(from: startDate, dateFormate: Constants.kAPIFormat),
                              endDate: MBPICUserSession.shared.dateFormatter.createString(from: endDate, dateFormate: Constants.kAPIFormat))
    }
    
    /**
     Filter Pending  history.
     
     - parameter category: Selected category.
     
     - returns: void
     */
    func setSelectedCategory(category: String) {
        
        switch category {
        case Localized("DropDown_All"):
            self.selectedActionType = "All"
            
        case Localized("DropDown_Broadcast_SMS"):
            self.selectedActionType = "S001"
            
        case Localized("DropDown_Process_core_services"):
            self.selectedActionType = "P002"
            
        case Localized("DropDown_Change_supplementary"):
            self.selectedActionType = "C003"
        
        case Localized("DropDown_Change_SIM"):
            self.selectedActionType = "SW009"
        
        case Localized("DropDown_Change_Group"):
            self.selectedActionType = "CG008"
        
        case Localized("DropDown_Change_Limit"):
            self.selectedActionType = "CPR007"
        
        case Localized("DropDown_Change_Tariff"):
            self.selectedActionType = "CH06"
            
        case Localized("DropDown_Retry_failed"):
            self.selectedActionType = "R004"
            
        default:
            break
        }
        
        
        /*
         S001 = broadcastsms
         P002 = process core services
         C003 = change supplementary
         R004 = retry failed
         */
        
        /*
         "DropDown_Broadcast_SMS"            = "Broadcast SMS";
         "DropDown_Process_core_services"    = "Process core services";
         "DropDown_Change_supplementary"     = "Change supplementary";
         "DropDown_Retry_failed"             = "Retry failed";
         */
        
        self.filterdActionsHistoryList = self.filterActionHistoryBy(type: self.selectedActionType, actionHistoryDetails: self.actionsHistoryList)
        
        
        self.reloadTableViewData()
    }
    
    func reloadTableViewData(){
        if (filterdActionsHistoryList?.count ?? 0) <= 0 {
            self.actionHistoryTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.actionHistoryTableView.hideDescriptionView()
        }
        self.actionHistoryTableView.reloadData()
        showPageNumber()
    }
    
    
    /**
     Filter action historhy by selected type.
     
     - parameter type: String value of action type.
     - parameter actionHistoryDetails: Array of Action History.
     
     
     - returns: Array of actionHistory.
     */
    func filterActionHistoryBy(type:String?, actionHistoryDetails : [OrderList]? ) -> [OrderList]? {
        guard let myType = type else {
            return []
        }
        
        if myType.isEqual("All", ignorCase: true) {
            return actionHistoryDetails
        }
        
        // Filtering actionHistoryDetail array
        let  filterdActionHistoryDetails = actionHistoryDetails?.filter() {
            
            if $0.orderKey == myType {
                return true
            } else {
                return false
            }
        }
        
        return filterdActionHistoryDetails
    }
    
    /**
     Show date picker.
     
     - parameter minDate: minimum date of picker.
     - parameter maxDate: maximum date of picker.
     - parameter currentDate: current date of picker.
     
     - returns: void
     */
    func showDatePicker(minDate : Date , maxDate : Date, currentDate:Date) {
        self.parent?.showNewDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate) { (selectedDate) in
            
            let todayDate = Date().todayDate()
            
            let selectedDateString = MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kDisplayFormat)
            
            let startFieldDate = MBPICUserSession.shared.dateFormatter.createDate(from: self.startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            let endFieldDate = MBPICUserSession.shared.dateFormatter.createDate(from: self.endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            
            if self.isStartDate ?? true {
                self.startDateLabel.text = selectedDateString
                self.startDateLabel.textColor = UIColor.mbBarOrange
                
                self.getActionHistory(startDate: MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kAPIFormat),
                                      endDate: MBPICUserSession.shared.dateFormatter.createString(from: endFieldDate ?? todayDate, dateFormate: Constants.kAPIFormat))
                
            } else {
                
                self.endDateLabel.text = selectedDateString
                
                self.endDateLabel.textColor = UIColor.mbBarOrange
                self.getActionHistory(startDate: MBPICUserSession.shared.dateFormatter.createString(from: startFieldDate ?? todayDate, dateFormate: Constants.kAPIFormat),
                                      endDate: MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kAPIFormat))
            }
        }
    }
    //MARK: - API Calls
    
    /**
     Call 'Action history' API .
     
     - parameter startDate: Start date.
     - parameter startDate: End date.
     
     - returns: records
     */
    func getActionHistory(startDate:String?, endDate:String?) {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getActionHistory(StartDate: startDate ?? "", EndDate: endDate ?? "", orderRequestType: "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let receivedactionHistoryList = Mapper<ActionHistoryModel>().map(JSONObject: resultData){
                        
                        self.actionsHistoryList = receivedactionHistoryList.orderList
                        /* Filter and set information accourding to seleted type */
                        self.filterdActionsHistoryList = self.filterActionHistoryBy(type: self.selectedActionType, actionHistoryDetails: receivedactionHistoryList.orderList)
                        
                        self.reloadTableViewData()
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewData()
        })
    }
    
    ///Retry Action history for specific Index
    func retryActionHisytory(forIndex index: Int)  {
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.retryActionHistory(retryOrderId: filterdActionsHistoryList?[index].orderId ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    if let rerequetActionHistory = Mapper<RerequetActionHistory>().map(JSONObject: resultData){
                        self.showAlertWithMessage(message: rerequetActionHistory.responseMsg, actionBlock: {
                            self.isReloading = true
                            self.dateSelected(selectedDateIndex: self.getCurrentSelectedDateIndex())
                        })
                    }
                    
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    ///Cnacle Action history for specific Index
    func cancelActionHisytory(forIndex index: Int)  {
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.cancelActionHistory(cancelOrderId: filterdActionsHistoryList?[index].orderId ?? "",  { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    if let rerequetActionHistory = Mapper<RerequetActionHistory>().map(JSONObject: resultData){
                        self.showAlertWithMessage(message: rerequetActionHistory.responseMsg, actionBlock: {
                            self.isReloading = true
                            self.dateSelected(selectedDateIndex: self.getCurrentSelectedDateIndex())
                        })
                    }
                    
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    ///Retry Action history for specific Index
    func getActionHisytoryDetai(forIndex index: Int)  {
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.actionHistoryDetail(detailOrderId: filterdActionsHistoryList?[index].orderId ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    if let actionHistoryDetail = Mapper<ActionHistoryDetailModel>().map(JSONObject: resultData){
                        
                        if let detailVC :ActionHistoryDetailVC = ActionHistoryDetailVC.instantiateViewControllerFromStoryboard() {
                            detailVC.actionDetails = actionHistoryDetail.orderdetails
                            self.presentPOPUP(detailVC, animated: true)
                        }
                    }
                    
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    //MARK: - IBActions
    
    @IBAction func startDateBtnPressed(_ sender: UIButton) {
        
        isStartDate = true;
        let todayDate = Date().todayDate()
        
        let currentDate = MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        let threeMonthsAgos = Date().todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLabel.text,
            endDateString.isBlank == false {
            
            
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: MBPICUserSession.shared.dateFormatter.createDate(from: endDateString, dateFormate: Constants.kDisplayFormat) ?? todayDate,
                                currentDate: currentDate)
            
        } else {
            
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate, currentDate: currentDate)
        }
        
    }
    
    @IBAction func endDateBtnPressed(_ sender: UIButton) {
        
        isStartDate = false;
        let todayDate = Date().todayDate()
        
        let currentDate = MBPICUserSession.shared.dateFormatter.createDate(from: endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        self.showDatePicker(minDate: MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate,
                            maxDate: todayDate,
                            currentDate: currentDate)
    }
    
    @objc
    func actionHistoryCancelButtonTapped(sender: UIButton)  {
        
        cancelActionHisytory(forIndex: sender.tag)
        
    }
    
    @objc
    func actionHistoryRetryButtonTapped(sender: UIButton)  {
        
        retryActionHisytory(forIndex: sender.tag)
        
    }
    
    @IBAction func detailButtonTapped(sender: MBButton)  {
        
        getActionHisytoryDetai(forIndex: sender.tag)

    }
}


//Mark: - Table View Methods
extension ActionHistoryVC : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return filterdActionsHistoryList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let actionHistoryHeader : ActionHistoryTableViewHeader = tableView.dequeueReusableHeaderFooterView() {
            actionHistoryHeader.setActionHistoryHeader(actionHistory: filterdActionsHistoryList?[section])
            return actionHistoryHeader
        }
        return UITableViewHeaderFooterView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 5,
            let actionHistoryDetailCell: ActionHistoryDetailButtonCell = tableView.dequeueReusableCell() {
            
            actionHistoryDetailCell.detailButton.setTitle(Localized("BtnTitle_Details"), for: .normal)
            actionHistoryDetailCell.detailButton.tag = indexPath.section
            actionHistoryDetailCell.detailButton.addTarget(self, action: #selector(detailButtonTapped(sender:)), for: .touchUpInside)
            
            return actionHistoryDetailCell
            
        } else if let actionHistoryCell: ActionHistoryTableViewCell = tableView.dequeueReusableCell() {
            
            actionHistoryCell.actionButton.tag = indexPath.section
            actionHistoryCell.actionButton.removeTarget(nil, action: nil, for: .allEvents)
            switch indexPath.row {
            case 0:
                actionHistoryCell.setActionHistoryDetail(actionTitle: Localized("Title_Total"), actionValue: filterdActionsHistoryList?[indexPath.section].totalCount)
                break
            case 1:
                actionHistoryCell.setActionHistoryDetail(actionTitle: Localized("Title_Success"), actionValue: filterdActionsHistoryList?[indexPath.section].success)
                break
            case 2:
                actionHistoryCell.setActionHistoryDetail(actionTitle: Localized("Title_Cancelled"), actionValue: filterdActionsHistoryList?[indexPath.section].cancelled)
                break
            case 3:
                actionHistoryCell.setActionHistoryDetail(actionTitle: Localized("Title_Failed"), actionValue: filterdActionsHistoryList?[indexPath.section].failed ,showActionButton: true, actionButtonTitle: Localized("BtnTitle_Retry"))
                actionHistoryCell.actionButton.addTarget(self, action: #selector(actionHistoryRetryButtonTapped(sender:)), for: .touchUpInside)
                break
            case 4:
                actionHistoryCell.setActionHistoryDetail(actionTitle: Localized("Title_Pending"), actionValue: filterdActionsHistoryList?[indexPath.section].pending, showActionButton: true, actionButtonTitle: Localized("BtnTitle_CancelRequest"))
                actionHistoryCell.actionButton.addTarget(self, action: #selector(actionHistoryCancelButtonTapped(sender:)), for: .touchUpInside)
                break
                
            default:
                break
            }
            
            
            return actionHistoryCell
            
        } else {
            return UITableViewCell()
        }
    }
}

// MARK: - <MBAccordionTableViewDelegate>

extension ActionHistoryVC : MBAccordionTableViewDelegate {
    
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        showPageNumber()
        
        /* checking if header is already at top position than do not scroll it to top again */
        if (Int(tableView.contentOffset.y) != section * Int(header?.frame.size.height ?? 0)) &&
            tableView.hasRow(at: IndexPath(row: 0, section: section)) {
            
            tableView.scrollToRow(at: IndexPath(row: 0, section: section), at: UITableView.ScrollPosition.top, animated: true)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

//MARK: - ScroolView Delegate
extension ActionHistoryVC:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        showPageNumber()
    }
    
    func showPageNumber() {
        
        pageCount.text = "\(actionHistoryTableView.lastVisibleSection())/\(filterdActionsHistoryList?.count ?? 0)"
    }
}
