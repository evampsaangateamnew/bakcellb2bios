//
//  ActionHistoryDetailButtonCell.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 1/28/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class ActionHistoryDetailButtonCell: UITableViewCell {

    @IBOutlet var detailButton: MBButton!
    
}
