//
//  ActionHistoryDetailCell.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 1/28/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class ActionHistoryDetailCell: UITableViewCell {
    
    //MARK:- IBOutlet
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    func setDetailInfo(aDetail: OrderDetailModel?) {
        titleLabel.text = aDetail?.msisdn ?? ""
        descriptionLabel.text = aDetail?.resultDescription ?? ""
    }
}
