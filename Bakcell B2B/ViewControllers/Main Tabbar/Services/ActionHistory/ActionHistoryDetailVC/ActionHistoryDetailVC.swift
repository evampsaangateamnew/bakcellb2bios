//
//  ActionHistoryDetailVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 1/28/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class ActionHistoryDetailVC: BaseVC {
    
    //MARK:- Properties
    var actionDetails : [OrderDetailModel]? = []
    
    //MARK:- IBOutlet
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var okButton: MBButton!
    @IBOutlet var msisdnTitle: UILabel!
    @IBOutlet var descriptionTitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        msisdnTitle.text = Localized("Title_MISDN")
        descriptionTitle.text = Localized("Title_Description")
        okButton.setTitle(Localized("BtnTitle_OK"), for: .normal)
        
        //checking if array have data than show header labels else hide them
        if ((self.actionDetails?.count ?? 0) > 0) {
            self.viewHeader.isHidden = false
        } else {
            self.viewHeader.isHidden = true
        }
        
        //init tableview
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.hideDefaultSeprator()
        
        self.reloadTableViewData()
    }
    
    
    //MARK: - FUNCTIONS
    
    func reloadTableViewData() {
        if self.actionDetails?.count == 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
    }
    
    
    // MARK: - IBActions
    
    @IBAction func okPressed(_ sender: MBButton) {
        
        self.dismiss(animated: true)
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>
extension ActionHistoryDetailVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return actionDetails?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :ActionHistoryDetailCell = tableView.dequeueReusableCell() {
            cell.setDetailInfo(aDetail: actionDetails?[indexPath.row])
            return cell
        }
        return UITableViewCell()
     
    }
    
}
