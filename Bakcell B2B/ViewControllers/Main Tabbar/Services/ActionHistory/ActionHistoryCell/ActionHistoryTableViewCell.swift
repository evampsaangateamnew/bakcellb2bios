//
//  ActionHistoryTableViewCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 9/27/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class ActionHistoryTableViewCell: UITableViewCell {

    //MARK: - IBOutlets

    @IBOutlet var actionTitleLable: MBLabel!
    @IBOutlet var actionValueLable: MBLabel!
    @IBOutlet var actionButton: MBButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    //MARK: - Functions
    
   
    
    func setActionHistoryDetail(actionTitle : String?, actionValue : String? , showActionButton : Bool  = false, actionButtonTitle : String = "")  {
            actionTitleLable.text = actionTitle ?? ""
            actionValueLable.text = actionValue ?? ""
            actionButton.isHidden = !showActionButton

        if (actionValue ?? "").toInt != 0,
            showActionButton   {
            actionButton.setTitle(actionButtonTitle, for: .normal)
            actionButton.isHidden = false
        } else{
            actionButton.isHidden = true
        }
        
    }
    
}
