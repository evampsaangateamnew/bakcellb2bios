//
//  ChangeGroupVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/28/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class ChangeGroupVC: BaseVC {
    struct GroupTypeModel {
        var groupType : GroupTypes?
        var groupCode : String = ""
        var groupName : String = ""
        var groupDisplayName : String = ""
    }
    //MARK: - Properties
    
    var allGroups : [GroupTypeModel] = [GroupTypeModel(groupType: .paybySubs, groupCode: "", groupName: GroupTypes.paybySubs.rawValue, groupDisplayName: GroupTypes.paybySubs.localizedString()),
                                        GroupTypeModel(groupType: .partPay, groupCode: "", groupName: GroupTypes.partPay.rawValue, groupDisplayName: GroupTypes.partPay.localizedString()),
                                        GroupTypeModel(groupType: .fullPay, groupCode: "", groupName: GroupTypes.fullPay.rawValue, groupDisplayName: GroupTypes.fullPay.localizedString())]
    
    var currentGroups : [GroupTypeModel] = []
    
    var selectedGroupFrom :GroupTypeModel?
    var selectedGroupTo :GroupTypeModel?
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel:MBLabel!
    @IBOutlet var changeGroupDescriptionLabel : MBLabel!
    @IBOutlet var changeGroupFromToLabel : MBLabel!
    @IBOutlet var convertFromDropDown : UIDropDown!
    @IBOutlet var groupTypeDropDown : UIDropDown!
    @IBOutlet var selectUsersButton : MBButton!
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userSelectionBaseVC = UserSelectionBaseVC()
        userSelectionBaseVC.loadUsersInformation { (_, groupData) in
            
            if let groupDataObj = groupData {
                groupDataObj.forEach({ (aGroup) in
                    
                    if let aUser = aGroup.usersData?.first {
                        self.currentGroups.append(GroupTypeModel(groupType: GroupTypes(rawValue: aUser.groupName), groupCode: aUser.groupCode, groupName: aUser.groupName, groupDisplayName: aUser.groupNameDisp))
                    }
                })
                
                self.setDropDown()
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"), {
                    self.navigationController?.popViewController(animated: true)
                })
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    //MARK: - Functions
    func loadViewLayout()  {
        
        titleLabel.text = Localized("Title_ChangeGroup")
        changeGroupDescriptionLabel.text = Localized("Title_ChangeGroupDescription")
        changeGroupFromToLabel.text = "" //Localized("Title_ChangeGroupFromTo")
        selectUsersButton.setTitle(Localized("BtnTitle_SelectUsers"), for: .normal)
    }
    
    //Initialze / Setup dropdown 
    func setDropDown() {
        self.currentGroups.forEach { (aCurrentGroup) in
            
            for i in 0..<self.allGroups.count {
                if aCurrentGroup.groupType == self.allGroups[i].groupType {
                    self.allGroups[i].groupCode = aCurrentGroup.groupCode
                }
            }
        }
        
        //Convert From Drop Down
        convertFromDropDown.placeholder = Localized("Title_FromGroup")
        convertFromDropDown.dataSource = currentGroups.map {$0.groupDisplayName}
        convertFromDropDown.didSelectOption { (index, option) in
            
            self.selectedGroupFrom = self.currentGroups[index]
        }
        
        //Group type To Drop Down
        groupTypeDropDown.placeholder = Localized("Title_toGroup")
        groupTypeDropDown.dataSource = allGroups.map {$0.groupDisplayName}
        groupTypeDropDown.didSelectOption { (index, option) in
            
            self.selectedGroupTo = self.allGroups[index]
            
            /*if self.currentGroups.contains(where: {$0.groupType == self.allGroups[index].groupType}) {
                
                self.selectedGroupTo = self.allGroups[index]
            } else {
                self.showErrorAlertWithMessage(message: Localized("Error_GroupNotExist"))
                self.groupTypeDropDown.reloadAllComponents()
            }*/
        }
    }
    
    //MARK: - IBActions
    @IBAction func selectUsersButtonPressed(_ sender: UIButton){
        
        if let groupTypeFromObj = self.selectedGroupFrom,
            let groupTypeToObj = self.selectedGroupTo,
            let selectedGroupTypeValue = groupTypeFromObj.groupType {
            
            
            if groupTypeFromObj.groupType == groupTypeToObj.groupType {
                
                self.showErrorAlertWithMessage(message: Localized("Error_CannotChangeToSameGroup"))
                
            } else {
                if let userSelectionVC :MultipleUserSelectionMainVC = MultipleUserSelectionMainVC.instantiateViewControllerFromStoryboard() {
                    
                    userSelectionVC.loadDataOfGroupType = [selectedGroupTypeValue]
                    userSelectionVC.setNextButtonCompletionBloch { (selectedUsers) in
                        
                        self.navigationController?.popViewController(animated: true)
                        
                        let confirmMessage = String(format: Localized("Message_GroupChangeMessage"), groupTypeFromObj.groupDisplayName, groupTypeToObj.groupDisplayName)
                        
                        self.showConfirmationAlert(message: confirmMessage, {
                            self.changeGroupForUsers(groupIdFrom: groupTypeFromObj.groupCode, groupIdTo: groupTypeToObj.groupCode, groupTypeTo: groupTypeToObj.groupType, selectedUsers: selectedUsers)
                        })
                        
                    }
                    
                    self.navigationController?.pushViewController(userSelectionVC, animated: true)
                }
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Error_SelectGroup"))
        }
    }
    
    //MARK: - API Calls
    
    //Update Limits for users
    func changeGroupForUsers(groupIdFrom :String, groupIdTo :String, groupTypeTo :GroupTypes?, selectedUsers : [String : UsersData]?)  {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.changeGroupForUsers(groupIdFrom: groupIdFrom, groupIdTo: groupIdTo, groupTypeTo: groupTypeTo, Users: selectedUsers, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    self.showSuccessAlertWithMessage(message: resultDesc)
                    
                    /* Delete Users info from userDefaults */
                    MBUtilities.clearJSONStringFromUserDefaults(key: APIsType.usersData.localizedAPIKey())
                    MBUtilities.clearJSONStringFromUserDefaults(key: APIsType.groupData.localizedAPIKey())
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
        })
    }
    
}
