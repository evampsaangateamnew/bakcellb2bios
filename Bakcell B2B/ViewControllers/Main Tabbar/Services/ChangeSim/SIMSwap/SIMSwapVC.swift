//
//  SIMSwapVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/2/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class SIMSwapVC: BaseVC {
    
    enum ChangeSimType {
        case simSwap
        case numberRestoration
    }
    
    enum ChangeSimIDType {
        case none
        case id
        case foreignID
        case TemporaryID
    }
    
    enum SubViewsType {
        case verify
        case validate
    }
    
    //MARK:- Properties
    var selectedChangeSimeType      : ChangeSimType = .simSwap
    var selectedIDType              : ChangeSimIDType = .none
    var selectedIDTypeId            : String?
    var selectedIDTypeName          : String?
    var strNavBarTitle              : String?
    var strTransactionId            : String?
    
    //MARK:- IBOutlets
    @IBOutlet var titleLabel                    : MBLabel!
    @IBOutlet var serialNumberDescriptionLabel  : MBLabel!
    @IBOutlet var idTypeLabel                   : MBMarqueeLabel!
    @IBOutlet weak var lblTypeValue             : MBMarqueeLabel!
    @IBOutlet var misdnDescriptionLabel         : MBLabel!
    
    @IBOutlet var serialNumberTextField         : MBTextField!
    @IBOutlet var pinTextField                  : MBTextField!
    @IBOutlet var misdnTextField                : MBTextField!
    @IBOutlet var newICCIDTextField             : MBTextField!
    @IBOutlet var contactNumberTextField        : MBTextField!
    @IBOutlet var veonTextfield: MBTextField!
    
    
    @IBOutlet var verifyButton                  : MBButton!
    @IBOutlet weak var lblVerify: MBMarqueeLabel!
    @IBOutlet var backButton                    : MBButton!
    @IBOutlet var doneButton                    : MBButton!
    
    @IBOutlet var idTypeDropDown                : UIDropDown!
    
    @IBOutlet var serialNumberParenView         : UIView!
    @IBOutlet var misdnNumberParentView         : UIView!
    @IBOutlet var scrollView: UIScrollView!
    
    //MARK:- ViewControllers Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.interactivePop()
        
        loadViewLayout()
        
        //enable/disable main views
        self.enableView(type: .verify)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    //MARK: -  Functions
    func loadViewLayout() {
        //commented by MIA
        /*if selectedChangeSimeType == .simSwap{
            titleLabel.text = Localized("Title_SimSwap")
        } else {
            //  titleLabel.text = Localized("Title_NumberRestoration")
            titleLabel.text = Localized("Title_RejectNumber")
        }*/
        titleLabel.text = strNavBarTitle ?? ""
        serialNumberDescriptionLabel.text = Localized("Title_SerialNumberDescription")
        idTypeLabel.text = Localized("Title_ID_Type")
        misdnDescriptionLabel.text = Localized("Title_FillRequiredFields")//("Title_MISDNDescription")
        serialNumberTextField.placeholder = Localized("Placeholder_EnterSerialNumber")
        misdnTextField.placeholder = Localized("Placeholder_EnterMISDN")
        newICCIDTextField.placeholder = Localized("Placeholder_EnterNewICCID")
        contactNumberTextField.placeholder = Localized("Placeholder_ContactNumber")
        pinTextField.placeholder = Localized("Placeholder_EnterPinSIMSwap")
        verifyButton.setTitle("", for: .normal)//if verified user "BtnTitle_Verified"
        lblVerify.text = Localized("BtnTitle_Verify")
        lblVerify.font = verifyButton.titleLabel?.font
        doneButton.setTitle(Localized("BtnTitle_Done"), for: .normal)
        backButton.setTitle(Localized("BtnTitle_Back"), for: .normal)
        //idTypeDropDown.placeholder = Localized("DropDown_Select_IDType")
        idTypeDropDown.rightImage = nil
        //idTypeDropDown
        idTypeDropDown.text = ""
        
        //setDropDown()
        setupType()
        
        serialNumberTextField.text = MBPICUserSession.shared.picUserInfo?.picSerialNo
        pinTextField.text = MBPICUserSession.shared.picUserInfo?.picPIN
        veonTextfield.text = MBPICUserSession.shared.picUserInfo?.picVEON
        serialNumberTextField.isUserInteractionEnabled = false
        pinTextField.isUserInteractionEnabled = false
        veonTextfield.isUserInteractionEnabled = false
        newICCIDTextField.isUserInteractionEnabled = false
        contactNumberTextField.isUserInteractionEnabled = false
    }
    
    //Initialze / Setup dropdown
    func setDropDown() {
        //Convert From Drop Down
        let idTypeTitles : [String] = [Localized("DropDown_Type_ID"),Localized("DropDown_Type_ForeignID")]
        idTypeDropDown.dataSource = idTypeTitles
        idTypeDropDown.didSelectOption { (index, option) in
            if index == 0 {
                self.selectedIDType = .id
            } else {
                self.selectedIDType = .foreignID
            }
        }
    }
    
    func setupType() {
        var typeName = MBPICUserSession.shared.picUserInfo?.document_type
        var typeFound = false
        for selectedType:DocumentType in  MBPICUserSession.shared.userDocumentTypes {
            if selectedType.id == typeName {
                selectedIDTypeId = selectedType.id
                selectedIDTypeName = selectedType.typeValue
                typeFound = true
                break
            }
        }
        if typeFound == false {
            for selectedType:DocumentType in  MBPICUserSession.shared.userDocumentTypes {
                if selectedType.typeValue?.lowercased().isEqual("id") ?? false ||
                selectedType.typeValue?.lowercased().isEqual("temporary id") ?? false ||
                    selectedType.typeValue?.lowercased().isEqual("foreign id") ?? false {
                    selectedIDTypeId = selectedType.id
                    selectedIDTypeName = selectedType.typeValue
                    typeFound = true
                    break
                }
            }
        }
        
        if typeFound == true {
            //idTypeDropDown.text = selectedIDTypeName
            lblTypeValue.text = selectedIDTypeName
        }
    }
    
    func enableView(type: SubViewsType)  {
        if type == .verify {
            serialNumberParenView.enableDisableAllSubView(isEnabled: true)
            misdnNumberParentView.enableDisableAllSubView(isEnabled: false)
            let bottomOffset = CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height)
            scrollView.setContentOffset(bottomOffset, animated: true)
        } else {
            serialNumberParenView.enableDisableAllSubView(isEnabled: false)
            misdnNumberParentView.enableDisableAllSubView(isEnabled: true)
            contactNumberTextField.isUserInteractionEnabled = false
            contactNumberTextField.text = MBPICUserSession.shared.picUserInfo?.contactNumber
            newICCIDTextField.isUserInteractionEnabled = true
        }
    }
    
    func resetViewsToDefaultState()  {
        
        //set default dropdown type
        selectedIDType = .none
        
        //enable/disable main views
        self.enableView(type: .verify)
        
        //make all fields empty
        serialNumberTextField.text = ""
        misdnTextField.text = ""
        newICCIDTextField.text = ""
        contactNumberTextField.text = ""
        pinTextField.text = ""
        
        idTypeDropDown.placeholder = Localized("DropDown_Select_IDType")
    }
    
    //MARK: - IBOutlets
    
    @IBAction func verifyButtonTapped(_ sender: MBButton){
        
        //validate fields
        /*if selectedIDType == .none {
            self.showErrorAlertWithMessage(message: Localized("Message_SelectIDType"))
            return
            
        } else */if (self.serialNumberTextField.text?.isBlank == true/* || self.serialNumberTextField.text?.length ?? 0 < Constants.SerialNumberMinLength || self.serialNumberTextField.text?.length ?? 0 > Constants.SerialNumberMaxLength*/) {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterSerialNumber"))
            return
            
        } else if self.pinTextField.text?.isBlank == true {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterPin"))
            return
        }
        
        //resigning first responder of all textFields
        self.view.endEditing(true)
        
        //calling API for verification
        //self.verifyDuplicateSim(documentType: (selectedIDType == .id) ? "1" : "2", documentNumber: self.serialNumberTextField.text ?? "", documentPin: self.pinTextField.text ?? "")
        verifySimInfo()
    }
    
    
    
    @IBAction func selectNumberTapped(_ sender: MBButton){
        if let userSelectionVC :SingleUserSelectionMainVC = SingleUserSelectionMainVC.instantiateViewControllerFromStoryboard() {
            
            userSelectionVC.setUserSelectedCompletionBloch { (selectedUsers) in
                
                self.misdnTextField.text = selectedUsers.first?.value.msisdn ?? ""
                userSelectionVC.navigationController?.popViewController(animated: true)
                
            }
            
            self.navigationController?.pushViewController(userSelectionVC, animated: true)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: MBButton){
        self.backPressed(sender)
    }
    
    @IBAction func doneButtonTapped(_ sender: MBButton){
        
        if self.misdnTextField.text?.isBlank == true {
            self.showErrorAlertWithMessage(message: Localized("Placeholder_EnterMISDN"))
            return
            
        } else if self.misdnTextField.text?.length != Constants.MSISDNLength {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
            return
            
        } else if self.newICCIDTextField.text?.isBlank == true ||
            self.newICCIDTextField.text?.length ?? 0 < Constants.ICCIDLength {
            self.showErrorAlertWithMessage(message: Localized("Message_ICCIDNumberIncorrect"))
            return
            
        } else if self.contactNumberTextField.text?.isBlank == true {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterContactNumber"))
            return
        }
        
        //resigning first responder of all textFields
        self.view.endEditing(true)
        
        //calling API for verification
        //self.vaidateDuplicateSim(msisdnNumber: self.misdnTextField.text ?? "", iccidNumber: self.newICCIDTextField.text ?? "", contactNumber: self.contactNumberTextField.text ?? "")
        self.duplicateSimNow()
    }
    
    //MARK: - API Calls
    /// Call 'verifyDuplicateSim' API .
    ///
    /// - returns: Void
    func verifySimInfo() {
        
        self.showActivityIndicator()
        
        /*API call tpo get data*/
        _ = MBAPIClient.sharedClient.verifyDuplicateSim(documentType: selectedIDTypeId ?? "1", documentNumber: serialNumberTextField.text ?? "", documentPin: self.pinTextField.text ?? "", voen: veonTextfield.text ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let responseHandler = Mapper<SimSwap>().map(JSONObject:resultData) {
                        
                        self.strTransactionId = responseHandler.transactionId
                        self.showSuccessAlertWithMessage(message: responseHandler.message) {
                            //enable/disable main views
                            self.enableView(type: .validate)
                        }
                    }
                    
                    
                    
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    /// Call 'suplicateSim' API .
    ///
    /// - returns: Void
    func duplicateSimNow() {
        
        self.showActivityIndicator()
        
        /*API call tpo get data*/
        _ = MBAPIClient.sharedClient.duplicateSimNow(msisdn: misdnTextField.text ?? "", iccId: newICCIDTextField.text ?? "", contactnumber: contactNumberTextField.text ?? "", transactionId: strTransactionId ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let responseHandler = Mapper<SimSwap>().map(JSONObject:resultData) {
                        
                        self.showSuccessAlertWithMessage(message: responseHandler.responseMsg) {
                            self.misdnTextField.text = ""
                            self.newICCIDTextField.text = ""
                        }
                    }
                    
                    
                    
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    
    /// Call 'verifyDuplicateSim' API .
    ///
    /// - returns: Void
    func verifyDuplicateSim(documentType: String, documentNumber: String, documentPin: String) {
        
        self.showActivityIndicator()
        
        /*API call tpo get data*/
        _ = MBAPIClient.sharedClient.verifyDuplicateSim(documentType: documentType, documentNumber: documentNumber, documentPin: documentPin, voen: veonTextfield.text ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //enable/disable main views
                    self.enableView(type: .validate)
                    
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    //MARK: - API Calls
    /// Call 'vaidateDuplicateSim' API .
    ///
    /// - returns: Void
    func vaidateDuplicateSim(msisdnNumber: String, iccidNumber: String, contactNumber: String) {
        
        self.showActivityIndicator()
        
        /*API call tpo get data*/
        _ = MBAPIClient.sharedClient.vaidateDuplicateSim(msisdnNumber: msisdnNumber, iccidNumber: iccidNumber, contactNumber: contactNumber, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    //reset screen to defautl state
                    self.resetViewsToDefaultState()
                    
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - TextField Delegates
extension SIMSwapVC: UITextFieldDelegate{
    
    
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false } // Check that if text is nil then return false
        
        // Length of text in UITextField
        let newLength = text.count + string.count - range.length
        
        if textField == self.serialNumberTextField {
            
            // Allow charactors check
            let allowedCharactors = Constants.allowedCapitalAlphabets + Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            return newLength <= Constants.SerialNumberMaxLength /* Restriction for Seral Number Field length */
            
        } else if textField == self.pinTextField {
            
            // Allow charactors check
            let allowedCharactors = Constants.allowedCapitalAlphabets + Constants.allowedNumbers
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            return newLength <= Constants.SevenDigitPinLength /* Restriction for PIN Field length */
            
        } else if textField == self.misdnTextField || textField == self.contactNumberTextField {
            return newLength <= Constants.MSISDNLength /* Restriction for MISDN length */
            
        }  else if textField == self.newICCIDTextField {
            return newLength <= Constants.ICCIDLength /* Restriction for ICCID length */
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // Try to find next responder
        if let nextField = textField.superview?.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
            
            if textField == self.pinTextField {
                verifyButtonTapped(MBButton())
            } else if textField == self.pinTextField {
                doneButtonTapped(MBButton())
            }
        }
        
        return false
    }
}

