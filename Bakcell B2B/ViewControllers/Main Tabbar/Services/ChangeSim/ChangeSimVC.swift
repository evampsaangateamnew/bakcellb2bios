//
//  ChangeSimVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/2/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class ChangeSimVC: BaseVC {
    
    var changeSimList : [Category] = [Category(Name: Localized("Title_SimSwap"), ImageName: "change-sim", Idenitifier: "sim_swap"),Category(Name: Localized("Title_RejectNumber"), ImageName: "restore-sim", Idenitifier: "number_restoration")]

    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var changeSimTableView : UITableView!
    
    //MARK:-  View Controller Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        changeSimTableView.estimatedRowHeight = 100
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }
    

    //MARK: - Functions
    func loadViewLayout(){
        titleLabel.text = Localized("Title_ChangeSim")
    }
    func redirectUserToSelectedController(Identifier identifier: String){
        switch identifier {
        case "sim_swap":
            //Load SIMSwap ViewController
            if let simSwapVC :SIMSwapVC = SIMSwapVC.instantiateViewControllerFromStoryboard() {
                simSwapVC.selectedChangeSimeType = .simSwap
                self.navigationController?.pushViewController(simSwapVC, animated: true)
            }
            break
        case "number_restoration":
            //Load Number Restoration ViewController
            if let numberRestoration :SIMSwapVC = SIMSwapVC.instantiateViewControllerFromStoryboard() {
                numberRestoration.selectedChangeSimeType = .numberRestoration
                self.navigationController?.pushViewController(numberRestoration, animated: true)
            }
            break
        default:
            break
        }
    }
}

//MARK: - TableView Delegates
extension ChangeSimVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return changeSimList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let changeSimCell: CategoriesTableViewCell = tableView.dequeueReusableCell(){
            changeSimCell.setCategoryInfo(aCategory: changeSimList[indexPath.row])
            return changeSimCell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        redirectUserToSelectedController(Identifier: changeSimList[indexPath.row].identifier)
    }
    
    
}
