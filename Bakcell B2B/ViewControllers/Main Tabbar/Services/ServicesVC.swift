//
//  ServicesVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/06/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit

class ServicesVC: BaseVC {
    
    var servicesList: [Items] = []
    
    
    // MARK: - IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var servicesTableView: UITableView!
    
    // MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        servicesTableView.estimatedRowHeight = 100
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop(false)
        
        loadViewLayout()
        
        MBPICUserSession.shared.appMenu?.menuHorizontal?.forEach({ (aHorizontalMenu) in
            if aHorizontalMenu.identifier == "services" {
                servicesList = aHorizontalMenu.items ?? []
            }
        })
        servicesList.sort{ $0.sortOrder.toInt < $1.sortOrder.toInt }
        
        
        self.reloadTableViewData()
        
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        titleLabel.text = Localized("Title_Services")
    }
    
    func reloadTableViewData(){
        if servicesList.count <= 0 {
            self.servicesTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.servicesTableView.hideDescriptionView()
        }
        self.servicesTableView.reloadData()
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func iconNameFor(Identifier identifier: String) -> String {
        switch identifier {
            
        case "core_services":
            return "core-services"
        
        case "action_history":
                return "action-history"
        
        case "broadcast_sms":
            return "broadcast-SMS"

        case "change_group":
            return "change-group"

        case "topUp":
            return "services_topup"

        case "change_limit":
            return "change-limit"

        case "change_sim":
            return "change-sim"

        case "invoice_information":
            return "invoiceinfo"

        case "packages":
            return "packages"

        default:
            return ""
        }
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier: String, titleString:String){
        switch identifier {
            
        case "core_services":
            //Load Group Users  ViewController
            if let userSelectionVC :MultipleUserSelectionMainVC = MultipleUserSelectionMainVC.instantiateViewControllerFromStoryboard() {
                
                userSelectionVC.setNextButtonCompletionBloch { (selectedUsers) in
                    
                    if let coreServiceVC :CoreServicesVC = CoreServicesVC.instantiateViewControllerFromStoryboard() {
                        coreServiceVC.selectedUserList = selectedUsers
                        coreServiceVC.selectionType = .group
                        self.navigationController?.pushViewController(coreServiceVC, animated: true)
                    }
                }
                
                self.navigationController?.pushViewController(userSelectionVC, animated: true)
            }
            break
        case "action_history":
            //Load ActionHistory ViewController
            if let actionHistoryVC :ActionHistoryVC = ActionHistoryVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(actionHistoryVC, animated: true)
            }
            break
        case "broadcast_sms":
            //Load BroadCastSMS ViewController
            if let broadcastSMSVC :BroadcastSMSVC = BroadcastSMSVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(broadcastSMSVC, animated: true)
            }
            break
        case "change_group":
            //Load ChangeGroup ViewController
            if let changeGroupVC :ChangeGroupVC = ChangeGroupVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(changeGroupVC, animated: true)
            }
            break
        case "top_up":
            
            //Load Group Users  ViewController
            if let userSelectionVC :SingleUserSelectionMainVC = SingleUserSelectionMainVC.instantiateViewControllerFromStoryboard() {
                
                userSelectionVC.setUserSelectedCompletionBloch { (selectedUsers) in
                    
                    //Load TopUpVC ViewController
                    if let topUpVC :TopUpVC = TopUpVC.instantiateViewControllerFromStoryboard() {
                        topUpVC.selectedUserMSISDN = selectedUsers.first?.value.msisdn ?? ""
                        self.navigationController?.pushViewController(topUpVC, animated: true)
                    }
                }
                
                self.navigationController?.pushViewController(userSelectionVC, animated: true)
            }
            
            break
        case "change_limit":
            //Load ChangeGroup ViewController
            if let changeLimitVC :ChangeLimitListVC = ChangeLimitListVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(changeLimitVC, animated: true)
            }
            break
        case "change_sim":
            //Load ChangeSim ViewController
            /*if let changeSimVC :ChangeSimVC = ChangeSimVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(changeSimVC, animated: true)
            }*/
            if let numberRestoration :SIMSwapVC = SIMSwapVC.instantiateViewControllerFromStoryboard() {
                numberRestoration.strNavBarTitle = Localized("Title_SimSwap")//titleString//Localized("Title_DashBoard")
                self.navigationController?.pushViewController(numberRestoration, animated: true)
            }
            break
        case "invoice_information":
            //Load MISDNInvoice ViewController
            if let invoiceInformationVC :InvoiceInformationVC = InvoiceInformationVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(invoiceInformationVC, animated: true)
            }
            break
        case "packages":
            if let supplementoryOfferVC :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(supplementoryOfferVC, animated: true)
            }
            break
            
        default:
            break
        }
    }
    
    
    
}
//MARK: TABLE VIEW METHODS
extension ServicesVC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicesList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let servicesTableCell :CategoriesTableViewCell =  tableView.dequeueReusableCell() {
        servicesTableCell.setItemInfo(aItem: servicesList[indexPath.row])
        return servicesTableCell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Redirect user to selected screen
        redirectUserToSelectedController(Identifier: servicesList[indexPath.row].identifier, titleString: servicesList[indexPath.row].title)
        
    }
}
