//
//  BroadcastSMSVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/28/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class BroadcastSMSVC: BaseVC {
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel : MBLabel!
    @IBOutlet var smsDescriptionTitleLabel: MBLabel!
    @IBOutlet var smsTextView : UITextView!
    @IBOutlet var symbolLengthCountTitleLabel: MBLabel!
    @IBOutlet var smsCountLabel: MBLabel!
    @IBOutlet var selectUsersButton: MBButton!
    
    //MARK:- View Controllerd Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    //MARK: - Functions
    func loadViewLayout(){
        smsTextView.layer.borderColor = UIColor.mbBorderGray.cgColor
        
        titleLabel.text = Localized("Title_BroadcastSMS")
        smsDescriptionTitleLabel.text = Localized("Ttitle_SMSDescription")
        smsTextView.placeholder = Localized("Placeholder_EnterText")
        symbolLengthCountTitleLabel.text = Localized("Title_SymbolsLength") + "0"
        smsCountLabel.text = Localized("Title_SMSCount") + "0"
        selectUsersButton.setTitle(Localized("BtnTitle_SelectUsers") , for: .normal)
        textViewDidChange(smsTextView)
    }
    
    func resetTextView() {
        smsTextView.text = ""
        textViewDidChange(smsTextView)
        smsTextView.placeholder = Localized("Placeholder_EnterText")
        symbolLengthCountTitleLabel.text = Localized("Title_SymbolsLength") + "0"
        smsCountLabel.text = Localized("Title_SMSCount") + "0"
    }
    
    func redirectToUserSelectionVC() {
        if let userSelectionVC :MultipleUserSelectionMainVC = MultipleUserSelectionMainVC.instantiateViewControllerFromStoryboard() {
            
            userSelectionVC.setNextButtonCompletionBloch { (selectedUsers) in
                
                /* POP UserSelection ViewController */
                self.navigationController?.popViewController(animated: true)
                
                //Redirect user to selected screen
                self.showConfirmationPOPUP(selectedUsersList: selectedUsers)
            }
            self.navigationController?.pushViewController(userSelectionVC, animated: true)
        }
    }
    
    func showConfirmationPOPUP( selectedUsersList : [String : UsersData]) {
        let message = String(format: Localized("Title_BroadcastConfrimationMessage"), "\(selectedUsersList.count) \(Localized("BtnTitle_Number"))").createAttributedString(stringsListForAttributed: ["\(selectedUsersList.count) \(Localized("BtnTitle_Number"))"], stringColor: .black)
        
        
        self.showConfirmationAlert(title: Localized("Title_Confirmation"), attributedMessage: message, okBtnTitle: Localized("BtnTitle_OK"), cancelBtnTitle: Localized("BtnTitle_NO"), alertType: .groupSelection, {
            
            var selectedNumbers :[String] = []
            selectedUsersList.forEach({ (key,value) in
                selectedNumbers.append(key)
            })
            
            self.sendFreeSMS(recieverMsisdns: selectedNumbers, textMessage: self.smsTextView.text)
            
        })
    }
    
    //MARK: - IBActions
    @IBAction func selectUsersButtonPressed(_ sender: UIButton){
        _ = smsTextView.resignFirstResponder()
        if smsTextView.text.isBlank{
            self.showAlertWithMessage(message: Localized("Ttitle_BroadcastMessageEmpty"))
        } else{
            redirectToUserSelectionVC()
        }
        
    }
    
    //MARK: - API Calls
    /// Call 'getFreeSMSStatus' API .
    ///
    /// - returns: Void
    func sendFreeSMS(recieverMsisdns: [String], textMessage: String) {
        
        self.showActivityIndicator()
        
        /*API call tpo get data*/
        _ = MBAPIClient.sharedClient.sendFreeSMS(recieverMsisdns: recieverMsisdns, textMessage: textMessage,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let freeSMSHandler = Mapper<FreeSMS>().map(JSONObject: resultData){
                        
                        self.showSuccessAlertWithMessage(message: freeSMSHandler.responseMsg)
                        self.resetTextView()
                    }
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - TextView delagates
extension BroadcastSMSVC : UITextViewDelegate {
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard let textViewValue = textView.text else { return false } // Check that if text is nill then return false
        
        //let newLength = textViewValue.count + text.count - range.length
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        
        let newTextAfterChange = (textViewValue as NSString).replacingCharacters(in: range, with: text)
        
        var smsAllowedTextLength = 0
        if newTextAfterChange.containsOtherThenAllowedCharactersForFreeSMSInEnglish() {
            smsAllowedTextLength = Constants.AzriAndRussianFreeSMSLength
        } else {
            smsAllowedTextLength = Constants.EnglishFreeSMSLength
        }
        
        if newLength <= smsAllowedTextLength {
            
            return true
            
        } else if newLength <= (smsAllowedTextLength * 2) {
            
            return true
        } else {
            
            if newLength > (smsAllowedTextLength * 2) {
                self.showAlertWithMessage(message: String(format: Localized("Message_SMSLimit"), (smsAllowedTextLength * 2)))
            }
            return false
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        
        if textView == self.smsTextView {
            
            if let placeholderLabel = self.smsTextView.viewWithTag(100) as? UILabel {
                placeholderLabel.isHidden = self.smsTextView.text.count > 0
            }
            
            if let newText = textView.text {
                
                var smsAllowedTextLength = 0
                if newText.containsOtherThenAllowedCharactersForFreeSMSInEnglish() {
                    smsAllowedTextLength = Constants.AzriAndRussianFreeSMSLength
                } else {
                    smsAllowedTextLength = Constants.EnglishFreeSMSLength
                }
                
                
                symbolLengthCountTitleLabel.text  = "\(Localized("Title_SymbolsLength"))\(textView.text.utf16.count)"
                
                if newText.utf16.count <= 0 {
                    smsCountLabel.text       = "\(Localized("Title_SMSCount"))\(0)"
                    
                } else if newText.utf16.count <= smsAllowedTextLength {
                    smsCountLabel.text       = "\(Localized("Title_SMSCount"))\(1)"
                    
                } else if newText.utf16.count <= (smsAllowedTextLength * 2) {
                    smsCountLabel.text       = "\(Localized("Title_SMSCount"))\(2)"
                }
                
                if newText.length > (smsAllowedTextLength * 2) {
                    self.showAlertWithMessage(message: String(format: Localized("Message_SMSLimit"), (smsAllowedTextLength * 2)))
                }
                
            } else {
                symbolLengthCountTitleLabel.text  = "\(Localized("Title_SymbolsLength"))\(0)"
                smsCountLabel.text       = "\(Localized("Title_SMSCount"))\(0)"
            }
        }
    }
}

