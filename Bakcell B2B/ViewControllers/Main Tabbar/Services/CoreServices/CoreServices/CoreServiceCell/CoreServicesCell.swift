//
//  CoreServicesCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class CoreServicesCell: UITableViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var detailsLabel: MBLabel!
    
    @IBOutlet var switchControl: MBSwitch!
    
    @IBOutlet weak var switchControalWidth: NSLayoutConstraint!
    
    //MARK: -  Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setLayoutForSwitchSettings(_ aCoreService: CoreServicesList?, isEnabled: Bool = true, showSwitch :Bool) {
        
        // Setting Enable/Disable
        titleLabel.isEnabled = isEnabled
        detailsLabel.isEnabled = isEnabled
        switchControl.isEnabled = isEnabled
        
        if let aCoreServiceObject = aCoreService {
            
            titleLabel.text = aCoreServiceObject.name
            detailsLabel.text = aCoreServiceObject.description
            detailsLabel.textColor = UIColor.mbTextGray
            switchControl.setOn((aCoreService?.status ?? "").isActive() , animated: true)
            
        } else {
            
            titleLabel.text = ""
            detailsLabel.text = ""
            switchControl.setOn(false, animated: true)
        }
        
        if showSwitch {
            switchControalWidth.constant = 49
            switchControl.isHidden = false
        } else {
            switchControalWidth.constant = 0
            switchControl.isHidden = true
        }
        
    }
    
}
