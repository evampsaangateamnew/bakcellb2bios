//
//  CoreServicesVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 06/07/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper

enum UserSelectionType {
    case group
    case individual
}

class CoreServicesVC: BaseVC {
    
    enum MBCoreServiceType: String {
        case internet       = "Internet"
        case suspend        = "suspend"
        case coreService    = "coreService"
        case UnSpecified    = ""
    }
    
    //MARK:- Properties
    var coreServicesData :  [CoreService]?
    var selectedUserList : [String: UsersData]?
    var selectionType : UserSelectionType?
    
    //MARK:- IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var tableView: UITableView!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = UIColor.clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180.0
        tableView.estimatedSectionHeaderHeight = 44
        tableView.allowsMultipleSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        
        // Loading Core service information
        if selectionType == .individual {
            /* AccountType always will be Corporate */
            getCoreServicesIndividual(AccountType: Constants.MBCustomerType.Corporate.rawValue, GroupType: MBSelectedUserSession.shared.groupType, TarrifType: MBSelectedUserSession.shared.brandNameLowerCaseStr, UserMSISDN: MBSelectedUserSession.shared.msisdn, SubscriberType: MBSelectedUserSession.shared.subscriberTypeLowerCaseStr )
            
        } else if selectionType == .group && selectedUserList?.count == 1 {
            
            /* AccountType always will be Corporate */
            getCoreServicesIndividual(AccountType: Constants.MBCustomerType.Corporate.rawValue, GroupType: selectedUserList?.values.first?.groupName ?? "", TarrifType: selectedUserList?.values.first?.brandName ?? "", UserMSISDN: selectedUserList?.values.first?.msisdn ?? "", SubscriberType:  Constants.MBSubscriberType.PostPaid.rawValue.lowercased())
            
        } else {
            getCoreServicesBulk()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        // Layout with localized string
        layoutViewController()
    }
    
    //MARK: - Functions
    func layoutViewController() {
        titleLabel.text = Localized("Title_CoreServices")
    }
    
    ///Reload table data
    func reloadTableViewData() {
        /*if (coreServicesData?.count ?? 0) <= 0 {
         self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
         
         } else {
         self.tableView.hideDescriptionView()
         }*/
        self.tableView.reloadData()
    }
    
    
    
    func typeOfDataInCoreService(allCoreServices: [CoreService]?) -> [MBCoreServiceType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBCoreServiceType] = []
        
        allCoreServices?.forEach({ (aCoreService) in
            dataTypes.append(.coreService)
        })
        
        //  Add internet offer
        // dataTypes.append(.internet)
        
        /* Show suspend number if user is individual or only selected one user */
        if selectionType == .individual ||
            (selectionType == .group && selectedUserList?.count == 1) {
            //  Add block number
            dataTypes.append(.suspend)
            
        }
        
        return dataTypes
    }
    
    @objc func suspendNumberButtonTapped() {
        
        // Calling APi to suspend number
        self.showConfirmationAlert(title: Localized("Title_Confirmation"), message: Localized("Message_SuspendNumberMessage"), okBtnTitle: Localized("BtnTitle_SUSPEND"), cancelBtnTitle: Localized("BtnTitle_CANCEL"), {
            
            var userMSISDN :String = ""
            // Loading Core service information
            if self.selectionType == .individual {
                userMSISDN = MBSelectedUserSession.shared.msisdn
                
            } else if self.selectionType == .group && self.selectedUserList?.count == 1 {
                userMSISDN = self.selectedUserList?.values.first?.msisdn ?? ""
            }
            
            self.reportLostSim(userMSISDN: userMSISDN, suspendMSISDN: MBPICUserSession.shared.msisdn)
        })
        
        /*
         if let suspendNumberVC :SuspendNumberVC = SuspendNumberVC.instantiateViewControllerFromStoryboard() {
         suspendNumberVC.selectedUserList = selectedUserList
         self.navigationController?.pushViewController(suspendNumberVC, animated: true)
         }
         */
    }
    
    func navigateToCorserviceDetail(selectedService : CoreServicesList?) {
        //Go to CoreServiceDetailVC
        if let coreServiceDetailVC :CoreServiceDetailVC = CoreServiceDetailVC.instantiateViewControllerFromStoryboard() {
            coreServiceDetailVC.selectedServiceInfo  = selectedService
            coreServiceDetailVC.selectedUsers = selectedUserList
            self.navigationController?.pushViewController(coreServiceDetailVC, animated: true)
        }
    }
    
    func navigateToFrwarCalls(forSection : Int,forRow: Int, selectedCoreServiceItem :CoreServicesList) {
        
        if let forwardNumberVC :ForwardNumberVC = ForwardNumberVC.instantiateViewControllerFromStoryboard() {
            forwardNumberVC.selectedCoreServiceListItem = selectedCoreServiceItem
            forwardNumberVC.selectedUsersType = selectionType
            forwardNumberVC.selectedUserList = selectedUserList
            
            forwardNumberVC.setCompletionHandler(completionBlock: { (newNumber) in
                self.updateCoreServiceForwordNumber(section: forSection, row: forSection, newForwardNumber: newNumber)
            })
            
            self.navigationController?.pushViewController(forwardNumberVC, animated: true)
        }
    }
    
    func isCallUnavailableActive() -> Bool {
        
        var isActive = false
        
        coreServicesData?.forEach({ (aCoreServiceCatagory) in
            aCoreServiceCatagory.coreServicesList?.forEach({ (aCoreService) in
                
                if aCoreService.offeringId.isEqual(Constants.K_I_Called_You_Turned_Off, ignorCase: true){
                    
                    isActive = aCoreService.status.isActive()
                }
            })
        })
        
        return isActive
    }
    
    
    func toActiveOrInActiveString(string: String?, isActive : Bool = false) -> String {
        
        if string?.isBlank  ?? true {
            return "Inactive"
        }
        
        if isActive {
            return "Active"
        } else {
            return "Inactive"
        }
        
    }
    
    func updateCoreServiceStatus(section : Int, row : Int, isActive : Bool = false) {
        
        if coreServicesData?.indices.contains(section) ?? false {
            
            let aCatagoryType = coreServicesData?[section]
            
            if aCatagoryType?.coreServicesList?.indices.contains(row) ?? false {
                
                if let aCoreService = aCatagoryType?.coreServicesList?[row] {
                    aCoreService.status = self.toActiveOrInActiveString(string: aCoreService.status, isActive: isActive)
                    
                    aCatagoryType?.coreServicesList?[row] = aCoreService
                    
                    coreServicesData?[section] = aCatagoryType!
                }
            }
        }
        
        // Reload TableView Data
        self.reloadTableViewData()
    }
    
    func updateCoreServiceForwordNumber(section : Int, row : Int, newForwardNumber : String = "") {
        
        if coreServicesData?.indices.contains(section) ?? false {
            
            let aCatagoryType = coreServicesData?[section]
            
            if aCatagoryType?.coreServicesList?.indices.contains(row) ?? false {
                
                if let aCoreService = aCatagoryType?.coreServicesList?[row] {
                    aCoreService.forwardNumber = newForwardNumber
                    aCoreService.status = self.toActiveOrInActiveString(string: aCoreService.status, isActive: true)
                    
                    aCatagoryType?.coreServicesList?[row] = aCoreService
                    
                    coreServicesData?[section] = aCatagoryType!
                }
            }
        }
        // Reload TableView Data
        self.tableView.reloadData()
    }
    
    
    //MARK: - API Calls
    /// Call 'getCoreServices' API .
    ///
    /// - returns: Void
    func getCoreServicesIndividual(AccountType userAccountType:  String, GroupType userGroupType : String, TarrifType tarrifType: String, UserMSISDN userMSISDN : String, SubscriberType subscriberType: String ) {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getCoreServices(accountType: userAccountType , groupType: userGroupType , tariffType: tarrifType, msisdn: userMSISDN, subscriberType: subscriberType, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let coreServicesResponseHandler = Mapper<CoreServices>().map(JSONObject: resultData) {
                        // Setting Data in ViewController
                        self.coreServicesData = coreServicesResponseHandler.coreServices
                        
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadTableViewData()
        })
    }
    
    func getCoreServicesBulk() {
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.getCoreServicesBulk(accountType: MBPICUserSession.shared.picUserInfo?.customerType ?? "", groupType: "", selectedUserMSISDN: selectedUserList?.keys.first ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let coreServicesResponseHandler = Mapper<CoreServices>().map(JSONObject: resultData) {
                        // Setting Data in ViewController
                        self.coreServicesData = coreServicesResponseHandler.coreServices
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadTableViewData()
        })
    }
    
    
    @objc func switchChanged(mySwitch: MBSwitch) {
        
        self.showActivityIndicator()
        
        let section = mySwitch.sectionTag
        let row = mySwitch.rowTag
        
        if self.coreServicesData?.indices.contains(section) ?? false {
            
            let aCatagoryType = self.coreServicesData?[section]
            
            if aCatagoryType?.coreServicesList?.indices.contains(row) ?? false {
                
                if let aCoreService = aCatagoryType?.coreServicesList?[row] {
                    
                    var selectedMSISDN :String = ""
                    
                    // Loading Core survice information
                    if selectionType == .individual {
                        selectedMSISDN = MBSelectedUserSession.shared.msisdn
                        
                    } else if selectionType == .group &&
                        selectedUserList?.count == 1 {
                        
                        selectedMSISDN = selectedUserList?.values.first?.msisdn ?? ""
                        
                    }
                    
                    /* Check if selected offer id is equal to Call Forward and user deactivating then call supplementary offers to deactivate Call Forward offer */
                    if aCoreService.offeringId.isEqual(Constants.K_Call_Forward, ignorCase: true) &&
                        mySwitch.isOn == false {
                        
                        _ = MBAPIClient.sharedClient.changeSupplementaryOffering(msisdn: selectedMSISDN, actionType: false, offeringId: aCoreService.offeringId, offerName: aCatagoryType?.coreServiceCategory ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                            
                            self.hideActivityIndicator()
                            
                            if error != nil {
                                
                                error?.showServerErrorInViewController(self)
                                // changing back to first value
                                mySwitch.setOn(!mySwitch.isOn, animated: true)
                            } else {
                                
                                // handling data from API response.
                                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                                    
                                    // Update current status in local storage
                                    self.updateCoreServiceStatus(section: section, row: row, isActive: mySwitch.isOn)
                                    self.reloadTableViewData()
                                    // Success Alert
                                    self.showSuccessAlertWithMessage(message: resultDesc)
                                    
                                } else {
                                    // Error Alert
                                    self.showErrorAlertWithMessage(message: resultDesc)
                                    
                                    // changing back to first value
                                    mySwitch.setOn(!mySwitch.isOn, animated: true)
                                }
                            }
                        })
                        
                    } else {
                        
                        _ = MBAPIClient.sharedClient.processBulkCoreServices(msisdns: [selectedMSISDN],
                                                                             offeringId: aCoreService.offeringId,
                                                                             forwardNumber: "",
                                                                             actionType: mySwitch.isOn,
                                                                             { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                                                                                
                                                                                self.hideActivityIndicator()
                                                                                
                                                                                if error != nil {
                                                                                    error?.showServerErrorInViewController(self)
                                                                                    // changing back to first value
                                                                                    mySwitch.setOn(!mySwitch.isOn, animated: true)
                                                                                } else {
                                                                                    
                                                                                    // handling data from API response.
                                                                                    if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                                                                                        
                                                                                        // Update current status in local storage
                                                                                        self.updateCoreServiceStatus(section: section, row: row, isActive: mySwitch.isOn)
                                                                                        self.reloadTableViewData()
                                                                                        
                                                                                        // Success Alert 
                                                                                         self.showSuccessAlertWithMessage(message: resultDesc)
                                                                                        
                                                                                    } else {
                                                                                        
                                                                                        // Error Alert
                                                                                        self.showErrorAlertWithMessage(message: resultDesc)
                                                                                        
                                                                                        // changing back to first value
                                                                                        mySwitch.setOn(!mySwitch.isOn, animated: true)
                                                                                    }
                                                                                }
                        })
                    }
                }
            }
        }
        
        
    }
    
    /// Call 'reportLostSim' API .
    ///
    /// - returns: Void
    func reportLostSim(userMSISDN :String, suspendMSISDN :String) {
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.reportLostSim(userMSISDN: userMSISDN, suspendMSISDN: suspendMSISDN, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let responseHandler = Mapper<MessageMSGResponse>().map(JSONObject: resultData) {
                        // Success Alert
                        self.showSuccessAlertWithMessage(message: responseHandler.responseMsg, {
                            self.navigationController?.popViewController(animated: true)
                        })
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc ?? "")
                    }
                } else {
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
        
    }
    
}

//MARK:- TABLE VIEW DELEGATE METHODS
extension CoreServicesVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.typeOfDataInCoreService(allCoreServices: coreServicesData).count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let coreServicesHeader : CoreServicesHeaderView = tableView.dequeueReusableHeaderFooterView() {
            
            let allTypeOfData = self.typeOfDataInCoreService(allCoreServices: coreServicesData)
            
            if allTypeOfData[section] == .suspend {
                coreServicesHeader.serviceTitle.text =  Localized("Title_SuspendedNumber")
            } else {
                coreServicesHeader.serviceTitle.text = (coreServicesData?[section].coreServiceCategory ?? "")}
            return coreServicesHeader
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let allTypeOfData = self.typeOfDataInCoreService(allCoreServices: coreServicesData)
        
        if allTypeOfData[section] == .internet || allTypeOfData[section] == .suspend { // Check if section is second last
            return 1
        } else {
            return coreServicesData?[section].coreServicesList?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let allTypeOfData = self.typeOfDataInCoreService(allCoreServices: coreServicesData)
        
        switch allTypeOfData[indexPath.section] {
            
        case .suspend:
            if let suspendedCell :SuspendedNumberCell = tableView.dequeueReusableCell() {
                suspendedCell.selectionStyle = UITableViewCell.SelectionStyle.none
                suspendedCell.suspendNumberButton.addTarget(self, action: #selector(suspendNumberButtonTapped), for: .touchUpInside)
                return suspendedCell
            }
            break
            
        case .coreService:
            var shouldShowSwitch :Bool = true
            /* Check if type in individual or 1 user selected then show status switch */
            if selectedUserList?.count ?? 0 ==  1 || selectionType == .individual  {
                shouldShowSwitch = true
            } else {
                shouldShowSwitch = false
            }
            
            if let aCoreService = coreServicesData?[indexPath.section].coreServicesList?[indexPath.row] {
                
                /* Check if offer id matches callForward and set layout accourdingly */
                if aCoreService.offeringId.isEqual(Constants.K_Call_Forward, ignorCase: true),
                    let callForwardCell :CallForwardCell = tableView.dequeueReusableCell() {
                    
                    callForwardCell.setLayoutForForwordSettings(number: aCoreService.forwardNumber, showSwitch: shouldShowSwitch)
                    
                    callForwardCell.switchControl.sectionTag = indexPath.section
                    callForwardCell.switchControl.rowTag = indexPath.row
                    
                    callForwardCell.switchControl.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                    
                    if aCoreService.status.isActive() == false {
                        
                        callForwardCell.selectionStyle = UITableViewCell.SelectionStyle.default
                        callForwardCell.switchControl.setOn(false, animated: true)
                        
                    } else {
                        callForwardCell.selectionStyle = UITableViewCell.SelectionStyle.none
                        callForwardCell.switchControl.setOn(true, animated: true)
                    }
                    
                    return callForwardCell
                    
                } else {
                    
                    if let coreServicesCell :CoreServicesCell = tableView.dequeueReusableCell() {
                        
                        coreServicesCell.selectionStyle = UITableViewCell.SelectionStyle.none
                        coreServicesCell.switchControl.sectionTag = indexPath.section
                        coreServicesCell.switchControl.rowTag = indexPath.row
                        
                        
                        coreServicesCell.switchControl.setOn(aCoreService.status.isActive(), animated: true)
                        
                        coreServicesCell.switchControl.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                        
                        /* Check if offer id matches IAmBack and user is not in bulk then set layout accourdingly */
                        if aCoreService.offeringId.isEqual(Constants.K_I_Am_Back, ignorCase: true),
                            shouldShowSwitch == true /* Using this check to identify user is bulk or not */ {
                            
                            /* Check if offer "Turned off" in "I Called You" is active then user can activate(click on)  "I Am Back"
                             Otherwise User cann't activate this offer */
                            coreServicesCell.setLayoutForSwitchSettings(aCoreService, isEnabled: self.isCallUnavailableActive(), showSwitch: shouldShowSwitch)
                            
                        } else {
                            coreServicesCell.setLayoutForSwitchSettings(aCoreService, showSwitch: shouldShowSwitch)
                        }
                        
                        return coreServicesCell
                    }
                }
            }
        default:
            break
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let allTypeOfData = self.typeOfDataInCoreService(allCoreServices: coreServicesData)
        
        switch allTypeOfData[indexPath.section] {
        case .coreService:
            if let aCoreService = coreServicesData?[indexPath.section].coreServicesList?[indexPath.row] {
                
                /* Check if selected offer is Call Forward and is not active then redirect to forward number setting page */
                if aCoreService.offeringId.isEqual(Constants.K_Call_Forward, ignorCase: true) {
                    
                    if aCoreService.status.isActive() == false {
                        navigateToFrwarCalls(forSection: indexPath.section, forRow: indexPath.row, selectedCoreServiceItem: aCoreService)
                    }
                    
                } else if selectionType == .group && selectedUserList?.count ?? 0 > 1 { /* Check id user is not individual then redirect user to detail page */
                    
                    navigateToCorserviceDetail(selectedService: coreServicesData?[indexPath.section].coreServicesList?[indexPath.row])
                    
                }
            }
        default:
            break
        }
    }
}

