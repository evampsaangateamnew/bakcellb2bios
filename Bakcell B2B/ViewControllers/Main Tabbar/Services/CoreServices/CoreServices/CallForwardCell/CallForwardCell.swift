//
//  CallForwardCell.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 10/1/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class CallForwardCell: UITableViewCell {
 
    //MARK:- IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var switchContainorView: UIView!
    @IBOutlet var switchControl: MBSwitch!
    
    @IBOutlet var switchControlWidth: NSLayoutConstraint!
    
    //MARK: -  Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setLayoutForForwordSettings(number: String?, showSwitch :Bool) {
        
        if let numberValue = number,
            numberValue.isBlank == false {
            
            titleLabel.text = Localized("Info_SettingTitle") + ": " + numberValue
            switchControl.isEnabled = true
        
        } else {
            titleLabel.text = Localized("Info_SettingTitle") + ": " + Localized("Info_None")
            switchControl.isEnabled = false
        }
        
        if showSwitch == true {
            switchControlWidth.constant = 66
            switchContainorView.isHidden = false
        } else {
            switchControlWidth.constant = 0
            switchContainorView.isHidden = true
        }
        
    }
    
}
