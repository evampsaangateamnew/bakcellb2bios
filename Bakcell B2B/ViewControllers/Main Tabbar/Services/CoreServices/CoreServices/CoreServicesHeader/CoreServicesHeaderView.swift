//
//  CoreServicesHeaderView.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/16/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class CoreServicesHeaderView: UITableViewHeaderFooterView {

    //MARK: - IBOutlets
    @IBOutlet var serviceTitle: MBLabel!

}
