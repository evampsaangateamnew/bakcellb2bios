//
//  ForwardNumberVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ForwardNumberVC: BaseVC {
    
    //MARK: - Properties
    
    var selectedUsersType : UserSelectionType?
    var selectedCoreServiceListItem : CoreServicesList?
    var selectedUserList : [String : UsersData]?
    
    fileprivate var didChangeNumberBlock : MBButtonWithParamCompletionHandler?
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var mobileNumberTextField: UITextField!
    @IBOutlet var saveButton: UIButton!
    
    //MARK: - ViewControllers Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mobileNumberTextField.delegate = self
        
        if let number = selectedCoreServiceListItem?.forwardNumber,
            number.isBlank == false {
            
            mobileNumberTextField.text = number
        } else {
            mobileNumberTextField.text = ""
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        // Layout with localized string
        layoutViewController()
    }
    
    //MARK: - Functions
    func layoutViewController() {
        titleLabel.text                     = Localized("Title_ForwardNumber")
        descriptionLabel.text               = Localized("Info_Discription")
        mobileNumberTextField.placeholder   =  Localized("PlaceHolder_FowardNumber")
        saveButton.setTitle(Localized("BtnTitle_SAVE"), for: UIControl.State.normal)
    }
    
    // Yes and No button completion handler
    func setCompletionHandler(completionBlock : @escaping MBButtonWithParamCompletionHandler ) {
        self.didChangeNumberBlock = completionBlock
    }
    
    //MARK: IBACTIONS
    @IBAction func savePressed(_ sender: Any) {
        
        var msisdn : String = ""
        // MSISDN validation
        if mobileNumberTextField.text?.isBlank == true {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
            return
        }
        if let mobileNumberText = mobileNumberTextField.text,
            mobileNumberText.isValidMSISDN() {
            msisdn = mobileNumberText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
            return
        }
        
        // TextField resignFirstResponder
        _ = mobileNumberTextField.resignFirstResponder()
        
        
        var selectedMSISDNs : [String] = []
        
        if selectedUsersType == .individual {
            selectedMSISDNs = [MBSelectedUserSession.shared.msisdn]
            
        } else if (selectedUsersType == .group &&
                selectedUserList?.count == 1) {
            
            selectedMSISDNs = [selectedUserList?.values.first?.msisdn ?? ""]
            
        } else {
            selectedUserList?.forEach { (key, aMSISDNDetailObject) in
                selectedMSISDNs.append(aMSISDNDetailObject.msisdn)
            }
        }
        
        callProcessBulkCoreServices(forwardNumber: msisdn, selectedUsers: selectedMSISDNs)
        
    }
    
    
    
    //MARK: - API Calls
    
    func callProcessBulkCoreServices(forwardNumber: String, selectedUsers: [String]) {
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.processBulkCoreServices(msisdns: selectedUsers, offeringId: selectedCoreServiceListItem?.offeringId ?? "", forwardNumber: forwardNumber,  actionType: true, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    var message :String?
                    if let response = Mapper<BulkCoreServices>().map(JSONObject: resultData) {
                        message = response.responseMsg
                    } else {
                        message = resultDesc
                    }
                    // Success Alert
                    self.showSuccessAlertWithMessage(message: message, {
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                } else {
                    // Error Alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                    
                }
            }
        })
    }
}

//MARK: - Textfield delagates

extension ForwardNumberVC : UITextFieldDelegate {
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false } // Check that if text is nill then return false
        
        let newLength = text.count + string.count - range.length
        
        if textField == mobileNumberTextField {
            
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            if newLength <= Constants.MSISDNLength && allowedCharacters.isSuperset(of: characterSet) {
                // Returning number input
                return  true
            } else {
                return  false
            }
            
        } else {
            return false
        }
    }
}
