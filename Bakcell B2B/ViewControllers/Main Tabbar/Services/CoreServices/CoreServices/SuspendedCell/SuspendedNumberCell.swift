//
//  SuspendedNumberCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/16/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class SuspendedNumberCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet var suspendNumberTitle: MBLabel!
    @IBOutlet var suspendNumberDescription: MBLabel!
    @IBOutlet var suspendNumberButton : MBButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setSuspendedDescription()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: -  Functions
    func setSuspendedDescription() {
        suspendNumberTitle.text = Localized("Info_SuspendedNumber")
        suspendNumberDescription.text = Localized("Info_SuspendedNumberDetail")
        suspendNumberButton.setTitle(Localized("BtnTitle_SUSPEND"), for: .normal)
        
    }
    

}
