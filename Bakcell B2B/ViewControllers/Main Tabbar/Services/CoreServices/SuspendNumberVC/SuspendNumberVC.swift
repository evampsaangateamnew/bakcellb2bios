//
//  SuspendNumberVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 7/6/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class SuspendNumberVC: BaseVC {
    
    var selectedCoreServiceListItem : CoreServicesList?
    var selectedUserList : [String : UsersData]?
    
    fileprivate var didChangeNumberBlock : MBButtonCompletionHandler?
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var mobileNumberTextField: UITextField!
    @IBOutlet var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mobileNumberTextField.delegate = self
        mobileNumberTextField.text = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        // Layout with localized string
        layoutViewController()
    }
    
    
    //MARK:- Functions
    func layoutViewController() {
        titleLabel.text                     = Localized("Title_SuspendNumber")
        descriptionLabel.text               = Localized("Info_SuspendNumber")
        mobileNumberTextField.placeholder   =  Localized("PlaceHolder_EnterSuspendNumber")
        saveButton.setTitle(Localized("BtnTitle_SAVE"), for: UIControl.State.normal)
    }
    
    // Yes and No button completion handler
    func setCompletionHandler(completionBlock : @escaping MBButtonCompletionHandler ) {
        self.didChangeNumberBlock = completionBlock
    }
    
    //MARK: IBACTIONS
    @IBAction func savePressed(_ sender: Any) {
        
        var msisdn : String = ""
        // MSISDN validation
        if mobileNumberTextField.text?.isBlank == true {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
            return
        }
        if let mobileNumberText = mobileNumberTextField.text,
            mobileNumberText.isValidMSISDN() {
            
            msisdn = mobileNumberText
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidNumber"))
            return
        }
        
        // TextField resignFirstResponder
        mobileNumberTextField.resignFirstResponder()
        
        // Calling APi to suspend number
        self.showConfirmationAlert(title: Localized("Title_Confirmation"), message: Localized("Message_SuspendNumberMessage"), okBtnTitle: Localized("BtnTitle_SUSPEND"), cancelBtnTitle: Localized("BtnTitle_CANCEL"), {
            self.reportLostSim(userMSISDN: self.selectedUserList?.values.first?.msisdn ?? "", suspendMSISDN: msisdn)
            
        })
        
    }
    
    //MARK: - API Calls
    /// Call 'reportLostSim' API .
    ///
    /// - returns: Void
    func reportLostSim(userMSISDN :String, suspendMSISDN :String) {
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.reportLostSim(userMSISDN: userMSISDN, suspendMSISDN: suspendMSISDN, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if let responseHandler = Mapper<MessageResponse>().map(JSONObject: resultData) {
                        // Success Alert
                        self.showSuccessAlertWithMessage(message: responseHandler.message, {
                            self.navigationController?.popViewController(animated: true)
                        })
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc ?? "")
                    }
                } else {
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
        
    }
}

//MARK: - Textfield delagates

extension SuspendNumberVC : UITextFieldDelegate {
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false } // Check that if text is nill then return false
        
        let newLength = text.count + string.count - range.length
        
        if textField == mobileNumberTextField {
            
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            if newLength <= Constants.MSISDNLength && allowedCharacters.isSuperset(of: characterSet) {
                // Returning number input
                return  true
            } else {
                return  false
            }
            
        } else {
            return false
        }
    }
}
