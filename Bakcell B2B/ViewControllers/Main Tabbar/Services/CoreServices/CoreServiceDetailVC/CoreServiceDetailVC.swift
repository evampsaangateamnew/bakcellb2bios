//
//  CoreServiceDetailVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/16/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class CoreServiceDetailVC: BaseVC {

    //MARK:-  Variables
    
    var selectedUsers : [String : UsersData]?
    var selectedServiceInfo : CoreServicesList?
    var coreServiceCategory : String?
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var coreServiceDescription : MBLabel!
    @IBOutlet var serviceSwitchButton : MBSwitch!
    @IBOutlet var saveButton : MBButton!
    
    //MARK: -  Viewcontroller Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }

    // MARK: -  Functions
    func loadViewLayout() {
        titleLabel.text = Localized("\(selectedServiceInfo?.name ?? "")")
        coreServiceDescription.text = Localized("\(selectedServiceInfo?.description ?? "")")
        saveButton.setTitle(Localized("BtnTitle_SAVE"), for: .normal)
        
        serviceSwitchButton.setOn(selectedServiceInfo?.status == "1" ? true : false, animated: true)
    }
    
    //MARK: -  IBActions
    @IBAction func saveButtonPressed(_ sender: UIButton){
        processCoreServices(turnOn: serviceSwitchButton.isOn)
    }
    
    
    func processCoreServices(turnOn:Bool) {
        
        self.showActivityIndicator()
        
             if let aCoreService = selectedServiceInfo {
                
                var selectedMSISDNs : [String] = []
                selectedUsers?.forEach { (key, aMSISDNDetailObject) in
                    selectedMSISDNs.append(aMSISDNDetailObject.msisdn)
                }
                _ = MBAPIClient.sharedClient.processBulkCoreServices(msisdns: selectedMSISDNs,
                                                                     offeringId: aCoreService.offeringId,
                                                                     actionType: turnOn,
                                                                     { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                    
                    self.hideActivityIndicator()
                    
                    if error != nil {
                        
                        error?.showServerErrorInViewController(self)
                        
                    } else {
                        
                        // handling data from API response.
                        if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                            
                            var message :String?
                            if let response = Mapper<BulkCoreServices>().map(JSONObject: resultData) {
                                message = response.responseMsg
                            } else {
                                message = resultDesc
                            }
                            // Success Alert
                            self.showSuccessAlertWithMessage(message: message, {
                                self.navigationController?.popViewController(animated: true)
                            })
                            
                        } else {
                            // Error Alert
                            self.showErrorAlertWithMessage(message: resultDesc)
                        }
                    }
                })
        }
        
 
    }
}
