//
//  InvoiceSummaryVC.swift
//  Bakcell B2B
//
//  Created by Muhammad Waqas on 5/13/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class InvoiceSummaryVC: BaseVC {
    
    //MARK: - Properties
    var summaryInvoiceList : SummaryInvoiceModel?
    let monthsTitles : [String] = MBUtilities.getMonthNames(totalNumberOfMonths: 3)
    
    var isStartDate : Bool?
    
    //MARK: -  IBOutlets
    @IBOutlet var monthSelectionDropDown : UIDropDown!
    @IBOutlet var companyInvoiceTableView : UITableView!
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        companyInvoiceTableView.delegate = self
        companyInvoiceTableView.dataSource = self
        companyInvoiceTableView.estimatedRowHeight = 60
        companyInvoiceTableView.estimatedSectionHeaderHeight = 60
        
        self.getCompanyInvoiceSummary(monthIndex: "-1")
        
        setDropDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        reloadTableViewData()
    }
    
    //MARK: - IBAction
    
    //MARK: - Functions
    func setDropDown()  {
        
        //Months Drop Down
        monthSelectionDropDown.dataSource = monthsTitles
        monthSelectionDropDown.placeholder = monthsTitles[0]
        
        monthSelectionDropDown.didSelectOption { (index, option) in
            
            switch(index) {
            case 0:
                self.getCompanyInvoiceSummary(monthIndex: "-1")
                break
            case 1:
                self.getCompanyInvoiceSummary(monthIndex: "-2")
                break
                
            case 2:
                self.getCompanyInvoiceSummary(monthIndex: "-3")
                break
                
            default:
                self.getCompanyInvoiceSummary(monthIndex: "-1")
                break
            }
            
        }
    }
    
    func reloadTableViewData() {
        
        if (summaryInvoiceList?.companySummary?.count ?? 0 <= 0) {
            
            self.companyInvoiceTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            
            self.companyInvoiceTableView.hideDescriptionView()
        }
        
        companyInvoiceTableView.reloadData()
    }
    
    /**
     Call 'getCompanyInvoice Summary' API .
     - returns: Void
     */
    func getCompanyInvoiceSummary(monthIndex : String) {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.companyInvoiceSummary(monthIndex: monthIndex,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let summaryInformation = Mapper<SummaryInvoiceModel>().map(JSONObject: resultData) {
                        
                        self.summaryInvoiceList = summaryInformation
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewData()
        })
    }
}

//MARK: - TableView Delegats
extension InvoiceSummaryVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (summaryInvoiceList?.companySummary?.count ?? 0 > 0) {
            
            return 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let summaryHeaderView : MISDNInvoiceFooterView = tableView.dequeueReusableHeaderFooterView() {
            summaryHeaderView.setInvoiceTotal(TotalInvoiceTitle: self.summaryInvoiceList?.totalDiscountLable ?? "", TotalInvoiceValue: self.summaryInvoiceList?.totalDiscountValue ?? "0")
            
            summaryHeaderView.totalAmountTitleLabel.setLabelFontType(.body)
            
            summaryHeaderView.topSeparatorView.isHidden = true
            summaryHeaderView.bottomSeparatorView.isHidden = true
            
            return summaryHeaderView
        }
        
        return UITableViewHeaderFooterView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return summaryInvoiceList?.companySummary?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let summaryCell :  SummaryInvoiceCell = tableView.dequeueReusableCell() {
            
            summaryCell.setSummaryInfo(aItem: summaryInvoiceList?.companySummary?[indexPath.row])
            return summaryCell
        }
        
        return UITableViewCell()
    }
}
