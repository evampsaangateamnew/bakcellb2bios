//
//  SummaryInvoiceCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class SummaryInvoiceCell: UITableViewCell {

    //MARK: -  IBOutlets
    @IBOutlet var summaryTitle : MBMarqueeLabel!
    @IBOutlet var summaryInvoice : MBLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Functions
    func setSummaryInfo(aItem: SummaryItem?)  {
        
        if let summaryItem = aItem {
            summaryTitle.text = summaryItem.label
            summaryInvoice.attributedText = MBUtilities.createAttributedText(summaryItem.value ?? "", textColor: .black, withManatSign: true)
            
        } else {
            summaryTitle.text = ""
            summaryInvoice.text = ""
        }
    }

}
