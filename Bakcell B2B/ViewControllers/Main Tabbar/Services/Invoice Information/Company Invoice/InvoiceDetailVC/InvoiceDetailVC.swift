//
//  InvoiceDetailVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 7/31/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class InvoiceDetailVC:  BaseVC {
    
    enum CompanyInvoiceType {
        case summary
        case details
    }
    
    //MARK: - Properties
    var summaryInvoiceList : [SummaryInvoice] = []
    var detailInvoiceList : [BillSubItemDetail]  = []
    let monthsTitles : [String] = MBUtilities.getMonthNames(totalNumberOfMonths: 3)
    
    var isStartDate : Bool?
    
    //MARK: -  IBOutlets
    @IBOutlet var monthSelectionDropDown : UIDropDown!
    @IBOutlet var companyInvoiceTableView : UITableView!
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        companyInvoiceTableView.delegate = self
        companyInvoiceTableView.dataSource = self
        companyInvoiceTableView.estimatedRowHeight = 60
        companyInvoiceTableView.estimatedSectionHeaderHeight = 60
        
        //first API call
        self.getCompanyInvoiceDetail(monthIndex: "-1")
        
        setDropDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        reloadTableViewData()
    }
    
    //MARK: - IBAction
    
    //MARK: - Functions
    func setDropDown()  {
        
        //Months Drop Down
        monthSelectionDropDown.dataSource = monthsTitles
        monthSelectionDropDown.placeholder = monthsTitles[0]
        
        monthSelectionDropDown.didSelectOption { (index, option) in
            
            switch(index) {
            case 0:
                self.getCompanyInvoiceDetail(monthIndex: "-1")
                break
            case 1:
                self.getCompanyInvoiceDetail(monthIndex: "-2")
                break
                
            case 2:
                self.getCompanyInvoiceDetail(monthIndex: "-3")
                break
                
            default:
                self.getCompanyInvoiceDetail(monthIndex: "-1")
                break
            }
        }
    }
    
    func reloadTableViewData() {
        
        if (detailInvoiceList.count <= 0) {
            
            self.companyInvoiceTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.companyInvoiceTableView.hideDescriptionView()
        }
        
        companyInvoiceTableView.reloadData()
    }
    
    /**
     Call 'getCompanyInvoice Detail' API .
     
     
     - returns: Void
     */
    func getCompanyInvoiceDetail(monthIndex : String) {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.companyInvoiceDetail(monthIndex: monthIndex,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let companyInvoiceInformation = Mapper<CompanyInvoiceDetail>().map(JSONObject: resultData){
                        
                        self.detailInvoiceList = companyInvoiceInformation.companyDetail?.billSubItemDetail ?? []
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewData()
        })
    }
}

//MARK: - TableView Delegats
extension InvoiceDetailVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (detailInvoiceList.count > 0) {
            return 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let detailHeaderView : DetailInvoiceHeaderView = tableView.dequeueReusableHeaderFooterView() {
            return detailHeaderView
        }
        
        return UITableViewHeaderFooterView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return detailInvoiceList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let detailCell :  DetailsInvoiceCell = tableView.dequeueReusableCell( ){
            
            detailCell.setItemDetail(itemDetail: detailInvoiceList[indexPath.row].details)
            return detailCell
        }
        
        return UITableViewCell()
    }
}
