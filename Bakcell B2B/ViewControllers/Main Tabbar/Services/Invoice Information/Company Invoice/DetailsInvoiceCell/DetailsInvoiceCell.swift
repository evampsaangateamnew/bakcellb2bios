//
//  DetailsInvoiceCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class DetailsInvoiceCell: UITableViewCell {
    //MARK: - IBOutlets
    @IBOutlet var serviceLabel : MBMarqueeLabel!
    @IBOutlet var quantityLabel : MBLabel!
    @IBOutlet var vahidiLabel : MBLabel!
    @IBOutlet var mablaqLabel : MBLabel!
    @IBOutlet var discountLabel : MBLabel!
    @IBOutlet var camiLabel : MBLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Functions
    func setInvoiceDetails(details: DetailInvoice? )  {
        serviceLabel.text = details?.service ?? ""
        quantityLabel.text = details?.quantity ?? ""
        vahidiLabel.text = details?.vahidi ?? ""
        
        mablaqLabel.attributedText = MBUtilities.createAttributedText(details?.mablaq ?? "", textColor: .mbTextGray, withManatSign: true)
        discountLabel.attributedText = MBUtilities.createAttributedText(details?.discount ?? "", textColor: .mbTextGray, withManatSign: true)
        camiLabel.attributedText = MBUtilities.createAttributedText(details?.cami ?? "", textColor: .mbTextGray, withManatSign: true)
    }
    
    func setItemDetail(itemDetail: BillSubItemData? )  {
        serviceLabel.text = itemDetail?.subitemTitle ?? ""
        quantityLabel.text = itemDetail?.detailUsage ?? ""
        vahidiLabel.text = itemDetail?.detailUnit ?? ""
        
        mablaqLabel.attributedText = MBUtilities.createAttributedText(itemDetail?.detailInvoice ?? "", textColor: .mbTextGray, withManatSign: true)
        discountLabel.attributedText = MBUtilities.createAttributedText(itemDetail?.detailDesc ?? "", textColor: .mbTextGray, withManatSign: true)
        camiLabel.attributedText = MBUtilities.createAttributedText(itemDetail?.detailTotal ?? "", textColor: .mbTextGray, withManatSign: true)
    }
    
}
