//
//  DetailInvoiceHeaderView.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class DetailInvoiceHeaderView: UITableViewHeaderFooterView {

    //MARK: - IBOutlets
    @IBOutlet var serviceTitleLabel : MBMarqueeLabel!
    @IBOutlet var quantityTitleLabel : MBMarqueeLabel!
    @IBOutlet var vahidiTitleLabel : MBMarqueeLabel!
    @IBOutlet var mablaqTitleLabel : MBMarqueeLabel!
    @IBOutlet var discountTitleLabel : MBMarqueeLabel!
    @IBOutlet var camiTitleLabel : MBMarqueeLabel!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        loadViewLayout()
    }
    
    
    

    //MARK: - Functions
    func loadViewLayout()  {
        serviceTitleLabel.text = Localized("Title_InvoiceServices")
        quantityTitleLabel.text = Localized("Title_InvoiceQuantity")
        vahidiTitleLabel.text = Localized("Title_InvoiceVahidi")
        mablaqTitleLabel.text = Localized("Title_InvoiceMablaq")
        discountTitleLabel.text = Localized("Title_InvoiceDiscount")
        camiTitleLabel.text = Localized("Title_InvoiceCami")
        
        //Text color
        
        serviceTitleLabel.textColor = .mbTextLighGray
        quantityTitleLabel.textColor = .mbTextLighGray
        vahidiTitleLabel.textColor = .mbTextLighGray
        mablaqTitleLabel.textColor = .mbTextLighGray
        discountTitleLabel.textColor = .mbTextLighGray
        camiTitleLabel.textColor = .mbTextLighGray
    }
}
