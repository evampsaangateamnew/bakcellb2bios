//
//  InvoiceInformationVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/3/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class InvoiceInformationVC: BaseVC {

    //MARK: - Properties
    var invoiceInformationList : [Category] = [Category(Name: Localized("Title_CompanyInvoice"), ImageName: "invoice-company", Idenitifier: "company_invoice"),Category(Name: Localized("Title_NumberInvoice"), ImageName: "invoice-msisdn", Idenitifier: "misdn_invoice")]
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var invoiceInformationTableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        invoiceInformationTableView.estimatedRowHeight = 100
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }


    //MARK: - Functions
     func loadViewLayout() {
        titleLabel.text = Localized("Title_InvoiceInformation")
    }
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier: String){
        switch identifier {
        case "misdn_invoice":
            //Load MISDNInvoiceVC ViewController
//            if let misdnInvoiceVC :MISDNInvoiceVC = MISDNInvoiceVC.instantiateViewControllerFromStoryboard() {
//                self.navigationController?.pushViewController(misdnInvoiceVC, animated: true)
//            }
            
            //Load Group Users  ViewController
            if let userSelectionVC :SingleUserSelectionMainVC = SingleUserSelectionMainVC.instantiateViewControllerFromStoryboard() {
                
                userSelectionVC.setUserSelectedCompletionBloch { (selectedUsers) in
                    
                    //Load MSISDNInvoiceInfoMainVC ViewController
                    if let msisdnInvoiceInfoMainVC :MSISDNInvoiceInfoMainVC = MSISDNInvoiceInfoMainVC.instantiateViewControllerFromStoryboard() {
                        msisdnInvoiceInfoMainVC.selectedUserMSISDN = selectedUsers.first?.value.msisdn ?? ""
                        msisdnInvoiceInfoMainVC.selectedUserFullName = selectedUsers.first?.value.custFullName ?? ""
                        self.navigationController?.pushViewController(msisdnInvoiceInfoMainVC, animated: true)
                    }
                }
                
                self.navigationController?.pushViewController(userSelectionVC, animated: true)
            }
            
            break
        case "company_invoice":
            //Load MISDNInvoiceVC ViewController
            if let companyInvoiceVC :CompanyInvoiceVC = CompanyInvoiceVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(companyInvoiceVC, animated: true)
            }
            break
        default:
            break
        }
        
    }

}
extension InvoiceInformationVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoiceInformationList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let invoiceInfoCell : CategoriesTableViewCell =  tableView.dequeueReusableCell() {
            invoiceInfoCell.setCategoryInfo(aCategory: invoiceInformationList[indexPath.row])
            return invoiceInfoCell
        } else {
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Redirect user to selected screen
        redirectUserToSelectedController(Identifier: invoiceInformationList[indexPath.row].identifier)
        
    }
}
