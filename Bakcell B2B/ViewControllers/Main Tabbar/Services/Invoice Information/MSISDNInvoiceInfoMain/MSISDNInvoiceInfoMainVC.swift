//
//  MSISDNInvoiceInfoMainVC.swift
//  Bakcell B2B
//
//  Created by Muhammad Waqas on 5/13/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import UIKit

class MSISDNInvoiceInfoMainVC: BaseVC {
    
    //MARK: - Properties
    var selectedUserFullName :String = ""
    var selectedUserMSISDN :String = ""
    
    var summaryInvoiceDetailObject : MISDNInvoiceVC?
    var detailInvoiceDetailObject : MISDNInvoiceDetailVC?
    
    //MARK: -  IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var shareButton : UIButton!
    @IBOutlet var summaryButton : MBButton!
    @IBOutlet var detailsButton : MBButton!
    @IBOutlet var childVCContainorView : UIView!
    
    
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        summaryButtonPressed(summaryButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    
    
    //MARK: - Functions
    
    func loadViewLayout(){
        titleLabel.text = Localized("Title_MsisdnInvoice")
        summaryButton.setTitle(Localized("BtnTitle_Summary"), for: .normal)
        detailsButton.setTitle(Localized("BtnTitle_Details"), for: .normal)
        
    }
    
    func redirectToVC(ofType selectedType : InvoiceDetailVC.CompanyInvoiceType) {
        
        if selectedType == .summary {
            
            if summaryInvoiceDetailObject == nil {
                summaryInvoiceDetailObject = MISDNInvoiceVC.instantiateViewControllerFromStoryboard()
                summaryInvoiceDetailObject?.selectedUserFullName = selectedUserFullName
                summaryInvoiceDetailObject?.selectedUserMSISDN = selectedUserMSISDN
            }
            self.removeChildViewController(childController: InvoiceDetailVC())
            
            self.addChildViewController(childController: summaryInvoiceDetailObject ?? MISDNInvoiceVC(), onView: self.childVCContainorView)
            
            
        } else if selectedType == .details {
            
            if detailInvoiceDetailObject == nil {
                detailInvoiceDetailObject = MISDNInvoiceDetailVC.instantiateViewControllerFromStoryboard()
                detailInvoiceDetailObject?.selectedUserFullName = selectedUserFullName
                detailInvoiceDetailObject?.selectedUserMSISDN = selectedUserMSISDN
                
            }
            self.removeChildViewController(childController: InvoiceDetailVC())
            
            self.addChildViewController(childController: detailInvoiceDetailObject ?? MISDNInvoiceDetailVC(), onView: self.childVCContainorView)
            
            
        }
    }
    
    //MARK: - IBAction
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        self.showShareAlert {
            
            let sharingItems:[AnyObject?] = ["test" as AnyObject]
            
            let activityViewController = UIActivityViewController(activityItems: sharingItems.compactMap({$0}), applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func summaryButtonPressed (_ sender: MBButton){
        detailsButton.setButtonLayoutType(.whiteButton)
        summaryButton.setButtonLayoutType(.grayButton)
        
        redirectToVC(ofType: .summary)
    }
    
    @IBAction func detailsButtonPressed(_ sender: MBButton){
        summaryButton.setButtonLayoutType(.whiteButton)
        detailsButton.setButtonLayoutType(.grayButton)
        
        redirectToVC(ofType: .details)
    }
}

