//
//  MISDNInvoiceTitleHeaderView.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/3/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class MISDNInvoiceTitleHeaderView: UITableViewHeaderFooterView {
    //MARK: -  IBOutlets
    @IBOutlet var misdnTitleLabel:MBMarqueeLabel!
    @IBOutlet var limitTitleLabel: MBMarqueeLabel!
    @IBOutlet var invoiceAmountTitleLabel: MBMarqueeLabel!
   
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        loadViewLayout()
    }
    
    
    
    //MARK: - Functions

    func loadViewLayout()  {
        misdnTitleLabel.text = Localized("Title_MISDN")
        limitTitleLabel.text = Localized("Title_Limit")
        invoiceAmountTitleLabel.text = Localized("Title_InvoiceAmountVAT")
        
        
        misdnTitleLabel.textColor = .mbTextLighGray
        invoiceAmountTitleLabel.textColor = .mbTextLighGray
        limitTitleLabel.textColor = .mbTextLighGray
        
    }

}
