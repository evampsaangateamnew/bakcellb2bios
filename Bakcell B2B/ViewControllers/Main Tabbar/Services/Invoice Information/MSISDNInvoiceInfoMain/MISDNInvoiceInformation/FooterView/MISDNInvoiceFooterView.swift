//
//  MISDNInvoiceFooterView.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/3/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class MISDNInvoiceFooterView: UITableViewHeaderFooterView {
    
    //MARK: -  IBOutlets
    @IBOutlet var totalAmountTitleLabel:MBMarqueeLabel!
    @IBOutlet var totalAmountValue :MBLabel!
    
    @IBOutlet var topSeparatorView :MBSeperatorView!
    @IBOutlet var bottomSeparatorView :MBSeperatorView!
    
    
    //MARK:- Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        loadViewLayout()
    }
    
    func loadViewLayout()  {
        totalAmountTitleLabel.text = Localized("Title_TotalAmount")
    }
    
    func setInvoiceTotal(TotalInvoiceTitle title: String, TotalInvoiceValue value: String) {
        totalAmountTitleLabel.text =  title
        totalAmountValue.attributedText =  MBUtilities.createAttributedText(value, textColor: .black, withManatSign: true)
    }
}
