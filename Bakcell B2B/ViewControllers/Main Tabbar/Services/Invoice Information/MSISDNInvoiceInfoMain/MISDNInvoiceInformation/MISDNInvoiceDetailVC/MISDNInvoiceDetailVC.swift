//
//  MISDNInvoiceDetailVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class MISDNInvoiceDetailVC: BaseVC {
    
    //MARK: - Properties
    var selectedUserFullName :String = ""
    var selectedUserMSISDN :String = ""
    
    var detailInvoiceList : [BillSubItemDetail]  = []
    let monthsTitles : [String] = MBUtilities.getMonthNames(totalNumberOfMonths: 3)
   
    //MARK: -  IBOutlets
    @IBOutlet var misdnNumber: MBLabel!
    @IBOutlet var misdnUserName: MBLabel!
    
    @IBOutlet var monthSelectionDropDown : UIDropDown!
    
    @IBOutlet var misdnInvoiceDetailTableView : UITableView!
   
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        misdnInvoiceDetailTableView.estimatedRowHeight = 60
        misdnInvoiceDetailTableView.estimatedSectionHeaderHeight = 60
        
        //first API call
        self.getMSISDNInvoiceDetail(monthIndex: "-1")
        
        //init dropdown
        setDropDown()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
        setSelectMISDNInfo()
    }
    
    //MARK: - Functions
    func loadViewLayout(){
        reloadTableViewData()
    }
    
    func setSelectMISDNInfo(){
        misdnNumber.text = selectedUserMSISDN
        misdnUserName.text = selectedUserFullName
    }
    
    //Initialze / Setup dropdown
    func setDropDown()  {
        
        //Months Drop Down
        monthSelectionDropDown.dataSource = monthsTitles
        monthSelectionDropDown.placeholder = monthsTitles[0]
        
        monthSelectionDropDown.didSelectOption { (index, option) in
            
            switch(index) {
            case 0:
                self.getMSISDNInvoiceDetail(monthIndex: "-1")
                break
            case 1:
                self.getMSISDNInvoiceDetail(monthIndex: "-2")
                break
                
            case 2:
                self.getMSISDNInvoiceDetail(monthIndex: "-3")
                break
                
            default:
                self.getMSISDNInvoiceDetail(monthIndex: "-1")
                break
            }
            
        }
    }
    
    func reloadTableViewData() {
        
        if ((detailInvoiceList.count) <= 0 ){
            self.misdnInvoiceDetailTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.misdnInvoiceDetailTableView.hideDescriptionView()
        }
        
        misdnInvoiceDetailTableView.reloadData()
    }
    
    //MARK: - API Calls
    /**
     Call 'getMSISDNInvoiceDetailDetail' API .
     - returns: Void
     */
    func getMSISDNInvoiceDetail(monthIndex : String) {
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getMSISDNInvoiceDetail(monthIndex: monthIndex, userMSISDN: selectedUserMSISDN ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let msisdnInvoiceInformation = Mapper<MSISDNInvoiceDetail>().map(JSONObject: resultData) {
                        
                        self.detailInvoiceList = msisdnInvoiceInformation.companyDetail?.billSubItemDetail ?? []
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewData()
        })
    }
}
//MARK: - TableView Delegats
extension MISDNInvoiceDetailVC: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (detailInvoiceList.count > 0) {
            return 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let detailHeaderView : DetailInvoiceHeaderView = tableView.dequeueReusableHeaderFooterView() {
            return detailHeaderView
        }
        
        return UITableViewHeaderFooterView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return detailInvoiceList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let detailCell :  DetailsInvoiceCell = tableView.dequeueReusableCell( ){
            
            detailCell.setItemDetail(itemDetail: detailInvoiceList[indexPath.row].details)
            return detailCell
        }
        
        return UITableViewCell()
    }
}
