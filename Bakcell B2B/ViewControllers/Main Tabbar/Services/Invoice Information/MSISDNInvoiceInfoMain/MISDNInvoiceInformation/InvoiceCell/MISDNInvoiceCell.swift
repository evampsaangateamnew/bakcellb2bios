//
//  MISDNInvoiceCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/3/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class MISDNInvoiceCell: UITableViewCell {

    //MARK: -  IBOutlets
    @IBOutlet var misdnLabel:MBMarqueeLabel!
    @IBOutlet var limitLabel: MBMarqueeLabel!
    @IBOutlet var invoiceAmountLabel: MBMarqueeLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: -  Functions
    func setInvoice(Invoice invoice:MISDNInvoice? )  {
        misdnLabel.text = invoice?.misdn ?? ""
        limitLabel.text = invoice?.limit  ?? ""
        invoiceAmountLabel.text = invoice?.invoiceAmount  ?? ""
    }
}
