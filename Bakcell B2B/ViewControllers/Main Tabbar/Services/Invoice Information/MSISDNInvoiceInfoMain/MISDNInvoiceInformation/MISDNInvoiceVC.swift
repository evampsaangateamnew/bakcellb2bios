//
//  MISDNInvoiceVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/3/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class MISDNInvoiceVC: BaseVC {

    //MARK: - Properties
    var selectedUserFullName :String = ""
    var selectedUserMSISDN :String = ""
    
    var msisdnInvoiceList : MISDNInvoiceModel? /*= [MISDNInvoice(MISDN: "111 111 11", Limit: "12", InvoiceAmount: "15.5", UserName: "Test Name"),
                                             MISDNInvoice(MISDN: "111 111 11", Limit: "12", InvoiceAmount: "15.5", UserName: "Test Name"),
                                             MISDNInvoice(MISDN: "111 111 11", Limit: "12", InvoiceAmount: "15.5", UserName: "Test Name"),
                                             MISDNInvoice(MISDN: "111 111 11", Limit: "12", InvoiceAmount: "15.5", UserName: "Test Name")]*/
    let monthsTitles : [String] = MBUtilities.getMonthNames(totalNumberOfMonths: 3)
    
    //MARK: - IBOutlets
    
//    @IBOutlet var shareButton: UIButton!
    
    @IBOutlet var monthSelectionDropDown : UIDropDown!
    
    @IBOutlet var misdnInvoiceTableView : UITableView!
    
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        misdnInvoiceTableView.estimatedRowHeight = 100
        misdnInvoiceTableView.estimatedSectionHeaderHeight = 60
        misdnInvoiceTableView.estimatedSectionFooterHeight = 60
        
        //first API call
        self.getMSISDNInvoice(monthIndex: "-1")
        
        //init dropdown
        setDropDown()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    
    
    //MARK: - Functions
    func loadViewLayout()  {
    }
    
    //Initialze / Setup dropdown
    //MARK: - Functions
    func setDropDown()  {
        
        //Months Drop Down
        monthSelectionDropDown.dataSource = monthsTitles
        monthSelectionDropDown.placeholder = monthsTitles[0]
        
        monthSelectionDropDown.didSelectOption { (index, option) in
            
            switch(index) {
            case 0:
                self.getMSISDNInvoice(monthIndex: "-1")
                break
            case 1:
                self.getMSISDNInvoice(monthIndex: "-2")
                break
                
            case 2:
                self.getMSISDNInvoice(monthIndex: "-3")
                break
                
            default:
                self.getMSISDNInvoice(monthIndex: "-1")
                break
            }
        }
    }
    
    func reloadTableViewData() {
        
        if (msisdnInvoiceList?.msisdnSummary?.count ?? 0 ) <= 0 {
//            shareButton.isHidden = true
            self.misdnInvoiceTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
//            shareButton.isHidden = false
            self.misdnInvoiceTableView.hideDescriptionView()
        }
        
        misdnInvoiceTableView.reloadData()
    }
    
    //MARK: - IBAction
  /*  @IBAction func shareButtonPressed(_ sender: UIButton) {
        self.showShareAlert {
            
            let sharingItems:[AnyObject?] = ["test" as AnyObject]
            
            let activityViewController = UIActivityViewController(activityItems: sharingItems.compactMap({$0}), applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
   */
    
    //MARK: - API Calls
    /**
     Call 'getMSISDNInvoice' API .
     - returns: Void
     */
    func getMSISDNInvoice(monthIndex : String) {
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getMSISDNInvoice(monthIndex: monthIndex, userMSISDN: selectedUserMSISDN,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let msisdnInvoiceInformation = Mapper<MISDNInvoiceModel>().map(JSONObject: resultData) {
                        
                        self.msisdnInvoiceList = msisdnInvoiceInformation
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewData()
        })
    }
}
extension MISDNInvoiceVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if (msisdnInvoiceList?.msisdnSummary?.count ?? 0) > 0 {
           return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let titleHeaderView : MISDNInvoiceTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
            return titleHeaderView
        }
        
        return MBAccordionTableViewHeaderView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let footerView : MISDNInvoiceFooterView = tableView.dequeueReusableHeaderFooterView() {
            footerView.setInvoiceTotal(TotalInvoiceTitle: self.msisdnInvoiceList?.totalLabel ?? "", TotalInvoiceValue: self.msisdnInvoiceList?.totalValue ?? "0")
            return footerView
        }
        
        return MBAccordionTableViewHeaderView()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (msisdnInvoiceList?.msisdnSummary?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let misdnInvoiceCell : MISDNInvoiceCell = tableView.dequeueReusableCell(){
            misdnInvoiceCell.setInvoice(Invoice: msisdnInvoiceList?.msisdnSummary?[indexPath.row])
            return misdnInvoiceCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let misdnInvoiceDetailVC :MISDNInvoiceDetailVC = MISDNInvoiceDetailVC.instantiateViewControllerFromStoryboard() {
//            misdnInvoiceDetailVC.selectedMISDNNumber = msisdnInvoiceList?[indexPath.row]
//            self.navigationController?.pushViewController(misdnInvoiceDetailVC, animated: true)
//        }
    }
    
}
