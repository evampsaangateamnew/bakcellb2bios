//
//  TarrifsVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/22/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class TarrifsVC: BaseVC {

    var tarrifsList : [Items] = []
        /*[
        Category(Name: Localized("Title_Tarrif"), ImageName: "tariffs", Idenitifier: "tarrifs"),
        Category(Name: Localized("Title_CUG"), ImageName: "CUG", Idenitifier: "cug")
    ]*/
    
    //MARKL: - IBOutlet
    @IBOutlet var tarrifsTitltLabel : MBLabel!
    @IBOutlet var tarrifsTableView : UITableView!
    
    //MARK: - View Controllers Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tarrifsTableView.estimatedRowHeight =  100
        tarrifsTableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop(false)
        
        loadViewLayout()
        
        MBPICUserSession.shared.appMenu?.menuHorizontal?.forEach({ (aHorizontalMenu) in
            if aHorizontalMenu.identifier == "tarrifs" {
                tarrifsList = aHorizontalMenu.items ?? []
            }
        })
        tarrifsList.sort{ $0.sortOrder.toInt < $1.sortOrder.toInt }
        self.reloadTableViewData()
        
    }

    
    //MARK: -  Functions
    func loadViewLayout() {
        tarrifsTitltLabel.text = Localized("Title_Tarrif")
    }
    
    func reloadTableViewData() {
        
        if (tarrifsList.count <= 0 ){
            self.tarrifsTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.tarrifsTableView.hideDescriptionView()
        }
        
        tarrifsTableView.reloadData()
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier: String){
        switch identifier {
        case "tarrifs":
            if let tariffsVC :TariffsMainVC = TariffsMainVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(tariffsVC, animated: true)
            }
            
        default:
            break
        }
    }

}

//MARK: - TableView Delegates
extension TarrifsVC : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tarrifsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let myCell: CategoriesTableViewCell = tableView.dequeueReusableCell() {
        myCell.setItemInfo(aItem: tarrifsList[indexPath.row])
        return myCell
        } else {
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Redirect user to selected screen
        redirectUserToSelectedController(Identifier: tarrifsList[indexPath.row].identifier)
    }
    
    
}
