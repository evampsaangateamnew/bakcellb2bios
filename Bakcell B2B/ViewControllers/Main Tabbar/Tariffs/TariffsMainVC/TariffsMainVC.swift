//
//  TariffsMainVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 7/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper
import UPCarouselFlowLayout


enum MBOfferType: String {
    case Price                      = "Price"
    case Rounding                   = "Rounding"
    case TextWithTitle              = "TextWithTitle"
    case TextWithOutTitle           = "TextWithOutTitle"
    case TextWithPoints             = "TextWithPoints"
    case Date                       = "Date"
    case Time                       = "Time"
    case RoamingDetails             = "RoamingDetails"
    case FreeResourceValidity       = "FreeResourceValidity"
    case TitleSubTitleValueAndDesc  = "TitleSubTitleValueAndDesc"
    case OfferGroup                 = "OfferGroup"
    case TypeTitle                  = "Type"
    case OfferValidity              = "OfferValidity"
    case OfferActive                = "OfferActive"
    case AttributeList              = "attributeList"
    case Usage                      = "Usage"
    case TopDescription             = "TopDescription"
    case Call                       = "Call"
    case SMS                        = "SMS"
    case Internet                   = "Internet"
    case MMS                        = "MMS"
    case Destination                = "Destination"
    case InternationalOnPeak        = "InternationalOnPeak"
    case InternationalOffPeak       = "InternationalOffPeak"
    case Advantages                 = "Advantages"
    case Classification             = "Classification"
}


class TariffsMainVC: BaseVC {
    
    //MARK:- Properties
    var tariffResponse : TariffsData?
    
    var redirectedFromNotification : Bool = false
    var offeringIdFromNotification : String = ""
    var redirectedFromHome : Bool = false
    
    // USED to toggle
    var selectedSectionViewType : MBSectionType = MBSectionType.UnSpecified
    
    //MARK:- IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK: - ViewController Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width * 0.79, height:  UIScreen.main.bounds.height * 0.80)
        layout.scrollDirection = .horizontal
        layout.sideItemScale = 0.8
        layout.sideItemAlpha = 0.8
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 12)
        collectionView.collectionViewLayout = layout
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.loadSelectedType()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        // setting localized strings
        loadViewLayout()
        
        /* if redirected form  Notification or Home then reload data */
        if redirectedFromNotification || redirectedFromHome {
            self.loadSelectedType()
        }
        
    }
    
    // MARK: - IBACTIONS
    @IBAction func subscribeButtonPressed(_ sender: UIButton) {
        self.redirectToUserSelectionVC(selectedTarrifIndex: sender.tag)
    }
    
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        
        titleLabel.text = Localized("Title_Tariffs")
    }
    
    
    func loadSelectedType() {
        
        // If data found nil
        if tariffResponse == nil {
            loadTariffDetails()
            return
            
        }
        
        if redirectedFromNotification || redirectedFromHome {
            
            let offerIndex = indexOfOfferingId(offeringId: offeringIdFromNotification, tariffData: self.tariffResponse)
            
            self.reloadCollectionViewDataAndScrollToItem(offerIndex)
            
        } else {
            self.reloadCollectionViewDataAndScrollToItem()
        }
        
    }
    
    
    func loadTariffDetails() {
        
        /*Loading initial data from UserDefaults*/
        
        if let tariffPrepaid :TariffsData = TariffsData.loadFromUserDefaults(key: "\(APIsType.tariffDetails.localizedAPIKey())\(MBPICUserSession.shared.picUserInfo?.picAllowedTariffs ?? "")") {
            
            self.tariffResponse = tariffPrepaid
            self.loadSelectedType()
            
            self.getTariffDetails(showIndicator: false)
            
        } else {
            self.getTariffDetails()
        }
        
    }
    
    
    /**
     Show details Popup for selected Tarrif.
     
     - parameter selectedTarrifIndex: Selected index of tarrif.
     - parameter selectedUserList: Selected users list from UsersViewController.
     
     - returns: void
     */
    func showPopUpWithTariffPrice(atIndex : Int, selectedUserList : [String : UsersData]) {
        var selectedOfferingId: String = ""
        var selectedOfferName: String = ""
        var selectedOfferPrice: String = "0"
        
        
        if let aTariffObject = tariffResponse?.tarrifs[atIndex] {
            
            if aTariffObject.tariffType == .cin {
                if let aCINObject = aTariffObject.valueObject as? Cin {
                    selectedOfferingId = aCINObject.header?.offeringId ?? ""
                    selectedOfferName = aCINObject.header?.name ?? ""
                }
                
            } else if aTariffObject.tariffType == .klass {
                if let aKlassObject = aTariffObject.valueObject as? Klass {
                    selectedOfferingId = aKlassObject.header?.offeringId ?? ""
                    selectedOfferName = aKlassObject.header?.name ?? ""
                }
            } else if aTariffObject.tariffType == .klassPostpaid {
                if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                    selectedOfferingId = aKlassPostpaidObject.header?.offeringId ?? ""
                    selectedOfferName = aKlassPostpaidObject.header?.name ?? ""
                    selectedOfferPrice = aKlassPostpaidObject.header?.priceValue ?? ""
                }
            } else if aTariffObject.tariffType == .individual {
                if let aIndividualObject = aTariffObject.valueObject as? Individual {
                    selectedOfferingId = aIndividualObject.header?.offeringId ?? ""
                    selectedOfferName = aIndividualObject.header?.name ?? ""
                    selectedOfferPrice = aIndividualObject.header?.priceValue ?? ""
                }
            } else if aTariffObject.tariffType == .corporate {
                if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                    selectedOfferingId = aCorporateObject.header?.offeringId ?? ""
                    selectedOfferName = aCorporateObject.header?.name ?? ""
                    selectedOfferPrice = aCorporateObject.header?.priceValue ?? ""
                }
            }
            
        }
        showConfirmationPOPUP(offeringId: selectedOfferingId, offerName: selectedOfferName, offerPrice: selectedOfferPrice, selectedUsersList: selectedUserList )
    }
    
    
    func showConfirmationPOPUP(offeringId: String?, offerName:String?, offerPrice:String?, selectedUsersList : [String : UsersData]) {
        
        let message = MBPICUserSession.shared.tarifPopUpStr?.createAttributedString(stringsListForAttributed: [], stringColor: .black)
        
        self.showConfirmationAlert(title: Localized("Title_Confirmation"), attributedMessage: message, okBtnTitle: Localized("BtnTitle_OK"), cancelBtnTitle: Localized("BtnTitle_NO"), alertType: .groupSelection, {
            
            self.changetTariff(offeringId: offeringId ?? "", selectedUsersList: selectedUsersList)
        })
    }
    
    /**
     Returns index of Offering id.
     
     - parameter offeringId: Selected index of tarrif.
     - parameter tariffData: Tarrifs detail.
     
     - returns: Integer value index of Offering id.
     */
    func indexOfOfferingId(offeringId : String?, tariffData : TariffsData?) -> Int {
        
        // check if found nil information
        if tariffData == nil {
            
            return 0
            
        } else {
            
            /* check if subscriberType is prepaid */
            let indexOfOffer = tariffData?.tarrifs.firstIndex(where: { (aTariffObject) -> Bool in
                
                var currentOfferingID:String?
                
                if aTariffObject.tariffType == .cin {
                    if let aCinObject = aTariffObject.valueObject as? Cin {
                        currentOfferingID = aCinObject.header?.offeringId
                    }
                    
                } else  if aTariffObject.tariffType == .klass {
                    if let aKlassObject = aTariffObject.valueObject as? Klass {
                        currentOfferingID = aKlassObject.header?.offeringId
                    }
                    
                } else  if aTariffObject.tariffType == .corporate {
                    if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                        currentOfferingID = aCorporateObject.header?.offeringId
                    }
                    
                } else  if aTariffObject.tariffType == .individual {
                    if let aIndividualObject = aTariffObject.valueObject as? Individual {
                        currentOfferingID = aIndividualObject.header?.offeringId
                    }
                    
                } else  if aTariffObject.tariffType == .klassPostpaid {
                    if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                        currentOfferingID = aKlassPostpaidObject.header?.offeringId
                    }
                    
                }
                
                return offeringId?.isEqual( currentOfferingID, ignorCase: true) ?? false
            })
            
            return indexOfOffer ?? 0
        }
    }
    

    func redirectToUserSelectionVC(selectedTarrifIndex : Int)  {
        if let userSelectionVC :MultipleUserSelectionMainVC = MultipleUserSelectionMainVC.instantiateViewControllerFromStoryboard() {
            
            userSelectionVC.setNextButtonCompletionBloch { (selectedUsers) in
                
                /* POP UserSelection ViewController */
                self.navigationController?.popViewController(animated: true)
                
                /* Redirect user to confirmation screen */
                self.showPopUpWithTariffPrice(atIndex: selectedTarrifIndex, selectedUserList: selectedUsers)
            }
            
            self.navigationController?.pushViewController(userSelectionVC, animated: true)
        }
    }
    

    
    /**
     USED to toggle section.
     
     - returns: Void.
     */
    
    func openSelectedSection() {
        
        let visbleIndexPath = collectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = collectionView.cellForItem(at: aIndexPath) as? TariffCell {
                
                var openSectionIndex : Int? = 0
                
                if let aTariffObject = tariffResponse?.tarrifs[cell.tableView.tag] {
                    let type = typeOfDataInSection(aTariffObject)
                    openSectionIndex = type.firstIndex(of:aTariffObject.openedViewSection)
                    
                    /*if selectedSectionViewType == MBSectionType.Header &&
                     type.contains(MBSectionType.Header) {
                     
                     openSectionIndex = 0
                     
                     } else if selectedSectionViewType == MBSectionType.PackagePrice && type.contains(MBSectionType.PackagePrice) {
                     
                     openSectionIndex = type.index(of:MBSectionType.PackagePrice)
                     
                     } else if selectedSectionViewType == MBSectionType.PaygPrice &&
                     type.contains(MBSectionType.PaygPrice) {
                     
                     openSectionIndex = type.index(of:MBSectionType.PaygPrice)
                     
                     } else if selectedSectionViewType == MBSectionType.Price &&
                     type.contains(MBSectionType.Price) {
                     
                     openSectionIndex = type.index(of:MBSectionType.Price)
                     
                     } else if selectedSectionViewType == MBSectionType.Detail &&
                     type.contains(MBSectionType.Detail) {
                     
                     openSectionIndex = type.index(of:MBSectionType.Detail)
                     
                     } else if selectedSectionViewType == MBSectionType.Description &&
                     type.contains(MBSectionType.Description) {
                     
                     openSectionIndex = type.index(of:MBSectionType.Description)
                     
                     }*/
                }
                
                cell.tableView.closeAllSectionsExcept(openSectionIndex ?? 0, shouldCallDelegate: false)
                
                openSectionAt(index: openSectionIndex ?? 0, tableView: cell.tableView)
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : MBAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            
            // tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: .top, animated: true)
            
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
    
    
    
    func reloadCollectionViewDataAndScrollToItem(_ itemIndex:Int = 0) {
        
        self.reloadCollectionViewData()
        
        if (self.tariffResponse?.tarrifs.count ?? 0) <= 0 {
            self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            self.view.hideDescriptionView()
            
            self.collectionView.performBatchUpdates(nil, completion: {
                (isCompleted) in
                
                if isCompleted == true {
                    
                    let selectTabIndexPath = IndexPath(item:itemIndex , section:0)
                    
                    self.collectionView.scrollToItem(at: selectTabIndexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
                    
                    
                    /* RESET CHECK when user is redirected to selected offer */
                    self.redirectedFromNotification = false
                    self.redirectedFromHome = false
                    
                }
            })
        }
    }
    
    func reloadCollectionViewData() {
        
        if (self.tariffResponse?.tarrifs.count ?? 0) <= 0 {
            self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            self.view.hideDescriptionView()
        }
        collectionView.reloadData()
    }
    
    
    //MARK: - API Calls
    
    /// Call 'getTariffDetails' API .
    ///
    /// - returns: Void
   
    
    func getTariffDetails(showIndicator :Bool = true) {
        
        if (showIndicator == true) {
            self.showActivityIndicator()
        }
        
        _ = MBAPIClient.sharedClient.getTariffDetails({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if (showIndicator == true) {
                self.hideActivityIndicator()
            }
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                self.reloadCollectionViewData()
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let tariffResponseData = Mapper<TariffsData>().map(JSONObject: resultData) {
                        
                        self.tariffResponse = tariffResponseData
                        self.loadSelectedType()
                        
                        /*Save data into user defaults*/
                        tariffResponseData.saveInUserDefaults(key: "\(APIsType.tariffDetails.localizedAPIKey())\(MBPICUserSession.shared.picUserInfo?.picAllowedTariffs ?? "")")
                        
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                    self.reloadCollectionViewData()
                }
            }
        })
    }
   
    
    /// Call 'changeTariff' API .
    ///
    /// - parameter offeringId:     offerId of selected offer
    /// - parameter selectedUsersList:     selected user for tariff migration.
    ///
    /// - returns: Void
    func changetTariff(offeringId: String, selectedUsersList : [String : UsersData]) {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.changeTariff(offeringId: offeringId, selectedUsersList: selectedUsersList, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let changeTariffResponse = Mapper<MessageMSGResponse>().map(JSONObject: resultData){
                        
                        // Show Error alert
                        self.showSuccessAlertWithMessage(message: changeTariffResponse.responseMsg)
                    } else {
                        // Show Error alert
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                } else {
                    // Show Error alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}



//MARK: - COLLECTION VIEW METHODS
extension TariffsMainVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tariffResponse?.tarrifs.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TariffCell", for: indexPath) as? TariffCell {
            cell.tableView.delegate = self
            cell.tableView.dataSource = self
            cell.tableView.allowMultipleSectionsOpen = false
            cell.tableView.keepOneSectionOpen = true;
            cell.tableView.tag = indexPath.item
            
            cell.tableView.reloadData()
            //cell.tableView.layoutIfNeeded()
            
            return cell
            
        } else {
            return UICollectionViewCell()
        }
        
        
    }
}
// MARK: - <UITableViewDataSource> / <UITableViewDelegate>

extension TariffsMainVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let aTariffObject = tariffResponse?.tarrifs[tableView.tag] {
            return typeOfDataInSection(aTariffObject).count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 35
        } else {
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let aTariffObject = tariffResponse?.tarrifs[tableView.tag] {
            
            let types = typeOfDataInSection(aTariffObject)
            
            switch types[section] {
                
            case .Header:
                if let headerView:IndividualTitleView = tableView.dequeueReusableHeaderFooterView() {
                    
                    var aOfferName = ""
                    var aPriceLabel = ""
                    var aPriceValue = ""
                    
                    if aTariffObject.tariffType == .cin {
                        if let aCinObject = aTariffObject.valueObject as? Cin {
                            aOfferName = aCinObject.header?.name ?? ""
                        }
                        
                    } else  if aTariffObject.tariffType == .klass {
                        if let aKlassObject = aTariffObject.valueObject as? Klass {
                            aOfferName = aKlassObject.header?.name ?? ""
                            aPriceLabel = aKlassObject.header?.priceLabel ?? ""
                            aPriceValue = aKlassObject.header?.priceValue ?? ""
                        }
                        
                    } else  if aTariffObject.tariffType == .corporate {
                        if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                            aOfferName = aCorporateObject.header?.name ?? ""
                            aPriceLabel = aCorporateObject.header?.priceLabel ?? ""
                            aPriceValue = aCorporateObject.header?.priceValue ?? ""
                        }
                        
                    } else  if aTariffObject.tariffType == .individual {
                        if let aIndividualObject = aTariffObject.valueObject as? Individual {
                            aOfferName = aIndividualObject.header?.name ?? ""
                            aPriceLabel = aIndividualObject.header?.priceLabel ?? ""
                            aPriceValue = aIndividualObject.header?.priceValue ?? ""
                        }
                        
                    } else  if aTariffObject.tariffType == .klassPostpaid {
                        if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                            aOfferName = aKlassPostpaidObject.header?.name ?? ""
                            aPriceLabel = aKlassPostpaidObject.header?.priceLabel ?? ""
                            aPriceValue = aKlassPostpaidObject.header?.priceValue ?? ""
                        }
                        
                    }
                    
                    headerView.setViewWith(titleText: aOfferName, priceLabel: aPriceLabel, priceValue: aPriceValue)
                    
                    return headerView
                }
                
                break
                
                
            case .PackagePrice:
                
                if let headerView:ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    
                    if aTariffObject.tariffType == .klassPostpaid {
                        if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                            
                            headerView.setViewWithTitle(title: aKlassPostpaidObject.packagePrice?.packagePriceLabel, isSectionSelected: (selectedSectionViewType ==  .PackagePrice || aTariffObject.openedViewSection == .PackagePrice) ? true : false, headerType: MBSectionType.PackagePrice)
                        }
                    } else if aTariffObject.tariffType == .klass {
                        if let aKlassObject = aTariffObject.valueObject as? Klass {
                            
                            headerView.setViewWithTitle(title: aKlassObject.packagePrice?.packagePriceLabel, isSectionSelected: (selectedSectionViewType ==  .PackagePrice || aTariffObject.openedViewSection == .PackagePrice) ? true : false, headerType: MBSectionType.PackagePrice)
                        }
                    }
                    return headerView
                }
                break
                
            case .PaygPrice:
                
                if let headerView:ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    if aTariffObject.tariffType == .klass {
                        if let aKlassObject = aTariffObject.valueObject as? Klass {
                            
                            headerView.setViewWithTitle(title: aKlassObject.packagePrice?.packagePriceLabel, isSectionSelected: (selectedSectionViewType ==  .PaygPrice || aTariffObject.openedViewSection == .PaygPrice) ? true : false, headerType: MBSectionType.PaygPrice)
                            return headerView
                        }
                    }
                }
                break
                
            case .Price:
                
                if let headerView:ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    
                    if aTariffObject.tariffType == .corporate {
                        if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                            
                            headerView.setViewWithTitle(title: aCorporateObject.price?.priceLabel, isSectionSelected: (selectedSectionViewType ==  .Price || aTariffObject.openedViewSection == .Price) ? true : false, headerType: MBSectionType.Price)
                            return headerView
                        }
                    }
                }
                break
                
            case .Detail:
                if let headerView:ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    
                    var titleString :String = Localized("Title_Details")
                    
                    if aTariffObject.tariffType == .corporate {
                        if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                            
                            titleString = aCorporateObject.details?.detailLabel ?? Localized("Title_Details")
                        }
                    } else if aTariffObject.tariffType == .klass {
                        if let aKlassObject = aTariffObject.valueObject as? Klass {
                            
                            titleString = aKlassObject.details?.detailLabel ?? Localized("Title_Details")
                        }
                    }
                    
                    headerView.setViewWithTitle(title: titleString, isSectionSelected: (selectedSectionViewType ==  .Detail || aTariffObject.openedViewSection == .Detail) ? true : false, headerType: MBSectionType.Detail)
                    return headerView
                    
                }
                break
                
            case .Description:
                if let headerView:ExpandableTitleHeaderView = tableView.dequeueReusableHeaderFooterView() {
                    
                    var titleString :String = Localized("Title_Description")
                    
                    if aTariffObject.tariffType == .corporate {
                        if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                            
                            titleString = aCorporateObject.description?.descLabel ?? Localized("Title_Description")
                        }
                    }
                    
                    headerView.setViewWithTitle(title: titleString, isSectionSelected: (selectedSectionViewType ==  .Description || aTariffObject.openedViewSection == .Description) ? true : false, headerType: MBSectionType.Description)
                    return headerView
                    
                }
                break
                
            case .Subscription:
                
                if let headerView:SubscribeButtonView = tableView.dequeueReusableHeaderFooterView() {
                    
                    var isSubscribedText :String? = ""
                    
                    if aTariffObject.tariffType == .cin {
                        if let aCINObject = aTariffObject.valueObject as? Cin {
                            
                            isSubscribedText = aCINObject.header?.subscribable
                        }
                        
                    } else if aTariffObject.tariffType == .klass {
                        if let aKlassObject = aTariffObject.valueObject as? Klass {
                            
                            isSubscribedText = aKlassObject.header?.subscribable
                        }
                        
                    } else if aTariffObject.tariffType == .klassPostpaid {
                        if let aklassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                            
                            isSubscribedText = aklassPostpaidObject.header?.subscribable
                        }
                        
                    } else if aTariffObject.tariffType == .individual {
                        if let aIndividualObject = aTariffObject.valueObject as? Individual {
                            
                            isSubscribedText = aIndividualObject.header?.subscribable
                        }
                        
                    } else if aTariffObject.tariffType == .corporate {
                        if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                            
                            isSubscribedText = aCorporateObject.header?.subscribable
                        }
                    }
                    headerView.subscribedButton.addTarget(self, action: #selector(subscribeButtonPressed(_:)), for: UIControl.Event.touchUpInside)
                    headerView.setViewForSubscribeButtonForTariff(isSubscribed: isSubscribedText, tag: tableView.tag)
                    return headerView
                    
                }
                break
                
            default:
                break
            }
            
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let aTariffObject = tariffResponse?.tarrifs[tableView.tag] {
            
            let sectionType = typeOfDataInSection(aTariffObject)
            
            switch sectionType[section] {
                
            case MBSectionType.Header:
                if aTariffObject.tariffType == .cin {
                    if let aCINObject = aTariffObject.valueObject as? Cin {
                        return typeOfDataInCINHeaderSection(aCINObject).count
                    }
                } else  if aTariffObject.tariffType == .klass {
                    if let aKlassObject = aTariffObject.valueObject as? Klass {
                        return aKlassObject.header?.attributes?.count ?? 0
                    }
                } else  if aTariffObject.tariffType == .klassPostpaid {
                    if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                        return aKlassPostpaidObject.header?.attributes?.count ?? 0
                    }
                } else  if aTariffObject.tariffType == .individual {
                    if let aIndividualObject = aTariffObject.valueObject as? Individual {
                        return typeOfDataInIndividualHeaderSection(aIndividualObject).count
                    }
                } else  if aTariffObject.tariffType == .corporate {
                    if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                        return aCorporateObject.header?.attributes?.count ?? 0
                    }
                }
                
                return 0
                
            case MBSectionType.PackagePrice:
                if aTariffObject.tariffType == .klass {
                    if let aKlassObject = aTariffObject.valueObject as? Klass {
                        return typeOfDataInKlassPackagePrice(aKlassObject.packagePrice).count
                    }
                } else if aTariffObject.tariffType == .klassPostpaid {
                    if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                        return typeOfDataInKlassPostpaidPackagePrice(aKlassPostpaidObject.packagePrice).count
                    }
                }
                return 0
                
            case MBSectionType.PaygPrice:
                if aTariffObject.tariffType == .klass {
                    if let aKlassObject = aTariffObject.valueObject as? Klass {
                        return typeOfDataInPaygPrice(aKlassObject.paygPrice).count
                    }
                }
                return 0
                
            case MBSectionType.Price:
                if aTariffObject.tariffType == .corporate {
                    if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                        return typeOfDataInCorporatePrice(aCorporateObject.price).count
                    }
                }
                return 0
                
            case MBSectionType.Detail:
                if aTariffObject.tariffType == .cin {
                    if let aCINObject = aTariffObject.valueObject as? Cin {
                        return typeOfDataInDetailSection(aCINObject.details).count
                    }
                } else  if aTariffObject.tariffType == .klass {
                    if let aKlassObject = aTariffObject.valueObject as? Klass {
                        return typeOfDataInDetailSection(aKlassObject.details).count
                    }
                } else  if aTariffObject.tariffType == .klassPostpaid {
                    if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                        return typeOfDataInDetailSection(aKlassPostpaidObject.details).count
                    }
                } else  if aTariffObject.tariffType == .individual {
                    if let aIndividualObject = aTariffObject.valueObject as? Individual {
                        return typeOfDataInDetailSection(aIndividualObject.details).count
                    }
                } else  if aTariffObject.tariffType == .corporate {
                    if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                        return typeOfDataInDetailSection(aCorporateObject.details).count
                    }
                }
                
                return 0
                
            case MBSectionType.Description:
                if aTariffObject.tariffType == .cin {
                    if let aCINObject = aTariffObject.valueObject as? Cin {
                        return typeOfDataInDescriptionSection(aCINObject.description).count
                    }
                } else  if aTariffObject.tariffType == .individual {
                    if let aIndividualObject = aTariffObject.valueObject as? Individual {
                        return typeOfDataInDescriptionSection(aIndividualObject.description).count
                    }
                } else  if aTariffObject.tariffType == .corporate {
                    if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                        return typeOfDataInDescriptionSection(aCorporateObject.description).count
                    }
                }
                
                return 0
                
            case MBSectionType.Subscription:
                return 0
                
            default:
                return 0
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let aTariffObject = tariffResponse?.tarrifs[tableView.tag] {
            let sectionDataType = typeOfDataInSection(aTariffObject)
            
            switch(sectionDataType[indexPath.section]) {
                
            case .Header:
                
                if aTariffObject.tariffType == .cin {
                    if let aCINObject = aTariffObject.valueObject as? Cin {
                        
                        switch(typeOfDataInCINHeaderSection(aCINObject)[indexPath.row]){
                        case .TopDescription:
                            
                            if let cell:BounsDescriptionCell = tableView.dequeueReusableCell() {
                                cell.setTopDescriptionLayoutValues(bonusTitle: aCINObject.header?.bonusTitle, bonusIconName: aCINObject.header?.bonusIconName, bonusDescription: aCINObject.header?.bonusDescription)
                                return cell
                            }
                            
                        case .Call:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aCINObject.header?.call, cellType:.Call)
                                return cell
                            }
                            
                        case .SMS:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aCINObject.header?.sms, cellType:.SMS)
                                return cell
                            }
                            
                        case .Internet:
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setInternetLayoutValues(internetHeader: aCINObject.header?.internet)
                                return cell
                            }
                            
                        default:
                            break
                        }
                    }
                } else  if aTariffObject.tariffType == .klass {
                    if let aKlassObject = aTariffObject.valueObject as? Klass {
                        if let cell : SingleAttributeCell = tableView.dequeueReusableCell() {
                            cell.setAttributeHeader(aKlassObject.header?.attributes?[indexPath.row])
                            
                            return cell
                        }
                    }
                } else  if aTariffObject.tariffType == .klassPostpaid {
                    if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                        
                        if let cell : SingleAttributeCell = tableView.dequeueReusableCell() {
                            cell.setAttributeHeader(aKlassPostpaidObject.header?.attributes?[indexPath.row])
                            
                            return cell
                        }
                    }
                } else  if aTariffObject.tariffType == .individual {
                    if let aIndividualObject = aTariffObject.valueObject as? Individual {
                        
                        switch(typeOfDataInIndividualHeaderSection(aIndividualObject)[indexPath.row]){
                            
                        case .Call:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aIndividualObject.header?.call, cellType:.Call)
                                return cell
                            }
                            
                        case .SMS:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aIndividualObject.header?.sms, cellType:.SMS)
                                return cell
                            }
                            
                        case .Internet:
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setInternetLayoutValues(internetHeader: aIndividualObject.header?.internet)
                                return cell
                            }
                            
                        default:
                            break
                        }
                    }
                } else  if aTariffObject.tariffType == .corporate {
                    if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                        if let cell : SingleAttributeCell = tableView.dequeueReusableCell() {
                            cell.setAttributeHeader(aCorporateObject.header?.attributes?[indexPath.row])
                            
                            return cell
                        }
                    }
                }
                break
                
            case .PackagePrice:
                if aTariffObject.tariffType == .klass {
                    if let aKlassObject = aTariffObject.valueObject as? Klass {
                        
                        switch(typeOfDataInKlassPackagePrice(aKlassObject.packagePrice)[indexPath.row]) {
                            
                        case .Call:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aKlassObject.packagePrice?.call, cellType:.Call, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        case .SMS:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aKlassObject.packagePrice?.sms, cellType:.SMS, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        case .Internet:
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setInternetLayoutValues(internetHeader: aKlassObject.packagePrice?.internet, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        default:
                            break
                        }
                    }
                    
                } else if aTariffObject.tariffType == .klassPostpaid {
                    
                    if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                        
                        switch(typeOfDataInKlassPostpaidPackagePrice(aKlassPostpaidObject.packagePrice)[indexPath.row]) {
                            
                        case .Call:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aKlassPostpaidObject.packagePrice?.call, cellType:.Call, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        case .SMS:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aKlassPostpaidObject.packagePrice?.sms, cellType:.SMS, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        case .Internet:
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setInternetLayoutValues(internetHeader: aKlassPostpaidObject.packagePrice?.internet, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        default:
                            break
                        }
                    }
                }
                
                
                break
                
            case .PaygPrice:
                if aTariffObject.tariffType == .klass {
                    if let aKlassObject = aTariffObject.valueObject as? Klass {
                        
                        switch(typeOfDataInPaygPrice(aKlassObject.paygPrice)[indexPath.row]) {
                            
                        case .Call:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aKlassObject.paygPrice?.call, cellType:.Call, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        case .SMS:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aKlassObject.paygPrice?.sms, cellType:.SMS, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        case .Internet:
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setInternetLayoutValues(internetHeader: aKlassObject.paygPrice?.internet, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        default:
                            break
                        }
                    }
                }
                break
                
            case .Price:
                if aTariffObject.tariffType == .corporate {
                    if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                        
                        switch(typeOfDataInCorporatePrice(aCorporateObject.price)[indexPath.row]) {
                            
                        case .Call:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aCorporateObject.price?.call, cellType:.Call, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        case .SMS:
                            
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setCallAndSMSLayoutValues(callHeaderType: aCorporateObject.price?.sms, cellType:.SMS, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        case .Internet:
                            if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                                
                                cell.setInternetLayoutValues(internetHeader: aCorporateObject.price?.internet, setBackgroundColorWhite:true)
                                return cell
                            }
                            
                        default:
                            break
                        }
                    }
                }
                break
                
            case .Detail:
                
                var aDetailObject : DetailsWithPriceArray?
                
                if aTariffObject.tariffType == .cin {
                    if let aCINObject = aTariffObject.valueObject as? Cin {
                        
                        aDetailObject = aCINObject.details
                    }
                } else  if aTariffObject.tariffType == .klass {
                    if let aKlassObject = aTariffObject.valueObject as? Klass {
                        aDetailObject = aKlassObject.details
                    }
                } else  if aTariffObject.tariffType == .klassPostpaid {
                    if let aKlassPostpaidObject = aTariffObject.valueObject as? KlassPostpaid {
                        aDetailObject = aKlassPostpaidObject.details
                    }
                } else  if aTariffObject.tariffType == .individual {
                    if let aIndividualObject = aTariffObject.valueObject as? Individual {
                        aDetailObject = aIndividualObject.details
                    }
                } else  if aTariffObject.tariffType == .corporate {
                    if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                        aDetailObject = aCorporateObject.details
                    }
                }
                
                
                switch (typeOfDataInDetailSection(aDetailObject)[indexPath.row]) {
                    
                case .Price:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setPriceLayoutValues(price: aDetailObject?.price?[indexPath.row])
                        return cell
                    }
                    
                    break
                case .Rounding:
                    
                    if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                        cell.setRoundingLayoutValues(rounding: aDetailObject?.rounding)
                        return cell
                    }
                    break
                case .TextWithTitle:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithTitleLayoutValues(textWithTitle: aDetailObject?.textWithTitle)
                        return cell
                    }
                    break
                case .TextWithOutTitle:
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aDetailObject?.textWithOutTitle)
                        return cell
                    }
                    break
                case .TextWithPoints:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setTextWithPointsLayoutValues(textWithPoint: aDetailObject?.textWithPoints)
                        return cell
                    }
                    break
                case .Date:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aDetailObject?.date)
                        return cell
                    }
                    break
                case .Time:
                    
                    if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                        cell.setDateAndTimeLayoutValues(dateAndTime: aDetailObject?.time)
                        return cell
                    }
                    break
                case .RoamingDetails:
                    
                    if let cell:RoamingDetailsCell = tableView.dequeueReusableCell() {
                        cell.setRoamingDetailsLayout(roamingDetails: aDetailObject?.roamingDetails)
                        return cell
                    }
                    break
                case .FreeResourceValidity:
                    
                    if let cell:FreeResourceValidityCell = tableView.dequeueReusableCell() {
                        cell.setFreeResourceValidityValues(freeResourceValidity: aDetailObject?.freeResourceValidity)
                        return cell
                    }
                    break
                case .TitleSubTitleValueAndDesc:
                    
                    if let cell:TitleSubTitleValueAndDescCell = tableView.dequeueReusableCell() {
                        cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aDetailObject?.titleSubTitleValueAndDesc)
                        return cell
                    }
                    
                    break
                default:
                    break
                }
                
            case .Description:
                if aTariffObject.tariffType == .cin {
                    if let aCINObject = aTariffObject.valueObject as? Cin {
                        
                        if let cell:TitleDescriptionCell = tableView.dequeueReusableCell() {
                            switch (typeOfDataInDescriptionSection(aCINObject.description)[indexPath.row]) {
                                
                            case .Advantages:
                                
                                cell.setTitleAndDescriptionWithOutIcon(title: aCINObject.description?.advantages?.title,
                                                                       description: aCINObject.description?.advantages?.description)
                                
                            case .Classification:
                                
                                cell.setTitleAndDescriptionWithOutIcon(title: aCINObject.description?.classification?.title,
                                                                       description: aCINObject.description?.classification?.description)
                            default:
                                break
                            }
                            
                            return cell
                        }
                    }
                } else  if aTariffObject.tariffType == .individual {
                    if let aIndividualObject = aTariffObject.valueObject as? Individual {
                        
                        if let cell:TitleDescriptionCell = tableView.dequeueReusableCell() {
                            switch (typeOfDataInDescriptionSection(aIndividualObject.description)[indexPath.row]) {
                                
                            case .Advantages:
                                
                                cell.setTitleAndDescriptionWithOutIcon(title: aIndividualObject.description?.advantages?.title,
                                                                       description: aIndividualObject.description?.advantages?.description)
                                
                            case .Classification:
                                
                                cell.setTitleAndDescriptionWithOutIcon(title: aIndividualObject.description?.classification?.title,
                                                                       description: aIndividualObject.description?.classification?.description)
                            default:
                                break
                            }
                            
                            return cell
                        }
                    }
                } else  if aTariffObject.tariffType == .corporate {
                    if let aCorporateObject = aTariffObject.valueObject as? Corporate {
                        
                        if let cell:TitleDescriptionCell = tableView.dequeueReusableCell() {
                            switch (typeOfDataInDescriptionSection(aCorporateObject.description)[indexPath.row]) {
                                
                            case .Advantages:
                                
                                cell.setTitleAndDescriptionWithOutIcon(title: aCorporateObject.description?.advantages?.title,
                                                                       description: aCorporateObject.description?.advantages?.description)
                                
                            case .Classification:
                                
                                cell.setTitleAndDescriptionWithOutIcon(title: aCorporateObject.description?.classification?.title,
                                                                       description: aCorporateObject.description?.classification?.description)
                            default:
                                break
                            }
                            
                            return cell
                        }
                        
                        
                    }
                }
                break
                
            default:
                return UITableViewCell()
            }
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - Helping functions
    
    func typeOfDataInSection(_ aTariff : KeyAny) -> [MBSectionType] {
        
        var dataTypes : [MBSectionType] = []
        
        switch aTariff.tariffType {
            
        case .klass:
            if let aKlassObject =  aTariff.valueObject as? Klass {
                // 1
                if aKlassObject.header != nil {
                    
                    dataTypes.append(MBSectionType.Header)
                }
                // 2
                if aKlassObject.packagePrice != nil  {
                    
                    dataTypes.append(MBSectionType.PackagePrice)
                }
                // 3
                if aKlassObject.paygPrice != nil {
                    
                    dataTypes.append(MBSectionType.PaygPrice)
                }
                // 4
                if aKlassObject.details != nil {
                    
                    dataTypes.append(MBSectionType.Detail)
                }
                
                dataTypes.append(MBSectionType.Subscription)
            }
            
            break
        case .cin:
            if let aCinObject =  aTariff.valueObject as? Cin {
                // 1
                if aCinObject.header != nil {
                    
                    dataTypes.append(MBSectionType.Header)
                }
                // 2
                if aCinObject.details != nil  {
                    
                    dataTypes.append(MBSectionType.Detail)
                }
                // 3
                if aCinObject.description != nil {
                    
                    dataTypes.append(MBSectionType.Description)
                }
                
                dataTypes.append(MBSectionType.Subscription)
            }
            break
        case .individual:
            
            if let aIndividualObject =  aTariff.valueObject as? Individual {
                // 1
                if aIndividualObject.header != nil {
                    
                    dataTypes.append(MBSectionType.Header)
                }
                // 2
                if aIndividualObject.details != nil  {
                    
                    dataTypes.append(MBSectionType.Detail)
                }
                // 3
                if aIndividualObject.description != nil {
                    
                    dataTypes.append(MBSectionType.Description)
                }
                
                dataTypes.append(MBSectionType.Subscription)
            }
            
            break
        case .corporate:
            if let aCorporateObject =  aTariff.valueObject as? Corporate {
                // 1
                if aCorporateObject.header != nil {
                    
                    dataTypes.append(MBSectionType.Header)
                }
                
                // 2
                if aCorporateObject.price != nil  {
                    
                    dataTypes.append(MBSectionType.Price)
                }
                
                // 3
                if aCorporateObject.details != nil  {
                    
                    dataTypes.append(MBSectionType.Detail)
                }
                // 4
                if aCorporateObject.description != nil {
                    
                    dataTypes.append(MBSectionType.Description)
                }
                
                dataTypes.append(MBSectionType.Subscription)
            }
            break
        case .klassPostpaid:
            
            if let aKlassPostpaidObject =  aTariff.valueObject as? KlassPostpaid {
                // 1
                if aKlassPostpaidObject.header != nil {
                    
                    dataTypes.append(MBSectionType.Header)
                }
                
                // 2
                if aKlassPostpaidObject.packagePrice != nil {
                    
                    dataTypes.append(MBSectionType.PackagePrice)
                }
                
                // 3
                if aKlassPostpaidObject.details != nil  {
                    
                    dataTypes.append(MBSectionType.Detail)
                }
                
                dataTypes.append(MBSectionType.Subscription)
            }
            break
        case .unSpecified:
            break
        }
        
        return dataTypes
    }
    
    func typeOfDataInCINHeaderSection(_ aCINData : Cin?) -> [MBOfferType] {
        
        var dataTypes : [MBOfferType] = []
        if let aHeaderObject =  aCINData?.header {
            
            // 1
            
            dataTypes.append(MBOfferType.TopDescription)
            
            // 2
            if aHeaderObject.call != nil {
                
                dataTypes.append(MBOfferType.Call)
            }
            // 3
            if aHeaderObject.sms != nil  {
                
                dataTypes.append(MBOfferType.SMS)
            }
            // 4
            if aHeaderObject.internet != nil {
                
                dataTypes.append(MBOfferType.Internet)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInIndividualHeaderSection(_ aIndividualData : Individual?) -> [MBOfferType] {
        
        var dataTypes : [MBOfferType] = []
        if let aHeaderObject =  aIndividualData?.header {
            
            // 1
            if aHeaderObject.call != nil {
                
                dataTypes.append(MBOfferType.Call)
            }
            // 2
            if aHeaderObject.sms != nil  {
                
                dataTypes.append(MBOfferType.SMS)
            }
            // 3
            if aHeaderObject.internet != nil {
                
                dataTypes.append(MBOfferType.Internet)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInKlassPackagePrice(_ aPackagePrice: PackagePrice?) -> [MBOfferType]  {
        
        var dataTypes : [MBOfferType] = []
        if let aPackagePriceObject =  aPackagePrice {
            
            // 1
            if aPackagePriceObject.call != nil {
                
                dataTypes.append(MBOfferType.Call)
            }
            // 2
            if aPackagePriceObject.sms != nil  {
                
                dataTypes.append(MBOfferType.SMS)
            }
            // 3
            if aPackagePriceObject.internet != nil {
                
                dataTypes.append(MBOfferType.Internet)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInKlassPostpaidPackagePrice(_ aPackagePrice: PackagePriceKlassPostpaid?) -> [MBOfferType]  {
        
        var dataTypes : [MBOfferType] = []
        if let aPackagePriceObject =  aPackagePrice {
            
            // 1
            if aPackagePriceObject.call != nil {
                
                dataTypes.append(MBOfferType.Call)
            }
            // 2
            if aPackagePriceObject.sms != nil  {
                
                dataTypes.append(MBOfferType.SMS)
            }
            // 3
            if aPackagePriceObject.internet != nil {
                
                dataTypes.append(MBOfferType.Internet)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInPaygPrice(_ aPaygPrice: PaygPrice?) -> [MBOfferType]  {
        
        var dataTypes : [MBOfferType] = []
        if let aPaygPriceObject =  aPaygPrice {
            
            // 1
            if aPaygPriceObject.call != nil {
                
                dataTypes.append(MBOfferType.Call)
            }
            // 2
            if aPaygPriceObject.sms != nil  {
                
                dataTypes.append(MBOfferType.SMS)
            }
            // 3
            if aPaygPriceObject.internet != nil {
                
                dataTypes.append(MBOfferType.Internet)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInCorporatePrice(_ aPrice: CinPostpaidPrice?) -> [MBOfferType]  {
        
        var dataTypes : [MBOfferType] = []
        if let aPriceObject =  aPrice {
            
            // 1
            if aPriceObject.call != nil {
                
                dataTypes.append(MBOfferType.Call)
            }
            // 2
            if aPriceObject.sms != nil  {
                
                dataTypes.append(MBOfferType.SMS)
            }
            // 3
            if aPriceObject.internet != nil {
                
                dataTypes.append(MBOfferType.Internet)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInDetailSection(_ aDetail : DetailsWithPriceArray?) -> [MBOfferType] {
        
        var dataTypes : [MBOfferType] = []
        
        if let aDetailObject =  aDetail {
            
            // 1
            if let count = aDetailObject.price?.count {
                
                for _ in 0..<count {
                    dataTypes.append(MBOfferType.Price)
                }
            }
            // 2
            if aDetailObject.rounding != nil  {
                
                dataTypes.append(MBOfferType.Rounding)
            }
            // 3
            if aDetailObject.textWithTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithTitle)
            }
            // 4
            if aDetailObject.textWithOutTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithOutTitle)
            }
            // 5
            if aDetailObject.textWithPoints != nil {
                
                dataTypes.append(MBOfferType.TextWithPoints)
            }
            // 6
            if aDetailObject.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(MBOfferType.TitleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailObject.date != nil {
                
                dataTypes.append(MBOfferType.Date)
            }
            // 8
            if aDetailObject.time != nil {
                
                dataTypes.append(MBOfferType.Time)
            }
            // 9
            if aDetailObject.roamingDetails != nil {
                
                dataTypes.append(MBOfferType.RoamingDetails)
            }
            // 10
            if aDetailObject.freeResourceValidity != nil {
                
                dataTypes.append(MBOfferType.FreeResourceValidity)
            }
        }
        return dataTypes
    }
    
    func typeOfDataInDescriptionSection(_ aDescription : TariffDescription?) -> [MBOfferType] {
        
        var dataTypes : [MBOfferType] = []
        
        if let aDescriptionObject =  aDescription {
            
            // 1
            if aDescriptionObject.advantages != nil  {
                
                dataTypes.append(MBOfferType.Advantages)
            }
            
            // 2
            if aDescriptionObject.classification != nil {
                
                dataTypes.append(MBOfferType.Classification)
            }
            
        }
        
        return dataTypes
    }
    
}

// MARK: - <MBAccordionTableViewDelegate> -

extension TariffsMainVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.stateIcon.image = UIImage.imageFor(name: "minus_sign")
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            
            if selectedSectionViewType != headerView.viewType {
                
                //selectedSectionViewType = headerView.viewType
            }
            
            // added
            if tariffResponse?.tarrifs[tableView.tag].openedViewSection != headerView.viewType {
                
                tariffResponse?.tarrifs[tableView.tag].openedViewSection = headerView.viewType
            }
            
        } else if (header as? IndividualTitleView) != nil {
            
            if selectedSectionViewType != .Header {
                selectedSectionViewType = .Header
            }
            
            // added
            if tariffResponse?.tarrifs[tableView.tag].openedViewSection != .Header {
                
                tariffResponse?.tarrifs[tableView.tag].openedViewSection = .Header
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleHeaderView {
            headerView.stateIcon.image = UIImage.imageFor(name: "plus_sign")
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        
        if let headerView = header as? ExpandableTitleHeaderView {
            
            //            if selectedSectionViewType == headerView.viewType {
            //                selectedSectionViewType = MBSectionType.Header
            //                tableView.toggleSection(0)
            //                tariffResponse?.tarrifs[tableView.tag].openedViewSection = .Header
            //
            //            }
            
            // added
            if tariffResponse?.tarrifs[tableView.tag].openedViewSection == headerView.viewType {
                
                tableView.toggleSection(0)
                tariffResponse?.tarrifs[tableView.tag].openedViewSection = .Header
            }
        } else if (header as? IndividualTitleView) != nil {
            
            //            if selectedSectionViewType == .Header {
            //                selectedSectionViewType = MBSectionType.Header
            //                tableView.toggleSection(0)
            //
            //            }
            // added
            if tariffResponse?.tarrifs[tableView.tag].openedViewSection == .Header {
                tableView.toggleSection(0)
                tariffResponse?.tarrifs[tableView.tag].openedViewSection = .Header
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        if let headerView = tableView.headerView(forSection: section) as? SubscribeButtonView {
            
            if headerView.viewType == MBSectionType.Subscription {
                
                return false
                
            } else {
                
                return true
            }
        } else {
            
            return true
        }
    }
}

extension TariffsMainVC : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == collectionView {
            openSelectedSection()
        }
    }
}
