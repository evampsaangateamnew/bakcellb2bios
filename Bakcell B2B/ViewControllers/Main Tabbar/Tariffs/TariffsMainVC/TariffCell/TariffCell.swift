//
//  TariffCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 5/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class TariffCell: UICollectionViewCell {

    //MARK: - IBOutlets
    @IBOutlet var tableView: MBAccordionTableView!

    //MARK: - Functions
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func awakeFromNib() {
        super.awakeFromNib()


        tableView.separatorColor = UIColor.clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180.0
        tableView.allowsSelection = false

        tableView.initialOpenSections = [0]
    }
   
}
