//
//  DashBoardVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/25/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class DashBoardVC: BaseVC {
    
    //MARK: - Properties
    
    
    //MARK: - IBOutlets
    @IBOutlet  var parentScroolView : UIScrollView!
    @IBOutlet  var titleLabel: MBLabel!
    @IBOutlet  var notificationCountLabel: MBLabel!
    @IBOutlet  var totalUsersTitleLabel: MBLabel!
    @IBOutlet  var totalUsersCountTitleLabel: MBLabel!
    //Limitsview outlets
    @IBOutlet  var totalLimitTitleLabel: MBLabel!
    @IBOutlet  var totalLimitUnitTitleLabel: MBLabel!
    @IBOutlet  var availableCreditBalanceTitleLabel: MBLabel!
    @IBOutlet  var availableCreditBalanceUnitTitleLabel: MBLabel!
    @IBOutlet  var availableCreditBalanceProgress: MBGradientProgressView!
    //Debitview outlets
    @IBOutlet  var outStandingDebtTitleLabel: MBLabel!
    @IBOutlet  var outStandingDebtUnitTitleLabel: MBLabel!
    @IBOutlet  var unBilledBalanceTitleLabel: MBLabel!
    @IBOutlet  var unBilledBalanceUnitTitleLabel: MBLabel!
    
    //Invoiceview outlets
    @IBOutlet  var totalAmountToBePaidTitleLabel: MBLabel!
    @IBOutlet  var totalAmountToBePaidUnitTitleLabel: MBLabel!
    @IBOutlet  var invoiceTitleLabel: MBLabel!
    @IBOutlet  var invoiceProgress: MBGradientProgressView!
    @IBOutlet  var invoiceExpireDateTitleLabel: MBLabel!
    @IBOutlet  var invoiceIssueDateTitleLabel: MBLabel!
    @IBOutlet  var invoiceRemainingDaysTitleLabel: MBLabel!
    //Menu
    @IBOutlet  var tarrifsTitleLabel: MBLabel!
    @IBOutlet  var packagesTitleLabel : MBLabel!
    @IBOutlet  var searchUserTitleLable : MBMarqueeLabel!
    
    //MARK: - View Controllers Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRefreshControalToScroolView()
        
        // Adding observer to call HomePage API
        NotificationCenter.default.addObserver(self, selector: #selector(setDashBoardInformation), name: NSNotification.Name(Constants.KN_ReloadHomePage), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop(false)
        
        loadViewLayout()
        
        // Redirect user to notification screen if user select screen when application is close
        if MBPICUserSession.shared.isPushNotificationSelected {
            
            // Reset to false
            MBPICUserSession.shared.isPushNotificationSelected = false
            
            notificationButtonPressed(UIButton())
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Functions
    func loadViewLayout() {
        titleLabel.text = Localized("Title_DashBoard")
        totalUsersTitleLabel.text = Localized("Title_TotalUsers")
        
        //Limitsview
        totalLimitTitleLabel.text = Localized("Title_TotalLimit")
        availableCreditBalanceTitleLabel.text = Localized("Title_AvailableCreditBalance")
        
        //Debitview
        outStandingDebtTitleLabel.text = Localized("Title_OutStandingDebt")
        unBilledBalanceTitleLabel.text = Localized("Title_UnBilledBalance")
        
        //Invoiceview
        totalAmountToBePaidTitleLabel.text = Localized("Title_TotalAmountToBePaid")
        invoiceTitleLabel.text = Localized("Title_Invoice")
        
        //Menu
        tarrifsTitleLabel.text = Localized("Title_Tarrif")
        packagesTitleLabel.text = Localized("Title_Packages")
        searchUserTitleLable.text = Localized("Title_SearchUser")
        
        setDashBoardInformation()
    }
    
    // Set Dash board information load from user default.
    @objc func setDashBoardInformation()  {
        
        if let usersCount = UserDefaults.standard.string(forKey: Constants.kUsersCount),
            usersCount.isEmpty == false {
           totalUsersCountTitleLabel.text  = usersCount
        } else {
            totalUsersCountTitleLabel.text  = "0"
        }
        
        /* Setting Unreead Notification Count */
        let notificationCount = MBUtilities.loadJSONStringFromUserDefaults(key: APIsType.notificationCount.keyValue())
        
        if notificationCount.toInt > 0 {
            notificationCountLabel.text = notificationCount
            notificationCountLabel.isHidden = false
        } else {
            notificationCountLabel.text = ""
            notificationCountLabel.isHidden = true
        }
        
        
        //Limitsview
        if let picBalanceInfo : PICBalanceData = PICBalanceData.loadFromUserDefaults(key: APIsType.queryBalanceResponseData.localizedAPIKey()) {
            
            //Load from PIC Balance information from userdefault.
            totalLimitUnitTitleLabel.attributedText = MBUtilities.createAttributedText(picBalanceInfo.totalLimit ?? "0", textColor: .mbTextGray, withManatSign: true)
            
            let creditAmount = "\(picBalanceInfo.availableCredit ?? "0") / \(picBalanceInfo.balanceCredit ?? "0")"
            availableCreditBalanceUnitTitleLabel.attributedText = MBUtilities.createAttributedText(creditAmount, textColor: .black, withManatSign: true)
            
            availableCreditBalanceProgress.setProgress(MBProgressUtilities.calculateProgress(total: picBalanceInfo.balanceCredit?.toDouble ?? 0, remaining: picBalanceInfo.availableCredit?.toDouble ?? 0), animated: true)
            
            //Debitview
            unBilledBalanceUnitTitleLabel.attributedText = MBUtilities.createAttributedText(picBalanceInfo.unbilledBalance ?? "0", textColor: .black, withManatSign: true)
            
            outStandingDebtUnitTitleLabel.attributedText = MBUtilities.createAttributedText(picBalanceInfo.outstandingDebt ?? "0", textColor: .black, withManatSign: true)
        } else {
            //Load default zero zero value if there is no value in userdefault.
            
            totalLimitUnitTitleLabel.attributedText = MBUtilities.createAttributedText("0", textColor: .mbTextGray, withManatSign: true)
            
            availableCreditBalanceUnitTitleLabel.attributedText = MBUtilities.createAttributedText("0 / 0", textColor: .black, withManatSign: true)
            
            availableCreditBalanceProgress.setProgress(1.0, animated: true)
            
            //Debitview
            unBilledBalanceUnitTitleLabel.attributedText = MBUtilities.createAttributedText( "0", textColor: .black, withManatSign: true)
            
            outStandingDebtUnitTitleLabel.attributedText = MBUtilities.createAttributedText("0", textColor: .black, withManatSign: true)
        }
        
        //Invoiceview
        if let userInviceInfo : QueryInvoiceResponseData = QueryInvoiceResponseData.loadArrayFromUserDefaults(key: APIsType.queryInvoiceResponseData.localizedAPIKey())?.first {
            /* Load from Query Invoice information from userdefault. */
            totalAmountToBePaidUnitTitleLabel.attributedText = MBUtilities.createAttributedText(userInviceInfo.invoiceAmount, textColor: .mbBrandRed, withManatSign: true)
            
            /* When status is 'c'(Close) then show invice progress green else calculate and show invoice data */
            if userInviceInfo.status?.isEqual("c", ignorCase: true) ?? false {
                
                invoiceRemainingDaysTitleLabel.text = Localized("Info_Paid")
                invoiceProgress.setProgress(1, animated: true)
                invoiceProgress.changeGradientLayerColors(newGradientColors: [UIColor.mbGreen.cgColor,UIColor.mbGreen.cgColor])
                
                invoiceIssueDateTitleLabel.text = ""
                invoiceExpireDateTitleLabel.text = userInviceInfo.settleDateDisp ?? ""
                
            } else {
                let computedValuesFromDates = MBProgressUtilities.calculateProgressValues(from: userInviceInfo.invoiceDate ?? "", to: userInviceInfo.dueDate ?? "", dateFormate: Constants.kInviceDateFormate, AdditionalValueInEndDate: -1)
                
                invoiceRemainingDaysTitleLabel.text = computedValuesFromDates.daysLeftDisplayValue
                invoiceProgress.setProgress(computedValuesFromDates.progressValue, animated: true)
                
                invoiceIssueDateTitleLabel.text = userInviceInfo.invoiceDateDisp ?? ""
                invoiceExpireDateTitleLabel.text = userInviceInfo.dueDateDisp ?? ""
            }
            
            
        } else {
            /* Load default zero zero value if there is no value in userdefault. */
            
            totalAmountToBePaidUnitTitleLabel.attributedText = MBUtilities.createAttributedText("0", textColor: .mbBrandRed, withManatSign: true)
            
            invoiceRemainingDaysTitleLabel.text = "0 \(Localized("Title_Days"))"
            invoiceProgress.setProgress(1, animated: true)
            
            invoiceIssueDateTitleLabel.text = ""
            invoiceExpireDateTitleLabel.text = ""
        }
        
    }
    
    // Pull to refresh to get latest dashboard data
    func addRefreshControalToScroolView() {
        
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .mbBrandRed
        refreshControl.addTarget(self,
                                 action: #selector(refreshOptions),
                                 for: .valueChanged)
        if #available(iOS 10.0, *) {
            
            parentScroolView.refreshControl = refreshControl
        } else {
            parentScroolView.addSubview(refreshControl)
        }
    }
    
    
    //MARK: -  IBActions
    @IBAction func showUserInvoiceButtonPressed(_ sender: UIButton){
        if let companyInvoiceVC :CompanyInvoiceVC = CompanyInvoiceVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(companyInvoiceVC, animated: true)
        }
    }
    
    @IBAction func notificationButtonPressed(_ sender: UIButton) {
        if let notificationsVC :NotificationsVC = NotificationsVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(notificationsVC, animated: true)
        }
    }
    
    @IBAction func tariffButtonPressed(_ sender: UIButton) {
        if let aViewController :TariffsMainVC = TariffsMainVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(aViewController, animated: true)
        }
    }
    
    @IBAction func packagesButtonPressed(_ sender:  UIButton){
        if let supplementoryOfferVC :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(supplementoryOfferVC, animated: true)
        }
        
    }
    @IBAction func searchUsersButtonPressed(_ sender:  UIButton){
        
        /* Get index of UserGroup from tabbar and selecting tabbar item */
        if let indexForUsers = MBPICUserSession.shared.appMenu?.menuHorizontal?.firstIndex(where: { $0.identifier == "users_group" }) {
            
            self.tabBarController?.selectedIndex = indexForUsers
        }
    }
    
    //MARK: -  APIs Calls
    
    // Stop UIRefresh controal animation
    @objc func refreshOptions(sender: UIRefreshControl) {
        
        /* Load number of unread notification */
        loadNotificationsCount()
        
        _ = MBAPIClient.sharedClient.gethomepageResume({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            sender.endRefreshing()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
//
                    // Parssing response data
                    if let appResumeHandler = Mapper<AppResumeData>().map(JSONObject: resultData) {
                        
                        /* Save RateUS information */
                        RateUsAlertVC.saveTitle(title: appResumeHandler.predefinedData?.rateUsPopupTitle)
                        RateUsAlertVC.saveMessage(message: appResumeHandler.predefinedData?.rateUsPopupContent)
                        
                        /* check if user login for the first time then ask to change password */
                        if appResumeHandler.picUserInformation?.picNewReset?.isEqual("1", ignorCase: true) ?? false {
                            if let changePasswordVC :ChangePasswordVC = ChangePasswordVC.instantiateViewControllerFromStoryboard() {
                                changePasswordVC.canUserNavigate = false
                                changePasswordVC.picUserInformation = appResumeHandler.picUserInformation
                                self.presentPOPUP(changePasswordVC, animated: true)
                            }
                        } else  if appResumeHandler.picUserInformation?.picTNC?.isEqual("0", ignorCase: true) ?? false {
                            /* check if terms and condition are updated, If yes, then ask user to accept new terms and conditions. */
                            if let termsConditionVC :TermsConditionsVC = TermsConditionsVC.instantiateViewControllerFromStoryboard() {
                                termsConditionVC.isPopUp = true
                                self.presentPOPUP(termsConditionVC, animated: true)
                            }
                        } else  if appResumeHandler.picUserInformation?.rateUsIOS?.isEqual("0", ignorCase: true) ?? false {
                            
                            /* Check is Rate Us is presented to user before*/
                            if MBUtilities.isRateUsShownBefore() == false {
                                
                                let timeDiffranceInHours = MBDateUtilities.calculateTotalHoursBetweenTwoDates(startingDate: MBUtilities.getLoginTime(), endingDate: Date().todayDateString(), dateFormate: Constants.kHomeDateFormate)
                                
                                if timeDiffranceInHours >= (appResumeHandler.predefinedData?.firstPopup?.toInt ?? 0) {
                                    if let rateUsAlert : RateUsAlertVC = RateUsAlertVC.fromNib() {
                                        rateUsAlert.setRateUs({
                                            self.redirectUserToAppStore()
                                        })
                                        self.presentPOPUP(rateUsAlert, animated: true, completion: nil)
                                    }
                                }
                                
                            } else {
                                let timeDiffranceInDays = MBDateUtilities.calculateTotalDaysBetween(startingDate: MBUtilities.getRateUsLaterTime(), endingDate: Date().todayDateString(), dateFormate: Constants.kHomeDateFormate)
                                
                                if timeDiffranceInDays >= (appResumeHandler.predefinedData?.lateOnPopup?.toInt ?? 0) {
                                    if let rateUsAlert : RateUsAlertVC = RateUsAlertVC.fromNib() {
                                        rateUsAlert.setRateUs({
                                            self.redirectUserToAppStore()
                                        })
                                        self.presentPOPUP(rateUsAlert, animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                        
                        /* Saving AppResume information in User Defaults */
                        MBUtilities.saveAppResumeInfo(loggedInUserMSISDN: MBPICUserSession.shared.msisdn, appResumeData: appResumeHandler)
                        
                        /* Reload Dashboard information */
                        self.setDashBoardInformation()
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
        })
    }
    
    
    /**
     call to get unread Notifications count Information.
     */
    func loadNotificationsCount() {
        
        /*API call to get data*/
        _ = MBAPIClient.sharedClient.getNotificationsCount({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            // handling data from API response.
            if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                
                // Parssing response data
                if let notificationsCountResponse = Mapper<NotificationsCount>().map(JSONObject:resultData) {
                    
                    /* Saving notification unread count */
                    MBUtilities.saveJSONStringInToUserDefaults(jsonString: notificationsCountResponse.notificationUnreadCount?.toInt.toString(), key: APIsType.notificationCount.keyValue())
                    
                    /* Setting notification unread count */
                    if let notificationCount = notificationsCountResponse.notificationUnreadCount?.toInt ,
                        notificationCount > 0 {
                        self.notificationCountLabel.isHidden = false
                        self.notificationCountLabel.text = notificationCount.toString()
                    } else {
                        /* Hide notification unread count label */
                        self.notificationCountLabel.isHidden = true
                        self.notificationCountLabel.text = ""
                    }
                }
            }
        })
    }
    
}
