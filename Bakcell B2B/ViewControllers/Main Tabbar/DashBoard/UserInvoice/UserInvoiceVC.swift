//
//  UserInvoiceVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class UserInvoiceVC: BaseVC {
    //MARK: - Properties
    
    var userInvoiceList : [DetailInvoice]  = [DetailInvoice(ServiceName: "Name", Quantity: "1", Vahid: "1", Mablaq: "1", Discount: "1", Cami: "1"),DetailInvoice(ServiceName: "Name", Quantity: "1", Vahid: "1", Mablaq: "1", Discount: "1", Cami: "1")]
    var selectedMISDNNumber : MISDNInvoice?
    //MARK: -  IBOutlets
    
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var invoiceAmountTitleLabel: MBLabel!
    @IBOutlet var invoiceAmountValueLabel: MBLabel!
    @IBOutlet var limitTitleLabel: MBLabel!
    @IBOutlet var limitValueLabel: MBLabel!
    @IBOutlet var userInvoiceDetailTableView : UITableView!
    
    //MARK: - View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userInvoiceDetailTableView.estimatedRowHeight = 60
        userInvoiceDetailTableView.estimatedSectionHeaderHeight = 60
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
        setUserInvoiceInfo() 
    }
    
    //MARK: - Functions
    func loadViewLayout(){
        titleLabel.text = Localized("Title_Invoice")
        invoiceAmountTitleLabel.text = Localized("Title_InvoiceAmountVAT")
        limitTitleLabel.text = Localized("Title_Limit")
        reloadTableViewData()
    }
    
    func setUserInvoiceInfo(){
        invoiceAmountValueLabel.text = "0000"
        limitValueLabel.text = "000"
    }
    
    func reloadTableViewData() {
        
        if (userInvoiceList.count <= 0 ){
            self.userInvoiceDetailTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        
        } else {
            self.userInvoiceDetailTableView.hideDescriptionView()
        }
        
        userInvoiceDetailTableView.reloadData()
    }
}
//MARK: - TableView Delegats
extension UserInvoiceVC: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if (userInvoiceList.count ) > 0 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        if let detailHeaderView : DetailInvoiceHeaderView = tableView.dequeueReusableHeaderFooterView() {
            return detailHeaderView
        } else {
            return MBAccordionTableViewHeaderView()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return userInvoiceList.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let detailCell :  DetailsInvoiceCell = tableView.dequeueReusableCell(){
            detailCell.setInvoiceDetails(details: userInvoiceList[indexPath.row])
            return detailCell
            
        }
        return UITableViewCell()
    }
    
    
}
