//
//  UsersVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/22/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class UsersVC: UserSelectionBaseVC {
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var searchTextField: MBTextField!
    @IBOutlet var usersButton: MBButton!
    @IBOutlet var groupsButton: MBButton!
    @IBOutlet var contentView: UIView!
    
    //MARK: - ViewControllers methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextField.delegate = self
        searchTextField.setRightButtonCompletionBlock {
            self.searchTextFeidlButtonPressed()
        }
        
        // Select individualList on start
        userButtonPressed(usersButton)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop(false)
        
        loadViewLayout()

        /* Load latest infomation */
        retriveUserData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        resetSearch()
    }
    

    //MARK:- Functions
    
    func loadViewLayout() {
        titleLabel.text = Localized("Title_Users")
        searchTextField.placeholder = Localized("Placeholder_SearchByNameORNumber")
        usersButton.setTitle(Localized("BtnTitle_Number"), for: .normal)
        groupsButton.setTitle(Localized("BtnTitle_Groups"), for: .normal)
        
    }
    
    //Load user data from userdefault
    func retriveUserData() {
        
        super.loadUsersInformation { (individualUsersList, groupUsersList) in
            
            self.individualUsers = individualUsersList
            self.filteredIndividualUsers = individualUsersList
            
            self.groupUsers = groupUsersList
            self.filteredGroupUsers = groupUsersList
            
            /* Load users information */
            self.loadUsersListOfType(self.userSelectedType)
        }
    }
    
    
    func searchTextFeidlButtonPressed() {
        
        //  if searchTextField.isFirstResponder == true {
        //  _ = searchTextField.resignFirstResponder()
        //  } else {
        //  searchTextField.becomeFirstResponder()
        //  }
    }
    
    func resetSearch() {
        searchTextField.text = ""
        _ = searchTextField.resignFirstResponder()
        
        
        filteredIndividualUsers = individualUsers
        filteredGroupUsers = groupUsers
        
    }
    
    /**
     Add child view controller to parent view.
     
     - Conditions:
     
     1. IndividualUserListVC: if UserSelectedType == individualList
     2. UserGroupListVC: else UserSelectedType == groupList
     
     - returns: void
     */
    func loadUsersListOfType(_ type : UserSelectedType) {
        
        userSelectedType = type
        
        if type == .individualList {
            if individualUsersListVC == nil {
                if let individualUserListObject :IndividualUserSelectionVC = IndividualUserSelectionVC.instantiateViewControllerFromStoryboard() {
                    
                    individualUserListObject.usersList = filteredIndividualUsers
                    individualUserListObject.delegate = self
                    individualUserListObject.isSingleUserSelection = true
                    
                    self.addChildViewController(childController: individualUserListObject, onView: contentView)
                    
                    individualUsersListVC = individualUserListObject
                }
            } else {
                self.removeChildViewController(childController: GroupUserSelectionVC())
                
                individualUsersListVC?.usersList = filteredIndividualUsers
                self.addChildViewController(childController: individualUsersListVC!, onView: contentView)
            }
        } else {
            if groupUsersListVC == nil {
                if let userGroupListObject :GroupUserSelectionVC = GroupUserSelectionVC.instantiateViewControllerFromStoryboard() {
                    
                    userGroupListObject.groupList = filteredGroupUsers
                    userGroupListObject.delegate = self
                    userGroupListObject.isSingleUserSelection = true
                    
                    self.addChildViewController(childController: userGroupListObject, onView: contentView)
                    
                    groupUsersListVC = userGroupListObject
                }
            } else {
                self.removeChildViewController(childController: IndividualUserSelectionVC())
                
                groupUsersListVC?.groupList = filteredGroupUsers
                self.addChildViewController(childController: groupUsersListVC!, onView: contentView)
            }
        }
        
    }
    
    func searchOffersByName(searchString: String = "") {
        
        if userSelectedType == .individualList {
            // find offers where name matches
            filteredIndividualUsers = self.filterUsers(byQueryString:searchString, fromUserList: individualUsers )
            
            // Redirect user to screen accourdingly
            loadUsersListOfType(.individualList)
        } else {
            // find offers where name matches
            filteredGroupUsers = self.filterGroups(byQueryString: searchString, fromGroupList: groupUsers)
            // Redirect user to screen accourdingly
            loadUsersListOfType(.groupList)
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func userButtonPressed(_ sender: UIButton) {
        usersButton.setButtonLayoutType(.grayButton)
        groupsButton.setButtonLayoutType(.whiteButton)
        
        resetSearch()
        
        loadUsersListOfType(.individualList)
    }
    
    @IBAction func groupButtonPressed(_ sender: UIButton) {
        usersButton.setButtonLayoutType(.whiteButton)
        groupsButton.setButtonLayoutType(.grayButton)
        
        resetSearch()
        
        loadUsersListOfType(.groupList)
    }
}


//MARK:- UsersList viewControllers Delegate
extension UsersVC: UsersListDelegate {
   
    // Selection delegate
    func singleUserDidSelect(_ selectedUser : UsersData?) {
        if let aSelectedUser = selectedUser {
            
            if let usrInfoVC :UserInformationVC = UserInformationVC.instantiateViewControllerFromStoryboard() {
                
                usrInfoVC.selectedUser = aSelectedUser
                self.navigationController?.pushViewController(usrInfoVC, animated: true)
            }
        }
    }
}

//MARK:- UITextFieldDelegate

extension UsersVC : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        super.setupViewResizerOnKeyboardShown()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        
        if textField == searchTextField {
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            // Filter offers with new name
            self.searchOffersByName(searchString: newString)
            
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            _ = textField.resignFirstResponder()
            //  loginPressed(UIButton())
            return false
        } else {
            return true
        }
    }
}




