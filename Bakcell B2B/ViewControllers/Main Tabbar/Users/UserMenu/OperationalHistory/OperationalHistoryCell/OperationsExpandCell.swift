//
//  OperationsExpandCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/26/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class OperationsExpandCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet var descriptionTitleLabel: MBLabel!
    @IBOutlet var descriptionValueLabel: MBLabel!
    
    @IBOutlet var clarificationTitleLabel: MBLabel!
    @IBOutlet var clarificationValueLabel: MBLabel!
    
     //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

    func setOperationHistory(history : OperationsRecord?) {
        
        descriptionTitleLabel.text = Localized("OpHist_Description")
        descriptionValueLabel.text = history?.description ?? ""
        clarificationValueLabel.text = Localized("OpHist_Clarification")
        clarificationTitleLabel.text = history?.clarification ?? ""
        
    }
    
}
