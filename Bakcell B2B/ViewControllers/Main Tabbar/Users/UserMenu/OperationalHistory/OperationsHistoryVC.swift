//
//  OperationsHistoryVC.swift
//  Bakcell
//
//  Created by AbdulRehman on 5/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import  ObjectMapper

class OperationsHistoryVC: BaseVC {
    
    // MARK: - Properties
    var operationsHistoryData : [OperationsRecord]?
    var filteredOperationsData : [OperationsRecord]?
    var transactionSelectedType : String? = Localized("DropDown_All")
    var isStartDate : Bool?
    
    
    // MARK: - IBOutlet
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var transactionDropDown: UIDropDown!
    @IBOutlet var dateLabel: MBLabel!
    @IBOutlet var tableView: MBAccordionTableView!
    @IBOutlet var pageCountLabel: MBLabel!
    @IBOutlet var spacificBtnView: UIView!
    @IBOutlet var endDateButton: UIButton!
    @IBOutlet var startDateButton: UIButton!
    @IBOutlet var startDateLabel: MBLabel!
    @IBOutlet var endDateLabel: MBLabel!
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var serviceTitleLabel: MBLabel!
    @IBOutlet var amountLabel: MBLabel!
    @IBOutlet var endingBalanceView: UIView!
    @IBOutlet var endingBalanceLabel: MBLabel!
    
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spacificBtnView.isHidden = true
        tableView.allowMultipleSectionsOpen = true
        tableView.delegate = self
        tableView.dataSource = self
        
        setUpDropDown()
        
        /* Hidding Ending Balance in case of postpaid user */
        if MBSelectedUserSession.shared.subscriberType() == Constants.MBSubscriberType.PostPaid {
            
            endingBalanceView.isHidden = true
        } else {
            endingBalanceView.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        titleLabel.text = Localized("Title_OperationHistory")
        dateLabel.text = Localized("Title_Date")
        serviceTitleLabel.text = Localized("Title_Service")
        amountLabel.text = Localized("Title_Amount")
        endingBalanceLabel.text = Localized("Title_EndingBalance")
    }
    
    //Initialze / Setup dropdown 
    func setUpDropDown() {
 
        // Load initional data
        self.operationsHistoryApiCall(StartDate: Date().todayDateString(dateFormate: Constants.kAPIFormat), EndDate: Date().todayDateString(dateFormate: Constants.kAPIFormat)) { (canShow) in
            
            if canShow == true && DisclaimerMessageVC.canShowDisclaimerOfType(.OperationHistory) == true {
                if let disclaimerMessageVC :DisclaimerMessageVC = DisclaimerMessageVC.instantiateViewControllerFromStoryboard() {
                    
                    disclaimerMessageVC.setDisclaimerAlertWith(description: Localized("Disclaimer_Description"), okBtnClickedBlock: { (dontShowAgain) in
                        
                        if dontShowAgain == true {
                            DisclaimerMessageVC.setDisclaimerStatusForType(.OperationHistory, canShow: false)
                        }
                    })
                    
                    self.presentPOPUP(disclaimerMessageVC, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
                }
            }
        }
        
        //Transactions Drop Down
        let transactionTtitles : [String] = [Localized("DropDown_All"),Localized("DropDown_DataBundles"), Localized("DropDown_SMSBundles"), Localized("DropDown_Duplicate"), Localized("DropDown_MRC"), Localized("DropDown_Payment"),Localized("DropDown_Adjustment"),Localized("DropDown_Content(CP)"),Localized("DropDown_Installment"),Localized("DropDown_MoneyTransfer"),Localized("DropDown_ChangeOwnership"),Localized("DropDown_ChangeCustomerType"),Localized("DropDown_Others")]
        transactionDropDown.placeholder = Localized("DropDown_All")
        transactionDropDown.dataSource = transactionTtitles
        transactionDropDown.didSelectOption { (index, option) in
            self.transactionSelectedType = option
            
            self.filteredOperationsData = self.filterHistoryBy(type: self.transactionSelectedType, operationRecords: self.operationsHistoryData, allTypes: self.transactionDropDown.dataSource)
            
            // Reload tableview data
            self.reloadTableViewData()
        }
        
        
        //Date Drop Down
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        
        let dates : [String] = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_CustomPeriod")]
        
        dateDropDown.dataSource = dates
        dateDropDown.didSelectOption { (index, option) in
            self.dateDropDownChanged( transacitonIndex: index)
        }
    }
    
    /**
     Date dropdown changed.
     
     - parameter transacitonIndex: Selected drpodown index.
     
     - returns: void
     */
    func dateDropDownChanged( transacitonIndex: Int?)  {
        
        let todayDateString = Date().todayDateString(dateFormate: Constants.kAPIFormat)
        switch transacitonIndex{
        case 0:
            print("default")
            self.spacificBtnView.isHidden = true
            
            self.operationsHistoryApiCall(StartDate: todayDateString, EndDate: todayDateString)
        case 1:
            print("Last 7 days")
            self.spacificBtnView.isHidden = true
            
            // Get data of previous seven days
            let sevenDaysAgo = Date().todayDateString(withAdditionalValue: -7)
            self.operationsHistoryApiCall(StartDate: sevenDaysAgo, EndDate: todayDateString)
            
        case 2:
            print("Last 30 days")
            self.spacificBtnView.isHidden = true
            
            // Get data of previous thirty days
            let thirtyDaysAgo = Date().todayDateString(withAdditionalValue: -30)
            self.operationsHistoryApiCall(StartDate: thirtyDaysAgo, EndDate: todayDateString)
            
        case 3:
            print("Previous month")
            self.spacificBtnView.isHidden = true
            
            // Getting previous month start and end date
            let startOfPreviousMonth = Date().todayDate().getPreviousMonth()?.startOfMonth() ?? Date().todayDate()
            let endOfPreviousMonth = Date().todayDate().getPreviousMonth()?.endOfMonth() ?? Date().todayDate()
            
            let startDateString = MBPICUserSession.shared.dateFormatter.createString(from: startOfPreviousMonth, dateFormate: Constants.kAPIFormat)
            let endDateString = MBPICUserSession.shared.dateFormatter.createString(from: endOfPreviousMonth, dateFormate: Constants.kAPIFormat)
            
            self.operationsHistoryApiCall(StartDate: startDateString, EndDate: endDateString)
            
        case 4:
            self.spacificBtnView.isHidden = false
            
            //For date formate
            self.startDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            self.endDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            
            self.startDateLabel.textColor = UIColor.mbTextGray
            self.endDateLabel.textColor = UIColor.mbTextGray
            
            self.operationsHistoryApiCall(StartDate: todayDateString, EndDate: todayDateString)
            
            print("this Month Days")
            
        default:
            print("destructive")
            
        }
    }
    
    /**
     Show date picker.
     
     - parameter minDate: minimum date of picker.
     - parameter maxDate: maximum date of picker.
     - parameter currentDate: current date of picker.
     
     - returns: void
     */
    func showDatePicker(minDate : Date , maxDate : Date, currentDate : Date){
        self.showNewDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate) { (selectedDate) in
            let todayDate = Date().todayDate(dateFormate: Constants.kDisplayFormat)
            
            let selectedDateString = MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kDisplayFormat)
            
            let startFieldDate = MBPICUserSession.shared.dateFormatter.createDate(from: self.startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            let endFieldDate = MBPICUserSession.shared.dateFormatter.createDate(from: self.endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            
            
            if self.isStartDate ?? true {
                self.startDateLabel.text = selectedDateString
                self.startDateLabel.textColor = UIColor.mbBarOrange
                self.operationsHistoryApiCall(StartDate: MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kAPIFormat),
                                              EndDate: MBPICUserSession.shared.dateFormatter.createString(from:endFieldDate ?? todayDate, dateFormate: Constants.kAPIFormat))
            } else {
                self.endDateLabel.text = selectedDateString
                self.endDateLabel.textColor = UIColor.mbBarOrange
                self.operationsHistoryApiCall(StartDate: MBPICUserSession.shared.dateFormatter.createString(from: startFieldDate ?? todayDate, dateFormate: Constants.kAPIFormat),
                                              EndDate: MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kAPIFormat))
            }
        }
    }
    
    /**
     Filters Operational History.
     
     - parameter type: Type for which you want to filter.
     - parameter operationRecords: All operationalhistory record.
     - parameter allTypes: All Types.Arrary of strings.
     
     - returns: Array of OperationsRecord.
     */
    func filterHistoryBy(type: String?, operationRecords: [OperationsRecord]?, allTypes: [String]? ) -> [OperationsRecord]? {
        
        guard let myType = type else {
            return nil
        }
        
        if myType == Localized("DropDown_All"){
            return operationRecords
            
        } else if myType == Localized("DropDown_Others"){
            
            // Get all recodes which does not mach any type
            let  filterdHistoryDetailsWhichDoesNotMatchType = operationRecords?.filter() {
                
                let aRecordType = ($0 as OperationsRecord).transactionType.lowercased()
                
                if (allTypes?.contains(where: { $0.lowercased() == aRecordType })) == false {
                    return true
                } else {
                    return false
                }
            }
            
            // Get Data of selected type "others"
            let  filterdHistoryDetailsForOthers = operationRecords?.filter() {
                
                if ($0 as OperationsRecord).transactionType.lowercased() == myType.lowercased() {
                    
                    return true
                } else {
                    return false
                }
            }
            
            // Combine both filtered data of "others" type and type does not match
            var filteredData = filterdHistoryDetailsForOthers
            filteredData?.append(contentsOf: filterdHistoryDetailsWhichDoesNotMatchType ?? [])
            
            return filteredData
            
        } else {
            
            let  filterdHistoryDetails = operationRecords?.filter() {
                
                if ($0 as OperationsRecord).transactionType.lowercased() == myType.lowercased() {
                    
                    return true
                } else {
                    return false
                }
            }
            return filterdHistoryDetails
        }
        
    }
    
    //Reload table view
    func reloadTableViewData() {
        
        if filteredOperationsData?.count ?? 0 <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
        showPageNumber()
    }
    
    // MARK: - IBActions
    
    @IBAction func startBtnAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = Date().todayDate()
        
        let currentDate = MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        let threeMonthsAgos = Date().todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLabel.text ,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: MBPICUserSession.shared.dateFormatter.createDate(from: endDateString, dateFormate: Constants.kDisplayFormat) ?? todayDate,
                                currentDate: currentDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate, currentDate: currentDate)
        }
        
    }
    
    @IBAction func endBtnAction(_ sender: UIButton) {
        
        isStartDate = false;
        
        let todayDate = Date().todayDate()
        
        let currentDate = MBPICUserSession.shared.dateFormatter.createDate(from: endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        self.showDatePicker(minDate: MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate,
                            maxDate: todayDate,
                            currentDate: currentDate)
    }
    
    //MARK: - API Calls
    /// Call 'operationsHistoryApiCall' API.
    ///
    /// - returns: Void
    func operationsHistoryApiCall(StartDate startDate : String, EndDate endDate: String, loadFromDefault: Bool = false, completionHandler: @escaping (Bool) -> Void = {_ in }) {
        
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getOperationsHistory(StartDate: startDate, EndDate: endDate, { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                completionHandler(false)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let opsHistoryResponse = Mapper<OperationsHistoryHandler>().map(JSONObject:resultData) {
                        
                        self.operationsHistoryData = opsHistoryResponse.records
                        self.filteredOperationsData = self.filterHistoryBy(type: self.transactionSelectedType, operationRecords: self.operationsHistoryData, allTypes: self.transactionDropDown.dataSource)
                        
                        completionHandler(true)
                    }
                } else {
                    
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                    completionHandler(false)
                }
            }
            
            // Reload tableview data
            self.reloadTableViewData()
        })
        
    }
    
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>

extension OperationsHistoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredOperationsData?.count ?? 0
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView : OperationsHeaderView = tableView.dequeueReusableHeaderFooterView(){
            headerView.setOperationalHistoryHeaderInfo(recordInfo: filteredOperationsData?[section])
            return headerView
        }
        return UIView()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let operationsExpandCell : OperationsExpandCell = tableView.dequeueReusableCell(){
            operationsExpandCell.setOperationHistory(history: filteredOperationsData?[indexPath.section])
            
            return operationsExpandCell
        }
        return UITableViewCell()
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - <MBAccordionTableViewDelegate>

extension OperationsHistoryVC : MBAccordionTableViewDelegate {
    
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

// MARK: UIScrollViewDelegate
extension OperationsHistoryVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        showPageNumber()
    }
    
    //Shows visiable Sections count
    func showPageNumber() {
        pageCountLabel.text = "\(tableView.lastVisibleSection())/\(filteredOperationsData?.count ?? 0)"
    }
}

