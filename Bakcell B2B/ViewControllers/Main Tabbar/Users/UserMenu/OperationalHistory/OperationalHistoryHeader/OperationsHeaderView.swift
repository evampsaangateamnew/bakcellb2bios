//
//  OperationsHeaderView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/26/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class OperationsHeaderView: MBAccordionTableViewHeaderView {
    
    //MARK: - IBOutlet
    @IBOutlet var dataBundleLabel: MBLabel!
    @IBOutlet var amountLabel: MBLabel!
    @IBOutlet var dateLabel: MBLabel!
    
    @IBOutlet var endingBalanceLabel: MBLabel!
    @IBOutlet var endingBalanceView: UIView!
    
    
    //MARK: - Functions
    func setOperationalHistoryHeaderInfo(recordInfo: OperationsRecord?) {
        dateLabel.text = recordInfo?.date ?? ""
        
        dataBundleLabel.text = recordInfo?.transactionType ?? ""
        
        amountLabel.attributedText = MBUtilities.createAttributedText(recordInfo?.amount,
                                                                      textColor: .mbTextLighGray,
                                                                      withManatSign: recordInfo?.currency.isEqual("AZN", ignorCase: true) ?? false )
        
        
        if MBSelectedUserSession.shared.subscriberType() == Constants.MBSubscriberType.PostPaid {
            endingBalanceView.isHidden = true
        } else {
            endingBalanceView.isHidden = false
            endingBalanceLabel.attributedText = MBUtilities.createAttributedText(recordInfo?.endingBalance, textColor: .mbTextLighGray, withManatSign:true )
            
        }
    }
}
