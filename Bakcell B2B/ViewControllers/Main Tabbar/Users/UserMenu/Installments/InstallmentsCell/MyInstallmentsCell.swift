//
//  MyInstallmentsCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class MyInstallmentsCell: UITableViewCell {

    //View6
    @IBOutlet var purchaseValueLabel: MBLabel!
    @IBOutlet var purchaseTitleLabel: MBLabel!
    //View5
    @IBOutlet var remainingPeriodTitleLabel: MBLabel!
    @IBOutlet var remainingPeriodEndDateLabel: MBLabel!
    @IBOutlet var remainingPeriodBeginDateLabel: MBLabel!
    @IBOutlet var remainingPeriodValueLable: MBLabel!
    @IBOutlet var remainingPeriodProgress: MBGradientProgressView!
    //View4
    @IBOutlet var remainingAmountTitleLabel: MBLabel!
    @IBOutlet var remainingAmountDateLabel: MBLabel!
    @IBOutlet var remainingAmountValueLabel: MBLabel!
    @IBOutlet var remainingAmountProgress: MBGradientProgressView!
    //View3
    @IBOutlet var nextPaymentTitleLabel: MBLabel!
    @IBOutlet var nextPaymentDateLabel: MBLabel!
    @IBOutlet var nextPaymentDaysLabel: MBLabel!
    @IBOutlet var nextPaymentProgress: MBGradientProgressView!
    //View2
    @IBOutlet var amountTitleLabel: MBLabel!
    @IBOutlet var amountValueLabel: MBLabel!
    //View1
    @IBOutlet var titleLabel: MBLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundAllCorners(radius: 8)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        // Configure the view for the selected state
    }
    
    func setUpInstallments(installmentsData : Installment?) {
        
        if let aInstallmentData = installmentsData {
            var  progress = 0.0
            
            //view 1
            titleLabel.text = aInstallmentData.name
            
            //View 2
            amountTitleLabel.attributedText = MBUtilities.createAttributedText(aInstallmentData.amountValue, textColor: .mbTextGray, withManatSign: true)
            amountValueLabel.text = aInstallmentData.amountLabel
            
            //View 3 (Next paymentView)
            nextPaymentTitleLabel.text = aInstallmentData.nextPaymentLabel

            let computedValuesFromDates = MBProgressUtilities.calculateProgressValuesForInstallment(from: aInstallmentData.nextPaymentInitialDate, to: aInstallmentData.nextPaymentValue)

            nextPaymentDateLabel.text = computedValuesFromDates.endDate

            // set days left
            nextPaymentDaysLabel.text = computedValuesFromDates.daysLeftDisplayValue
            if computedValuesFromDates.daysLeft < aInstallmentData.installmentFeeLimit.toInt {
                
                nextPaymentDaysLabel.textColor = UIColor.mbBrandRed
            } else {
                
                nextPaymentDaysLabel.textColor = UIColor.black
            }

            self.nextPaymentProgress.setProgress(computedValuesFromDates.progressValue, animated: true)

            
            //View 4
            remainingAmountTitleLabel.text = aInstallmentData.remainingAmountLabel
            remainingAmountDateLabel.isHidden = true
            
            let attributedString:NSMutableAttributedString = NSMutableAttributedString()
            attributedString.append(MBUtilities.createAttributedText(aInstallmentData.remainingAmountCurrentValue, textColor: .mbTextBlack, withManatSign: true))
            attributedString.append(MBUtilities.createAttributedText(" / ", textColor: .mbTextBlack, withManatSign: false))
            attributedString.append(MBUtilities.createAttributedText(aInstallmentData.remainingAmountTotalValue, textColor: .mbTextBlack, withManatSign: true))

            remainingAmountValueLabel.attributedText = attributedString
            
            progress = (aInstallmentData.remainingAmountTotalValue.toDouble - aInstallmentData.remainingAmountCurrentValue.toDouble) / aInstallmentData.remainingAmountTotalValue.toDouble
            remainingAmountProgress.setProgress(Float(progress), animated: true)
            
            //View5

            // Begin Date
            
            if let remainingPeriodStartDate  = MBPICUserSession.shared.dateFormatter.createDate(from: aInstallmentData.remainingPeriodBeginDateValue, dateFormate: Constants.kHomeDateFormate) {

                remainingPeriodBeginDateLabel.text = "\(aInstallmentData.remainingPeriodBeginDateLabel): \(MBPICUserSession.shared.dateFormatter.createString(from: remainingPeriodStartDate, dateFormate: Constants.kDisplayFormat) )"

            } else {
                remainingPeriodBeginDateLabel.text = ""
            }

            // End Date
            if let remainingPeriodEndDate  = MBPICUserSession.shared.dateFormatter.createDate(from: aInstallmentData.remainingPeriodEndDateValue, dateFormate: Constants.kHomeDateFormate) {
                
                remainingPeriodEndDateLabel.text = "\(aInstallmentData.remainingPeriodEndDateLabel): \(MBPICUserSession.shared.dateFormatter.createString(from: remainingPeriodEndDate, dateFormate: Constants.kDisplayFormat) )"
            } else {
                remainingPeriodEndDateLabel.text = ""
            }


            
            remainingPeriodTitleLabel.text = aInstallmentData.remainingPeriodLabel
            remainingPeriodValueLable.text = "\(aInstallmentData.remainingCurrentPeriod)/\(aInstallmentData.remainingTotalPeriod)"

            progress = (aInstallmentData.remainingTotalPeriod.toDouble - aInstallmentData.remainingCurrentPeriod.toDouble) / aInstallmentData.remainingTotalPeriod.toDouble
            remainingPeriodProgress.setProgress(Float(progress), animated: true)
            
            //view 6
            if let purchaseDate = MBPICUserSession.shared.dateFormatter.createDate(from: aInstallmentData.purchaseDateValue, dateFormate: Constants.kHomeDateFormate) {
                
                purchaseValueLabel.text = "\(MBPICUserSession.shared.dateFormatter.createString(from: purchaseDate, dateFormate: Constants.kDisplayFormat) )"
            } else {
                purchaseValueLabel.text = ""
            }
            purchaseTitleLabel.text = "\(aInstallmentData.purchaseDateLabel):"
            
        }
    }
}
