//
//  MyInstallmentsVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import  ObjectMapper

class MyInstallmentsVC: BaseVC {
    // MARK: - Properties
    var installmentsData : Installments?
    
    // MARK: - IBOutlet
    @IBOutlet var installmentsDescriptionLabel: MBLabel!
    @IBOutlet var installmentsTableView: UITableView!
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var introLabel: MBLabel!
    
    // MARK: - View Controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        loadViewLayout()
        reloadTableViewData()
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        titleLabel.text = Localized("Title_Installments")
        installmentsDescriptionLabel.text = installmentsData?.installmentDescription
       
    }
    
    func reloadTableViewData()  {
        if (installmentsData?.installments?.count ?? 0 <= 0 ){
            self.installmentsTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.installmentsTableView.hideDescriptionView()
        }
        
        installmentsTableView.reloadData()
    }
}

//MARK: - TABLE VIEW DELEGATE METHODS
extension MyInstallmentsVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return installmentsData?.installments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let myInstallmentsCell = tableView.dequeueReusableCell(withIdentifier: "installmentsCell") as? MyInstallmentsCell {
            myInstallmentsCell.setUpInstallments(installmentsData: installmentsData?.installments?[indexPath.row])
            
            return myInstallmentsCell
        }
        return UITableViewCell()
    }
}
