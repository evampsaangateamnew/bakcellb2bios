//
//  UssageSummaryHeader.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/9/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

//import MarqueeLabel

class UssageSummaryHeader: MBAccordionTableViewHeaderView {

    //MARK: - IBOutlets
    @IBOutlet var plusImage: UIImageView!
    @IBOutlet var charedLabel: MBLabel!
    @IBOutlet var usageType: MBLabel!
    @IBOutlet var usage: MBLabel!
    //MARK: - Class methods
    override func awakeFromNib() {
        super.awakeFromNib()

//        usage.setupMarqueeAnimation()
    }
    //MARK: - Functions
    func setSumaryDetail(summary: SummaryList?)  {
        
        usageType.text = summary?.name  ?? ""
        usage.text = summary?.totalUsage ?? ""
        charedLabel.text = summary?.totalCharge ?? ""
        charedLabel.attributedText = MBUtilities.createAttributedText(summary?.totalCharge, textColor: .mbTextGray, withManatSign: summary?.totalCharge.isStringAnNumber() ?? true)
        
        
    }
    

  
}
