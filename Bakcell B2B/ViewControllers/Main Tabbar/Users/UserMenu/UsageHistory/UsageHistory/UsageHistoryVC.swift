//
//  UsageHistoryVC.swift
//  Bakcell
//
//  Created by AbdulRehman on 5/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class UsageHistoryVC: BaseVC {
    
    enum UsageSummaryType {
        case usageHistory
        case usageSummary
        case verifyPin
    }
    
    
    //MARK: - Properties
    var usageSummaryVC:UsageHistorySummaryVC?
    var usageDetailVC:UsageHistoryDetailVC?
    var verifyPinVC : VerifyPinVC?
    var selectedSummaryType: UsageSummaryType = .usageSummary
    
    //MARK: - IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var buttonContainorView: UIView!
    @IBOutlet var childViewControllerContainor: UIView!
    @IBOutlet var detailsButton: MBButton!
    @IBOutlet var summaryButton: MBButton!
    
    @IBOutlet var topTabViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.summaryPressed(summaryButton)
        
        /* Removed on cilent request to change.
         
         //hide tabView in case of corporate customer
        if MBSelectedUserSession.shared.userCustomerType() == Constants.MBCustomerType.Corporate {
            buttonContainorView.isHidden = true
            topTabViewHeightConstraint.constant = 0
            summaryButton.isEnabled = false
            detailsButton.isEnabled = false
            
        } else {
            buttonContainorView.isHidden = false
            topTabViewHeightConstraint.constant = 50
            
            summaryButton.isEnabled = true
            detailsButton.isEnabled = true
        }*/
        
        buttonContainorView.isHidden = false
        topTabViewHeightConstraint.constant = 50
        
        summaryButton.isEnabled = true
        detailsButton.isEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        if MBSelectedUserSession.shared.userCustomerType() != Constants.MBCustomerType.Corporate {
            //Removing usageHistory when app in background
            NotificationCenter.default.addObserver(self, selector: #selector(detailsPressed(_:)), name: NSNotification.Name(Constants.kAppDidBecomeActive), object: nil)
        }
        
        loadViewLayout()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        titleLabel.text = Localized("Title_UsageHistory")
        summaryButton .setTitle(Localized("BtnTitle_Summary"), for: UIControl.State.normal)
        detailsButton.setTitle(Localized("BtnTitle_Details"), for: UIControl.State.normal)
    }
    
    /**
     Add child view controller to parent view.
    
     - Conditions:
     
        1. UsageHistorySummaryVC: if selectedSummaryType == usageSummary
        2. UsageHistoryVC: if selectedSummaryType == usageHistory
        3. VerifyPinVC: if selectedSummaryType == verifyPin
     
     - returns: void
     */
    func reDirectUserToSelectedScreen() {
        
        if selectedSummaryType == .usageSummary {
            self.removeChildViewController(childController: UsageHistoryVC())
            self.removeChildViewController(childController: VerifyPinVC())
            
            if (usageSummaryVC == nil) {
                usageSummaryVC = UsageHistorySummaryVC.instantiateViewControllerFromStoryboard()
            }
            self.addChildViewController(childController: usageSummaryVC ?? UsageHistorySummaryVC(), onView: childViewControllerContainor)
        }
        else if selectedSummaryType == .usageHistory {
            self.removeChildViewController(childController: UsageHistorySummaryVC())
            self.removeChildViewController(childController: VerifyPinVC())
            
            if (usageDetailVC == nil) {
                usageDetailVC = UsageHistoryDetailVC.instantiateViewControllerFromStoryboard()
            }
            self.addChildViewController(childController: usageDetailVC ?? UsageHistoryVC(), onView: childViewControllerContainor)
        }
        else if selectedSummaryType == .verifyPin {
            self.removeChildViewController(childController: UsageHistorySummaryVC())
            self.removeChildViewController(childController: UsageHistoryVC())
            
            if (verifyPinVC == nil) {
                verifyPinVC = VerifyPinVC.instantiateViewControllerFromStoryboard()
            }
            self.addChildViewController(childController: verifyPinVC ?? VerifyPinVC(), onView: childViewControllerContainor)
        }
    }
    
    //MARK: IBACTIONS
    @IBAction func summaryPressed(_ sender: MBButton) {
        
        summaryButton.setButtonLayoutType(.grayButton)
        detailsButton.setButtonLayoutType(.whiteButton)
        
        selectedSummaryType = .usageSummary
        reDirectUserToSelectedScreen()
    }
    
    @IBAction func detailsPressed(_ sender: MBButton) {
        detailsButton.setButtonLayoutType(.grayButton)
        summaryButton.setButtonLayoutType(.whiteButton)
        
        if MBSelectedUserSession.shared.isOTPVerified == true {
            selectedSummaryType = .usageHistory
            reDirectUserToSelectedScreen()
            
        } else {
            selectedSummaryType = .verifyPin
            reDirectUserToSelectedScreen()
        }
    }
    
    
}
