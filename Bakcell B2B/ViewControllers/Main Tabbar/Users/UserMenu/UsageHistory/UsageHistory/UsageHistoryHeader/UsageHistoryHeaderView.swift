//
//  UsageHistoryHeaderView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 12/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class UsageHistoryHeaderView: MBAccordionTableViewHeaderView {
    
    @IBOutlet var dateTimeLabel: MBLabel!
    @IBOutlet var statusLabel: MBLabel!
    @IBOutlet var paidLabel: MBLabel!
    @IBOutlet var remainingLabel: MBLabel!
    
    //MARK: - Class Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    //MARk: - Functions
    
    func setUsageHistory(history: DetailsHistory?) {
        
        dateTimeLabel.text = history?.startDateTime ?? ""
        statusLabel.text = history?.service ?? ""
        paidLabel.text = history?.usage ?? ""
        remainingLabel.attributedText = MBUtilities.createAttributedText(history?.chargedAmount ?? "",
                                                                           textColor: .mbTextGray,
                                                                           withManatSign:history?.chargedAmount.isStringAnNumber() ?? false)
    }
    
}
