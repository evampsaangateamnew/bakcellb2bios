//
//  UsageHistorySummaryVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/15/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class UsageHistorySummaryVC: BaseVC {
    
    //MARK:- Properties
    var isStartDate : Bool?
    var usageSummaryHistoryResponse : UsageSummaryHistoryHandler?
    var selectedSectionIndexs: [Int] = []
    
    //MARK:- IBOutlet
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var usageTableVIew: MBAccordionTableView!
    @IBOutlet var pageCount: MBLabel!
    
    @IBOutlet var spacificBtnView: UIView!
    @IBOutlet var endDateButton: UIButton!
    @IBOutlet var startDateButton: UIButton!
    @IBOutlet var startDateLabel: MBLabel!
    @IBOutlet var endDateLabel: MBLabel!
    @IBOutlet var usageTypeLabel: MBLabel!
    @IBOutlet var usageLabel: MBLabel!
    @IBOutlet var chargedLabel: MBLabel!
    
    
    
    //MARK: - View Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usageTableVIew.allowMultipleSectionsOpen = true
        usageTableVIew.delegate = self
        usageTableVIew.dataSource = self
        
        reloadTableViewData()
        
        // Hidding counter because there is no counter in design on usage summery
        pageCount.isHidden = true
        
        
        spacificBtnView.isHidden = true
        
        let todayDateString = Date().todayDateString(dateFormate: Constants.kAPIFormat)
        // Load current day history
        self.getUsageSummaryHistory(startDate: todayDateString, endDate: todayDateString)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadViewLayout()
    }
    
    
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        usageTypeLabel.text = Localized("Title_Service")
        usageLabel.text = Localized("Title_Usage")
        chargedLabel.text = Localized("Title_Charged")
        setDropDown()
    }
    
    //Initialze / Setup dropdown 
    func setDropDown() {
        
        //Date Drop Down
        
        
        let datesTtitles : [String] = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_SelectPeriod")]
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        
        dateDropDown.dataSource = datesTtitles
        dateDropDown.didSelectOption { (index, option) in
            self.dateSelected(selectedDateIndex: index)
        }
        
    }
    
    /**
     Date dropdown value selected.
     
     - parameter selectedDateIndex: selected dropdown index.
     
     - returns: void
     */
    func dateSelected(selectedDateIndex: Int)  {
        
        let todayDateString = Date().todayDateString(dateFormate: Constants.kAPIFormat)
        switch selectedDateIndex{
        case 0:
            print("default")
            self.spacificBtnView.isHidden = true
            
            self.getUsageSummaryHistory(startDate: todayDateString, endDate: todayDateString)
            
        case 1:
            print("Last 7 days")
            self.spacificBtnView.isHidden = true
            
            // Get data of previous seven days
            let sevenDaysAgo = Date().todayDateString(withAdditionalValue: -7)
            self.getUsageSummaryHistory(startDate: sevenDaysAgo, endDate: todayDateString)
            
            
        case 2:
            print("Last 30 days")
            self.spacificBtnView.isHidden = true
            
            // Get data of previous thirty days
            let thirtyDaysAgo = Date().todayDateString(withAdditionalValue: -30)
            self.getUsageSummaryHistory(startDate: thirtyDaysAgo, endDate: todayDateString)
            
        case 3:
            print("Previous month")
            self.spacificBtnView.isHidden = true
            
            // Getting previous month start and end date
            let startOfPreviousMonth = Date().todayDate().getPreviousMonth()?.startOfMonth() ?? Date().todayDate()
            let endOfPreviousMonth = Date().todayDate().getPreviousMonth()?.endOfMonth() ?? Date().todayDate()
            
            let startDateString = MBPICUserSession.shared.dateFormatter.createString(from: startOfPreviousMonth, dateFormate: Constants.kAPIFormat)
            let endDateString = MBPICUserSession.shared.dateFormatter.createString(from: endOfPreviousMonth, dateFormate: Constants.kAPIFormat)
            
            self.getUsageSummaryHistory(startDate: startDateString, endDate: endDateString )
            
        case 4:
            self.spacificBtnView.isHidden = false
            
            //For date formate
            self.startDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            self.endDateLabel.text = Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            
            self.startDateLabel.textColor = UIColor.mbTextGray
            self.endDateLabel.textColor = UIColor.mbTextGray
            
            self.getUsageSummaryHistory(startDate: todayDateString, endDate: todayDateString)
            
            print("this Month Days")
            
        default:
            print("destructive")
            
        }
    }
    
    /**
     Show date picker.
     
     - parameter minDate: minimum date of picker.
     - parameter maxDate: maximum date of picker.
     - parameter currentDate: current date of picker.
     
     - returns: void
     */
    func showDatePicker(minDate : Date , maxDate : Date, currentDate : Date) {
        self.parent?.showNewDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate) { (selectedDate) in
            
            //For date formate
            let todayDate = Date().todayDate()
            
            let selectedDateString = MBPICUserSession.shared.dateFormatter.createString(from: selectedDate)
            
            let startFieldDate = MBPICUserSession.shared.dateFormatter.createDate(from: self.startDateLabel.text ?? "")
            let endFieldDate = MBPICUserSession.shared.dateFormatter.createDate(from: self.endDateLabel.text ?? "")
            
            if self.isStartDate ?? true {
                self.startDateLabel.text = selectedDateString
                self.startDateLabel.textColor = UIColor.mbBarOrange
                
                self.getUsageSummaryHistory(startDate: MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kAPIFormat),
                                            endDate: MBPICUserSession.shared.dateFormatter.createString(from:endFieldDate ?? todayDate, dateFormate: Constants.kAPIFormat))
                
            } else {
                
                self.endDateLabel.text = selectedDateString
                self.endDateLabel.textColor = UIColor.mbBarOrange
                
                self.getUsageSummaryHistory(startDate: MBPICUserSession.shared.dateFormatter.createString(from: startFieldDate ?? todayDate, dateFormate: Constants.kAPIFormat),
                                            endDate: MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kAPIFormat))
            }
        }
    }
    
    //Reload table view
    func reloadTableViewData() {
        if usageSummaryHistoryResponse?.summaryList?.count ?? 0 <= 0 {
            self.usageTableVIew.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            self.usageTableVIew.hideDescriptionView()
            
        }
        self.usageTableVIew.reloadData()
        self.showPageNumber()
    }
    
    //MARK: - API Calls
    
    /**
     Call 'getUsageSummaryHistory' API .
     
     - parameter startDate: Start date.
     - parameter startDate: End date.
     
     
     - returns: SummaryHistory
     */
    func getUsageSummaryHistory(startDate:String?, endDate:String?) {
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getUsageSummaryHistory(StartDate: startDate ?? "", EndDate: endDate ?? "",{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let usageHistoryList = Mapper<UsageSummaryHistoryHandler>().map(JSONObject: resultData){
                        
                        self.usageSummaryHistoryResponse = usageHistoryList
                        self.pageCount.text = "\(self.usageSummaryHistoryResponse?.summaryList?.count ?? 0) / \(self.usageSummaryHistoryResponse?.summaryList?.count ?? 0)"
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewData()
        })
    }
    
    
    // MARK: - IBAction
    
    @IBAction func startBtnAction(_ sender: UIButton) {
        
        isStartDate = true;
        
        let todayDate = Date().todayDate()
        
        let currentDate = MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "") ?? todayDate
        
        let threeMonthsAgos = Date().todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate
        
        if let endDateString = endDateLabel.text,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: MBPICUserSession.shared.dateFormatter.createDate(from: endDateString) ?? todayDate,
                                currentDate: currentDate)
            
        } else {
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: todayDate,
                                currentDate: currentDate)
        }
        
    }
    
    @IBAction func endBtnAction(_ sender: UIButton) {
        
        isStartDate = false;
        let todayDate =  Date().todayDate()
        
        let currentDate = MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "") ?? todayDate
        
        self.showDatePicker(minDate: MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "") ?? todayDate,
                            maxDate: todayDate,
                            currentDate: currentDate)
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate> 

extension UsageHistorySummaryVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (usageSummaryHistoryResponse?.summaryList?.count) ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerView : UssageSummaryHeader = tableView.dequeueReusableHeaderFooterView() {
            
            headerView.setSumaryDetail(summary: usageSummaryHistoryResponse?.summaryList?[section])
            
            if selectedSectionIndexs.contains(section) {
                headerView.plusImage.image  = UIImage.init(named: "circleminus")
            } else {
                headerView.plusImage.image = UIImage.init(named: "pluspressed")
            }
            
            
            return headerView
            
        }
        else {
            return MBAccordionTableViewHeaderView()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (usageSummaryHistoryResponse?.summaryList?[section].records?.count)!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 53.0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: UsageSummaryExpandCell = tableView.dequeueReusableCell(){
            
            
            if let aRecordDetail = usageSummaryHistoryResponse?.summaryList?[indexPath.section].records?[indexPath.row] {
                
                cell.label1.text = aRecordDetail.service_type
                cell.label2.text = aRecordDetail.total_usage
                
                if aRecordDetail.unit.lowercased() == Localized("Title_Voice") ||
                    aRecordDetail.unit.lowercased() == Localized("Title_SMS") {
                    
                    cell.label3.attributedText = MBUtilities.createAttributedText(aRecordDetail.chargeable_amount, textColor: .mbTextGray, withManatSign: true)
                } else {
                    cell.label3.text = aRecordDetail.chargeable_amount.appending(" ").appending(aRecordDetail.unit)
                }
                
                return cell
                
            }
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
}

// MARK: - <MBAccordionTableViewDelegate>

extension UsageHistorySummaryVC : MBAccordionTableViewDelegate {
    
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? UssageSummaryHeader {
            
            headerView.plusImage.image  = UIImage.imageFor(name:  "circleminus")
            showPageNumber()
            
            // Adding section from selectedSectionIndexs
            selectedSectionIndexs.append(section)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        
        if let headerView = header as? UssageSummaryHeader {
            headerView.plusImage.image = UIImage.imageFor(name:  "pluspressed")
            
            showPageNumber()
            
            // Removing section from selectedSectionIndexs
            if let sectionValue = selectedSectionIndexs.firstIndex(of: section) {
                selectedSectionIndexs.remove(at: sectionValue)
            }
        }
    }
}

extension UsageHistorySummaryVC:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        showPageNumber()
    }
    
    // Set visible sections and total sections
    func showPageNumber() {
        
        pageCount.text = "\(usageTableVIew.lastVisibleSection() )/\(usageSummaryHistoryResponse?.summaryList?.count ?? 0)"
    }
}
