//
//  UsageHistoryDetailVC.swift
//  Bakcell
//
//  Created by AbdulRehman on 5/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper


class UsageHistoryDetailVC: BaseVC {
    
    //MARK:- Properties
    var usageDetailsHistoryCompleteResponse : UsageDetailsHistoryHandler = UsageDetailsHistoryHandler()
    var usageDetailsHistoryFilteredData : UsageDetailsHistoryHandler = UsageDetailsHistoryHandler()
    var isStartDate : Bool!
    var selectedOption: String = "All"
    
    //MARK:- IBOutlet
    
    @IBOutlet var pageCount: MBLabel!
    
    @IBOutlet var categoryDropDown: UIDropDown!
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var tableView: MBAccordionTableView!
    
    @IBOutlet var spacificBtnView: UIView!
    @IBOutlet var endDateButton: UIButton!
    @IBOutlet var startDateButton: UIButton!
    @IBOutlet var startDateLabel: MBLabel!
    @IBOutlet var endDateLabel: MBLabel!
    @IBOutlet var dateLabel: MBLabel!
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var usageLabel: MBLabel!
    @IBOutlet var chargeLabel: MBLabel!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsMultipleSelection = true
        spacificBtnView.isHidden = true
        
        
        // Load Initial data
        let todayDateString = Date().todayDateString(dateFormate: Constants.kAPIFormat)
        
        self.getUsageDetailsHistory(startDate: todayDateString, endDate: todayDateString, withAnimation: false) { (canShowDisclaimer) in
            
            if canShowDisclaimer == true && DisclaimerMessageVC.canShowDisclaimerOfType(.UsageHistory) == true {
                if let disclaimerMessageVC :DisclaimerMessageVC = DisclaimerMessageVC.instantiateViewControllerFromStoryboard() {
                    
                    disclaimerMessageVC.setDisclaimerAlertWith(description: Localized("Disclaimer_Description"), okBtnClickedBlock: { (dontShowAgain) in
                        
                        if dontShowAgain == true {
                            DisclaimerMessageVC.setDisclaimerStatusForType(.UsageHistory, canShow: false)
                        }
                    })
                    
                    if self.navigationController?.parent != nil {
                        self.navigationController?.parent?.presentPOPUP(disclaimerMessageVC, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
                    } else {
                        self.presentPOPUP(disclaimerMessageVC, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadViewController()
    }
    
    
    //MARK: - FUNCTIONS
    //Set localized for labels
    func loadViewController(){
        
        dateLabel.text = Localized("Title_Date")
        titleLabel.text = Localized("Title_Service")
        usageLabel.text = Localized("Title_Usage")
        chargeLabel.text = Localized("Title_Charged")
    
        setUpDropDown()
    }
    
    //Initialze / Setup dropdown 
    func setUpDropDown() {
        
        //Category Drop Down
        let categoryTtitles : [String] = [Localized("DropDown_All"),Localized("DropDown_Internet"), Localized("DropDown_Voice"), Localized("DropDown_SMS"),Localized("DropDown_Others")]
        categoryDropDown.placeholder = Localized("DropDown_All")
        
        categoryDropDown.dataSource = categoryTtitles
        categoryDropDown.didSelectOption { (index, option) in
            self.setSelectedCategory(category: option)
        }
        
        
        //Date Drop Down
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        
        let dates : [String] = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_SelectPeriod")]
        
        dateDropDown.dataSource = dates
        dateDropDown.didSelectOption { (index, option) in
            self.dateSelected(selectedDateIndex: index)
        }
    }
    
    /**
     Date dropdown selection.
     
     - parameter selectedDateIndex: Selected dropdown indx.
     
     - returns: void
     */
    func dateSelected(selectedDateIndex:Int)    {
        let todayDateString = Date().todayDateString(dateFormate: Constants.kAPIFormat)
        
        
        switch selectedDateIndex{
        case 0:
            print("default")
            self.spacificBtnView.isHidden = true
            
            self.getUsageDetailsHistory(startDate: todayDateString, endDate: todayDateString)
            
        case 1:
            print("Last 7 days")
            self.spacificBtnView.isHidden = true
            
            // Get data of previous seven days
            let sevenDaysAgo = Date().todayDateString(withAdditionalValue: -7)
            self.getUsageDetailsHistory(startDate: sevenDaysAgo, endDate: todayDateString)
            
            
        case 2:
            print("Last 30 days")
            self.spacificBtnView.isHidden = true
            
            // Get data of previous thirty days
            let thirtyDaysAgo = Date().todayDateString(withAdditionalValue: -30)
            self.getUsageDetailsHistory(startDate: thirtyDaysAgo, endDate: todayDateString)
            
        case 3:
            print("Previous month")
            self.spacificBtnView.isHidden = true
            
            // Getting previous month start and end date
            let startOfPreviousMonth = Date().todayDate().getPreviousMonth()?.startOfMonth() ?? Date().todayDate()
            let endOfPreviousMonth =  Date().todayDate().getPreviousMonth()?.endOfMonth() ?? Date().todayDate()
            
            let startDateString = MBPICUserSession.shared.dateFormatter.createString(from: startOfPreviousMonth, dateFormate: Constants.kAPIFormat)
            let endDateString = MBPICUserSession.shared.dateFormatter.createString(from: endOfPreviousMonth, dateFormate: Constants.kAPIFormat)
            
            self.getUsageDetailsHistory(startDate: startDateString, endDate: endDateString)
            
        case 4:
            self.spacificBtnView.isHidden = false
            
            // For specific period of time
            
            self.startDateLabel.text =  Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            self.endDateLabel.text =  Date().todayDateString(dateFormate: Constants.kDisplayFormat)
            
            self.startDateLabel.textColor = UIColor.mbTextGray
            self.endDateLabel.textColor = UIColor.mbTextGray
            
            self.getUsageDetailsHistory(startDate: todayDateString, endDate: todayDateString)
            
            
        default:
            print("destructive")
            
        }
    }
    
    /**
     Filter usage history.
     
     - parameter category: Selected category.
     
     - returns: void
     */
    func setSelectedCategory(category: String) {
        
        switch category {
        case Localized("DropDown_All"):
            self.selectedOption = "All"
            
        case Localized("DropDown_Internet"):
            self.selectedOption = "data"
            
        case Localized("DropDown_Voice"):
            self.selectedOption = "voice"
            
        case Localized("DropDown_SMS"):
            self.selectedOption = "sms"
            
        case Localized("DropDown_Others"):
            self.selectedOption = "others"
            
        default:
            break
        }
        
        let records : [DetailsHistory]? = self.filterUsageDetailBy(type: self.selectedOption, usageDetails: self.usageDetailsHistoryCompleteResponse.records)
        
        self.usageDetailsHistoryFilteredData.records = records
        
        
        self.reloadTableViewData()
    }
    
    /**
     Show date picker.
     
     - parameter minDate: minimum date of picker.
     - parameter maxDate: maximum date of picker.
     - parameter currentDate: current date of picker.
     
     - returns: void
     */
    func showDatePicker(minDate : Date , maxDate : Date, currentDate:Date) {
        self.parent?.showNewDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate) { (selectedDate) in
           
            let todayDate = Date().todayDate()
            
            let selectedDateString = MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kDisplayFormat)
            
            let startFieldDate = MBPICUserSession.shared.dateFormatter.createDate(from: self.startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
            let endFieldDate = MBPICUserSession.shared.dateFormatter.createDate(from: self.endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat)
          
            if self.isStartDate ?? true {
                self.startDateLabel.text = selectedDateString
                self.startDateLabel.textColor = UIColor.mbBarOrange
                
                self.getUsageDetailsHistory(startDate: MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kAPIFormat),
                                            endDate: MBPICUserSession.shared.dateFormatter.createString(from:endFieldDate ?? todayDate, dateFormate: Constants.kAPIFormat))
                
            } else {
                
                self.endDateLabel.text = selectedDateString
                
                self.endDateLabel.textColor = UIColor.mbBarOrange
                self.getUsageDetailsHistory(startDate: MBPICUserSession.shared.dateFormatter.createString(from: startFieldDate ?? todayDate, dateFormate: Constants.kAPIFormat),
                                            endDate: MBPICUserSession.shared.dateFormatter.createString(from: selectedDate, dateFormate: Constants.kAPIFormat))
            }
        }
    }
    
    func reloadTableViewData() {
        
        if usageDetailsHistoryFilteredData.records?.count ?? 0 <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
        
        self.showPageNumber()
        
    }
    
    /**
     Filter Usagehistorhy detail.
     
     - parameter type: String value of usage type.
     - parameter usageDetails: Array of DetailsHistory.
     
     
     - returns: Array of DetailsHistory.
     */
    func filterUsageDetailBy(type:String?, usageDetails : [DetailsHistory]? ) -> [DetailsHistory]? {
        
        guard let myType = type else {
            return []
        }
        
        if myType.isEqual("All", ignorCase: true) {
            return usageDetails
        }
        
        // Filtering offers array
        var filterdUsageDetails : [DetailsHistory] = []
        
        if let offers = usageDetails {
            
            for aDetailHistory in offers {
                
                if let historyDataType = aDetailHistory.type {
                    
                    if historyDataType.lowercased() == myType.lowercased() {
                        
                        filterdUsageDetails.append(aDetailHistory)
                    }
                }
            }
            
        } else {
            return []
        }
        
        return filterdUsageDetails
    }
    
    
    // MARK: - IBActions
    
    @IBAction func startBtnAction(_ sender: UIButton) {
        
        isStartDate = true;
        let todayDate = Date().todayDate()
        
        let currentDate = MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
        
        let threeMonthsAgos = Date().todayDate().getMonth(WithAdditionalValue: -3)?.startOfMonth() ?? todayDate

        if let endDateString = endDateLabel.text,
            endDateString.isBlank == false {
            
            self.showDatePicker(minDate: threeMonthsAgos,
                                maxDate: MBPICUserSession.shared.dateFormatter.createDate(from: endDateString, dateFormate: Constants.kDisplayFormat) ?? todayDate,
                                currentDate: currentDate)
            
        } else {
            
            self.showDatePicker(minDate: threeMonthsAgos, maxDate: todayDate, currentDate: currentDate)
        }
        
    }
    
    @IBAction func endBtnAction(_ sender: UIButton) {
        
        isStartDate = false;
        let todayDate = Date().todayDate()
        
        let currentDate = MBPICUserSession.shared.dateFormatter.createDate(from: endDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate
       
        self.showDatePicker(minDate: MBPICUserSession.shared.dateFormatter.createDate(from: startDateLabel.text ?? "", dateFormate: Constants.kDisplayFormat) ?? todayDate,
                            maxDate: todayDate,
                            currentDate: currentDate)
    }
    
    //MARK: - API Calls
    
    /**
     Call 'getUsageDetailsHistory' API .
     
     - parameter startDate: Start date.
     - parameter startDate: End date.
     - parameter withAnimation: Bool for animation. Bydefault it's true.
     
     - returns: records
     */
    func getUsageDetailsHistory(startDate:String?, endDate:String?, withAnimation : Bool = true, completionHandler: @escaping (Bool) -> Void = {_ in }) {
       
        MBActivityIndicator.shared.showActivityIndicator(withAnimation: withAnimation)
        
        _ = MBAPIClient.sharedClient.getUsageDetailsHistory(StartDate: startDate ?? "", EndDate: endDate ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            MBActivityIndicator.shared.hideActivityIndicator(withAnimation: withAnimation)
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                completionHandler(false)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let usageHistoryList = Mapper<UsageDetailsHistoryHandler>().map(JSONObject: resultData){
                        
                        self.usageDetailsHistoryCompleteResponse = usageHistoryList.copy()
                        
                        self.usageDetailsHistoryFilteredData.records = self.filterUsageDetailBy(type: self.selectedOption, usageDetails: usageHistoryList.records)
                        
                        completionHandler(true)
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            self.reloadTableViewData()
        })
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>

extension UsageHistoryDetailVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (usageDetailsHistoryFilteredData.records?.count) ?? 0
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView : UsageHistoryHeaderView = tableView.dequeueReusableHeaderFooterView(){
            
            
            headerView.setUsageHistory(history: usageDetailsHistoryFilteredData.records?[section])
            
            return headerView}
        return MBAccordionTableViewHeaderView()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell : UsageHistoryExpandCell = tableView.dequeueReusableCell(){
            cell.setHistoryInfo(history: usageDetailsHistoryFilteredData.records?[indexPath.section])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    }

// MARK: - <MBAccordionTableViewDelegate>

extension UsageHistoryDetailVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        showPageNumber()
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}
//MARK: - ScroolView Delegate
extension UsageHistoryDetailVC : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        showPageNumber()
    }
    
    func showPageNumber() {
        
        pageCount.text = "\(tableView.lastVisibleSection())/\(usageDetailsHistoryFilteredData.records?.count ?? 0)"
    }
}
