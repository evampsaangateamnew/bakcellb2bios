//
//  UsageHistoryExpandCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 11/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class UsageHistoryExpandCell: UITableViewCell {

    @IBOutlet var numberLabel: MBLabel!
    @IBOutlet var toLabel: MBLabel!
    @IBOutlet var destDetailLabel: MBLabel!
    @IBOutlet var periodLabel: MBLabel!
    @IBOutlet var typeLabel: MBLabel!
    @IBOutlet var destinationLabel: MBLabel!
    @IBOutlet var zoneLabel: MBLabel!
    @IBOutlet var peakLabel: MBLabel!
    
    //MARK:- Cell Class Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        loadViewLayout()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Functions
    
    //Set Localized for labels
    func loadViewLayout()  {
        toLabel.text = Localized("Title_To")
        peakLabel.text = Localized("Expand_PeakOffPeak")
        zoneLabel.text = Localized("Expand_Zone")
        destinationLabel.text = Localized("Expand_Destination")
    }
    
    //Set history details
    func setHistoryInfo(history: DetailsHistory?)  {
        numberLabel.text = history?.number ?? ""
        typeLabel.text = history?.period ?? ""
        periodLabel.text = history?.zone ?? ""
        destDetailLabel.text = history?.destination ?? ""
    }
    
}
