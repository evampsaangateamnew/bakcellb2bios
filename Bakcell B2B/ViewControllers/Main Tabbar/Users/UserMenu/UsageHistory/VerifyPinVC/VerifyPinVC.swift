//
//  VerifyPinVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 5/29/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class VerifyPinVC: BaseVC {
    
    //MARK: - Properties
    var numberText : String?
    var pinText : String?
    
    //MARK: - IBOutlet
    @IBOutlet var categoryDropDown: UIDropDown!
    @IBOutlet var dateDropDown: UIDropDown!
    @IBOutlet var pinTextField: UITextField!
    @IBOutlet var descriptionLabel: MBLabel!
    @IBOutlet var pinNotRecieveLabel: MBLabel!
    @IBOutlet var submitButton: MBButton!
    @IBOutlet var resendLabel: MBLabel!
    
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadViewController()
        
        pinTextField.delegate = self
        setDropDown()
        sendPin()
    }
    
    //MARK: - FUNCTIONS
    
    //Set localized for labels
    func loadViewController(){
        descriptionLabel.text = Localized("Title_PinDescription")
        pinNotRecieveLabel.text = Localized("Title_PinNotRecieved")
        resendLabel.text = Localized("Title_RESEND")
        submitButton.setTitle(Localized("BtnTitle_Submit"), for: UIControl.State.normal)
        
        pinTextField.textColor = UIColor.mbTextGray
        pinTextField.attributedPlaceholder =  NSAttributedString(string: Localized("PlaceHolder_EnterPIN"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.mbTextGray])
    }
    
   //Initialze dropdown with data sourece
    func setDropDown() {
        
        //Category Drop Down
        let categoryTtitles : [String] = [Localized("DropDown_All"),Localized("DropDown_Internet"), Localized("DropDown_Voice"), Localized("DropDown_SMS"),Localized("DropDown_Others")]
        categoryDropDown.placeholder = Localized("DropDown_All")
        
        categoryDropDown.dataSource = categoryTtitles
        
        
        //Date Drop Down
        dateDropDown.placeholder = Localized("DropDown_CurrentDay")
        
        let dates : [String] = [Localized("DropDown_CurrentDay"),Localized("DropDown_Last7Days"), Localized("DropDown_Last30Days"), Localized("DropDown_PreviousMonth"),Localized("DropDown_CustomPeriod")]
        
        dateDropDown.dataSource = dates
        
        
    }
    
    //MARK: - API Calls
    
    /**
     Call 'verifypin' API .
     
     - parameter accountId: String value of account id.
     - parameter pin: String value of PIN.
     
     - returns: void
     */
    func verifyPin(accountId:String?, pin:String?) {
        
        MBActivityIndicator.shared.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.verifyPin(UserName: MBPICUserSession.shared.msisdn, RequestType: .UsageHistory, Pin: pin?.trimmWhiteSpace ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            MBActivityIndicator.shared.hideActivityIndicator(withAnimation: false)
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    
                    if let _ = Mapper<MessageResponse>().map(JSONObject: resultData) {
                        
                        MBSelectedUserSession.shared.isOTPVerified = true
                        if let usageSummaryVC = self.parent as? UsageHistoryVC {
                            
                            if let usageHistory :UsageHistoryDetailVC = UsageHistoryDetailVC.instantiateViewControllerFromStoryboard() {
                                
                                UIView.transition(with: usageSummaryVC.childViewControllerContainor, duration: 0.5, options: .transitionFlipFromRight, animations: {
                                    usageSummaryVC.addChildViewController(childController: usageHistory, onView: usageSummaryVC.childViewControllerContainor)
                                    usageSummaryVC.removeChildViewController(childController: VerifyPinVC())
                                    
                                    
                                }, completion:nil)
                            }
                        }
                    } else {
                        
                        // Show error alert to user
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                }
            }
        })
    }
    
    
    /**
     Call 'reSendPin' API for logged in user 'MSISDN'.
     
     - parameter UserName: UserName.
     - parameter ofType: Resend type.
     
     - returns: void
     */
    func reSendPin(UserName selectedUserName : String, ofType : Constants.MBResendType ) {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.historyReSendPIN(UserName: MBPICUserSession.shared.msisdn, ofType: ofType,  { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                
                self.showAlertWithMessage(message: resultDesc)
            }
        })
    }
    
    /**
     Call 'sendPin' API for logged in user 'MSISDN'.
     
     - returns: void
     */
    func sendPin() {
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.sendPinForSelectedUser(UserName: MBPICUserSession.shared.msisdn , { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
//                self.showAlertWithMessage(message: resultDesc)
            }
        })
    }
    
    
    //MARK: IBACTIONS
    @IBAction func submitPressed(_ sender: Any) {
        var otp : String = ""
        // OTP validation
        if let otpText = pinTextField.text,
            otpText.lengthWithOutSpace == 4 {
            
            otp = otpText.trimmWhiteSpace
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidOTP"))
            return
        }
        pinTextField.resignFirstResponder()
        
        // API call for OTP verification
        self.verifyPin(accountId: numberText, pin: otp)
        
    }
    
    @IBAction func reSendPinPressed(_ sender: Any) {
        // API call for resending OTP
        reSendPin(UserName: MBSelectedUserSession.shared.userName(), ofType: .UsageHistory)
    }
    
    
}


//MARK: - Textfield delagates

extension VerifyPinVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        
        //Set max Limit for textfields
        let newLength = text.count + string.count - range.length
        
        //  OTP textfield
        if textField == pinTextField {
            
            // limiting OTP lenght to 4 digits
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            
            if newLength <= Constants.VerifyPinLenght && allowedCharacters.isSuperset(of: characterSet) {
                // Next button enable disable
                if newLength == Constants.VerifyPinLenght {
                    submitButton.isEnabled = true
                    submitButton.alpha = 1
                } else {
                    submitButton.isEnabled = false
                    submitButton.alpha = 0.5
                }
                // Returnimg number input
                return  true
            } else {
                return  false
            }
        } else  {
            return false
        }
    }
}
