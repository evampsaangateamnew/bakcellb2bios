//
//  UserInfoCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/29/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class UserInfoCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet var nameLabel : MBMarqueeLabel!
    @IBOutlet var misdnLabel: MBLabel!
    @IBOutlet var currentTarrifLabel: MBLabel!
    @IBOutlet var currentTarrifStatusLabel: MBLabel!
    @IBOutlet var currentTarrifButton: MBButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadViewLayout()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Functions
    func loadViewLayout(){
        nameLabel.textColor = .mbTextBlack
    }
    
    func setUserInfo(userName:String?, msisdn:String?, tarrifStatus isUserActive: Bool? , tarrifStatusColor statusColor: UIColor, tarrifStatusText : String?, tarrifsTitle: String?) {
        loadViewLayout()
        nameLabel.text = userName ?? ""
        misdnLabel.text =  "+994 " + (msisdn ?? "").trimmWhiteSpace
        currentTarrifLabel.text = tarrifsTitle ?? ""
        
        if (isUserActive ?? false) == true {
            currentTarrifStatusLabel.text = Localized("Info_Active")
            
        } else {
            currentTarrifStatusLabel.text = tarrifStatusText   
        }
        
        currentTarrifStatusLabel.textColor = statusColor
        
    }
    
}
