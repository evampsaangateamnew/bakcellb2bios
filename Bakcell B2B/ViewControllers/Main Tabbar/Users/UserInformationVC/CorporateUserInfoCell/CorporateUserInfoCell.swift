//
//  CorporateUserInfoCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/9/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class CorporateUserInfoCell: UITableViewCell {
    //MARK:- IBOutlets

    
    @IBOutlet var outStandingIndivisualDebitTitleLabel : MBLabel!
    @IBOutlet var outStandingIndivisualDebitValue : MBLabel!
    @IBOutlet var corporateTitleLabel: MBLabel!
    @IBOutlet var indivisualTitleLabel: MBLabel!
    @IBOutlet var balanceTitleLabel : MBLabel!
    @IBOutlet var corporateBalance : MBLabel!
    @IBOutlet var indivisualBalance: MBLabel!
    @IBOutlet var currentCreditTitleLabel : MBLabel!
    @IBOutlet var corporateCurrentCredit : MBLabel!
    @IBOutlet var indivisualCurrentCredit: MBLabel!
    @IBOutlet var availableCreditTitleLabel : MBLabel!
    @IBOutlet var corporateAvailableCredit : MBLabel!
    @IBOutlet var indivisualAvailableCredit: MBLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        loadViewLayout()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Functions
//    func loadViewLayout(){
//        balanceTitleLabel.text = Localized("TitleLabel_Balance")
//        corporateTitleLabel.text = Localized("Title_Corporate")
//        outStandingIndivisualDebitTitleLabel.text = Localized("Title_OutStandingIndivisualDebt")
//        indivisualTitleLabel.text = Localized("Title_Indivisual")
//        currentCreditTitleLabel.text = Localized("Title_CurrentCredit")
//        availableCreditTitleLabel.text = Localized("Title_AvailableCredit")
//        
//    }
    
    func setCorporateUserInfo(userPaymentInformation: Postpaid?)  {
        
        outStandingIndivisualDebitTitleLabel.text = userPaymentInformation?.outstandingIndividualDebtLabel ?? ""
        outStandingIndivisualDebitValue.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.outstandingIndividualDebt ?? "0", textColor: .black)
        
        indivisualTitleLabel.text = userPaymentInformation?.individualLabel ?? ""
        corporateTitleLabel.text = userPaymentInformation?.corporateLabel ?? ""
        
        balanceTitleLabel.text = userPaymentInformation?.balanceLabel ?? ""
        corporateBalance.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.balanceCorporateValue ?? "0", textColor: .black)
        indivisualBalance.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.balanceIndividualValue ?? "0", textColor: .black)
        
        currentCreditTitleLabel.text = userPaymentInformation?.currentCreditLabel ?? ""
        corporateCurrentCredit.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.currentCreditCorporateValue ?? "0", textColor: .black)
        indivisualCurrentCredit.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.currentCreditIndividualValue ?? "0", textColor: .black)
        
        availableCreditTitleLabel.text = userPaymentInformation?.availableCreditLabel ?? ""
        corporateAvailableCredit.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.availableBalanceCorporateValue ?? "0", textColor: .black)
        indivisualAvailableCredit.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.availableBalanceIndividualValue ?? "0", textColor: .black)
        
    }
}
