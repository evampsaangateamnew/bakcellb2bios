//
//  UserMenuCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/29/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit


class UserMenuCell: UICollectionViewCell {
    //MARK: -  IBOutlets
    @IBOutlet var menuItemImage: UIImageView!
    @IBOutlet var menuItemTitle: MBMarqueeLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Functions
    func setMenuInfo(MenuInformation menuInfo: Items){
        menuItemTitle.lineBreakMode = .byWordWrapping
        menuItemTitle.text = menuInfo.title
        menuItemImage.image = UIImage.imageFor(name: menuInfo.iconName)
    }
}
