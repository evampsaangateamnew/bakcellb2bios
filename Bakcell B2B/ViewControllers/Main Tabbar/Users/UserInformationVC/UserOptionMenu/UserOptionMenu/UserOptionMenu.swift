//
//  UserOptionMenu.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/29/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
@objc
protocol UserOptionMenuProtocoal {
    func menuOptionTapped(_ identifire:String)
}
class UserOptionMenu: UITableViewCell {

    //MARKL - Properties
    var menuItemsList : [Items] = [Items(identifier: "packages", title: Localized("Title_Subscription"), sortOrder: "", iconName: "my-subscriptions"),
                                   /*MenuList(identifier: "subscription", title: Localized("Title_Subscriptions"), sortOrder: "", iconName: "my-subscriptions"),*/
                                      Items(identifier: "core_services", title: Localized("Title_CoreServices"), sortOrder: "", iconName: "manage-number"),
                                      Items(identifier: "usage_history", title: Localized("Title_UsageHistory"), sortOrder: "", iconName: "usermenu-usage-history"),
                                      Items(identifier: "operation_history", title: Localized("Title_OperationHistory"), sortOrder: "", iconName: "usermenu-operation-history"),
                                      Items(identifier: "invoices", title: Localized("Title_Invoice"), sortOrder: "", iconName: "usermenu-invoice"),
                                      Items(identifier: "installments", title: Localized("Title_Installments"), sortOrder: "", iconName: "usermenu-installments"),
                                      Items(identifier: "user_profile", title: Localized("Title_UserProfile"), sortOrder: "", iconName: "usermenu-profile")]

    
    weak var delegate:UserOptionMenuProtocoal?
    
    //MARKL - IBOutlets
    @IBOutlet var optionCollectionView : UICollectionView!
    @IBOutlet weak var optionCollectionViewHeight: NSLayoutConstraint!
    
    //MARKL - Cell methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let screenWidth = UIScreen.main.bounds.size.width - 42
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        flowLayout.minimumLineSpacing = 5.0
        flowLayout.minimumInteritemSpacing = 5.0
        self.optionCollectionView.collectionViewLayout = flowLayout
        
        self.optionCollectionView.dataSource = self
        self.optionCollectionView.delegate = self
       
        
        let userMenuCell = UINib(nibName: "UserMenuCell", bundle: nil)
        self.optionCollectionView.register(userMenuCell, forCellWithReuseIdentifier: "UserMenuCell")
        optionCollectionViewHeight.constant = CGFloat(Int((UIScreen.main.bounds.size.width / 3 * getHeigtMultiplyer()) - (getHeigtMultiplyer() - 1) * 4 - 15))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Functions
    
    /**
     Get height for collection view.
     
     - Contditions:
        1. if items count modulus '3' is not equal to zero mean we have an extra cell. So menuItemsList.count / 3 + 1.
        2.menuItemsList.count / 3 + 1.
     - returns: HeightofCollectionView
     */
    func getHeigtMultiplyer() -> CGFloat {
        if (menuItemsList.count % 3 != 0) {
            return  CGFloat(menuItemsList.count / 3 + 1)
        }
        else{
            return CGFloat(menuItemsList.count / 3)
        }
    }
    
}
extension UserOptionMenu: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItemsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserMenuCell", for: indexPath) as? UserMenuCell{
            cell.setMenuInfo(MenuInformation: menuItemsList[indexPath.row])
    
            return cell
        }
        return UICollectionViewCell()
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.menuOptionTapped(menuItemsList[indexPath.item].identifier)
    }
    
    
    
}
