//
//  PostPaidUserInfoCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/9/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class IndivisualUserInfoCell: UITableViewCell {
    //MARK:- IBOutlets
    
    @IBOutlet var outStandingDebtTitleLabel : MBLabel!
    @IBOutlet var outStandingDebtValue : MBLabel!
    @IBOutlet var balanceTitleLabel : MBLabel!
    @IBOutlet var balance: MBLabel!
    @IBOutlet var currentCreditTitleLabel : MBLabel!
    @IBOutlet var currentCredit : MBLabel!
    @IBOutlet var availableCreditTitleLabel : MBLabel!
    @IBOutlet var availableCredit : MBLabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //        loadViewLayout()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Functions
    //    func loadViewLayout(){
    //        balanceTitleLabel.text = Localized("TitleLabel_Balance")
    //        outStandingDebtTitleLabel.text = Localized("Title_OutStandingDebt")
    //        currentCreditTitleLabel.text = Localized("Title_CurrentCredit")
    //        availableCreditTitleLabel.text = Localized("Title_AvailableCredit")
    //
    //    }
    func setIndividualCorporateUserInfo(userPaymentInformation: Postpaid?)  {
        
        
        outStandingDebtTitleLabel.text = userPaymentInformation?.outstandingIndividualDebtLabel ?? ""
        outStandingDebtValue.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.outstandingIndividualDebt ?? "0", textColor: .black)
        
        balanceTitleLabel.text = userPaymentInformation?.balanceLabel ?? ""
        balance.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.balanceIndividualValue ?? "0", textColor: .black)
        
        currentCreditTitleLabel.text = userPaymentInformation?.currentCreditLabel ?? ""
        currentCredit.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.currentCreditIndividualValue ?? "0", textColor: .black)
        
        availableCreditTitleLabel.text = userPaymentInformation?.availableCreditLabel ?? ""
        availableCredit.attributedText =  MBUtilities.createAttributedText(userPaymentInformation?.availableBalanceIndividualValue ?? "0", textColor: .black)
        
    }
    
}
