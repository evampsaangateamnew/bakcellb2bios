//
//  UserInformationVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/29/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class UserInformationVC: BaseVC {
    
    enum UserInformationTypes {
        case tarrifsInfo
        case menuInfo
        case MRC
        case payBySUBs
        case fullAndPartialPay
        
    }
    //MARK: - Properties
    var userInfoType : [UserInformationTypes] = []
    
    var selectedUser : UsersData?
    var userInformation  : IndividualUserInformation?
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var userInformationTableView: UITableView!
    
    //Mark: -  View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        userInformationTableView.delegate = self
        userInformationTableView.dataSource = self
        userInformationTableView.estimatedRowHeight = 100
        
        getUserInfo()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    override func backPressed(_ sender: AnyObject) {
        super.backPressed(sender)
        MBSelectedUserSession.shared.clearUserSession()
    }
    
    //MARK: - Functions
    func loadViewLayout()  {
        titleLabel.text = Localized("Title_UserInformation")
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter viewControllerIdentifer: key for selected controller
     
     - returns: void
     */
    func redirectUserToViewController(viewControllerIdentifer: String) {
        switch viewControllerIdentifer {
        case "core_services":
            if let coreServicesVC :CoreServicesVC = CoreServicesVC.instantiateViewControllerFromStoryboard() {
                coreServicesVC.selectionType = .individual
                self.navigationController?.pushViewController(coreServicesVC, animated: true)
            }
            break
        case "packages":
            if let subscriptionVC :MySubscribeVC = MySubscribeVC.instantiateViewControllerFromStoryboard() {
                
                self.navigationController?.pushViewController(subscriptionVC, animated: true)
            }
            break
            /* case "subscription":
             if let subscriptionVC = MySubscribeVC.instantiateViewControllerFromStoryboard() {
             
             self.navigationController?.pushViewController(subscriptionVC, animated: true)
             }
             break */
            
        case "usage_history":
            if let usageHistoryMainVC :UsageHistoryVC = UsageHistoryVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(usageHistoryMainVC, animated: true)
            }
            break
        case "operation_history":
            if let operationsHistoryVC :OperationsHistoryVC = OperationsHistoryVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(operationsHistoryVC, animated: true)
            }
            break
        case "invoices":
            //Load MSISDNInvoiceInfoMainVC ViewController
            if let msisdnInvoiceInfoMainVC :MSISDNInvoiceInfoMainVC = MSISDNInvoiceInfoMainVC.instantiateViewControllerFromStoryboard() {
                msisdnInvoiceInfoMainVC.selectedUserMSISDN = selectedUser?.msisdn ?? ""
                msisdnInvoiceInfoMainVC.selectedUserFullName = selectedUser?.custFullName ?? ""
                self.navigationController?.pushViewController(msisdnInvoiceInfoMainVC, animated: true)
            }
            break
        case "installments":
            if let myInstallmentsVC :MyInstallmentsVC = MyInstallmentsVC.instantiateViewControllerFromStoryboard() {
                myInstallmentsVC.installmentsData = userInformation?.homePageData?.installments
                self.navigationController?.pushViewController(myInstallmentsVC, animated: true)
            }
            break
        case "user_profile":
            if let myProfile :MyProfileVC = MyProfileVC.instantiateViewControllerFromStoryboard() {
                myProfile.selectedProfileType = .individual
                self.navigationController?.pushViewController(myProfile, animated: true)
            }
            break
            
        default:
            break
        }
        
    }
    
    @objc func redirecteToTarrifsVC(){
        if let aViewController :TariffsMainVC = TariffsMainVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(aViewController, animated: true)
        }
    }
    
    func reloadTableViewData() {
        
        if (userInfoType.count <= 0 ){
            self.userInformationTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.userInformationTableView.hideDescriptionView()
        }
        
        userInformationTableView.reloadData()
    }
    
    
    //MARK:- API Calls
    
    func getUserInfo(){
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.getUserInformation(userMSISDN: selectedUser?.msisdn ?? "",{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let userInformationResponse = Mapper<IndividualUserInformation>().map(JSONObject:resultData) {
                        
                        self.userInformation = userInformationResponse
                        
                        /*Setting data into user Shared object*/
                        MBSelectedUserSession.shared.userInfo = userInformationResponse.customerData
                        
                        self.userInfoType = [.tarrifsInfo]
                        
                        if (userInformationResponse.homePageData?.balance?.postpaid) != nil {
                            
                            /* Check for user group type if PayBySUB then show Individual information view else show Individual and Corporate information View */
                            if (self.selectedUser?.groupName.isGroupPayBySUBs() ?? false) {
                                self.userInfoType.append(.payBySUBs)
                            } else {
                                self.userInfoType.append(.fullAndPartialPay)
                            }
                            
                            /* Forcefully updating GroupType because it is empty in userinfo some times  */
                            MBSelectedUserSession.shared.UpdateGroupType(self.selectedUser?.groupName ?? "")
                        }
                        self.userInfoType.append(.MRC)
                        self.userInfoType.append(.menuInfo)
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadTableViewData()
        })
    }
}

//MARK: -  TableView Delegates
extension UserInformationVC: UITableViewDataSource,UITableViewDelegate,UserOptionMenuProtocoal{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userInfoType.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch userInfoType[indexPath.row] {
            
        case .tarrifsInfo:
            if let userInfoCell : UserInfoCell = tableView.dequeueReusableCell() {
                
                userInfoCell.setUserInfo(userName: selectedUser?.custFullName,
                                         msisdn: selectedUser?.msisdn,
                                         tarrifStatus: MBSelectedUserSession.shared.isStatusActive(),
                                         tarrifStatusColor: MBSelectedUserSession.shared.isStatusColor(),
                                         tarrifStatusText: MBSelectedUserSession.shared.userInfo?.status,
                                         tarrifsTitle: MBSelectedUserSession.shared.userInfo?.offeringNameDisplay )
                
                //  userInfoCell.currentTarrifButton.addTarget(self, action: #selector(redirecteToTarrifsVC), for: .touchUpInside)
                return userInfoCell
            }
  
        case .menuInfo:
            if let userInfoCell : UserOptionMenu = tableView.dequeueReusableCell() {
                userInfoCell.delegate = self
                return userInfoCell
            }
            
        case .MRC:
            if let mrcProgressCell : MRCProgressCell = tableView.dequeueReusableCell() {
                mrcProgressCell.setMRCValue(mrcInfo: userInformation?.homePageData?.mrc, balanceValue: "")
                return mrcProgressCell
            }
            
        case .fullAndPartialPay:
            if let corporateUserInfoCell : CorporateUserInfoCell = tableView.dequeueReusableCell() {
                corporateUserInfoCell.setCorporateUserInfo(userPaymentInformation: userInformation?.homePageData?.balance?.postpaid)
                return corporateUserInfoCell
            }
            
        case .payBySUBs:
            if let indivisualUserInfoCell : IndivisualUserInfoCell = tableView.dequeueReusableCell() {
                indivisualUserInfoCell.setIndividualCorporateUserInfo(userPaymentInformation: userInformation?.homePageData?.balance?.postpaid)
                return indivisualUserInfoCell
            }
            
        }
        return UITableViewCell()
    }
    
    func menuOptionTapped(_ identifire: String) {
        redirectUserToViewController(viewControllerIdentifer: identifire)
    }    
}
