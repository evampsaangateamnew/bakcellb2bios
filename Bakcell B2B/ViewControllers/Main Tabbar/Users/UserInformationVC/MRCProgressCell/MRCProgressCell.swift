//
//  MRCProgressCell.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 8/15/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class MRCProgressCell: UITableViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet var mrcBalanceValueLabel : MBLabel!
    @IBOutlet var mrcBalanceLabel : MBLabel!
    @IBOutlet var mrcTitleLabel : MBMarqueeLabel!
    @IBOutlet var mrcValueLabel : MBLabel!
    @IBOutlet var mrcDateTitleLabel : MBLabel!
    @IBOutlet var mrcDateLabel: MBLabel!
    @IBOutlet var mrcDaysRemainingLabel: MBLabel!
    @IBOutlet var mrcProgressView: MBGradientProgressView!
    @IBOutlet weak var balanceViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var balanceView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadViewLayout()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    //MARK: - Functions
    func loadViewLayout(){
        
        mrcBalanceValueLabel.text = ""
        mrcBalanceLabel.text = ""
        mrcTitleLabel.text = ""
        mrcValueLabel.attributedText = MBUtilities.createAttributedText("0", textColor: .black, withManatSign: true)
        mrcDateTitleLabel.text = ""
        mrcDateLabel.text = ""
        mrcDaysRemainingLabel.text = "0 \(Localized("Title_Days"))"
        
        mrcBalanceLabel.text = Localized("Title_Balance")
        mrcProgressView.setProgress(1, animated: true)
    
        mrcValueLabel.textColor = .mbTextBlack
        mrcDaysRemainingLabel.textColor = .mbTextBlack
        mrcDateLabel.textColor = .mbTextBlack
        
    }
    
    func setMRCValue(mrcInfo: Mrc?, balanceValue : String =  "") {
        
        
        if balanceValue.isBlank{
            balanceViewHeight.constant = 0
            balanceView.isHidden = true
        } else {
            balanceViewHeight.constant = 20
            balanceView.isHidden = false
            mrcBalanceValueLabel.attributedText = MBUtilities.createAttributedText(balanceValue , textColor: .black, withManatSign: true)
        }
        
        if mrcInfo?.mrcTitleValue?.isBlank ?? false == false {
            if mrcInfo?.mrcTitleValue?.isHasFreeText() ?? false{
                mrcValueLabel.text = mrcInfo?.mrcTitleValue ?? ""
                
            } else {
                mrcValueLabel.attributedText = MBUtilities.createAttributedText(mrcInfo?.mrcTitleValue ?? "", textColor: .black, withManatSign: true)
            }
        } else {
            mrcValueLabel.text = ""
        }
        
        mrcTitleLabel.text = mrcInfo?.mrcTitleLabel ?? ""
        mrcDateTitleLabel.text = mrcInfo?.mrcDateLabel ?? ""
        
        //If date or initial date is not coming, show expired in date and show progress bar to Full and days to 0
        if (mrcInfo?.mrcDate?.isBlank ?? true) == false && (mrcInfo?.mrcInitialDate?.isBlank ?? true) == false {
            
            let computedValuesFromDates = MBProgressUtilities.calculateProgressValuesForMRC(from: mrcInfo?.mrcInitialDate ?? "", to: mrcInfo?.mrcDate ?? "", type: mrcInfo?.mrcType ?? "")
            
            // Set end date
            self.mrcDateLabel.text = computedValuesFromDates.endDate
            
            // Set days left values
            self.mrcDaysRemainingLabel.text = computedValuesFromDates.daysLeftDisplayValue
            
            // Highlight left days value
            if computedValuesFromDates.daysLeft < mrcInfo?.mrcLimit?.toInt ?? 0 {
                mrcDaysRemainingLabel.textColor = UIColor.mbBrandRed
            } else {
                mrcDaysRemainingLabel.textColor = UIColor.black
            }
            
            if (mrcInfo?.mrcStatus ?? "").isEqual("UnPaid", ignorCase: true)
                && MBSelectedUserSession.shared.subscriberType() == .PrePaid {
                // set progress value
                self.mrcProgressView.setProgress(1, animated: true)
                mrcDaysRemainingLabel.textColor = UIColor.mbBrandRed
                
                var mrcTypeString = ""
                if (mrcInfo?.mrcType ?? "").isEqual("daily", ignorCase: true) {
                    mrcTypeString = "0 \(Localized("Title_Hours"))"
                    
                } else {
                    mrcTypeString = "0 \(Localized("Title_Days"))"
                }
                
                mrcDaysRemainingLabel.text = mrcTypeString
                
            } else {
                mrcProgressView.setProgress(computedValuesFromDates.progressValue, animated: true)
            }
        } else {
            mrcDaysRemainingLabel.textColor = UIColor.mbBrandRed
            mrcDateLabel.text = Localized("Expired_Date")
            
            var mrcTypeString = ""
            if (mrcInfo?.mrcType ?? "").isEqual("daily", ignorCase: true) {
                mrcTypeString = "0 \(Localized("title_Hours"))"
                
            } else {
                mrcTypeString = "0 \(Localized("Title_Days"))"
            }
            
            mrcDaysRemainingLabel.text = mrcTypeString
            mrcProgressView.setProgress(1, animated: true)
        }
        
    }
    
    //    }
    
    
}
