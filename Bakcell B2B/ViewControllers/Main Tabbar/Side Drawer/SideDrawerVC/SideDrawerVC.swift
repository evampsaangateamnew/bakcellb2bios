//
//  SideDrawerVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/06/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController


class SideDrawerVC: BaseVC {
    
    //MARK:- IBOutlet
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var userPerfileNameLabel: MBLabel!
    @IBOutlet var userTypeLabel: MBLabel!
    @IBOutlet var tableView: UITableView!
    
    //MARK:- ViewController Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting TableView delegate and dataSource to self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        // Setting Profile images layout
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        if let profileImageURL = URL(string: MBPICUserSession.shared.picUserInfo?.customProfileImage ?? ""),
            UIApplication.shared.canOpenURL(profileImageURL) {
            
            MBAPIClient.sharedClient.request(profileImageURL).responseData { response in
                
                if let imageDate = response.result.value,
                    let image = UIImage(data: imageDate) {
                    self.profileImageView.image = image
                } else {
                    self.profileImageView.image = UIImage.imageFor(name: "profile")
                }
            }
        } else {
            self.profileImageView.image = UIImage.imageFor(name: "profile")
        }
        
        // Reload TableView Data
        tableView.reloadData()
        
        // Setting User name and MSISDN
        userPerfileNameLabel.text = MBPICUserSession.shared.userName()
        userTypeLabel.text = MBPICUserSession.shared.msisdn
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.closeSideMenu()
    }
    
    
    
    //MARK:- Functions
    /**
     close side menu.
     
     - returns: void
     */
    
    func closeSideMenu(shouldSelectDashboard:Bool = true) {
        if let drawer = self.parent as? KYDrawerController,
            drawer.drawerState == .opened {
            drawer.setDrawerState(.closed, animated: false)
            
            if shouldSelectDashboard {
                self.setSelectTabbarIndex(0)
            }
        }
    }
    
    /**
     Set tabbar selected index.
     
     - parameter selectedIndex: Index which need to be set as selected.
     
     - returns: void.
     */
    func setSelectTabbarIndex(_ selectedIndex: Int) {
        if let drawer = self.parent as? KYDrawerController ,
            let tabBarCtr = drawer.mainViewController as? UITabBarController {
            tabBarCtr.selectedIndex = selectedIndex
        }
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier : String) {
        
        switch identifier {
            
        case "notifications":
            // Load Notifications viewController
            if let notifications :NotificationsVC = NotificationsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(notifications, animated: true)
            }
            
        case "store_locator":
            //  load StoreLocator viewController
            if let store :StoreLocatorVC = StoreLocatorVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(store, animated: true)
            }
            break
            
        case "terms_and_conditions":
            //  load Terms&Conditions viewController
            if let termsConditionVC :TermsConditionsVC = TermsConditionsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(termsConditionVC, animated: true)
            }
            break
            
        case "faq":
            
            // load FAQs viewController
            if let faqs :FAQsVC = FAQsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(faqs, animated: true)
            }
            
        case "live_chat":
            
            // load LiveChat viewController
            if MBPICUserSession.shared.liveChatObject == nil {
                if let myWebView :LiveChatWebViewVC = LiveChatWebViewVC.instantiateViewControllerFromStoryboard() {
                    MBPICUserSession.shared.liveChatObject = myWebView
                }
            }
            
            self.navigationController?.pushViewController(MBPICUserSession.shared.liveChatObject!, animated: true)
            
        case "contact_us":
            
            // load ContactUs viewController
            if let contactUs :ContactUsVC = ContactUsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(contactUs, animated: true)
            }
            
        case "settings":
            // load Settings viewController
            if let settingsVC :SettingsVC = SettingsVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(settingsVC, animated: true)
            }
            break
            
        case"rate_us":
            self.closeSideMenu(shouldSelectDashboard: false)
            break
            
        case "logout":
            
            self.closeSideMenu(shouldSelectDashboard: false)
            
            // Show logout confirmation alert to User
            self.showLogOutAlert({
                self.logOut()
            })
            
            break
            
        default:
            break
        }
    }
    
    /**
     Provide image name against identifiers. which are mapped.
     
     - parameter identifier: key for mapped image.
     
     - returns: image name for mapped identifier.
     */
    func imageNameFor(Identifier identifier : String) -> String {
        
        switch identifier.lowercased() {
            
        case "notifications":
            return "menu1"
            
        case "store_locator":
            return "menu2"
            
        case "faq":
            return "menu3"
            
        case "tutorial_and_faqs":
            return "tutorials"
            
        case "live_chat":
            return "menu5"
            
        case "contact_us":
            return "menu6"
            
        case "terms_and_conditions":
            return "menu7"
            
        case "settings":
            return "menu8"
            
        case "logout":
            return "menu9"
        case "rate_us":
            return "rate-us"
            
        default:
            return ""
            
        }
    }
    
    //MARK:- IBACTIONS
    @IBAction func myProfilePressed(_ sender: UIButton) {
        // Initialize and push to Profile controller
        if let myProfile :MyProfileVC = MyProfileVC.instantiateViewControllerFromStoryboard() {
            myProfile.selectedProfileType = .pic
            self.navigationController?.pushViewController(myProfile, animated: true)
        }
    }
    
    //MARK: - API Calls
    /**
     Logout user from current session.
     
     - returns: void
     */
    func logOut() {
        
        /* API call to Logout user */
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.logOut({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self,{
                    // Clear user data
                    BaseVC.logout()
                })
                
            } else {
                // handling data from API response.
                if resultCode != Constants.MBAPIStatusCode.succes.rawValue {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc, {
                        // Clear user data
                        BaseVC.logout()
                    })
                } else {
                    // Clear user data
                    BaseVC.logout()
                }
            }
        })
    }
}

// MARK: UITableViewDelegate and UITableViewDataSource
extension SideDrawerVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MBPICUserSession.shared.appMenu?.menuVertical?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:SideBarCell = tableView.dequeueReusableCell() {
            
            if let aMenuItem = MBPICUserSession.shared.appMenu?.menuVertical?[indexPath.row] {
                /* Set cell data and return */
                cell.sideBarLabel.text = aMenuItem.title
                
                let iconName = self.imageNameFor(Identifier: aMenuItem.identifier)
                cell.sideBarImgView.image = UIImage.imageFor(name: iconName)
                
            } else {
                /* Set empty cell data and return */
                cell.sideBarLabel.text = ""
                cell.sideBarImgView.image = UIImage.imageFor(name: "")
            }
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Redirect user to selected screen
        redirectUserToSelectedController(Identifier: MBPICUserSession.shared.appMenu?.menuVertical?[indexPath.row].identifier ?? "")
        
        // Deselect user selected row
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


