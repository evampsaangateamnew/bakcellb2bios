//
//  SideBarCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/06/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit

class SideBarCell: UITableViewCell {

    @IBOutlet var sideBarImgView: UIImageView!
    @IBOutlet var sideBarLabel: MBLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
