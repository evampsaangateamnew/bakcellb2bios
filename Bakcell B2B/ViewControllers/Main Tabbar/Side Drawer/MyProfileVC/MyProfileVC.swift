//
//  MyProfileVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/20/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper
import AVFoundation
import Photos

class MyProfileVC: BaseVC {
    
    public enum ProfileType {
        case pic
        case individual
    }
    
    public enum ResourceAccessType {
        case camera
        case gallery
    }
    
    //MARK: - Variable
    public var selectedProfileType : ProfileType = .pic
    
    private var imagePicker: UIImagePickerController =  UIImagePickerController()
    
    private var titles: [String] = []
    //  var images: [String] = ["Loyalty","Billing","sim","PIN","PUK","Activation-code","Email"]
    private var descriptionArr : [String] = []
    private var updateImageArr: [String] = [Localized("Info_Camera"),Localized("Info_Gallery"),Localized("Info_RemovePhoto")]
    
    
    //MARK: - IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var settingButton: UIButton!
    @IBOutlet var userNameLabel: MBLabel!
    @IBOutlet var userNumberLabel: MBLabel!
    @IBOutlet var profileTableView: UITableView!
    @IBOutlet var profileImageOptionsView: UIView!
    @IBOutlet var imageUpdateTableView: UITableView!
    @IBOutlet weak var updateImageMenuTop: NSLayoutConstraint!
    
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileTableView.delegate = self
        profileTableView.dataSource = self
        
        imageUpdateTableView.delegate = self
        imageUpdateTableView.dataSource = self
        
        imageUpdateTableView.roundAllCorners(radius: 8)
        
        profileImageOptionsView.isHidden = true
        
        
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.roundAllCorners(radius: profileImageView.frame.height/2)
        settingButton.roundAllCorners(radius: settingButton.frame.height/2)
        
        
        if selectedProfileType == .pic {
            userNameLabel.text = MBPICUserSession.shared.userName()
            userNumberLabel.text = MBPICUserSession.shared.msisdn
            settingButton.isUserInteractionEnabled = true
            
        } else {
            userNameLabel.text = MBSelectedUserSession.shared.userName()
            userNumberLabel.text = MBSelectedUserSession.shared.msisdn
            settingButton.isUserInteractionEnabled = false
        }
        
        //  Load profile image of user
        self.loadProfileImage()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        if selectedProfileType == .pic {
            // Title localization
            titleLabel.text = Localized("Title_Profile")
        } else {
            titleLabel.text = Localized("Title_IndividualProfile")
        }
        
        
        reloadTableViewData()
    }
    
    //MARK:- IBACTIONS
    @IBAction func imageUpdatePressed(_ sender: UIButton) {
        
        if profileImageOptionsView.isHidden == true {
            
            
            UIView.transition(with: profileImageOptionsView, duration: 0.5, options: [.transitionFlipFromLeft, .showHideTransitionViews], animations: {
                
                self.profileImageOptionsView.isHidden = false
                
            }, completion:nil)
            
            
        } else {
            UIView.transition(with: profileImageOptionsView, duration: 0.5, options: [.transitionFlipFromRight, .showHideTransitionViews], animations: {
                
                self.profileImageOptionsView.isHidden = true
                
            }, completion:nil)
        }
        
    }
    
    //MARK: - Functions
    func reloadTableViewData() {
        
        if selectedProfileType == .pic {
            titles = [Localized("Info_Name"),
                      Localized("Info_Surname"),
                      Localized("Info_Company"),
                      Localized("Info_Email"),
                      Localized("Info_Contact"),
                      Localized("Info_SerialNumberOfID"),
                      Localized("Info_PINOfID"),
                      Localized("Info_VEON")]
            
            self.descriptionArr = [MBPICUserSession.shared.picUserInfo?.firstName ?? "",
                                   MBPICUserSession.shared.picUserInfo?.lastName ?? "",
                                   MBPICUserSession.shared.picUserInfo?.companyName ?? "",
                                   MBPICUserSession.shared.picUserInfo?.email ?? "",
                                   MBPICUserSession.shared.picUserInfo?.contactNumber ?? "",
                                   MBPICUserSession.shared.picUserInfo?.picSerialNo ?? "",
                                   MBPICUserSession.shared.picUserInfo?.picPIN ?? "",
                                   MBPICUserSession.shared.picUserInfo?.picVEON ?? ""]
            
        } else {
            titles = [Localized("Info_LoyalityIndicator"),
                      Localized("Info_BillingLanguage"),
                      Localized("Info_SIM"),
                      Localized("Info_PIN"),
                      Localized("Info_PUK"),
                      Localized("Info_ActivationDate")
                /* , Localized("Info_Email")*/ ]
            
            self.descriptionArr = [MBSelectedUserSession.shared.userInfo?.loyaltySegment ?? "",
                                   MBSelectedUserSession.shared.userSelectedBillingLanguageLanguage(),
                                   MBSelectedUserSession.shared.userInfo?.simNumber ?? "",
                                   MBSelectedUserSession.shared.userInfo?.pinCode ?? "",
                                   MBSelectedUserSession.shared.userInfo?.pukCode ?? "",
                                   MBDateUtilities.displayFormatedDateString(dateString: MBSelectedUserSession.shared.userInfo?.effectiveDate ?? "")
                /*, MBSelectedUserSession.shared.userInfo?.email ?? ""*/ ]
        }
        
        self.profileTableView.reloadData()
    }
    
    func loadProfileImage() {
        
        var profileImageURLString = ""
        
        if selectedProfileType == .pic {
            profileImageURLString = MBPICUserSession.shared.picUserInfo?.customProfileImage ?? ""
            
        } else {
            profileImageURLString = MBSelectedUserSession.shared.userInfo?.imageURL ?? ""
        }
        
        if let profileImageURL = URL(string: profileImageURLString),
            UIApplication.shared.canOpenURL(profileImageURL) {
            
            print("image URL: \(profileImageURL.absoluteString)")
            // Load profile image of user
            MBAPIClient.sharedClient.request(profileImageURLString).responseData { response in
                
                if let imageDate = response.result.value ,
                    let image = UIImage(data: imageDate) {
                    self.profileImageView.image = image
                } else {
                    self.profileImageView.image = UIImage.imageFor(name: "Profile_edit_icon")
                }
            }
        } else {
            self.profileImageView.image = UIImage.imageFor(name: "Profile_edit_icon")
        }
    }
    
    func loadImagePickerControllerFor(type: ResourceAccessType) {
        
        if type == .camera {
            
            // Get current access status for camera
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            
            switch cameraAuthorizationStatus {
                
            case .authorized:
                self.loadImagePickerControllerForCamera()
                break
                
            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                    if granted {
                        
                        // User enable camera access to app
                        self.loadImagePickerControllerForCamera()
                    } else {
                        // Acces denied
                        self.showAccessAlert(message: Localized("Message_EnableCameraAccess"))
                    }
                }
                break
                
            case .denied, .restricted:
                // Access denied
                self.showAccessAlert(message: Localized("Message_EnableCameraAccess"))
                break
                
            default:
                break
            }
            
        } else {
            
            // Get the current authorization state of Photos
            let status = PHPhotoLibrary.authorizationStatus()
            
            if (status == PHAuthorizationStatus.authorized) {
                // Access has been granted.
                self.loadImagePickerControllerForGallery()
                
            } else if (status == PHAuthorizationStatus.notDetermined) {
                
                // Access has not been determined.
                PHPhotoLibrary.requestAuthorization({ (newStatus) in
                    
                    // Get Access
                    if (newStatus == PHAuthorizationStatus.authorized) {
                        
                        self.loadImagePickerControllerForGallery()
                        
                    } else {
                        // Access denied and show user access message
                        self.showAccessAlert(message: Localized("Message_EnablePhotosAccess"))
                    }
                })
                
            } else if (status == PHAuthorizationStatus.denied) || (status == PHAuthorizationStatus.restricted) {
                // Access has been denied.
                self.showAccessAlert(message: Localized("Message_EnablePhotosAccess"))
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }
    }
    
    func loadImagePickerControllerForGallery() {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
        
    }
    
    func loadImagePickerControllerForCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            present(imagePicker, animated: true, completion: nil)
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == profileImageOptionsView {
                imageUpdatePressed(UIButton())
            }
        }
        
    }
    
    //MARK: - API Calls
    /// Call 'billingLanguageChangeApiCall' API.
    ///
    /// - returns: Void
    func billingLanguageChange(language lang : String) {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.changeBillingLanguage(language: lang, {(response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let changeLangResponse = Mapper<ChangeBillingLanguage>().map(JSONObject:resultData) {
                        
                        self.showSuccessAlertWithMessage(message: changeLangResponse.message)
                        
                        // Update billing language.
                        MBSelectedUserSession.shared.userInfo?.billingLanguage = lang
                        self.reloadTableViewData()
                        
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    // Remove profile image
    func removeProfileImage() {
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.uploadImage(image: nil ,isUploadImage: false, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    if self.selectedProfileType == .pic {
                        MBPICUserSession.shared.picUserInfo?.customProfileImage = ""
                    } else {
                        MBSelectedUserSession.shared.userInfo?.imageURL = ""
                    }
                    
                    self.loadProfileImage()
                    
                    self.showAlertWithMessage(message: resultDesc ?? "")
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - TABLE VIEW DELEGATE METHODS
extension MyProfileVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == imageUpdateTableView {
            return updateImageArr.count
        } else {
            return descriptionArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:ProfileCell = tableView.dequeueReusableCell() {
            
            if tableView == self.profileTableView {
                
                cell.centerTitleLabel.isHidden = true
                
                cell.titleLabel.isHidden = false
                cell.descriptionLabel.isHidden = false
                cell.LineView.isHidden = false
                
                
                cell.titleLabel.text = titles[indexPath.row]
                cell.descriptionLabel.text = descriptionArr[indexPath.row]
                
                
                if selectedProfileType == .individual {
                    //Aligning the arrow with the text
                    /*if titles[indexPath.row].isEqual( Localized("Info_BillingLanguage")) ||
                     titles[indexPath.row].isEqual( Localized("Info_Email")) {
                     
                     cell.arrowWidthConstraint.constant = 22
                     } else {
                     cell.arrowWidthConstraint.constant = 0
                     }*/
                    
                    cell.arrowWidthConstraint.constant = 0
                    
                    if titles[indexPath.row].isEqual( Localized("Info_SIM"), ignorCase: true) {
                        cell.descriptionLabel.startMarqueeAnimation()
                    } else {
                        cell.descriptionLabel.stopMarqueeAnimation()
                    }
                } else {
                    cell.arrowWidthConstraint.constant = 0
                    cell.descriptionLabel.stopMarqueeAnimation()
                }
                
            } else if tableView == imageUpdateTableView {
                
                cell.titleLabel.isHidden = true
                cell.descriptionLabel.isHidden = true
                cell.arrowImageView.isHidden = true
                cell.LineView.isHidden = true
                
                cell.centerTitleLabel.isHidden = false
                
                cell.centerTitleLabel.text = updateImageArr[indexPath.row]
                
                if indexPath.row == updateImageArr.count - 1 {
                    cell.centerTitleLabel.textColor = UIColor.mbBrandRed
                } else {
                    cell.centerTitleLabel.textColor = UIColor.mbTextGray
                }
            }
            cell.selectionStyle = .none
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == imageUpdateTableView {
            
            if indexPath.row == 0 {
                
                self.loadImagePickerControllerFor(type: .camera)
                
            } else if indexPath.row == 1 {
                
                self.loadImagePickerControllerFor(type: .gallery)
                
            } else if indexPath.row == 2 {
                
                self.removeProfileImage()
            }
            
            // Hide option TableView
            imageUpdatePressed(UIButton())
            
        } else if tableView == tableView {
            
            if selectedProfileType == .individual {
                if (titles[indexPath.row].isEqual( Localized("Info_BillingLanguage"), ignorCase: true)  && (selectedProfileType == .pic)) {
                    
                    if let changelang : LanguageSelectionVC =  LanguageSelectionVC.fromNib()  {
                        
                        changelang.setLanguageSelectionAlert(MBSelectedUserSession.shared.selectedBillingLanguageID()) { (newSelectedLanguage) in
                            
                            if MBSelectedUserSession.shared.selectedBillingLanguageID() != newSelectedLanguage {
                                
                                // Setting user language
                                //                                self.billingLanguageChange(language: newSelectedLanguage.rawValue)
                            }
                        }
                        
                        self.presentPOPUP(changelang, animated: true, completion:  nil)
                    }
                    
                } else if (titles[indexPath.row].isEqual( Localized("Info_Email"), ignorCase: true) && (selectedProfileType == .pic)) {
                    
                    if let changeEmailVC :ChangeEmailVC = ChangeEmailVC.instantiateViewControllerFromStoryboard() {
                        self.navigationController?.pushViewController(changeEmailVC, animated: true)
                    }
                }
            }
        }
    }
}


extension MyProfileVC :UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
            self.showActivityIndicator()
            
            _ = MBAPIClient.sharedClient.uploadImage(image: image, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                
                self.hideActivityIndicator()
                if error != nil {
                    error?.showServerErrorInViewController(self)
                    
                } else {
                    // handling data from API response.
                    if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                        // Parssing response data
                        if let uploadImageResponse = Mapper<UploadImage>().map(JSONObject: resultData) {
                            
                            self.showSuccessAlertWithMessage(message: resultDesc)
                            
                            if self.selectedProfileType == .pic {
                                MBPICUserSession.shared.picUserInfo?.customProfileImage = uploadImageResponse.imageURL
                            } else {
                                MBSelectedUserSession.shared.userInfo?.imageURL = uploadImageResponse.imageURL
                            }
                            
                            self.loadProfileImage()
                            
                        } else {
                            self.showAlertWithMessage(message: resultDesc)
                        }
                    } else {
                        
                        // Show error alert to user
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                }
            })
            
            if picker.sourceType == .camera {
                
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
            
        }
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        // self.showErrorAlertWithMessage(message: error?.localizedDescription)
    }
}
