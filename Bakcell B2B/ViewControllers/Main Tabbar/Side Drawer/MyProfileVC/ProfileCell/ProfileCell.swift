//
//  ProfileCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 7/3/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet var titleLabel: MBMarqueeLabel!
    @IBOutlet var descriptionLabel: MBMarqueeLabel!
    @IBOutlet var arrowImageView: UIImageView!
    @IBOutlet var LineView: UIView!
    @IBOutlet var arrowWidthConstraint: NSLayoutConstraint!

    @IBOutlet var centerTitleLabel: MBLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
