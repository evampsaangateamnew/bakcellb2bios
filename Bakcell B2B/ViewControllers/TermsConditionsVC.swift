//
//  TermsConditionsVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/06/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class TermsConditionsVC: BaseVC {
    
    // MARK: - Properties
    var isPopUp : Bool = false
    
    // MARK: - IBOutlet
    
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var termsConditionMainView: UIView!
    @IBOutlet var termsConditionMainTextView: UITextView!
    
    /* POPUP View related IBOutlets */
    @IBOutlet var termsConditionPopUpView: UIView!
    @IBOutlet var termsConditionPopUpContainorView: UIView!
    @IBOutlet var termsConditionPopUpTitleLabel: UILabel!
    @IBOutlet var termsConditionPopUpDescriptionLabel: UILabel!
    @IBOutlet var termsConditionPopUpTextView: UITextView!
    
    @IBOutlet var checkBoxButton: MBButton!
    @IBOutlet var IAcceptLabel: MBLabel!
    @IBOutlet var cancelButton: MBButton!
    @IBOutlet var acceptButton: MBButton!
    
    // MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        loadViewLayout()
                
        termsConditionPopUpContainorView.layer.borderColor = UIColor.mbBorderGray.cgColor
        
        if isPopUp {
            termsConditionPopUpView.isHidden = false
            termsConditionMainView.isHidden = true
            
            acceptButton.setState(isEnable: false)
            checkBoxButton.isSelected = false
            checkBoxButton.setImage(UIImage.imageFor(name: "Checkbox-state-2"), for: .normal)
            self.view.backgroundColor = UIColor.clear
            
        } else {
            termsConditionMainView.isHidden = false
            termsConditionPopUpView.isHidden = true
            self.view.backgroundColor = UIColor.white
        }
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        
        titleLabel.text = Localized("Terms_Title")
        
        
        termsConditionPopUpTitleLabel.text = Localized("Terms_Title")
        termsConditionPopUpDescriptionLabel.text = Localized("Title_AcceptTermsMessage")
        
        IAcceptLabel.text = Localized("BtnTitle_Terms")
        cancelButton.setTitle(Localized("Title_Login"), for: UIControl.State.normal)
        acceptButton.setTitle(Localized("BtnTitle_Accept"), for: UIControl.State.normal)
        
        if let predefineData : NewPredefinedData = NewPredefinedData.loadFromUserDefaults(key: APIsType.terms_Conditions.localizedAPIKey()) {
            termsConditionMainTextView.loadHTMLString(htmlString: predefineData.content ?? "")
            termsConditionPopUpTextView.loadHTMLString(htmlString: predefineData.content ?? "")
        } else {
            termsConditionMainTextView.text = ""
            termsConditionPopUpTextView.text = ""
        }
    }
    
    //MARK: - IBACTIONS
    @IBAction func cancelPressed(_ sender: UIButton) {
        
        /* API call to Logout user */
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.logOut({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                self.dismiss(animated: false, completion: {
                    error?.showServerErrorInViewController(self, {
                        // Clear user data
                        BaseVC.logout()
                    })
                })
                
            } else {
                // handling data from API response.
                if resultCode != Constants.MBAPIStatusCode.succes.rawValue {
                    self.dismiss(animated: false, completion: {
                        // Show error alert to user
                        self.showErrorAlertWithMessage(message: resultDesc, {
                            // Clear user data
                            BaseVC.logout()
                        })
                    })
                } else {
                    self.dismiss(animated: true, completion: {
                        // Clear user data
                        BaseVC.logout()
                    })
                }
            }
        })
    }
    
    @IBAction func acceptPressed(_ sender: UIButton) {
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.acceptTnC({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    
                    // Parssing response data
                    if let _ = Mapper<MessageResponse>().map(JSONObject:resultData) {
                        // Hide Popup on success
                        self.dismiss(animated: true, completion: nil)
                        
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            
        })
    }
    
    @IBAction func checkBoxPressed(_ sender: UIButton) {
        
        if sender.isSelected == true {
            acceptButton.setState(isEnable: false)
            sender.isSelected = false
            sender.setImage(UIImage.imageFor(name: "Checkbox-state-2"), for: .normal)
        } else {
            acceptButton.setState(isEnable: true)
            sender.isSelected = true
            sender.setImage(UIImage.imageFor(name: "Checkbox-state-1"), for: .normal)
        }
        
    }
    
    
}
