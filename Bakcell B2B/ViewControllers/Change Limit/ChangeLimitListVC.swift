//
//  ChangeLimitListVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 6/29/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class ChangeLimitListVC: BaseVC {
    
    var servicesList: [Category] = []
    
    
    // MARK: - IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var changeLimitTableView: UITableView!
    
    // MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        changeLimitTableView.estimatedRowHeight = 90
        changeLimitTableView.rowHeight = UITableView.automaticDimension
        changeLimitTableView.dataSource = self
        changeLimitTableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
        
        
        servicesList = [ Category(Name: Localized("Title_individual"), ImageName: "change-limit-ind" ,Idenitifier: "individual"),
                         Category(Name: Localized("Title_Corporate"), ImageName: "change-limit-corporate" ,Idenitifier: "corporate")]
        
        
        self.reloadTableViewData()
        
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout() {
        titleLabel.text = Localized("Title_ChangeLimit")
    }
    
    func reloadTableViewData() {
        if servicesList.count <= 0 {
            self.changeLimitTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.changeLimitTableView.hideDescriptionView()
        }
        self.changeLimitTableView.reloadData()
    }
    
    /**
     Redirect user accourding to provided identifier.
     
     - parameter identifier: key for selected controller
     
     - returns: void
     */
    func redirectUserToSelectedController(Identifier identifier: String, Users selectedUsers : [String : UsersData]){
        switch identifier {
        case "individual":
            // Redirect user to chnageLimitVC.
            if let chnageLimitVC :ChangeLimitVC = ChangeLimitVC.instantiateViewControllerFromStoryboard() {
                chnageLimitVC.selectedChangeLimitType = .individual
                chnageLimitVC.selectedUsersList = selectedUsers
                self.navigationController?.pushViewController(chnageLimitVC, animated: true)
            }
            break
        case "corporate":
            if let chnageLimitVC :ChangeLimitVC = ChangeLimitVC.instantiateViewControllerFromStoryboard() {
                chnageLimitVC.selectedChangeLimitType = .corporate
                chnageLimitVC.selectedUsersList = selectedUsers
                self.navigationController?.pushViewController(chnageLimitVC, animated: true)
            }
            break
            
        default:
            
            break
        }
    }
    
}
//MARK: TABLE VIEW METHODS
extension ChangeLimitListVC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let servicesTableCell : CategoriesTableViewCell =  tableView.dequeueReusableCell() {
            servicesTableCell.setCategoryInfo(aCategory: servicesList[indexPath.row])
            return servicesTableCell
        } else {
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let userSelectedIdentifier :String = self.servicesList[indexPath.row].identifier
        
        if let userSelectionVC :MultipleUserSelectionMainVC = MultipleUserSelectionMainVC.instantiateViewControllerFromStoryboard() {
            
            if userSelectedIdentifier == "individual" {
                
              userSelectionVC.loadDataOfGroupType = [.paybySubs]
            
            } else if userSelectedIdentifier == "corporate" {
                
                userSelectionVC.loadDataOfGroupType = [.partPay, .fullPay]
            }
            
            userSelectionVC.setNextButtonCompletionBloch { (selectedUsers) in
                
                //Redirect user to selected screen
                self.redirectUserToSelectedController(Identifier: userSelectedIdentifier, Users: selectedUsers)
            }
            
            self.navigationController?.pushViewController(userSelectionVC, animated: true)
        }
        
        
    }
}

