//
//  ChangeLimitVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 7/2/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ChangeLimitVC: BaseVC {
    
    enum ChangeLimitType  : String {
        case corporate = "corporate"
        case individual = "individual"
    }
    
    // MARK: - Properties
    var selectedChangeLimitType : ChangeLimitType = .individual
    var selectedUsersList : [String : UsersData]?
    var kMinLimit : Float = 0
    var kMaxLimit : Float = 500
    
    // MARK: - IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var descriptionLabel: MBLabel!
    @IBOutlet var creditLimitTitleLabel: MBLabel!
    @IBOutlet var creditLimitValueLabel: MBLabel!
    @IBOutlet var orTitleLabel: MBLabel!
    
    @IBOutlet var creditLimitSlider: MBSlider!
    @IBOutlet var creditLimitTextField: MBTextField!
    @IBOutlet var changeButton: MBButton!
    
    
    // MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        creditLimitTextField.delegate = self
        
        loadLimitValue()
        
        creditLimitSlider.addTarget(self, action: #selector(sliderValueDidChange(_:)), for: .valueChanged)
        //NotificationCenter.default.addObserver(self, selector: #selector(textFieldTextDidChange), name: UITextField.textDidChangeNotification, object: nil)
        
        /* Check if user selected single user */
        if (selectedUsersList?.count ?? 0) == 1,
            let selectedUserData = selectedUsersList?.first?.value {
            getChangeLimitMinMax(selectedUserData)
        } else {
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout() {
        if selectedChangeLimitType == .corporate {
            titleLabel.text = Localized("Title_CorporateLimit")
            descriptionLabel.text = Localized("description_CorporateLimit")
            
        } else {
            titleLabel.text = Localized("Title_IndividualLimit")
            descriptionLabel.text = Localized("description_IndividualLimit")
        }
        
        creditLimitTitleLabel.text = Localized("Title_CreditLimit")
        orTitleLabel.text = Localized("Title_OR")
        
        creditLimitTextField.placeholder = Localized("Placeholder_EnterLimit")
        changeButton.setTitle(Localized("BtnTitle_Change"), for: UIControl.State.normal)
    }
    
    func loadLimitValue() {
        
        self.creditLimitSlider.value = self.kMinLimit
        self.creditLimitSlider.minimumValue = 0
        self.creditLimitSlider.maximumValue = self.kMaxLimit
        self.creditLimitTextField.text = "\(self.creditLimitSlider.minimumValue.toRoundedInt)"
        creditLimitValueLabel.text = String(format: Localized("Title_CreditValue"), "\(self.kMinLimit.toRoundedInt)","\(self.kMaxLimit.toRoundedInt)")
        creditLimitTextField.text = "\(self.kMinLimit.toRoundedInt)"
    }
    
    @objc func sliderValueDidChange(_ sender:MBSlider) {
        if creditLimitValueLabel.isFirstResponder {
            creditLimitValueLabel.resignFirstResponder()
        }
        
        let newRoundedValue = sender.value.toRoundedInt
        
        updateSelectedCreditLimitValue(newRoundedValue, changeSliderValue: false)
    }
    
    func updateSelectedCreditLimitValue(_ newValue:Int, changeSliderValue:Bool) {
        
        /*newValue >= creditLimitSlider.minimumValue.toRoundedInt && */
        if (newValue <= creditLimitSlider.maximumValue.toRoundedInt) {
            
            creditLimitValueLabel.text = String(format: Localized("Title_CreditValue"), "\(newValue)","\(creditLimitSlider.maximumValue.toRoundedInt)")
            
            creditLimitTextField.text = "\(newValue)"
            
            if changeSliderValue == true {
                creditLimitSlider.value = Float(newValue)
            }
        } else {
            // updateSelectedCreditLimitValue(creditLimitSlider.minimumValue.toRoundedInt, changeSliderValue: true)
            self.showErrorAlertWithMessage(message: Localized("Message_GenralError"))
        }
    }
    
    @objc func textFieldTextDidChange() {
        if let newValue = creditLimitTextField.text?.toInt {
            updateSelectedCreditLimitValue(newValue, changeSliderValue: true)
            
        } else {
            updateSelectedCreditLimitValue(0, changeSliderValue: true)
        }
    }
    
    //MARK: - IBAction
    @IBAction func changePressed(_ sender:MBButton) {
        
        if let amountText = creditLimitTextField.text,
            amountText.isBlank == false,
            amountText.isNumeric,
            amountText.toInt <= creditLimitSlider.maximumValue.toRoundedInt{
            
            creditLimitValueLabel.text = String(format: Localized("Title_CreditValue"), "\(amountText)","\(creditLimitSlider.maximumValue.toRoundedInt)")
            
            creditLimitSlider.value = amountText.toFloat
            
            self.creditLimitSlider.value = self.kMinLimit
            self.creditLimitSlider.minimumValue = 0
            self.creditLimitSlider.maximumValue = self.kMaxLimit
            if amountText.toFloat == self.kMaxLimit && self.kMinLimit == self.kMaxLimit {
                //show popup for user to change limit
                self.showErrorAlertWithMessage(message: Localized("Message_ReachedMaxLimit"))
            } else {
                updateLimit(amountText)
                creditLimitValueLabel.resignFirstResponder()
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidLimit"))
        }
    }
    
    //MARK: - API Calls
    
    //Update Limits for users
    func updateLimit(_ newSelectedAmount:String)  {
        
        var totalLimitValue :String = ""
        var balanceCreditValue :String = ""
        
        if let picBalanceInfo : PICBalanceData = PICBalanceData.loadFromUserDefaults(key: APIsType.queryBalanceResponseData.localizedAPIKey()) {
            
            totalLimitValue = picBalanceInfo.totalLimit ?? ""
            balanceCreditValue = picBalanceInfo.balanceCredit ?? ""
            
            
            /* Check if user is corporate type */
           /* if selectedChangeLimitType == .corporate {
                
                
                var usersCount :Double = 0
                
                //Load Group Data
                if let groupDataHandler : GroupData = GroupData.loadFromUserDefaults(key: APIsType.groupData.localizedAPIKey()) {
                    
                    let usersGroupDataObj = UserSelectionBaseVC.filterGroups(byGroupType: [.partPay, .fullPay], fromGroupList: groupDataHandler.usersGroupData)
                    
                    usersGroupDataObj.forEach { (aGroup) in
                        usersCount = usersCount + (aGroup.usersData?.count.toDouble() ?? 0)
                    }
                }
                
                /* show error to user if selected ammount from slider is grater then max limit of a user */
                if newSelectedAmount.toDouble > (totalLimitValue.toDouble / usersCount) {
                    
                    self.showErrorAlertWithMessage(message: Localized("Message_GreaterLimit"))
                    return
                }
            }*/
            
        }
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.updateLimitsForUsers(amount: newSelectedAmount, balance: balanceCreditValue, companyValue: MBPICUserSession.shared.picUserInfo?.picAttribute2 ?? "", totalLimit: totalLimitValue, lowerLimit: self.kMinLimit.toString(), maxLimit: self.kMaxLimit.toString(), Users: selectedUsersList, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    self.showSuccessAlertWithMessage(message: resultDesc)
                    self.changeButton.setState(isEnable: false)
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
        })
    }
    
    //Update Limits for users
    func getChangeLimitMinMax(_ selectedUser :UsersData)  {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getChangeLimitMinMax(groupType: selectedUser.groupName, companyStandardValue: MBPICUserSession.shared.picUserInfo?.picAttribute2 ?? "", offeringId: selectedUser.tarifId, msisdnuser: selectedUser.msisdn, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue,
                    let response = Mapper<LimitMaxMinModel>().map(JSONObject: resultData) {
                    
                    self.kMinLimit = response.initialLimit?.toFloat ?? self.kMinLimit
                    self.kMaxLimit = response.maxLimit?.toFloat ?? self.kMaxLimit
                    
                    self.loadLimitValue()
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
        })
    }
    
}

//MARK: - Textfield delagates
extension ChangeLimitVC : UITextFieldDelegate {
    
    //  Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        // Check for MobileNumber textField
        if textField == creditLimitTextField {
            // Check for number text
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            
            if newString.toFloat <= kMaxLimit &&
                allowedCharacters.isSuperset(of: characterSet) {
                return  true
            } else {
                return  false
            }
        } else {
            return false
        }
    }
    
}
