//
//  IndividualUserSelectionVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class IndividualUserSelectionVC: UIViewController {
    
    //MARK:- Properties
    weak var delegate: UsersListDelegate?
    var usersList : [UsersData]? = []
    var isSingleUserSelection :Bool = false
    
    //MARK:- IBOutlet
    @IBOutlet var individualUserTableView : UITableView!
    
    //MARK:- ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        individualUserTableView.delegate = self
        individualUserTableView.dataSource = self
        individualUserTableView.allowsMultipleSelection = true
        
        individualUserTableView.hideDefaultSeprator()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadTableViewData()
        
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        
        reloadTableViewData()
    }
    
    //MARK:- Function
    func reloadTableViewData() {
        
        if usersList?.count ?? 0 <= 0 {
            self.individualUserTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.individualUserTableView.hideDescriptionView()
        }
        
         self.individualUserTableView.reloadData()
        
         self.individualUserTableView.scrollToFirstRow()
    }
    
}


//MARK: - Table View Delegates
extension IndividualUserSelectionVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell : UsersCell = tableView.dequeueReusableCell() {
            
            if let aUser = usersList?[indexPath.row] {
                cell.setLayout(aUser: usersList?[indexPath.row], showIndicator: isSingleUserSelection)
                
                if let selectedUserList = (self.parent as? UserSelectionBaseVC)?.selectedUserList {
                    if selectedUserList.keys.contains(aUser.msisdn) {
                        tableView.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.none)
                    } else {
                        tableView.deselectRow(at: indexPath, animated: true)
                    }
                } else {
                    tableView.deselectRow(at: indexPath, animated: true)
                }
                
            } else {
                cell.setLayout(aUser: nil, showIndicator: false)
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.singleUserDidSelect(usersList?[indexPath.row])
        let selectedCell:UsersCell = tableView.cellForRow(at: indexPath) as! UsersCell
        selectedCell.myContentView.backgroundColor = .lightText
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.delegate?.singleUserDidDeselect(usersList?[indexPath.row])
        let selectedCell:UsersCell = tableView.cellForRow(at: indexPath) as! UsersCell
        selectedCell.myContentView.backgroundColor = .white
        
    }
    
}

//MARK: - Scroll View Delegates
extension IndividualUserSelectionVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.delegate?.mbScrollViewDidScroll(scrollView)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.delegate?.mbScrollViewDidEndScrool(scrollView)
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.delegate?.mbScrollViewDidEndScrool(scrollView)
    }
}
