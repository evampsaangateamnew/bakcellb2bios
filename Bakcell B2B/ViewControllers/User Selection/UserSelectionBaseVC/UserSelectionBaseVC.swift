//
//  UserSelectionBaseVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 12/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

// Scrolling and selection Protocol
protocol UsersListDelegate: class {
    func mbScrollViewDidScroll(_ scrollView: UIScrollView)
    func mbScrollViewDidEndScrool (_ scollview: UIScrollView)
    
    func singleUserDidSelect(_ selectedUser:UsersData?)
    func userGroupDidSelect(_ selectedGroup:UsersGroupData?)
    
    func singleUserDidDeselect(_ selectedUser:UsersData?)
    func userGroupDidDeselect(_ selectedGroup:UsersGroupData?)
}

extension UsersListDelegate {
    func mbScrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    func mbScrollViewDidEndScrool (_ scollview: UIScrollView){
        
    }
    
    func singleUserDidSelect(_ selectedUser:UsersData?){
        
    }
    func userGroupDidSelect(_ selectedGroup:UsersGroupData?){
        
    }
    
    func singleUserDidDeselect(_ selectedUser:UsersData?){
        
    }
    func userGroupDidDeselect(_ selectedGroup:UsersGroupData?){
        
    }
}

class UserSelectionBaseVC: BaseVC {
    
    //MARK: - Properties
    
    public var selectedUserList : [String : UsersData] = [:]
    
    var userSelectedType = UserSelectedType.individualList
    var loadDataOfGroupType :[GroupTypes]? = [.paybySubs, .partPay, .fullPay]
    
    var individualUsersListVC : IndividualUserSelectionVC?
    var groupUsersListVC : GroupUserSelectionVC?
    
    var individualUsers : [UsersData]?
    var groupUsers : [UsersGroupData]?
    
    var filteredIndividualUsers : [UsersData]?
    var filteredGroupUsers : [UsersGroupData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.interactivePop()
    }
    
    //MARK: - Remove observes
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Load user data from userdefault ans API
    func loadUsersInformation(_ completionBlock: @escaping (_ individualUsersList: [UsersData]?, _ groupUsersList: [UsersGroupData]?) -> Void) {
        
        var usersResponseList : [UsersData]?
        var userGroupLists : [UsersGroupData]?
        
        //Load User Data
        if let usersDataHandler : [UsersData] = UsersData.loadArrayFromUserDefaults(key: APIsType.usersData.localizedAPIKey()){
            //usersResponseList = usersDataHandler
            usersResponseList = UserSelectionBaseVC.filterUsers(byGroupType: self.loadDataOfGroupType, fromUserList: usersDataHandler)
        }
        
        //Load Group Data
        if let groupDataHandler : GroupData = GroupData.loadFromUserDefaults(key: APIsType.groupData.localizedAPIKey()){
            
            //  userGroupLists = groupDataHandler.usersGroupData
            userGroupLists = UserSelectionBaseVC.filterGroups(byGroupType: self.loadDataOfGroupType, fromGroupList: groupDataHandler.usersGroupData)
        }
        
        if (usersResponseList?.count ?? 0) <= 0 ||
            (userGroupLists?.count ?? 0) <= 0 {
            
            self.showActivityIndicator(withAnimation: false)
            _ = MBAPIClient.sharedClient.getUserGroupData(customerID: MBPICUserSession.shared.picUserInfo?.customerID ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                
                self.hideActivityIndicator()
                
                if error != nil {
                    error?.showServerErrorInViewController(self)
                    completionBlock(usersResponseList, userGroupLists)
                } else {
                    // handling data from API response.
                    if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                        if let userGroupHandler = Mapper<UserGroupData>().map(JSONObject: resultData) {
                            
                            /* Save Users in userDefaults */
                            MBUtilities.saveUserGroupData(userGroupData: userGroupHandler)
                            
                            //Load User Data
                            if let usersDataHandler : [UsersData] = userGroupHandler.users {
                                
                                // usersResponseList = usersDataHandler
                                usersResponseList = UserSelectionBaseVC.filterUsers(byGroupType: self.loadDataOfGroupType, fromUserList: usersDataHandler)
                            }
                            
                            //Load Group Data
                            if let groupDataHandler : GroupData = userGroupHandler.groupData {
                                
                                //  userGroupLists = groupDataHandler.usersGroupData
                                userGroupLists = UserSelectionBaseVC.filterGroups(byGroupType: self.loadDataOfGroupType, fromGroupList: groupDataHandler.usersGroupData)
                            }
                            
                            
                            completionBlock(usersResponseList, userGroupLists)
                            
                        } else {
                            // Show error alert to user
                            self.showErrorAlertWithMessage(message: resultDesc)
                        }
                    } else {
                        // Show error alert to user
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                }
            })
        } else {
            /* return info */
            completionBlock(usersResponseList, userGroupLists)
        }
    }

}
//MARK: - Fliter Methods
extension UserSelectionBaseVC {
    
    /**
     Filter users group List.
     
     - parameter byQueryString: word on which we want to filter.
     - parameter fromGroupList: Group users list.
     
     - returns: Array of filtered group users.
     */
    public func filterGroups(byQueryString queryString : String? ,fromGroupList groupList : [UsersGroupData]? ) -> [UsersGroupData] {
        
        // return complete array if string is empty
        if let searchString = queryString?.trimmWhiteSpace {
            
            if searchString.isBlank == true {
                return groupList ?? []
            } else {
                // Filtering Ussers array
                var filteredUsersList : [UsersGroupData] = []
                if let groupListObject = groupList {
                    
                    groupListObject.forEach { (aGroup) in
                        
                        let filterdUsers = self.filterUsers(byQueryString: searchString, fromUserList: aGroup.usersData)
                        
                        if filterdUsers.count > 0 {
                            var aGroupObject = aGroup
                            aGroupObject.usersData = filterdUsers
                            filteredUsersList.append(aGroupObject)
                        }
                    }
                }
                
                return filteredUsersList
            }
            
        } else {
            return []
        }
        
    }
    
    /**
     Filter users List.
     
     - parameter byQueryString: word on which we want to filter.
     - parameter fromGroupList: Users list.
     
     - returns: Array of filtered users .
     */
    public func filterUsers(byQueryString queryString : String? ,fromUserList usersList : [UsersData]? ) -> [UsersData] {
        
        // return complete array if string is empty
        if let searchString = queryString?.trimmWhiteSpace {
            
            if searchString.isBlank == true {
                return usersList ?? []
            } else {
                // Filtering Ussers array
                var filteredUsersList : [UsersData] = []
                if let usersListObject = usersList {
                    
                    filteredUsersList = usersListObject.filter() {
                        
                        /*let containsFirstName = $0.custFstName.containsSubString(subString: queryString)
                        let containsLastName = $0.custLastName.containsSubString(subString: queryString)*/
                        
                        let containsFullName = $0.custFullName.containsSubString(subString: queryString)
                        let containsMSISDNName = $0.msisdn.containsSubString(subString: queryString)
                        
                        
                        if containsFullName == true || containsMSISDNName == true {
                            return true
                        } else {
                            return false
                        }
                    }
                }
                
                return filteredUsersList
            }
            
        } else {
            return []
        }
    }
    
    
    /**
     Filter users group List.
     
     - parameter byGroupType: group on which we want to filter.
     - parameter fromGroupList: Group users list.
     
     - returns: Array of filtered group users.
     */
    class public func filterGroups(byGroupType groupsType : [GroupTypes]? ,fromGroupList groupList : [UsersGroupData]? ) -> [UsersGroupData] {
        
        // return complete array if string is empty
        if groupsType?.count ?? 0 <= 0 {
            return groupList ?? []
        } else {
            // Filtering Ussers array
            var filteredUsersList : [UsersGroupData] = []
            if let groupListObject = groupList {
                
                filteredUsersList = groupListObject.filter { (aGroup) -> Bool in
                    
                    if let groupName = aGroup.usersData?.first?.groupName,
                        let aUserGroupType = GroupTypes.init(rawValue: groupName) {
                        
                        return groupsType?.contains(aUserGroupType) ?? false
                        
                    } else {
                        return false
                    }
                }
                
               /* groupListObject.forEach { (aGroup) in
                    
                    let filterdUsers = self.filterUsers(byGroupType: groupsType, fromUserList: aGroup.usersData)
                    
                    if filterdUsers.count > 0 {
                        var aGroupObject = aGroup
                        aGroupObject.usersData = filterdUsers
                        filteredUsersList.append(aGroupObject)
                    }
                }*/
            }
            
            return filteredUsersList
        }
        
    }
    
    /**
     Filter users List.
     
     - parameter groupsType: group on which we want to filter.
     - parameter fromGroupList: Users list.
     
     - returns: Array of filtered users .
     */
    class public func filterUsers(byGroupType groupsType : [GroupTypes]? ,fromUserList usersList : [UsersData]? ) -> [UsersData] {
        
        // return complete array if string is empty
        if groupsType?.count ?? 0 <= 0 {
            return usersList ?? []
        } else {
            // Filtering Ussers array
            var filteredUsersList : [UsersData] = []
            if let usersListObject = usersList {
                
                filteredUsersList = usersListObject.filter() {
                    
                    if let aUserGroupType = GroupTypes.init(rawValue: $0.groupName) {
                        return groupsType?.contains(aUserGroupType) ?? false
                    } else {
                        return false
                    }
                }
            }
            
            return filteredUsersList
        }
    }
}

//MARK:- KeyBoard Notifications
extension UserSelectionBaseVC {
    func setupViewResizerOnKeyboardShown() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShowForResizing(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHideForResizing(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShowForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let window = self.view.window?.frame {
            self.tabBarController?.tabBar.isHidden = true
            
            // We're not just minusing the kb height from the view height because
            // the view could already have been resized for the keyboard before
            self.view.frame = CGRect(x: self.view.frame.origin.x,
                                     y: self.view.frame.origin.y,
                                     width: self.view.frame.width,
                                     height: window.origin.y + window.height - keyboardSize.height)
            
        } else {
            debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
        }
    }
    
    @objc func keyboardWillHideForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.tabBarController?.tabBar.isHidden = false
            
            let viewHeight = self.view.frame.height
            self.view.frame = CGRect(x: self.view.frame.origin.x,
                                     y: self.view.frame.origin.y,
                                     width: self.view.frame.width,
                                     height: viewHeight + keyboardSize.height)
        } else {
            debugPrint("We're about to hide the keyboard and the keyboard size is nil. Now is the rapture.")
        }
        
        // Removing observers
        NotificationCenter.default.removeObserver(self)
    }
}
