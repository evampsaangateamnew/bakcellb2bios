//
//  MultipleUserSelectionMainVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class MultipleUserSelectionMainVC: UserSelectionBaseVC {
    
    //MARK: - Properties
    fileprivate var nextButtonCompletionBlock : ([String : UsersData]) -> () = {_ in }
    
    private var  searchContainorViewHeight: (withScroll:CGFloat,withOutScroll:CGFloat) {
        
        return (56,92)
    }
    
    private var  scrollViewDiffrance : CGFloat {
        
        return (searchContainorViewHeight.withOutScroll - searchContainorViewHeight.withScroll)
    }
    
    private let nextButtonContainorView_Normal_Height : CGFloat = 40
    
    private let topConstraintDefaultValue : CGFloat = 42
    private let leftConstraintDefaultValue : CGFloat = 8
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var searchTextField: MBTextField!
    @IBOutlet var usersButton: MBButton!
    @IBOutlet var groupsButton: MBButton!
    
    @IBOutlet var selectedCountLabel: MBLabel!
    @IBOutlet var nextButton: MBButton!
    @IBOutlet var selectAllButton: UIButton!
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var nextButtonViewView: UIView!
    
    @IBOutlet var searchContainorViewHeigthConstraint: NSLayoutConstraint!
    @IBOutlet var nextViewHeigthConstraint: NSLayoutConstraint!
    @IBOutlet var leftConstraint: NSLayoutConstraint!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextField.delegate = self
        searchTextField.setRightButtonCompletionBlock {
            self.searchTextFeidlButtonPressed()
        }
        retriveUserData()
        
        // Select individualList on start
        userButtonPressed(usersButton)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadViewLayout()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        _ = searchTextField.resignFirstResponder()
    }
    
    //MARK: - Remove observes
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Functions
    
    func retriveUserData()  {
        
        super.loadUsersInformation { (individualUsersList, groupUsersList) in
            
            self.individualUsers = individualUsersList
            self.filteredIndividualUsers = individualUsersList
            
            self.groupUsers = groupUsersList
            self.filteredGroupUsers = groupUsersList
            
            /* Load users information */
            self.loadUsersListOfType(self.userSelectedType)
        }
    }
    
    func loadViewLayout() {
        titleLabel.text = Localized("Title_Users")
        searchTextField.placeholder = Localized("Placeholder_SearchByNameORNumber")
        usersButton.setTitle(Localized("BtnTitle_Number"), for: .normal)
        groupsButton.setTitle(Localized("BtnTitle_Groups"), for: .normal)
        nextButton.setTitle(Localized("BtnTitle_NEXT"), for: .normal)
        selectedCountLabel.text = "\(selectedUserList.count) \(Localized("Info_Selected"))"
        
    }
    
    func searchTextFeidlButtonPressed() {
        print("Search button selected")
        
    }
    
    //MARK: - IBActions
    
    @IBAction func userButtonPressed(_ sender: UIButton) {
        usersButton.setButtonLayoutType(.grayButton)
        groupsButton.setButtonLayoutType(.whiteButton)
        
        resetSearch()
        
        loadUsersListOfType(.individualList)
    }
    
    @IBAction func groupButtonPressed(_ sender: UIButton) {
        usersButton.setButtonLayoutType(.whiteButton)
        groupsButton.setButtonLayoutType(.grayButton)
        
        resetSearch()
        
        loadUsersListOfType(.groupList)
        
    }
    
    @IBAction func selectAllButtonPressed(_ sender: UIButton) {
        
        if selectedUserList.count == 0 { /* If no user selected yet */
            
            /* Check current selected tab type */
            if userSelectedType == .individualList {
                
                /* Loop through selected information in background thread */
                DispatchQueue.global(qos: .background).async {
                    
                    /* Formate all selected users */
                    self.filteredIndividualUsers?.forEach { (aUser) in
                        self.selectedUserList.updateValue(aUser, forKey: aUser.msisdn)
                    }
                    
                    /* Update UI on main thread */
                    DispatchQueue.main.async {
                        self.checkAndSetNextSelectionView()
                        self.individualUsersListVC?.reloadTableViewData()
                    }
                }
                
            } else {
                /* Loop through selected information in background thread */
                DispatchQueue.global(qos: .background).async {
                    
                    self.filteredGroupUsers?.forEach { (aGroup) in
                        
                        aGroup.usersData?.forEach { (aUser) in
                            self.selectedUserList.updateValue(aUser, forKey: aUser.msisdn)
                        }
                    }
                    /* Update UI on main thread */
                    DispatchQueue.main.async {
                        self.checkAndSetNextSelectionView()
                        self.groupUsersListVC?.reloadTableViewData()
                    }
                }
            }
            
        } else /* if more then one user is selected */ {
            
            selectedUserList.removeAll()
            checkAndSetNextSelectionView()
            
            if userSelectedType == .individualList {
                individualUsersListVC?.reloadTableViewData()
            } else {
                groupUsersListVC?.reloadTableViewData()
            }
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: MBButton) {
        //        self.backPressed(sender)
        nextButtonCompletionBlock(selectedUserList)
    }
    
    func resetSearch() {
        searchTextField.text = ""
        _ = searchTextField.resignFirstResponder()
        
        
        filteredIndividualUsers = individualUsers
        filteredGroupUsers = groupUsers
        
        self.resetSearchBarView()
        
    }
    
    //MARK:- Functions
    func loadUsersListOfType(_ type : UserSelectedType) {
        
        userSelectedType = type
        
        if type == .individualList {
            if individualUsersListVC == nil {
                if let individualUserListObject :IndividualUserSelectionVC = IndividualUserSelectionVC.instantiateViewControllerFromStoryboard() {
                    
                    individualUserListObject.usersList = filteredIndividualUsers
                    individualUserListObject.delegate = self
                    self.addChildViewController(childController: individualUserListObject, onView: contentView)
                    
                    individualUsersListVC = individualUserListObject
                }
            } else {
                self.removeChildViewController(childController: GroupUserSelectionVC())
                
                individualUsersListVC?.usersList = filteredIndividualUsers
                self.addChildViewController(childController: individualUsersListVC!, onView: contentView)
                individualUsersListVC?.reloadTableViewData()
            }
        } else {
            if groupUsersListVC == nil {
                if let userGroupListObject :GroupUserSelectionVC = GroupUserSelectionVC.instantiateViewControllerFromStoryboard() {
                    
                    userGroupListObject.groupList = filteredGroupUsers
                    userGroupListObject.delegate = self
                    self.addChildViewController(childController: userGroupListObject, onView: contentView)
                    
                    groupUsersListVC = userGroupListObject
                }
            } else {
                self.removeChildViewController(childController: IndividualUserSelectionVC())
                
                groupUsersListVC?.groupList = filteredGroupUsers
                self.addChildViewController(childController: groupUsersListVC!, onView: contentView)
                groupUsersListVC?.reloadTableViewData()
            }
        }
        
    }
    
    func searchOffersByName(searchString: String = "") {
        
        if userSelectedType == .individualList {
            // find offers where name matches
            
            filteredIndividualUsers = self.filterUsers(byQueryString:searchString, fromUserList: individualUsers)
            
            // Redirect user to screen accourdingly
            loadUsersListOfType(.individualList)
        } else {
            // find offers where name matches
            //
            filteredGroupUsers = self.filterGroups(byQueryString: searchString, fromGroupList: groupUsers)
            // Redirect user to screen accourdingly
            loadUsersListOfType(.groupList)
        }
    }
    
    func checkAndSetNextSelectionView() {
        
        UIView.animate(withDuration: 0.3) {
            if self.selectedUserList.count > 0 {
                self.nextViewHeigthConstraint.constant = self.nextButtonContainorView_Normal_Height
                self.nextButtonViewView.isHidden = false
            } else {
                self.nextViewHeigthConstraint.constant = 0
                //  self.nextButtonViewView.isHidden = true
            }
            self.view.layoutIfNeeded()
        }
        
        selectedCountLabel.text = "\(selectedUserList.count) \(Localized("Info_Selected"))"
        
        if selectedUserList.count == 0 {
            selectAllButton.setImage(UIImage.imageFor(name: "all-user-unselected"), for: .normal)
        }
        else if selectedUserList.count < individualUsers?.count ?? 0 {
            selectAllButton.setImage(UIImage.imageFor(name: "partial-user-selected"), for: .normal)
        }
        else{
            selectAllButton.setImage(UIImage.imageFor(name: "all-user-selected"), for: .normal)
        }
    }
    
    func resetSearchBarView() {
        self.view.layoutIfNeeded()
        
        self.topConstraint.constant = self.topConstraintDefaultValue
        self.leftConstraint.constant = self.leftConstraintDefaultValue
        
        self.titleLabel.alpha = 1.0
        
        self.searchContainorViewHeigthConstraint.constant = self.searchContainorViewHeight.withOutScroll
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideSearchBarView() {
        self.view.layoutIfNeeded()
        
        self.leftConstraint.constant = self.titleLabel.frame.x
        self.topConstraint.constant =  self.backButton.frame.y
        
        self.titleLabel.alpha = 0.0
        
        self.searchContainorViewHeigthConstraint.constant = self.searchContainorViewHeight.withScroll
        
        UIView.animate(withDuration: 0.3) {
            
            self.view.layoutIfNeeded()
        }
        
    }
    
    func setNextButtonCompletionBloch(_ nextButtonBlock : @escaping ([String : UsersData]) -> () = {_ in }) {
        
        nextButtonCompletionBlock = nextButtonBlock
    }
}


// UsersList viewControllers Delegate
extension MultipleUserSelectionMainVC: UsersListDelegate {
    
    func mbScrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentSize.height > self.contentView.frame.height &&  scrollView.contentSize.height >  (self.contentView.frame.size.height + searchTextField.frame.origin.y + searchTextField.frame.size.height) {
            if scrollView.contentOffset.y > 0 && scrollView.contentOffset.y < self.scrollViewDiffrance  {
                
                self.topConstraint.constant =  self.topConstraintDefaultValue - scrollView.contentOffset.y
                
                self.leftConstraint.constant = (self.leftConstraintDefaultValue + scrollView.contentOffset.y)
                
                self.searchContainorViewHeigthConstraint.constant = self.searchContainorViewHeight.withOutScroll - scrollView.contentOffset.y
                
                self.titleLabel.alpha = (1 - scrollView.contentOffset.y/self.scrollViewDiffrance) - 0.3
                
            } else if scrollView.contentOffset.y >= self.scrollViewDiffrance {
                
                self.hideSearchBarView()
            } else if scrollView.contentOffset.y <= 0 {
                self.resetSearchBarView()
                
            }
        } else  {
            self.resetSearchBarView()
        }
    }
    
    
    func mbScrollViewDidEndScrool(_ scrollview: UIScrollView) {
        if (((backButton.frame.origin.y + backButton.frame.size.height) / 2 ) + backButton.frame.origin.y) <= topConstraint.constant {
            self.resetSearchBarView()
        }
        else  if scrollview.contentSize.height >  (self.contentView.frame.size.height + searchTextField.frame.origin.y + searchTextField.frame.size.height) {
            self.hideSearchBarView()
        }
    }
    
    // Selection delegate
    
    func singleUserDidSelect(_ selectedUser : UsersData?) {
        if let aSelectedUser = selectedUser {
            selectedUserList.updateValue(aSelectedUser, forKey: aSelectedUser.msisdn)
            checkAndSetNextSelectionView()
        }
    }
    
    func userGroupDidSelect(_ selectedGroup: UsersGroupData?) {
        
        if let aSelectedGroup = selectedGroup {
            
            /* Loop through selected information in background thread */
            DispatchQueue.global(qos: .background).async {
                
                aSelectedGroup.usersData?.forEach { (aUser) in
                    self.selectedUserList.updateValue(aUser, forKey: aUser.msisdn)
                }
                /* Update UI on main thread */
                DispatchQueue.main.async {
                    self.checkAndSetNextSelectionView()
                }
            }
        }
    }
    
    func singleUserDidDeselect(_ selectedUser:UsersData?) {
        if let aSelectedUser = selectedUser {
            if selectedUserList.keys.contains(aSelectedUser.msisdn) == true {
                
                selectedUserList.removeValue(forKey: aSelectedUser.msisdn)
                checkAndSetNextSelectionView()
            }
        }
    }
    
    func userGroupDidDeselect(_ selectedGroup: UsersGroupData?) {
        
        if let aSelectedGroup = selectedGroup {
            /* Loop through selected information in background thread */
            DispatchQueue.global(qos: .background).async {
                aSelectedGroup.usersData?.forEach { (aUser) in
                    self.selectedUserList.removeValue(forKey: aUser.msisdn)
                }
                /* Update UI on main thread */
                DispatchQueue.main.async {
                    self.checkAndSetNextSelectionView()
                }
            }
        }
    }
}


//MARK:- UITextFieldDelegate

extension MultipleUserSelectionMainVC : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        setupViewResizerOnKeyboardShown()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        
        if textField == searchTextField {
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            // Filter offers with new name
            self.searchOffersByName(searchString: newString)
            
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            _ = textField.resignFirstResponder()
            
            return false
        } else {
            return true
        }
    }
}

