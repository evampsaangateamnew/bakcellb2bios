//
//  GroupUserSelectionVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class GroupUserSelectionVC: UIViewController {
    //MARK:- Properties
    weak var delegate: UsersListDelegate?
    var groupList : [UsersGroupData]? = []
    var isSingleUserSelection :Bool = false
    
    //MARK:- IBOutlet
    @IBOutlet var usersGroupTableView : MBAccordionTableView!
    
    //MARK:- ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usersGroupTableView.delegate = self
        usersGroupTableView.dataSource = self
        usersGroupTableView.allowMultipleSectionsOpen = true
        usersGroupTableView.allowsMultipleSelection = true
        self.usersGroupTableView.scrollsToTop = true
        usersGroupTableView.hideDefaultSeprator()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadTableViewData()
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        
        reloadTableViewData()
    }
    
    //MARK:- Function
    func reloadTableViewData(){
        if groupList?.count ?? 0 <= 0 {
            self.usersGroupTableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.usersGroupTableView.hideDescriptionView()
        }
        
        self.usersGroupTableView.reloadData()
        self.usersGroupTableView.scrollToFirstRow()
    }
}

//MARK: - Table View Delegates
extension GroupUserSelectionVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return groupList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let hederView:ExpandableUsersGroupView  = tableView.dequeueReusableHeaderFooterView() {
            
            hederView.setLayout(aGroup: groupList?[section])
            return hederView
        }
        
        return MBAccordionTableViewHeaderView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupList?[section].usersData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell : UsersCell = tableView.dequeueReusableCell() {
            
            let aUserObject = groupList?[indexPath.section].usersData?[indexPath.row]
            
            cell.setLayout(aUser: aUserObject, showGroupName: false, showIndicator: isSingleUserSelection)
            
            if let selectedUserList = (self.parent as? UserSelectionBaseVC)?.selectedUserList {
                if selectedUserList.keys.contains(aUserObject?.msisdn ?? "") {
                    tableView.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.none)
                } else {
                    tableView.deselectRow(at: indexPath, animated: true)
                }
            } else {
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.singleUserDidSelect(groupList?[indexPath.section].usersData?[indexPath.row])
        let selectedCell:UsersCell = tableView.cellForRow(at: indexPath) as! UsersCell
        selectedCell.myContentView.backgroundColor = .clear
        //selectedCell.contentView.backgroundColor = .white
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.delegate?.singleUserDidDeselect(groupList?[indexPath.section].usersData?[indexPath.row])
        let selectedCell:UsersCell = tableView.cellForRow(at: indexPath) as! UsersCell
        selectedCell.myContentView.backgroundColor = .white
//        selectedCell.contentView.backgroundColor = .white
        
    }
}

extension GroupUserSelectionVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableUsersGroupView {
            
            headerView.setExpandStatus(true)
            groupList?[section].isSectionExpanded = true
        }
        
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableUsersGroupView {
            headerView.setExpandStatus(false)
            groupList?[section].isSectionExpanded = false
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didLogPressedSection section: Int, withHeader header: UITableViewHeaderFooterView?, longPressGestureState state: UIGestureRecognizer.State) {
        
        if let headerView = header as? ExpandableUsersGroupView,
            isSingleUserSelection == false {
            
            switch state {
            case .possible:
                print("Long press gesture state : possible\n")
            case .began:
                print("Long press gesture state : began\n")
                self.delegate?.userGroupDidSelect(groupList?[section])
                
                headerView.myContentView.showRippleAnimation()
                break
            case .changed:
                print("Long press gesture state : changed\n")
            case .ended:
                
                self.reloadTableViewData()
                break
            case .cancelled:
                print("Long press gesture state : cancelled\n")
            case .failed:
                print("Long press gesture state : failed\n")
            default:
                break
            }
        }
    }
}

//MARK: - Scroll View Delegates
extension GroupUserSelectionVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.delegate?.mbScrollViewDidScroll(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.delegate?.mbScrollViewDidEndScrool(scrollView)
    }
}

