//
//  MySubscribeVC.swift
//  Bakcell
//
//  Created by Abdulrehman Warraich on 9/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import ObjectMapper

class MySubscribeVC: BaseVC {
    
    //MARK:- Properties
    var mySubscriptionData : MySubscription?
    var selectedTabType : MBOfferTabType = MBOfferTabType.Internet
    var selectedUsageType : MBOfferTabType?
    var isRedirectedFromHome : Bool = false

    var tabMenuArray : [MBOfferTabType] = [MBOfferTabType.Call,
                                           MBOfferTabType.Internet,
                                           MBOfferTabType.SMS,
                                           MBOfferTabType.Hybrid,
                                           MBOfferTabType.Roaming]
    
    
    //MARK:- IBOutlet
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    
    
    
    //MARK:- ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load MySubscription
        getMySubscriptionOfferings()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        loadViewLayout()
        
        self.selectTabOfType(selectedTabType)
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout() {
        
        titleLabel.text = Localized("Title_Subscription")
    }
    
    func loadMySubcribtion(_ newSubcribtion : MySubscription) {
        //  set new data
        self.mySubscriptionData = newSubcribtion
        
        // Redirect to selected Tab
        self.reDirectUserToSelectedScreen(selectedItem: self.selectedTabType)
        
    }
    
    func selectTabOfType(_ userSelectedTabType: MBOfferTabType = .Internet) {
        
        reloadCollectionViewData()
        
        let index = self.tabMenuArray.firstIndex(of: userSelectedTabType)
        
        let selectTabIndexPath = IndexPath(item:index ?? 0, section:0)
        
        self.collectionView.selectItem(at: selectTabIndexPath , animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
        
    }
    
    func reloadCollectionViewData()  {
        
        //  if (tabMenuArray.count <= 0 ){
        //  self.collectionView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        //
        //  }
        //  else {
        //  self.collectionView.hideDescriptionView()
        //  }
        
        collectionView.reloadData()
    }
    
    
    func reDirectUserToSelectedScreen(selectedItem : MBOfferTabType) {
        
        // Removing subview from main view then adding updated again
        self.removeChildViewController(childController: SubscribeMainVC())
        
        var mySelectedTabItem : MBOfferTabType = selectedItem
        
        if let subscriptionVC :SubscribeMainVC = SubscribeMainVC.instantiateViewControllerFromStoryboard() {
            
            if let mySubscription = mySubscriptionData {
                
                if isRedirectedFromHome &&
                    ( selectedUsageType == .Call
                        || selectedUsageType == .SMS
                        || selectedUsageType == .Internet) {
                    
                    mySelectedTabItem = .AllInclusive
                }
                
                
                switch mySelectedTabItem {
                    
                case MBOfferTabType.Call:
                    if let offers = mySubscription.callOffers {
                        subscriptionVC.offers = offers
                    }
                    
                case MBOfferTabType.Internet:
                    
                    if let offers = mySubscription.internetOffers {
                        subscriptionVC.offers = offers
                    }
                    
                case MBOfferTabType.SMS:
                    if let offers = mySubscription.smsOffers {
                        subscriptionVC.offers = offers
                    }
                    
                case MBOfferTabType.Campaign
                    :
                    if let offers = mySubscription.campaignOffers {
                        subscriptionVC.offers = offers
                    }
                    
                case MBOfferTabType.TM:
                    if let offers = mySubscription.tmOffers {
                        subscriptionVC.offers = offers
                    }
                    
                case MBOfferTabType.Hybrid:
                    if let offers = mySubscription.hybridOffers {
                        subscriptionVC.offers = offers
                    }
                case MBOfferTabType.Roaming:
                    if let offers = mySubscription.roamingOffers {
                        subscriptionVC.offers = offers
                    }
                    
                case MBOfferTabType.AllInclusive:
                    
                    if selectedUsageType == .Call {
                        
                        if let offers = mySubscription.voiceInclusiveOffers {
                            
                            if offers.count > 0 {
                                subscriptionVC.offers = offers
                                
                            } else if let mainOffers = mySubscription.callOffers {
                                
                                subscriptionVC.offers = mainOffers
                                mySelectedTabItem = .Call
                            }
                            
                        } else if let offers = mySubscription.callOffers {
                            
                            subscriptionVC.offers = offers
                            mySelectedTabItem = .Call
                        }
                        
                    } else if selectedUsageType == .SMS {
                        
                        if let offers = mySubscription.smsInclusiveOffers  {
                            
                            if offers.count > 0 {
                                subscriptionVC.offers = offers
                                
                            } else if let mainOffers = mySubscription.smsOffers {
                                
                                subscriptionVC.offers = mainOffers
                                mySelectedTabItem = .SMS
                            }
                        } else if let offers = mySubscription.smsOffers {
                            
                            subscriptionVC.offers = offers
                            mySelectedTabItem = .SMS
                        }
                        
                    } else if selectedUsageType == .Internet {
                        
                        if let offers = mySubscription.internetInclusiveOffers {
                            if offers.count > 0 {
                                subscriptionVC.offers = offers
                            } else if let mainOffers = mySubscription.internetOffers {
                                
                                subscriptionVC.offers = mainOffers
                                mySelectedTabItem = .Internet
                            }
                            
                        } else if let offers = mySubscription.internetOffers {
                            
                            subscriptionVC.offers = offers
                            mySelectedTabItem = .Internet
                        }
                    } else {
                        subscriptionVC.offers = []
                    }
                    
                    if mySelectedTabItem == .AllInclusive {
                        subscriptionVC.selectedUsageType = self.selectedUsageType
                        
                        tabMenuArray = [MBOfferTabType.Call,
                                        MBOfferTabType.Internet,
                                        MBOfferTabType.SMS,
                                        MBOfferTabType.Hybrid,
                                        MBOfferTabType.Roaming,
                                        MBOfferTabType.AllInclusive]
                        
                    } else {
                        subscriptionVC.selectedUsageType = nil
                        mySelectedTabItem = selectedItem
                        
                        tabMenuArray = [MBOfferTabType.Call,
                                        MBOfferTabType.Internet,
                                        MBOfferTabType.SMS,
                                        MBOfferTabType.Hybrid,
                                        MBOfferTabType.Roaming]
                    }
                    
                }
                
            }
            
            /* if selected type is not roaming and user redirected from home and offers count is 0 the load Hybrid section */
            //  if self.selectedTabType != .Roaming && isRedirectedFromHome && subscriptionVC.offers.count <= 0 {
            
            /*  Redirected user to selected type of data comming from home and if offers count is 0 then load Hybrid section */
            if isRedirectedFromHome && subscriptionVC.offers.count <= 0 {
                
                // Set selected type to Hybrid
                mySelectedTabItem = .Hybrid
                
                if let offers = mySubscriptionData?.hybridOffers {
                    subscriptionVC.offers = offers
                }
                
            }
            
            //Reset redirection check
            isRedirectedFromHome = false
            
            // setting selected type
            self.selectedTabType = mySelectedTabItem
            subscriptionVC.selectedTabType = mySelectedTabItem
            
            // Higlight selected collectionView cell color.
            self.selectTabOfType(mySelectedTabItem)
            
            
            self.addChildViewController(childController: subscriptionVC, onView: mainView)
            
        }
    }
    
    //MARK: - API calls
    
    func getMySubscriptionOfferings() {

        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getSubscriptions({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let mySubscriptionOffersHandler = Mapper<MySubscription>().map(JSONObject: resultData){
                        
                        self.mySubscriptionData = mySubscriptionOffersHandler
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Redirect to selected Tab
            self.reDirectUserToSelectedScreen(selectedItem: self.selectedTabType)
        })
    }
}

//MARK: - UICollectionViewDelegate

extension MySubscribeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let str: String = tabMenuArray[indexPath.item].localizedString()
        
        return CGSize(width: ((str as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.mbArial(fontSize: 14)]).width + 30), height: 35)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabMenuArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as? SubscriptionCell {
            
            cell.titleLabel.text = tabMenuArray[indexPath.item].localizedString()
            cell.layer.borderColor = UIColor.mbBorderGray.cgColor
            cell.titleLabel.highlightedTextColor = UIColor.black
            
            
            let bgColorView = UIView()
            bgColorView.backgroundColor = UIColor.mbButtonBackgroundGray
            cell.selectedBackgroundView = bgColorView
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if mySubscriptionData != nil {
            
            // Check if user select same selected tab again if true then do nothing.
            if selectedTabType == tabMenuArray[indexPath.item] {
                return
            }
            
            // load selected tab information
            selectedTabType = tabMenuArray[indexPath.item]
            
            reDirectUserToSelectedScreen(selectedItem: tabMenuArray[indexPath.item])
            
        } else {
            // get all information
            selectedTabType = tabMenuArray[indexPath.item]
            getMySubscriptionOfferings()
        }
        
    }
    
}

