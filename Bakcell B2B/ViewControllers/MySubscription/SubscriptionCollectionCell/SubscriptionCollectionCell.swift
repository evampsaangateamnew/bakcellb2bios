//
//  SubscriptionCollectionCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SubscriptionCollectionCell: UICollectionViewCell {
    
    @IBOutlet var tableView: MBAccordionTableView!
    
    override func awakeFromNib() {

        tableView.hideDefaultSeprator()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180.0
        tableView.allowsSelection = false

        tableView.initialOpenSections = [0]

    }
    
}
