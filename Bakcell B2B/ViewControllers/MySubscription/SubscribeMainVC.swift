//
//  SubscribeMainVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import ObjectMapper
import UPCarouselFlowLayout

class SubscribeMainVC: UIViewController {
    
    //MARK:- Properties
    var offers : [SubscriptionOffers] = []
    var selectedTabType : MBOfferTabType = MBOfferTabType.Internet
    var selectedUsageType : MBOfferTabType?
    var isReloaded : Bool = true
    
    
    //MARK:- IBOutlet
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var offerNumberLabel: UILabel!
    
    @IBOutlet var noOfferFoundView: UIView!
    @IBOutlet var offerTitleLabel: UILabel!
    @IBOutlet var noOfferDiscription: UITextView!
    @IBOutlet var offersButton: MBButton!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if offers.count == 0 {
            collectionView.isHidden = true
            noOfferFoundView.isHidden = false
            
            offerNumberLabel.text = "0/0"
            
            
        } else {
            collectionView.isHidden = false
            noOfferFoundView.isHidden = true
            offerNumberLabel.text = "1/\(offers.count)"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadViewLayout()
        
        /* Collection view layout */
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width - 40) , height: (UIScreen.main.bounds.height * 0.60))
        layout.scrollDirection = .vertical
        layout.sideItemScale = 0.8
        layout.sideItemAlpha = 0.6
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 12)
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    //MARK: - IBACTIONS
    @IBAction func reNewButtonPressed(_ sender: UIButton) {
        let selectedOffer = offers[sender.tag]
        
        self.showConfirmationAlert(message: "\(Localized("Message_RenewOffer"))", balanceAmount: nil, {
            self.changeSupplementaryOffering(selectedOffer: selectedOffer, actionType: true)
        })
        
    }
    
    @IBAction func deActivateButtonPressed(_ sender: UIButton) {
        let selectedOffer = offers[sender.tag]
        self.showConfirmationAlert(message: "\(Localized("Message_DeactivateOffer"))", balanceAmount: nil, {
            self.changeSupplementaryOffering(selectedOffer: selectedOffer, actionType: false)
        })
    }
    
    @IBAction func offersButtonPressed(_ sender: UIButton) {
        
        if let supplementoryOfferVC :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard() {
        
        // Always one call session 
        // supplementoryOfferVC.selectedTabType = .Call
        supplementoryOfferVC.selectedTabType = selectedTabType
        self.navigationController?.pushViewController(supplementoryOfferVC, animated: true)
        }
        
    }
    
    //MARK: - FUNCTIONS
    func loadViewLayout() {
        offerTitleLabel.text = Localized("NoSubscriptions_text")
        noOfferDiscription.text = Localized("Description_NoSubscriptions")
        offersButton .setTitle(Localized("OFFERS_BTNTITLE"), for: UIControl.State.normal)
    }
    
    // MARK: - API Calls
    
    /// Call 'changeSupplementaryOffering' API .
    ///
    /// - returns: Void
    func changeSupplementaryOffering(selectedOffer: SubscriptionOffers, actionType:Bool) {
        
        self.showActivityIndicator()

        _ = MBAPIClient.sharedClient.changeSupplementaryOffering(msisdn: MBSelectedUserSession.shared.msisdn, actionType: actionType, offeringId: selectedOffer.header?.offeringId ?? "", offerName: selectedOffer.header?.offerName ?? "", { (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in

            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    // Parssing response data
                    if let mySubscriptionOffersHandler = Mapper<SupplementaryMySubscription>().map(JSONObject: resultData),
                        let newSubscriptionData = mySubscriptionOffersHandler.mySubscriptionsData {

                        // set data in main subcribtion and reload.
                        if let mainSubcribtion = self.parent as? MySubscribeVC {
                            mainSubcribtion.loadMySubcribtion(newSubscriptionData)
                        }

                        // Show succes alert
                        self.showSuccessAlertWithMessage(message: mySubscriptionOffersHandler.message)

                    } else {
                        // Show Error alert
                        self.showErrorAlertWithMessage(message: resultDesc )

                    }
                } else {
                    // Show Error alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - Collection View Delegate
extension SubscribeMainVC : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionCollectionCell", for: indexPath) as? SubscriptionCollectionCell {
            
            cell.tableView.tag = indexPath.item
            cell.tableView.delegate = self
            cell.tableView.dataSource = self
            cell.tableView.allowMultipleSectionsOpen = false
            cell.tableView.keepOneSectionOpen = true;
            
            cell.tableView.reloadData()
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    // USED to toggle
    func openSelectedSection() {
        
        
        let visbleIndexPath = collectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = collectionView.cellForItem(at: aIndexPath) as? SubscriptionCollectionCell {
                
                let type = typeOfDataInAOffer(aOffer: offers[cell.tableView.tag])
                
                if offers[cell.tableView.tag].openedViewSection == MBSectionType.Header && type.contains(MBSectionType.Header) {
                    
                    openSectionAt(index: 0, tableView: cell.tableView)
                    
                } else if offers[cell.tableView.tag].openedViewSection == MBSectionType.Detail && type.contains(MBSectionType.Detail) {
                    
                    let index = type.firstIndex(of:MBSectionType.Detail)
                    
                    openSectionAt(index: index ?? 0, tableView: cell.tableView)
                    
                } else {
                    openSectionAt(index: 0, tableView: cell.tableView)
                }
            }
        }
    }
    
    func openSectionAt(index : Int , tableView : MBAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            
            // tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: .top, animated: true)
            
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
    
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>

extension SubscribeMainVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return typeOfDataInAOffer(aOffer: offers[tableView.tag]).count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 45
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let type = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        switch type[section] {
            
        case MBSectionType.Header:
            
            let offerName :String = offers[tableView.tag].header?.offerName ?? ""
            if let headerView:SupplementaryOffersHeaderView = tableView.dequeueReusableHeaderFooterView(){
                headerView.setViewForOfferNameWithSickerValues(offerName: offerName, stickerTitle: nil, stickerColorCode: nil)
                return headerView
            }
            
        case MBSectionType.Detail:
            if let headerView:SupplementaryOffersHeaderView = tableView.dequeueReusableHeaderFooterView(){
                let title :String = Localized("Title_Details")
                
                if offers[tableView.tag].openedViewSection == MBSectionType.Detail {
                    headerView.setViewWithTitle(title: title, isSectionSelected: true, headerType: MBSectionType.Detail)
                } else {
                    headerView.setViewWithTitle(title: title, isSectionSelected: false, headerType: MBSectionType.Detail)
                }
                return headerView
            }
            
        case MBSectionType.Subscription:
            
            if let headerView:SubscribeButtonView = tableView.dequeueReusableHeaderFooterView() {
                
                let aOffer = offers[tableView.tag]
                headerView.setRenewDeActivateButton(tag: tableView.tag, renewEnable: aOffer.header?.btnRenew?.isButtonEnabled() ?? false, deactivateEnable: aOffer.header?.btnDeactivate?.isButtonEnabled() ?? false)
                
                headerView.activateRenewButton.addTarget(self, action: #selector(reNewButtonPressed(_:)), for: UIControl.Event.touchUpInside)
                headerView.subscribedButton.addTarget(self, action: #selector(deActivateButtonPressed(_:)), for: UIControl.Event.touchUpInside)
                return headerView
            }
            
        default:
            break
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        switch types[section] {
            
        case MBSectionType.Header:
            return typeOfDataInHeaderSection(aHeader: offers[tableView.tag].header).count
            
        case MBSectionType.Detail:
            return typeOfDataInDetailSection(aDetails: offers[tableView.tag].details).count
            
        case MBSectionType.Subscription:
            return 0
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        switch types[indexPath.section] {
            
        case MBSectionType.Header:
            
            let headerDataType = typeOfDataInHeaderSection(aHeader: offers[tableView.tag].header)
            
            if let aHeader = offers[tableView.tag].header {
                
                switch headerDataType[indexPath.row] {
                    
                case MBOfferType.TypeTitle:
                    
                    if let cell : SupplementaryExpandCell = tableView.dequeueReusableCell() {
                        cell.setOfferType(header: aHeader)
                        return cell
                    }
                    
                case MBOfferType.OfferGroup:
                    
                    if let cell : SupplementaryExpandCell = tableView.dequeueReusableCell() {
                        cell.setOfferGroup(offerGroup: aHeader.offerGroup)
                        return cell
                    }
                    
                case MBOfferType.OfferActive:
                    if let cell : SupplementaryExpandCell = tableView.dequeueReusableCell() {
                        cell.setOfferStatus(header: aHeader)
                        return cell
                    }
                    
                case MBOfferType.AttributeList:
                    if let cell:AttributeListCell = tableView.dequeueReusableCell() {
                        cell.setAttributeListOfMySubscription(attributeList: aHeader.attributeList?[0])
                        return cell
                    }
                    
                case MBOfferType.Usage:
                    
                    let remaingData = headerDataType.filter {$0 != MBOfferType.Usage}
                    let index = indexPath.row - remaingData.count
                    
                    if let cell:SubscriptionUsageCell = tableView.dequeueReusableCell() {
                        cell.setProgressLayout(aUsage: aHeader.usage?[index], isRenewButtonEnabled: aHeader.btnRenew?.isButtonEnabled() ?? false, selectedUsageType: self.selectedUsageType)
                        return cell
                    }
                    
                default:
                    break
                }
            }
            
        case MBSectionType.Detail :
            
            let aOfferDetails = offers[tableView.tag].details
            
            let containingDataTypes = typeOfDataInDetailSection(aDetails: aOfferDetails)
            
            switch containingDataTypes[indexPath.row] {
                
            case MBOfferType.Price:
                
                if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                    cell.setPriceLayoutValues(price: aOfferDetails?.price, showIcon: false)
                    return cell
                }
                
            case MBOfferType.Rounding:
                
                if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                    cell.setRoundingLayoutValues(rounding: aOfferDetails?.rounding, showIcon: false)
                    return cell
                }
                
            case MBOfferType.TextWithTitle:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setTextWithTitleLayoutValues(textWithTitle: aOfferDetails?.textWithTitle)
                    return cell
                }
                
            case MBOfferType.TextWithOutTitle:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aOfferDetails?.textWithOutTitle)
                    return cell
                }
                
            case MBOfferType.TextWithPoints:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setTextWithPointsLayoutValues(textWithPoint: aOfferDetails?.textWithPoints)
                    return cell
                }
                
            case MBOfferType.Date:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setDateAndTimeLayoutValues(dateAndTime: aOfferDetails?.date)
                    return cell
                }
                
            case MBOfferType.Time:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setDateAndTimeLayoutValues(dateAndTime: aOfferDetails?.time)
                    return cell
                }
                
            case MBOfferType.RoamingDetails:
                
                if let cell:RoamingDetailsCell = tableView.dequeueReusableCell() {
                    cell.setRoamingDetailsLayout(roamingDetails: aOfferDetails?.roamingDetails)
                    return cell
                }
                
            case MBOfferType.FreeResourceValidity:
                
                if let cell:FreeResourceValidityCell = tableView.dequeueReusableCell() {
                    cell.setFreeResourceValidityValues(freeResourceValidity: aOfferDetails?.freeResourceValidity)
                    return cell
                }
                
            case MBOfferType.TitleSubTitleValueAndDesc:
                
                if let cell:TitleSubTitleValueAndDescCell = tableView.dequeueReusableCell() {
                    cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aOfferDetails?.titleSubTitleValueAndDesc)
                    return cell
                }
                
            default:
                break
            }
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    //MARK: - Helper fuction for data loading
    
    func typeOfDataInAOffer(aOffer : SubscriptionOffers?) -> [MBSectionType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBSectionType] = []
        
        if let aOffer =  aOffer {
            // 1
            if aOffer.header != nil {
                
                dataTypes.append(MBSectionType.Header)
            }
            // 2
            if aOffer.details != nil  {
                
                dataTypes.append(MBSectionType.Detail)
            }
            
            dataTypes.append(MBSectionType.Subscription)
        }
        return dataTypes
    }
    
    func typeOfDataInHeaderSection(aHeader : SubscriptionHeader?) -> [MBOfferType] {
        
        var dataTypeArray : [MBOfferType] = []
        if let aHeader =  aHeader {
            
            if selectedTabType == MBOfferTabType.Internet ||
                selectedTabType == MBOfferTabType.Roaming ||
                selectedUsageType == MBOfferTabType.Internet {
                
                if checkOfferContainsHeaderType(aHeader: aHeader) {
                    dataTypeArray.append(MBOfferType.TypeTitle)
                }
                
                if checkOfferContainsOfferGroup(aHeader: aHeader) {
                    dataTypeArray.append(MBOfferType.OfferGroup)
                }
            }
            
            dataTypeArray.append(MBOfferType.OfferActive)
            
            if aHeader.attributeList?.indices.contains(0) ?? false {
                dataTypeArray.append(MBOfferType.AttributeList)
            }
            
            if aHeader.usage != nil {
                aHeader.usage?.forEach({ (aUsage) in
                    
                    dataTypeArray.append(MBOfferType.Usage)
                })
            }
            
        }
        return dataTypeArray
    }
    
    func typeOfDataInDetailSection(aDetails : DetailsAndDescription?) -> [MBOfferType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBOfferType] = []
        
        if let aDetailsData =  aDetails {
            // 1
            if aDetailsData.price != nil {
                
                dataTypes.append(MBOfferType.Price)
            }
            
            // 2
            if aDetailsData.rounding != nil  {
                
                dataTypes.append(MBOfferType.Rounding)
            }
            // 3
            if aDetailsData.textWithTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithTitle)
            }
            // 4
            if aDetailsData.textWithOutTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithOutTitle)
            }
            // 5
            if aDetailsData.textWithPoints != nil {
                
                dataTypes.append(MBOfferType.TextWithPoints)
            }
            // 6
            if aDetailsData.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(MBOfferType.TitleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailsData.date != nil {
                
                dataTypes.append(MBOfferType.Date)
            }
            // 8
            if aDetailsData.time != nil {
                
                dataTypes.append(MBOfferType.Time)
            }
            // 9
            if aDetailsData.roamingDetails != nil {
                
                dataTypes.append(MBOfferType.RoamingDetails)
            }
            // 10
            if aDetailsData.freeResourceValidity != nil {
                
                dataTypes.append(MBOfferType.FreeResourceValidity)
            }
        }
        return dataTypes
    }
    
    func checkOfferContainsOfferGroup(aHeader : SubscriptionHeader?) -> Bool {
        
        // Atleast one because there is one with offer name
        var isContainsOfferGroup : Bool = false
        
        if selectedTabType == MBOfferTabType.Internet ||
            selectedTabType == MBOfferTabType.Roaming ||
            selectedUsageType == MBOfferTabType.Internet {
            
            if let aHeader =  aHeader {
                
                if aHeader.offerGroup != nil {
                    isContainsOfferGroup = true
                }
            }
        }
        
        return isContainsOfferGroup
    }
    
    func checkOfferContainsHeaderType(aHeader : SubscriptionHeader?) -> Bool {
        
        // Atleast one because there is one with offer name
        var isContainsOfferGroup : Bool = false
        
        if selectedTabType == MBOfferTabType.Roaming {
            
            if let aHeader =  aHeader {
                
                if aHeader.type != nil {
                    isContainsOfferGroup = true
                }
            }
        }
        
        return isContainsOfferGroup
    }
    
}


// MARK: - MBAccordionTableViewDelegate

extension SubscribeMainVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.setExpandStateIcon(true)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if offers[tableView.tag].openedViewSection != headerView.viewType {
                
                offers[tableView.tag].openedViewSection = headerView.viewType;
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.setExpandStateIcon(false)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if offers[tableView.tag].openedViewSection == headerView.viewType {
                offers[tableView.tag].openedViewSection = MBSectionType.Header;
                tableView.toggleSection(0)
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        
        if let headerView = tableView.headerView(forSection: section) as? SubscribeButtonView {
            
            if headerView.viewType == MBSectionType.Subscription {
                
                return false
                
            } else {
                
                return true
            }
        } else {
            
            return true
        }
    }
}

// MARK: - ScrollView Delegate
extension SubscribeMainVC : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == collectionView {
            openSelectedSection()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let indexPath = collectionView.indexPathForItem(at: visiblePoint)
        
        if offers.count > 0 {
            
            let index : Int = indexPath?.item ?? 0
            offerNumberLabel.text = "\(index + 1 )/\(offers.count)"
            
        } else {
            offerNumberLabel.text = "0/\(offers.count)"
        }
    }
}
