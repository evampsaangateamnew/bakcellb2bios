//
//  SubscriptionUsageCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/27/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SubscriptionUsageCell: UITableViewCell {
    
    //MARK:- Properties
    
    //MARK:- IBOutlet
    @IBOutlet var myContentView: UIView!
    @IBOutlet var remainingView: UIView!
    @IBOutlet var renewalView: UIView!
    @IBOutlet var sepratorView: UIView!
    
    @IBOutlet var iconImageView: UIImageView!
    
    @IBOutlet var remainingTitle: MBMarqueeLabel!
    @IBOutlet var remainingValue: MBLabel!
    
    @IBOutlet var renewalTitle: MBLabel!
    @IBOutlet var renewalValue: MBLabel!
    @IBOutlet var activationDate: MBMarqueeLabel!
    @IBOutlet var renewalDate: MBMarqueeLabel!
    
    @IBOutlet var renewalProgress: MBGradientProgressView!
    @IBOutlet var remainingProgress: MBGradientProgressView!
    
    @IBOutlet var topProgressViewHeight: NSLayoutConstraint!
    @IBOutlet var bottomProgressViewHeight: NSLayoutConstraint!
    
    //MARK:- UITableViewCell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- Functions
    func setProgressLayout(aUsage:Usage?, isRenewButtonEnabled: Bool = false, selectedUsageType: MBOfferTabType?) {
        
        var progressBarBackGroundColor: UIColor = UIColor(hexString:"#e5e9eb")
        
        
        if let aUsageData = aUsage {
            
            if (selectedUsageType == .Call && aUsage?.type.trimmWhiteSpace == "513") ||
                (selectedUsageType == .SMS && aUsage?.type.trimmWhiteSpace == "514") ||
                (selectedUsageType == .Internet && aUsage?.type.trimmWhiteSpace == "511") {
                
                self.myContentView.backgroundColor =  UIColor.mbBackgroundGray
                self.remainingView.backgroundColor =  UIColor.mbBackgroundGray
                self.renewalView.backgroundColor =  UIColor.mbBackgroundGray
                self.sepratorView.backgroundColor =  UIColor.white
                
                progressBarBackGroundColor = UIColor(hexString:"#C0C0C0")
                
            } else {
                self.myContentView.backgroundColor =  UIColor.mbBackgroundGray
                self.remainingView.backgroundColor =  UIColor.white
                self.renewalView.backgroundColor =  UIColor.white
                self.sepratorView.backgroundColor =  UIColor.mbBackgroundGray
            }
            
            
            // If remainingUsage or totalUsage is empty then hide usage progress view
            if aUsageData.remainingUsage.isBlank ||
                aUsageData.totalUsage.isBlank ||
                aUsageData.totalUsage.isEqual("0", ignorCase: true) {
                
                topProgressViewHeight.constant = 0
                
                remainingTitle.isHidden = true
                remainingValue.isHidden = true
                remainingProgress.isHidden = true
                
                iconImageView.isHidden = true
                
            } else if aUsageData.remainingUsage.isHasFreeText() ||
                aUsageData.totalUsage.isHasFreeText() { /* Check If remainingUsage or totalUsage value is 'free' ten set progress color green */
                
                topProgressViewHeight.constant = 45
                
                remainingTitle.isHidden = false
                remainingValue.isHidden = false
                remainingProgress.isHidden = false
                
                //Icon setting
                if aUsageData.iconName.isBlank == false {
                    
                    iconImageView.isHidden = false
                    iconImageView.image = UIImage.imageFor(key: aUsageData.iconName)
                } else {
                    iconImageView.isHidden = true
                }
                
                // Top remaining values
                remainingTitle.text = aUsageData.remainingTitle
                
                remainingValue.text = Localized("Title_FREE")
                
                remainingProgress.changeGradientLayerColors(newGradientColors: [UIColor.mbGreen.cgColor,UIColor.mbGreen.cgColor], newBackgroundColor: progressBarBackGroundColor)
                
                remainingProgress.setProgress(1, animated: true)
                
            } else { /* Setting usage progress*/
                
                topProgressViewHeight.constant = 45
                
                remainingTitle.isHidden = false
                remainingValue.isHidden = false
                remainingProgress.isHidden = false
                
                //Icon setting
                if aUsageData.iconName.isBlank == false {
                    
                    iconImageView.isHidden = false
                    iconImageView.image = UIImage.imageFor(key: aUsageData.iconName)
                } else {
                    iconImageView.isHidden = true
                }
                
                // Top remaining values
                remainingTitle.text = aUsageData.remainingTitle
                
                remainingValue.text = "\(aUsageData.remainingUsage) \(aUsageData.unit) / \(aUsageData.totalUsage)"
                
                remainingProgress.changeGradientLayerColors(newBackgroundColor: progressBarBackGroundColor)
                
                let progressValue : Double = (aUsageData.totalUsage.toDouble - aUsageData.remainingUsage.toDouble) / aUsageData.totalUsage.toDouble
                
                remainingProgress.setProgress(Float(progressValue), animated: true)
            }
            
            
            /* Setting activationDate and renewalDate progress */
            if aUsageData.activationDate == nil || aUsageData.renewalDate  == nil {
                
                bottomProgressViewHeight.constant = 0
                
                activationDate.isHidden = true
                
                renewalProgress.isHidden = true
                renewalValue.isHidden = true
                renewalTitle.isHidden = true
                renewalDate.isHidden = true
                
            } else if aUsageData.activationDate?.isBlank == true || aUsageData.renewalDate?.isBlank == true {
                
                renewalProgress.changeGradientLayerColors(newBackgroundColor: progressBarBackGroundColor)
                
                bottomProgressViewHeight.constant = 62
                
                activationDate.isHidden = true
                
                renewalProgress.isHidden = false
                renewalValue.isHidden = false
                renewalTitle.isHidden = false
                renewalDate.isHidden = false
                
                renewalTitle.text = aUsageData.renewalTitle
                // set progress view value
                renewalProgress.setProgress(1, animated: true)
                
                renewalValue.text = "0 \(Localized("Info_days"))"
                activationDate.text = ""
                renewalDate.text = Localized("Info_Expired")
                
            } else {
                
                renewalProgress.changeGradientLayerColors(newBackgroundColor: progressBarBackGroundColor)
                
                bottomProgressViewHeight.constant = 62
                
                renewalTitle.isHidden = false
                renewalValue.isHidden = false
                renewalProgress.isHidden = false
                activationDate.isHidden = false
                renewalDate.isHidden = false

                // Sending empty type to calculate daliy progress
                 let computedValuesFromDates = MBProgressUtilities.calculateProgressValuesForMRC(from: aUsageData.activationDate ?? "", to: aUsageData.renewalDate ?? "", type: "")
                
                // set progress view value
                if computedValuesFromDates.daysLeft <= 0 {
                    renewalProgress.setProgress(1, animated: true)
                } else {
                    
                    renewalProgress.setProgress(computedValuesFromDates.progressValue, animated: true)
                }
                
                renewalValue.text = computedValuesFromDates.daysLeftDisplayValue
                
                // activationDate setting
                activationDate.text = "\(Localized("Info_Activated")): \(computedValuesFromDates.startDate)"
                
                if isRenewButtonEnabled == false {
                    // Bottom Renewal View
                    renewalTitle.text = Localized("Title_ToExpiration")
                    
                    renewalDate.text = "\(Localized("Info_Validity")): \(computedValuesFromDates.endDate)"
                } else {
                    
                    // Bottom Renewal View
                    renewalTitle.text = aUsageData.renewalTitle
                    
                    // renewalDate setting
                    renewalDate.text = "\(Localized("Info_RenewDate")): \(computedValuesFromDates.endDate)"
                }
            }
        }
    }
}
