//
//  TopUpVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class TopUpVC: BaseVC {
    
    //MARK: - Properties
    var selectedUserMSISDN :String = ""
    //MARK: - IBOutlet
    @IBOutlet var mobileNumberTextField: MBTextField!
    @IBOutlet var cardNumberTextField: REFormattedNumberField!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var topUpButton: UIButton!
    
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interactivePop()
        
        loadViewLayout()
        
        //mobileNumberTextField.delegate = self
        mobileNumberTextField.text = selectedUserMSISDN
        mobileNumberTextField.isEnabled = false
        //cardNumber_txt.delegate = self
        
        
    }
    
    //MARK: - FUNCTIONS
    
    func loadViewLayout(){
        cardNumberTextField.format = "XXXXX-XXXXX-XXXXX"
        cardNumberTextField.adjustsFontSizeToFitDevice()
        
        titleLabel.text = Localized("Title_TopUp")
        descriptionLabel.text = Localized("TopUp_Description")
        
        topUpButton.setTitle(Localized("BtnTitle_Topup"), for: UIControl.State.normal)
        mobileNumberTextField.placeholder = Localized("Placeholder_MobileNumber")
        cardNumberTextField.attributedPlaceholder = NSAttributedString(string: Localized("Placeholder_EnterCodeOfMoneyCard"),  attributes: [NSAttributedString.Key.foregroundColor: UIColor.mbPlaceholderGray])
    }
    
    //MARK:- IBACTIONS
    @IBAction func topUpPressed(_ sender: UIButton) {
        
        var reciverMSISDN = ""
        if let msisdnText = mobileNumberTextField.text,
            msisdnText.isValidMSISDN() {
            
            reciverMSISDN = msisdnText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterNumber"))
            return
        }
        
        var cardPinNumber = ""
        if cardNumberTextField.plainText().isValidCardNumber() {
            
            cardPinNumber = cardNumberTextField.plainText()
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidCardNumber"))
            return
        }
        
        _ = mobileNumberTextField.resignFirstResponder()
        _ = cardNumberTextField.resignFirstResponder()
        
        let attributtedConformationMessage = String(format: Localized("Message_TopUpConfirmation"), reciverMSISDN).createAttributedString(stringsListForAttributed: [reciverMSISDN], stringColor: .black)
        
        
        self.showConfirmationAlert(title: Localized("Title_Confirmation"), attributedMessage: attributtedConformationMessage, okBtnTitle: Localized("BtnTitle_OK"), cancelBtnTitle: Localized("BtnTitle_NO"), {
            /* User confirmed to toup */
            self.getTopUp(SubscriberType: MBPICUserSession.shared.picUserInfo?.subscriberType ?? "", CardNumber: cardPinNumber, TopUp: reciverMSISDN)
            
        })
        
    }
    
    //MARK: - API Call
    /// Call 'TopUp' API.
    ///
    /// - returns: Void
    
    func getTopUp( SubscriberType subscriberType: String, CardNumber cardPinNumber : String, TopUp topUpNum : String) {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getTopUp(SubscriberType: subscriberType, CardPin: cardPinNumber , TopupNumber: topUpNum , { ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let _ = Mapper<TopUp>().map(JSONObject:resultData) {
                        
                        let messageString = String(format: Localized("Message_TopUp_Success"), topUpNum).createAttributedString(stringsListForAttributed: [topUpNum], stringColor: UIColor.black)
                        self.showAlert(title: Localized("Title_successful"), attributedMessage: messageString)
                        
                        self.cardNumberTextField.text = ""
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
}

//MARK: Textfield delagates
extension TopUpVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length
        
        
        
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
        let characterSet = CharacterSet(charactersIn: string)
        if textField == mobileNumberTextField {
            if newLength <= Constants.MSISDNLength && allowedCharacters.isSuperset(of: characterSet) {
                return  true
            } else {
                return  false
            }
            
        } else {
            return false // Bool
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == mobileNumberTextField {
            
            cardNumberTextField.becomeFirstResponder()
            return false
        
        } else if textField == cardNumberTextField {
            
            _ = textField.resignFirstResponder()
            topUpPressed(UIButton())
            return false
            
        } else {
            return true
        }
    }
}
