//
//  FAQsVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/5/18.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


class FAQsVC: BaseVC {
    
    //MARK: - Properties
    var showSearch : Bool = true
    var faq : [FAQList]?
    var qaList : [QAList]?
    var filterQaList : [QAList]?
    
    //MARK: - IBOutlet
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var searchView: UIView!
    @IBOutlet var titleLabel: MBLabel!
    
    @IBOutlet var dropDown: UIDropDown!
    @IBOutlet var tableView: MBAccordionTableView!
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interactivePop()
        
        loadViewLayout()
        
        searchTextField.delegate = self
        searchView.isHidden = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowMultipleSectionsOpen = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180
        tableView.allowsSelection = false
        
        // reload data first
        reloadTableViewData()
        // Load FAQs data
        loadFAQsData()
    }
    
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        titleLabel.text = Localized("Title_FAQ")
        
        searchTextField.attributedPlaceholder = NSAttributedString(string: Localized("Title_Search"),
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    //Initialze / Setup dropdown 
    func setDropDown(){
        
        if self.faq?.indices.contains(0) ?? false {
            self.dropDown.placeholder = self.faq?[0].title ?? ""
            self.qaList = self.faq?[0].qaList
            self.filterQaList = self.qaList
            
            self.reloadTableViewData()
            
        }
        
        var titles : [String] = []
        
        faq?.forEach({ (aFAQList) in
            titles.append(aFAQList.title)
        })
        
        self.dropDown.dataSource = titles
        
        
        self.dropDown.didSelectOption { (index, option) in
            
            self.qaList = self.faq?[index].qaList
            self.filterQaList = self.qaList
            
            self.reloadTableViewData()
            
        }
        
    }
    
    ///Collapse All Sections
    private func collapseAllSections() {
        if let count = self.filterQaList?.count {
            for i in 0..<count {
                if tableView.isSectionOpen(i) {
                    tableView.toggleSection(i)
                }
            }
        }
    }
    
    ///Load FAQ Data
    func loadFAQsData() {
        // Parssing response data
        if let faqResponseHandler :FAQListHandler = FAQListHandler.loadFromUserDefaults(key: APIsType.faqDetails.localizedAPIKey()){
            
            self.faq = faqResponseHandler.faqList
            self.qaList = self.faq?[0].qaList
            self.filterQaList = self.qaList
            
            self.reloadTableViewData()
            
            self.setDropDown()
            
        } else {
            FAQsAPICall()
        }
    }
    
    ///Reload TableView Data
    func reloadTableViewData() {
        
        if (filterQaList?.count ?? 0) <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"), centerYConstant: -65)
            
        } else {
            self.tableView.hideDescriptionView()
        }
        
        tableView.reloadData()
    }
    
    //MARK: - IBActions
    
    @IBAction func searchPressed(_ sender: UIButton) {
        
        if showSearch == false {
            searchButton.setImage(UIImage.imageFor(name:"actionbar_search") , for: UIControl.State.normal)
            searchView.isHidden = true
            titleLabel.isHidden = false
            showSearch = true
            
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
            
            searchOffersByName(searchString: "")
            
        } else {
            searchButton.setImage(UIImage.imageFor(name: "Cross") , for: UIControl.State.normal)
            titleLabel.isHidden = true
            searchView.isHidden = false
            showSearch = false
            
            searchTextField.becomeFirstResponder()
        }
    }
    
    
    //MARK: - API Calls
    
    /// Call 'FAQs' API.
    ///
    /// - returns: Void
    func FAQsAPICall() {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.getFAQs({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let faqResponseHandler = Mapper<FAQListHandler>().map(JSONObject:resultData) {
                        
                        if let faqList = faqResponseHandler.faqList {
                            // Saving Supplementary Offers data in user defaults
                            faqResponseHandler.saveInUserDefaults(key: APIsType.faqDetails.localizedAPIKey())
                            
                            self.faq = faqList
                            self.qaList = self.faq?[0].qaList
                            self.filterQaList = self.qaList
                            
                            self.setDropDown()
                        }
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Reload table View data
            self.reloadTableViewData()
        })
    }
}

//MARK: - Table View Delegates

extension FAQsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let aQaList = filterQaList {
            return aQaList.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerView:ExpandableTitleView = tableView.dequeueReusableHeaderFooterView() {
            
            headerView.titleLabel.text = filterQaList?[section].question
            headerView.titleLabel.setLabelFontType(.body)
            
            // Check if section is expanded then set expaned image else set unexpended image
            headerView.setExpandStatus(filterQaList?[section].isSectionExpanded ?? false)
            
            return headerView
        } else {
            return MBAccordionTableViewHeaderView()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:FAQsCell = tableView.dequeueReusableCell() {
            cell.setDescriptionText(filterQaList?[indexPath.section].answer)
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    
}

// MARK: - <MBAccordionTableViewDelegate>

extension FAQsVC : MBAccordionTableViewDelegate {
   
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleView {
            
            headerView.setExpandStatus(true)
            filterQaList?[section].isSectionExpanded = true
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? ExpandableTitleView {
            headerView.setExpandStatus(false)
            filterQaList?[section].isSectionExpanded = false
        }
    }
}

//MARK: - UITextFieldDelegate
extension FAQsVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == searchTextField {
            
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            self.searchOffersByName(searchString: newString)
            //  tableView.closeAllSections()
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
   
    
    /**
     Search offer by name.
     
     - parameter searchString: keyword against offer to be searched.
     
     - returns: void.
     */
    func searchOffersByName(searchString: String?) {
        
        if searchString?.isBlank ?? true {
            
            filterQaList = qaList
        } else {
            
            filterQaList = searchQABy(searchString: searchString ?? "", qaList: qaList)
        }
        
        self.reloadTableViewData()
    }
    
    /**
     Search FAQ by string.
     
     - parameter searchString: keyword against offer to be searched.
     
     - returns: QAList. Array of FAQ's.
     */
    func searchQABy(searchString: String, qaList: [QAList]?) -> [QAList]? {
        
        if (qaList?.count ?? 0) > 0 {
            
            let filterQAList = qaList?.filter {
                
                return ($0 as QAList).answer.containsSubString(subString: searchString) || ($0 as QAList).question.containsSubString(subString: searchString)
                
            }
            
            if filterQAList != nil {
                
                return filterQAList
            } else {
                return []
            }
            
        } else {
            return []
        }
    }
    
    
}
