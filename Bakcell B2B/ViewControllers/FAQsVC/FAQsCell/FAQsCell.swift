//
//  FAQsCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/5/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class FAQsCell: UITableViewCell {
    
    //static let ID = "cellID"

    //MARK: - IBOutlets
    @IBOutlet var descriptionLabel: MBLabel!
    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setDescriptionText(_ descriptionText : String?) {
        self.descriptionLabel.text = descriptionText ?? ""
    }
    
}
