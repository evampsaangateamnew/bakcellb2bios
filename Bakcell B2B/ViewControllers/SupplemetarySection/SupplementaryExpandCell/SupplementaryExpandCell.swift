//
//  SupplementaryExpandCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 7/5/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SupplementaryExpandCell: UITableViewCell {


    @IBOutlet var myContentView: UIView!
    
    // ALL main view outlet for difftrent types
    @IBOutlet var OfferGroupView: UIView!
    @IBOutlet var ValidityView: UIView!


    // OfferGroup view iner outlets
    @IBOutlet var groupNameLabel: MBMarqueeLabel!
    @IBOutlet var mobileImageView: UIImageView!
    @IBOutlet var tabletImageView: UIImageView!
    @IBOutlet var desktopImageView: UIImageView!

    // Validity view iner outlets
    @IBOutlet var validityLabel: MBMarqueeLabel!
    @IBOutlet var priceLabel: MBLabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Setting background color
        self.myContentView.backgroundColor = UIColor.mbBackgroundGray
        self.OfferGroupView.backgroundColor = UIColor.mbBackgroundGray
        self.ValidityView.backgroundColor = UIColor.mbBackgroundGray


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setOfferGroup(offerGroup : OfferGroup?) {

        //"Desktop"
        //"Mobile"
        // Hiding all views first
        hideAllViews()

        if let offerGroup = offerGroup {

            OfferGroupView.isHidden = false

            groupNameLabel.text = offerGroup.groupName ?? ""

            mobileImageView.image = UIImage.imageFor(name:  "device1b")
            tabletImageView.image = UIImage.imageFor(name:  "device2b")
            desktopImageView.image = UIImage.imageFor(name:  "device3b")

            if let groupValue = offerGroup.groupValue {

                if groupValue.isEqual("Mobile", ignorCase: true) {

                    mobileImageView.image = UIImage.imageFor(name:  "device1a")

                } else if groupValue.isEqual("Desktop", ignorCase: true) {

                    desktopImageView.image = UIImage.imageFor(name:  "device3a")

                } else if groupValue.isEqual("tab", ignorCase: true) {
                    tabletImageView.image = UIImage.imageFor(name:  "device2a")
                }
            }
            
        } else {
            OfferGroupView.isHidden = true
        }
    }

    func setOfferValidity(header : Header?) {

        // Hiding all views first
        hideAllViews()

        ValidityView.isHidden = false
        validityLabel.textColor = UIColor.mbTextGray

        if let header = header{
            var validityTitleString : String = ""

            if let validityTitle = header.validityTitle {

                if !validityTitle.isBlank {
                    validityTitleString = validityTitle + ": "
                }
            }

            if let validityValue = header.validityValue {
                validityLabel.text = validityTitleString + validityValue
            } else {
                validityLabel.text = ""
            }

            if let price = header.price {
                priceLabel.attributedText = MBUtilities.createAttributedText(price, textColor: UIColor.mbTextGray, withManatSign: true)
            } else {
                priceLabel.text = ""
            }

        }
    }

    func setOfferStatus(header : SubscriptionHeader?) {

        // Hiding all views first
        hideAllViews()

        ValidityView.isHidden = false

        if let header = header {
            validityLabel.textColor = UIColor.mbGreen

            if header.isFreeResource?.isEqual("true", ignorCase: true) == true {

                validityLabel.text = header.status ?? ""
                priceLabel.text = Localized("Title_FREE")

            } else {
                validityLabel.text = header.status ?? ""
                priceLabel.attributedText = MBUtilities.createAttributedText(header.price ?? "", textColor: UIColor.mbTextGray, withManatSign: true)
            }
        } else {
            validityLabel.text = ""
        }


    }

    func setOfferType(header : Header?) {

        // Hiding all views first
        hideAllViews()

        OfferGroupView.isHidden = false

        if let header = header {

            if let type = header.type {

                //  Internet --  "Интернет" --  "İnternet"
                //  Call   --  "Звонки" --  "Zənglər"
                //  Campaign  --  "Кампании" --  "Kampaniyalar"

                mobileImageView.image = UIImage.imageFor(name:  "roamingcallb")
                tabletImageView.image = UIImage.imageFor(name:  "roaminginternetb")
                desktopImageView.image = UIImage.imageFor(name:  "romaingcampb")

                if type.isEqual("Call", ignorCase: true) ||
                    type.isEqual("Звонки", ignorCase: true) ||
                    type.isEqual("Zənglər", ignorCase: true) {

                    mobileImageView.image = UIImage.imageFor(name:  "roamingcalla")
                    groupNameLabel.text = Localized("Call")

                } else if type.isEqual("Internet", ignorCase: true) ||
                    type.isEqual("Кампании", ignorCase: true) ||
                    type.isEqual("Kampaniyalar", ignorCase: true) {

                    tabletImageView.image = UIImage.imageFor(name:  "roaminginterneta")
                    groupNameLabel.text = Localized("Internet")

                } else if type.isEqual("Campaign", ignorCase: true) ||
                    type.isEqual("Кампании", ignorCase: true) ||
                    type.isEqual("Kampaniyalar", ignorCase: true) {

                    desktopImageView.image = UIImage.imageFor(name:  "romaingcampa")
                    groupNameLabel.text = Localized("Campaign")
                }
            }
        }
    }

    func setOfferType(header : SubscriptionHeader?) {

        // Hiding all views first
        hideAllViews()

        OfferGroupView.isHidden = false

        if let header = header {

            if let type = header.type {

                mobileImageView.image = UIImage.imageFor(name:  "roamingcallb")
                tabletImageView.image = UIImage.imageFor(name:  "roaminginternetb")
                desktopImageView.image = UIImage.imageFor(name:  "romaingcampb")

                if type.isEqual("Call", ignorCase: true) ||
                    type.isEqual("Звонки", ignorCase: true) ||
                    type.isEqual("Zənglər", ignorCase: true) {

                    mobileImageView.image = UIImage.imageFor(name:  "roamingcalla")

//                    // Using DropDown_Voice for call text insted of calls
//                    groupName_lbl.text = Localized("DropDown_Voice")

                    groupNameLabel.text = Localized("Call")
                } else if type.isEqual("Internet", ignorCase: true) ||
                    type.isEqual("Кампании", ignorCase: true) ||
                    type.isEqual("Kampaniyalar", ignorCase: true) {

                    tabletImageView.image = UIImage.imageFor(name:  "roaminginterneta")
                    groupNameLabel.text = Localized("Internet")

                } else if type.isEqual("Campaign", ignorCase: true) ||
                    type.isEqual("Кампании", ignorCase: true) ||
                    type.isEqual("Kampaniyalar", ignorCase: true) {
                    
                    desktopImageView.image = UIImage.imageFor(name:  "romaingcampa")
                    groupNameLabel.text = Localized("Campaign")
                }
            }
        }
    }

    func hideAllViews() {
        OfferGroupView.isHidden = true
        ValidityView.isHidden = true
    }
    
}
