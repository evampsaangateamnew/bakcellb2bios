//
//  SupplementaryOffersHeaderView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit



class SupplementaryOffersHeaderView: MBAccordionTableViewHeaderView {
    
    //MARK:- Properties
    var viewType : MBSectionType = MBSectionType.UnSpecified
    
    //MARK: - IBOutlet
    @IBOutlet var myContainorView: UIView!
    @IBOutlet var containorViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var stickerView: UIView!
    @IBOutlet var stickerLabel: MBLabel!
    @IBOutlet var stickerWidthConstraint: NSLayoutConstraint!
    @IBOutlet var stickerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var detailView: UIView!
    @IBOutlet var offerLabel: MBMarqueeLabel!
    
    @IBOutlet var stateIconImageView: UIImageView!
    
    //MARK: - View Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    //MARK: - Functions
    func setViewForOfferNameWithSickerValues(offerName : String?, stickerTitle : String?,stickerColorCode : String? ) {
        
        //View Type
        viewType = MBSectionType.Header
        offerLabel.setLabelFontType(.title)
        
        //Hiding subscribe button
        stateIconImageView.isHidden = true
        
        
        detailView.isHidden = false
        offerLabel.isHidden = false
        offerLabel.textColor = UIColor.mbTextBlack
        
        // Setting top sticker
        if let stickerTitle = stickerTitle {
            
            stickerHeightConstraint.constant = 20
            //            topSpaceConstraint.constant = 16
            stickerView.isHidden = false
            stickerView.roundTopCorners(radius: 8)
            
            stickerLabel.text = stickerTitle
            
            if  let stickerColorCode = stickerColorCode {
                
                // If sticker text is "" empty string
                if !stickerColorCode.isEmpty {
                    stickerView.backgroundColor = UIColor(hexString: stickerColorCode)
                } else {
                    // Set default value
                    stickerView.backgroundColor = UIColor.mbBorderGray
                }
                
            } else {
                // Set default value
                stickerView.backgroundColor = UIColor.mbGreen
            }
            
        } else {
            stickerHeightConstraint.constant = 0
            //            topSpaceConstraint.constant = 0
            stickerView.isHidden = true
        }
        
        //Seting Detailview
        detailView.roundTopCorners(radius: 8)
        detailView.backgroundColor = UIColor.mbBackgroundGray
        
        
        // Setting offer name
        if let offerName = offerName {
            offerLabel.text = offerName
        }
    }
    
    func setViewWithTitle(title : String?, isSectionSelected: Bool, headerType:MBSectionType ) {
        
        //View Type
        viewType = headerType
        offerLabel.setLabelFontType(.body)
   
        setExpandStateIcon(isSectionSelected)
        
        //Hiding subscribe button
        stickerView.isHidden = true
        
        detailView.isHidden = false
        offerLabel.isHidden = false
        stateIconImageView.isHidden = false
        offerLabel.textColor = UIColor.mbTextBlack
        
        stickerHeightConstraint.constant = 0
        
        //Setting Detailview
        self.layoutIfNeeded()
        detailView.roundTopCorners(radius: 0.0)
        detailView.backgroundColor = UIColor.mbBackgroundGray
        
        // Setting offer name
        if let title = title {
            offerLabel.text = title
        } else {
            offerLabel.text = ""
        }
    }
    
    func setExpandStateIcon(_ isExpanded:Bool = false) {
        
        if isExpanded {
            stateIconImageView.image = UIImage.imageFor(name: "minus_sign")
        } else {
            stateIconImageView.image = UIImage.imageFor(name: "plus_sign")
        }
    }
}
