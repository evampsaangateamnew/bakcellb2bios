//
//  SupplementaryOffersCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class SupplementaryOffersCell: UICollectionViewCell {
    
    class var identifier:String {
        return "SupplementaryOffersCell"
    }
    
    @IBOutlet var tableView: MBAccordionTableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        tableView.hideDefaultSeprator()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180.0

        tableView.allowsSelection = false

        tableView.initialOpenSections = [0]
    }
    
}
