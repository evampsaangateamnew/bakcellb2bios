//
//  FilterPopUpVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

enum MBFilterType: String {
    case App        = "app"
    case Tablet     = "tab"
    case Desktop    = "desktop"
}

typealias MBFilterCompletionHandler = (_ filteredOffersArray: [Offers], _ selectedFilterOptions : [MBFilterType : [KeyValue]]) -> Void


class FilterPopUpVC: UIViewController {
    
    // MARK:- Properties
    var filters : Filters = Filters()
    var offers : [Offers] = []
    var isRoamingFilter : Bool = false
    
    var selectedFilterType : MBFilterType = MBFilterType.App
    var selectedTabFiltersData : [KeyValue] = [KeyValue]()
    var selectedFilterOptions : [MBFilterType : [KeyValue]] = [:]
    //    [ MBFilterType.App:[],  MBFilterType.Tablet:[], MBFilterType.Desktop:[] ]
    
    fileprivate var completionHandlerBlock : MBFilterCompletionHandler?
    
    // MARK:- IBOutlet
    @IBOutlet var phoneView: UIView!
    @IBOutlet var tabletView: UIView!
    @IBOutlet var desktopView: UIView!
    @IBOutlet var bgView: UIView!
    @IBOutlet var containtView: UIView!
    
    //seprator used in top stackView
    @IBOutlet var sepratorLabel1: MBLabel!
    @IBOutlet var sepratorLabel2: MBLabel!
    
    ////seprator used in top of reset button
    @IBOutlet var sepratorLabel3: MBLabel!
    @IBOutlet var tiltLabel: MBLabel!
    
    @IBOutlet var phoneButton: UIButton!
    @IBOutlet var tabletButton: UIButton!
    @IBOutlet var desktopButton: UIButton!
    
    @IBOutlet var resetButton: UIButton!
    @IBOutlet var applyButton: UIButton!
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var stackViewHeight: NSLayoutConstraint!
    
    //MARK: - ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containtView.roundAllCorners(radius: 8)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelectionDuringEditing = true
        tableView.setEditing(true, animated: true)
        tableView.hideDefaultSeprator()
        
        // For selection clear background color
        let clearView = UIView()
        clearView.backgroundColor = UIColor.clear // Whatever color you like
        UITableViewCell.appearance().selectedBackgroundView = clearView
        UITableViewCell.appearance().tintColor = UIColor.mbBrandRed
        
        // Set localized data
        layoutViewController()
        
        // For selecting initial value and layout
        loadInitialValues()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: - Button Actions
    @IBAction func phoneBtnPressed(_ sender: UIButton) {
        
        didSelectButton(selectedButton: phoneButton)
        
        if let app = filters.app {
            self.selectedTabFiltersData = app
        }
        
        let allOption : KeyValue = KeyValue(key: "All", value: Localized("DropDown_All"))
        self.selectedTabFiltersData.insert(allOption, at: 0)
        tableView.setContentOffset(CGPoint.zero, animated: true)
        tableView.reloadData()
        
    }
    
    @IBAction func tabletBtnPressed(_ sender: UIButton) {
        
        didSelectButton(selectedButton: tabletButton)
        
        if let tab = filters.tab {
            self.selectedTabFiltersData = tab
        }
        
        let allOption : KeyValue = KeyValue(key: "All", value: Localized("DropDown_All"))
        self.selectedTabFiltersData.insert(allOption, at: 0)
        tableView.setContentOffset(CGPoint.zero, animated: true)
        tableView.reloadData()
        
    }
    
    @IBAction func desktopBtnPressed(_ sender: UIButton) {
        
        didSelectButton(selectedButton: desktopButton)
        
        if let desktop = filters.desktop {
            self.selectedTabFiltersData = desktop
        }
        
        let allOption : KeyValue = KeyValue(key: "All", value: Localized("DropDown_All"))
        self.selectedTabFiltersData.insert(allOption, at: 0)
        tableView.setContentOffset(CGPoint.zero, animated: true)
        tableView.reloadData()
    }
    
    @IBAction func resetBtnPressed(_ sender: UIButton) {
        
        reSetAllButtonSelection()
        
    }
    
    @IBAction func applyBtnPressed(_ sender: UIButton) {
        
        var filterSelectedOptions : [KeyValue] = [KeyValue]()
        
        let appArray : [KeyValue] = selectedFilterOptions[MBFilterType.App]?.copy() ?? [KeyValue]()
        
        // Check and rename all key value
        if let appIndex = appArray.firstIndex(where: {$0.value == Localized("DropDown_All")}) {
            
            let appAllKeyValueObject: KeyValue = appArray[appIndex]
            appAllKeyValueObject.value = "Mobile"
            
            filterSelectedOptions.append(appAllKeyValueObject)
            
        } else {
            filterSelectedOptions.append(contentsOf: appArray)
        }
        
        // Check and rename all key value
        let tabArray : [KeyValue] = selectedFilterOptions[MBFilterType.Tablet]?.copy() ?? [KeyValue]()
        if let tabIndex = tabArray.firstIndex(where: {$0.value == Localized("DropDown_All")}){
            
            let tabAllKeyValueObject: KeyValue = tabArray[tabIndex]
            tabAllKeyValueObject.value = "Tab"
            
            filterSelectedOptions.append(tabAllKeyValueObject)
            
        } else {
            filterSelectedOptions.append(contentsOf: tabArray)
        }
        
        // Check and rename all key value
        let desktopArray : [KeyValue] = selectedFilterOptions[MBFilterType.Desktop]?.copy() ?? [KeyValue]()
        if let desktopIndex = desktopArray.firstIndex(where: {$0.value == Localized("DropDown_All")}) {
            
            let desktopAllKeyValueObject: KeyValue = desktopArray[desktopIndex]
            desktopAllKeyValueObject.value = "Desktop"
            
            filterSelectedOptions.append(desktopAllKeyValueObject)
            
        } else {
            filterSelectedOptions.append(contentsOf: desktopArray)
        }
        
        let filteredOffers = filterOfferBySelectedOptions(selectedOptions: filterSelectedOptions, offerArray: offers)
        
        self.completionHandlerBlock?(filteredOffers, selectedFilterOptions)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Functions
    
    func layoutViewController() {
        
        if isRoamingFilter {
            tiltLabel.text = Localized("PleaseSelect_title")
        } else {
            tiltLabel.text = Localized("PackageType_Selection")
        }
        
        resetButton.setTitle(Localized("RESET_btn"), for: UIControl.State.normal)
        applyButton.setTitle(Localized("APPLY_btn"), for: UIControl.State.normal)
        
    }
    
    func loadInitialValues() {
        
        let allOption : KeyValue = KeyValue(key: "All", value: Localized("DropDown_All"))
        
        if isRoamingFilter {
            stackViewHeight.constant = 0
            sepratorLabel3.backgroundColor = UIColor.clear
            resetButton.isHidden = true
            
            // Check if data not set then fill with initial data in App section
            if selectedFilterOptions[MBFilterType.App] == nil {
                var appFilterData = filters.app?.copy()
                appFilterData?.insert(allOption, at: 0)
                
                selectedFilterOptions.updateValue(appFilterData ?? [], forKey: MBFilterType.App)
            }
            
            phoneBtnPressed(phoneButton)
            
            return
            
        } else {
            
            stackViewHeight.constant = 50
            resetButton.isHidden = false
            
            // Check if data not set then fill with initial data in App section
            if selectedFilterOptions[MBFilterType.App] == nil {
                
                var appFilterData = filters.app?.copy()
                appFilterData?.insert(allOption, at: 0)
                
                selectedFilterOptions.updateValue(appFilterData ?? [], forKey: MBFilterType.App)
            }
            
            // Check if data not set then fill with initial data in Tablet section
            if selectedFilterOptions[MBFilterType.Tablet] == nil {
                var tabFilterData = filters.tab?.copy()
                tabFilterData?.insert(allOption, at: 0)
                
                selectedFilterOptions.updateValue(tabFilterData ?? [], forKey: MBFilterType.Tablet)
            }
            
            // Check if data not set then fill with initial data in Desktop section
            if selectedFilterOptions[MBFilterType.Desktop] == nil {
                var desktopFilterData = filters.desktop?.copy()
                desktopFilterData?.insert(allOption, at: 0)
                
                selectedFilterOptions.updateValue(desktopFilterData ?? [], forKey: MBFilterType.Desktop)
            }
        }
        
        // Select Tab accourding to data
        let dataContainsOfType = typeOffDataAvalible(filter: filters)
        
        switch dataContainsOfType[0] {
            
        case MBFilterType.App:
            phoneBtnPressed(phoneButton)
            
        case MBFilterType.Tablet:
            tabletBtnPressed(tabletButton)
            
        case MBFilterType.Desktop:
            desktopBtnPressed(desktopButton)
            
        }
        
        sepratorLabel1.isHidden = true
        sepratorLabel2.isHidden = true
        
        if dataContainsOfType.contains(MBFilterType.Tablet) {
            sepratorLabel1.isHidden = false
        }
        
        if dataContainsOfType.contains(MBFilterType.Desktop) {
            sepratorLabel2.isHidden = false
        }
        
        // set top tabs images
        setTopSectionImage()
    }
    
    
    func typeOffDataAvalible(filter : Filters) -> [MBFilterType] {
        
        
        var dataContainsOfType : [MBFilterType] = []
        
        if filter.app != nil {
            dataContainsOfType.append(MBFilterType.App)
            phoneView.isHidden = false
            
        } else {
            phoneView.isHidden = true
        }
        
        if filter.tab != nil {
            dataContainsOfType.append(MBFilterType.Tablet)
            tabletView.isHidden = false
            
        } else {
            tabletView.isHidden = true
        }
        
        if filter.desktop != nil {
            dataContainsOfType.append(MBFilterType.Desktop)
            desktopView.isHidden = false
            
        } else {
            desktopView.isHidden = true
        }
        
        return dataContainsOfType
    }
    
    
    func didSelectButton(selectedButton : UIButton) {
        
        phoneButton.backgroundColor = UIColor.clear
        tabletButton.backgroundColor = UIColor.clear
        desktopButton.backgroundColor = UIColor.clear
        
        switch selectedButton {
            
        case phoneButton:
            phoneButton.isSelected = true
            selectedFilterType = MBFilterType.App
            phoneButton.backgroundColor = UIColor(hexString: "#EDEDED")
            break
            
        case tabletButton:
            tabletButton.isSelected = true
            selectedFilterType = MBFilterType.Tablet
            tabletButton.backgroundColor = UIColor(hexString: "#EDEDED")
            break
            
        case desktopButton:
            desktopButton.isSelected = true
            selectedFilterType = MBFilterType.Desktop
            desktopButton.backgroundColor = UIColor(hexString: "#EDEDED")
            break
            
        default:
            break
        }
        
    }
    
    func reSetAllButtonSelection() {
        
        phoneButton.setImage(UIImage.imageFor(name:  "ic_filter_phone_red"), for: UIControl.State.normal)
        phoneButton.isSelected = false
        
        tabletButton.setImage(UIImage.imageFor(name:  "ic_filter_tablet_red"), for: UIControl.State.normal)
        tabletButton.isSelected = false
        
        desktopButton.setImage(UIImage.imageFor(name:  "ic_filter_tv_red"), for: UIControl.State.normal)
        desktopButton.isSelected = false
        
        let allOption : KeyValue = KeyValue(key: "All", value: Localized("DropDown_All"))
        
        if isRoamingFilter {
            
            // Fill with initial data in App section
            var appFilterData = filters.app?.copy()
            appFilterData?.insert(allOption, at: 0)
            
            selectedFilterOptions.updateValue(appFilterData ?? [], forKey: MBFilterType.App)
            
        } else {
            // Fill with initial data in App section
            var appFilterData = filters.app?.copy()
            appFilterData?.insert(allOption, at: 0)
            
            selectedFilterOptions.updateValue(appFilterData ?? [], forKey: MBFilterType.App)
            
            // Fill with initial data in Tablet section
            var tabFilterData = filters.tab?.copy()
            tabFilterData?.insert(allOption, at: 0)
            
            selectedFilterOptions.updateValue(tabFilterData ?? [], forKey: MBFilterType.Tablet)
            
            // Fill with initial data in Desktop section
            var desktopFilterData = filters.desktop?.copy()
            desktopFilterData?.insert(allOption, at: 0)
            
            selectedFilterOptions.updateValue(desktopFilterData ?? [], forKey: MBFilterType.Desktop)
        }
        
        tableView.reloadData()
    }
    
    func setCompletionHandler(completionBlock : @escaping MBFilterCompletionHandler ) {
        
        self.completionHandlerBlock = completionBlock
        
    }
    
    func filterOfferBySelectedOptions(selectedOptions : [KeyValue] , offerArray : [Offers]? ) -> [Offers] {
        
        // return complete array if no option is selected.
        if selectedOptions.count == 0 {
            
            if let offers = offerArray {
                return offers
            }
        }
        
        var allFilteredOffers : [Offers] = []
        
        // Check and rename all key value
        if selectedOptions.contains(where: {$0.value?.lowercased() == "mobile" }) {
            
            if isRoamingFilter &&
                (offerArray?.count ?? 0) > 0 {
                
                allFilteredOffers.append(contentsOf: offerArray ?? [])
 
            } else {
                // All Mobile selected
                let aAllKeyValueObject: KeyValue = KeyValue(key: "All", value: "mobile")
                
                let filteredOffersForMobile = self.filterOfferByGroupValue(selectedOption: aAllKeyValueObject, offerArray: offerArray)
                
                if filteredOffersForMobile.count > 0 {
                    
                    allFilteredOffers.append(contentsOf: filteredOffersForMobile)
                }
            }
            
        }
        
        // Check and rename all key value
        if selectedOptions.contains(where: {$0.value?.lowercased() == "tab" }) {
            
            // All Tab selected
            let aAllKeyValueObject: KeyValue = KeyValue(key: "All", value: "tab")
            
            let filteredOffersForTab = self.filterOfferByGroupValue(selectedOption: aAllKeyValueObject, offerArray: offerArray)
            
            if filteredOffersForTab.count > 0 {
                
                allFilteredOffers.append(contentsOf: filteredOffersForTab)
            }
        }
        
        // Check and rename all key value
        if selectedOptions.contains(where: {$0.value?.lowercased() == "desktop" }) {
            
            // All Desktop selected
            let aAllKeyValueObject: KeyValue = KeyValue(key: "All", value: "desktop")
            
            let filteredOffersForDesktop = self.filterOfferByGroupValue(selectedOption: aAllKeyValueObject, offerArray: offerArray)
            
            if filteredOffersForDesktop.count > 0 {
                
                allFilteredOffers.append(contentsOf: filteredOffersForDesktop)
            }
        }
        
        
        
        // Filtering offers array
        if let offers = offerArray {
            
            let filteredOffersByOfferFilterKey = offers.filter() {
                
                if let appOfferFilter = ($0 as Offers).header?.appOfferFilter {
                    
                    var offerContainsSelectedValue : Bool = false
                    
                    for aSelectedValue in selectedOptions {
                        
                        if let key = aSelectedValue.key {
                            
                            if appOfferFilter.contains(key){
                                offerContainsSelectedValue = true
                                break
                            }
                        }
                    }
                    
                    return offerContainsSelectedValue
                    
                } else {
                    return false
                }
            }
            
            
            allFilteredOffers.append(contentsOf: filteredOffersByOfferFilterKey)
            
        }
        
        return allFilteredOffers
    }
    
    func filterOfferByGroupValue(selectedOption : KeyValue? , offerArray : [Offers]? ) -> [Offers] {
        
        if selectedOption == nil {
            
            return []
        }
        
        var filteredOffers : [Offers] = []
        
        // Filtering offers array
        if let offers = offerArray {
            
            filteredOffers = offers.filter() {
                
                if let appOfferFilter = ($0 as Offers).header?.offerGroup?.groupValue {
                    
                    if selectedOption?.value?.lowercased() == appOfferFilter.lowercased() {
                        return true
                    } else {
                        return false
                    }
                    
                } else {
                    return false
                }
            }
        }
        
        return filteredOffers
    }
    
    
    func setTopSectionImage() {
        
        // APP Tab Topp image
        if selectedFilterOptions[MBFilterType.App]?.count == 0 {
            
            phoneButton.setImage(UIImage.imageFor(name:  "ic_filter_phone"), for: UIControl.State.normal)
            
        } else if selectedFilterOptions[MBFilterType.App]?.contains(where: {$0.value == Localized("DropDown_All")}) ?? false {
            
            phoneButton.setImage(UIImage.imageFor(name:  "ic_filter_phone_red"), for: UIControl.State.normal)
            
        } else {
            
            phoneButton.setImage(UIImage.imageFor(name:  "ic_filter_phone_yellow"), for: UIControl.State.normal)
            
        }
        
        // Tablet Tab Topp image
        if selectedFilterOptions[MBFilterType.Tablet]?.count == 0 {
            
            tabletButton.setImage(UIImage.imageFor(name:  "ic_filter_tablet"), for: UIControl.State.normal)
        } else if selectedFilterOptions[MBFilterType.Tablet]?.contains(where: {$0.value == Localized("DropDown_All")}) ?? false {
            
            tabletButton.setImage(UIImage.imageFor(name:  "ic_filter_tablet_red"), for: UIControl.State.normal)
        } else {
            
            tabletButton.setImage(UIImage.imageFor(name:  "ic_filter_tablet_yellow"), for: UIControl.State.normal)
        }
        
        // Desktop Tab Topp image
        if selectedFilterOptions[MBFilterType.Desktop]?.count == 0 {
            
            desktopButton.setImage(UIImage.imageFor(name:  "ic_filter_tv"), for: UIControl.State.normal)
        } else if selectedFilterOptions[MBFilterType.Desktop]?.contains(where: {$0.value == Localized("DropDown_All")}) ?? false {
            
            desktopButton.setImage(UIImage.imageFor(name:  "ic_filter_tv_red"), for: UIControl.State.normal)
        } else {
            
            desktopButton.setImage(UIImage.imageFor(name:  "ic_filter_tv_yellow"), for: UIControl.State.normal)
        }
        
    }
}

//MARK: - TableView Delegate and DataSource
extension FilterPopUpVC: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedTabFiltersData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = selectedTabFiltersData[indexPath.row].value ?? ""
        
        if  selectedFilterOptions[selectedFilterType]?.contains(where: {$0.value == selectedTabFiltersData[indexPath.row].value}) ?? false {
            
            // below line is not working as expected and Donot have time to debug
            // cell.isSelected = true
            
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: UITableView.ScrollPosition.none)
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        // Highlight top buttons images
        self.setTopSectionImage()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedFilterOptions[selectedFilterType]?.append(selectedTabFiltersData[indexPath.row])
        
        // Set Top Tabs images
        self.setTopSectionImage()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let index = selectedFilterOptions[selectedFilterType]?.firstIndex(where: { (item) -> Bool in
            return item.value == selectedTabFiltersData[indexPath.row].value
        })
        
        if index != nil {
            
            if var arr : [KeyValue] = selectedFilterOptions[selectedFilterType] {
                arr.remove(at: index!)
                
                selectedFilterOptions.updateValue(arr, forKey: selectedFilterType)
            }
        }
        
        // Set Top Tabs images
        self.setTopSectionImage()
    }
}
