//
//  SuplementaryOffersVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import UPCarouselFlowLayout
import ObjectMapper

class SuplementaryOffersVC: UIViewController {
    
    var selectedSectionViewType : MBSectionType = MBSectionType.UnSpecified
    var prepareVCForRoaming : Bool = false
    
    var errorMessage : String?
    let layout = UPCarouselFlowLayout()
    
    // Values sent from supplementaryVC
    var offers : [Offers] = []
    var selectedoffersType : MBOfferTabType = MBOfferTabType.Call
    var isCardView : Bool = true
    var selectedOfferIndex = 0
    
    // Outlets
    @IBOutlet var offerNumberLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    
    
    //MARK: - ViewController Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(switchHorizontalPaging), name: NSNotification.Name("switchHorizontalPaging"), object: nil)
        
        if prepareVCForRoaming {
            layout.itemSize = CGSize(width: UIScreen.main.bounds.width * 0.79, height:  ((UIScreen.main.bounds.height * 0.65) + 50))
            
        } else {
            
            layout.itemSize = CGSize(width: UIScreen.main.bounds.width * 0.79, height:  UIScreen.main.bounds.height * 0.65)
        }
        
        layout.sideItemScale = 0.8
        layout.sideItemAlpha = 0.6
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 12)
        
        // CardView setting
        switchCardView(showCardView: isCardView)
        
        if offers.count == 0 {
            collectionView.isHidden = true
            offerNumberLabel.text = "0/0"
            
            self.view.showDescriptionViewWithImage(description: errorMessage ?? Localized("Message_NoPackageAvalible"), centerYConstant: -65)
            
        } else {
            collectionView.isHidden = false
            offerNumberLabel.text = "1/\(offers.count)"
            
            self.view.hideDescriptionView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /* Redirect user to selected offer */
        self.collectionView.performBatchUpdates(nil, completion: {
            (isCompleted) in
            
            if isCompleted == true {
                if self.offers.count > 0 {
                    
                    if self.isCardView {
                        self.collectionView.scrollToItem(at: IndexPath(item:self.selectedOfferIndex, section:0), at: .centeredHorizontally, animated: true)
                    } else {
                        self.collectionView.scrollToItem(at: IndexPath(item:self.selectedOfferIndex, section:0), at: .centeredVertically, animated: true)
                    }
                }
            }
        })
       
    }
    
    // Remove notification
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    //MARK: - IBAction
    @IBAction func SubscribeButtonPressed(_ sender: UIButton) {
        
        if let selectedButtonTitle = (sender as UIButton).title(for: .normal) {

            var isTypeRenewValue: Bool = false
            if selectedButtonTitle.trimmWhiteSpace.isEqual( Localized("BtnTitle_RENEW"), ignorCase: true) {

                isTypeRenewValue = true
            }

            let selectedOffer = offers[sender.tag]
            redirectToUserSelectionVC(selectedOffer: selectedOffer, isTypeRenew: isTypeRenewValue)
        }
    }
    
    //MARK: - Fuctions
    
    @objc func switchHorizontalPaging() {
        
        if isCardView == false {
            isCardView = true
            
        } else {
            isCardView = false
        }
        
        switchCardView(showCardView: isCardView)
        collectionView.setCollectionViewLayout(layout, animated: true)
        
        openSelectedSection()
        
    }
    
    func switchCardView(showCardView : Bool) {
        
        if showCardView {
            layout.scrollDirection = .horizontal
            
        } else {
            layout.scrollDirection = .vertical
        }
        
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    func redirectToUserSelectionVC(selectedOffer : Offers, isTypeRenew: Bool)  {
        if let supplementaryMainVC = self.parent as? SupplementaryVC {
            if let userSelectionVC :MultipleUserSelectionMainVC = MultipleUserSelectionMainVC.instantiateViewControllerFromStoryboard() {
                
                userSelectionVC.setNextButtonCompletionBloch { (selectedUsers) in
                    
                    /* POP UserSelection ViewController */
                    self.navigationController?.popViewController(animated: true)
                    
                    //Redirect user to selected screen
                    self.showConfirmationPOPUP(selectedOffer: selectedOffer, isTypeRenew: isTypeRenew, selectedUsersList: selectedUsers)
                }
                
                supplementaryMainVC.navigationController?.pushViewController(userSelectionVC, animated: true)
            }
        }
        
    }
    
    func showConfirmationPOPUP(selectedOffer : Offers, isTypeRenew: Bool, selectedUsersList : [String : UsersData]) {
      
        var attributedMessagesString :NSAttributedString = NSAttributedString()
        if isTypeRenew {
            
            attributedMessagesString = String(format: Localized("Message_RenewOfferConfirmationMessage"), "\(selectedUsersList.count) Users").createAttributedString(stringsListForAttributed: ["\(selectedUsersList.count) \(Localized("Title_Users"))"], stringColor: .black)
        } else {
            

            
            attributedMessagesString = String(format: Localized("Message_SubscribeOfferConfirmation"), "\(selectedUsersList.count)",selectedOffer.header?.price ?? "").createAttributedString(stringsListForAttributed: ["\(selectedUsersList.count) Users","\(selectedOffer.header?.price ?? "") AZN","perform this action"], stringColor: .black)
        }
        
        self.showConfirmationAlert(title: Localized("Title_Confirmation"), attributedMessage: attributedMessagesString, okBtnTitle: Localized("BtnTitle_OK"), cancelBtnTitle: Localized("BtnTitle_NO"), alertType: .groupSelection, {
            
            self.changeSupplementaryOfferingBulk(selectedOffer: selectedOffer, selectedUsersList: selectedUsersList)
        })
    }
    
    
    //MARK: - API Calls
    
    /// Call 'changeSupplementaryOffering' API .
    ///
    /// - returns: Void
    func changeSupplementaryOfferingBulk(selectedOffer: Offers, selectedUsersList : [String : UsersData]) {
        
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.changeSupplementaryOfferingBulk(msisdns: selectedUsersList, offeringId: selectedOffer.header?.offeringId ?? "", price: selectedOffer.header?.price ?? "", actionType: true,  { (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Show succes alert
                    self.showSuccessAlertWithMessage(message: resultDesc)
                    
                    // reload CollectionView
                    self.collectionView.reloadData()
                    
                } else {
                    
                    // Show Error alert
                    self.showErrorAlertWithMessage(message: resultDesc)
                    
                }
            }
        })
    }
}

// MARK: - <UICollectionViewDataSource> / <UICollectionViewDelegate> -

extension SuplementaryOffersVC : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SupplementaryOffersCell.identifier, for: indexPath) as? SupplementaryOffersCell {
            
            cell.tableView.delegate = self
            cell.tableView.dataSource = self
            cell.tableView.allowMultipleSectionsOpen = false
            cell.tableView.keepOneSectionOpen = true;
            cell.tableView.tag = indexPath.item
            
            cell.tableView.reloadData()
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
    // USED to toggle
    func openSelectedSection() {
        
        let visbleIndexPath = collectionView.indexPathsForVisibleItems
        
        visbleIndexPath.forEach { (aIndexPath) in
            
            if let cell = collectionView.cellForItem(at: aIndexPath) as? SupplementaryOffersCell {
                
                let types = typeOfDataInAOffer(aOffer: offers[cell.tableView.tag])
                
                if (selectedSectionViewType == .Detail || selectedSectionViewType == .Description) && isCardView {
                    
                    if types.contains(selectedSectionViewType) {
                        
                        openSectionAt(index: types.firstIndex(of: selectedSectionViewType) ?? 0, tableView: cell.tableView)
                    } else {
                        openSectionAt(index: 0, tableView: cell.tableView)
                    }
                    
                } else if (isCardView == false) {
                    
                    if types.contains(offers[cell.tableView.tag].openedViewSection) {
                        
                        openSectionAt(index: types.firstIndex(of: offers[cell.tableView.tag].openedViewSection) ?? 0, tableView: cell.tableView)
                    } else {
                        openSectionAt(index: 0, tableView: cell.tableView)
                    }
                } else {
                    openSectionAt(index: 0, tableView: cell.tableView)
                }
            }
        }
    }
    
    
    func openSectionAt(index : Int , tableView : MBAccordionTableView) {
        
        if tableView.isSectionOpen(index) == false {
            
            // tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: .top, animated: true)
            tableView.toggleSection(withOutCallingDelegates: index)
            
        }
    }
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>

extension SuplementaryOffersVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return typeOfDataInAOffer(aOffer: offers[tableView.tag]).count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            // For view setting incase of sticker
            if let stickerTitle = offers[tableView.tag].header?.stickerLabel {
                
                if stickerTitle.length > 0 {
                    return 61
                } else {
                    return 45
                }
            } else {
                return 45
            }
            
        } else {
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        let typeOfDataInOffer = types[section]
        
        switch typeOfDataInOffer {
            
        case MBSectionType.Header:
            if let headerView:SupplementaryOffersHeaderView = tableView.dequeueReusableHeaderFooterView() {
                headerView.setViewForOfferNameWithSickerValues(offerName: offers[tableView.tag].header?.offerName, stickerTitle: offers[tableView.tag].header?.stickerLabel, stickerColorCode: offers[tableView.tag].header?.stickerColorCode)
                return headerView
            }
            
        case MBSectionType.Detail:
            if let headerView:SupplementaryOffersHeaderView = tableView.dequeueReusableHeaderFooterView() {
                
                if ((selectedSectionViewType == MBSectionType.Detail && isCardView == true) ||
                    (offers[tableView.tag].openedViewSection == MBSectionType.Detail && isCardView == false)) {
                    
                    headerView.setViewWithTitle(title: Localized("Title_Details"), isSectionSelected: true, headerType: MBSectionType.Detail)
                } else {
                    headerView.setViewWithTitle(title: Localized("Title_Details"), isSectionSelected: false, headerType: MBSectionType.Detail)
                }
                return headerView
            }
            
        case MBSectionType.Description:
            if let headerView:SupplementaryOffersHeaderView = tableView.dequeueReusableHeaderFooterView() {
                
                if ((selectedSectionViewType == MBSectionType.Description && isCardView == true) ||
                    (offers[tableView.tag].openedViewSection == MBSectionType.Description && isCardView == false)) {
                    
                    headerView.setViewWithTitle(title: Localized("Title_Description"), isSectionSelected: true, headerType: MBSectionType.Description)
                } else {
                    headerView.setViewWithTitle(title: Localized("Title_Description"), isSectionSelected: false, headerType: MBSectionType.Description)
                }
                return headerView
            }
            
        case MBSectionType.Subscription:
            if let headerView : SubscribeButtonView = tableView.dequeueReusableHeaderFooterView() {
                
                headerView.viewType = .Subscription
               /* Renew offer code
                /* Check if offer is renewable then enable renew and sent as Subscribed */
                if offers[tableView.tag].header?.btnRenew?.isButtonEnabled() ?? false {

                    headerView.setRenewAndSubscribedButton(tag: tableView.tag)

                }
                    /* else if offers[tableView.tag].header?.btnDeactivate?.isButtonEnabled() ?? false {/* Check if offer is not renewable then disable renew and sent as Subscribed */

                    headerView.setSubscribedButton(tag: tableView.tag)

                     } */
                else {
                    headerView.setSubscribeButtonLayout(tag: tableView.tag)
                }
            */
                headerView.setSubscribeButtonLayout(tag: tableView.tag)
                headerView.activateRenewButton.addTarget(self, action: #selector(SubscribeButtonPressed(_:)), for: UIControl.Event.touchUpInside)
                return headerView
            }
            
        default:
            break
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        switch types[section] {
            
        case MBSectionType.Header:
            return typeOfDataInHeaderSection(aHeader: offers[tableView.tag].header).count
            
        case MBSectionType.Detail:
            return typeOfDataInDetailOrDescriptionSection(offers[tableView.tag].details).count
            
        case MBSectionType.Description:
            return typeOfDataInDetailOrDescriptionSection(offers[tableView.tag].description).count
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let types = typeOfDataInAOffer(aOffer: offers[tableView.tag])
        
        let typeOfDataInOffer = types[indexPath.section]
        
        switch typeOfDataInOffer {
            
        case MBSectionType.Header:
            
            let typeOfData = typeOfDataInHeaderSection(aHeader: offers[tableView.tag].header)
            
            
            switch typeOfData[indexPath.row] {
                
            case MBOfferType.TypeTitle:
                if let cell : SupplementaryExpandCell = tableView.dequeueReusableCell() {
                    cell.setOfferType(header: offers[tableView.tag].header)
                    return cell
                }
                break
                
            case MBOfferType.OfferGroup:
                if let cell : SupplementaryExpandCell = tableView.dequeueReusableCell() {
                    cell.setOfferGroup(offerGroup: offers[tableView.tag].header?.offerGroup)
                    return cell
                }
                break
                
            case MBOfferType.OfferValidity:
                if let cell : SupplementaryExpandCell = tableView.dequeueReusableCell() {
                    cell.setOfferValidity(header: offers[tableView.tag].header)
                    return cell
                }
                break
            case .AttributeList:
                if let cell : AttributeListCell = tableView.dequeueReusableCell() {
                    if let aHeader = offers[tableView.tag].header {
                        let attributedListIndex : Int = indexPath.row - (typeOfData.firstIndex(where:{$0 == .AttributeList}) ?? 0)
                        cell.setAttributeList(attributeList: aHeader.attributeList?[attributedListIndex], offerType: aHeader.type ?? "")
                    }
                    return cell
                }
                break
                
            default:
                break
            }
            
            
            
        case MBSectionType.Detail , MBSectionType.Description :
            
            var aDetailsOrDescription : DetailsAndDescription?
            
            if typeOfDataInOffer == MBSectionType.Detail {
                aDetailsOrDescription = offers[tableView.tag].details
            } else {
                aDetailsOrDescription = offers[tableView.tag].description
            }
            
            let containingDataTypes = typeOfDataInDetailOrDescriptionSection(aDetailsOrDescription)
            
            switch containingDataTypes[indexPath.row] {
                
            case MBOfferType.Price:
                
                if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                    cell.setPriceLayoutValues(price: aDetailsOrDescription?.price, showIcon: false)
                    return cell
                }
            case MBOfferType.Rounding:
                
                if let cell:PriceAndRoundingCell = tableView.dequeueReusableCell() {
                    cell.setRoundingLayoutValues(rounding: aDetailsOrDescription?.rounding, showIcon: false)
                    return cell
                }
                
            case MBOfferType.TextWithTitle:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setTextWithTitleLayoutValues(textWithTitle: aDetailsOrDescription?.textWithTitle)
                    return cell
                }
            case MBOfferType.TextWithOutTitle:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setTextWithOutTitleLayoutValues(textWithOutTitle: aDetailsOrDescription?.textWithOutTitle)
                    return cell
                }
                
            case MBOfferType.TextWithPoints:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setTextWithPointsLayoutValues(textWithPoint: aDetailsOrDescription?.textWithPoints)
                    return cell
                }
                
            case MBOfferType.Date:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setDateAndTimeLayoutValues(dateAndTime: aDetailsOrDescription?.date)
                    return cell
                }
                
            case MBOfferType.Time:
                
                if let cell:TextDateTimeCell = tableView.dequeueReusableCell() {
                    cell.setDateAndTimeLayoutValues(dateAndTime: aDetailsOrDescription?.time)
                    return cell
                }
                
            case MBOfferType.RoamingDetails:
                
                if let cell:RoamingDetailsCell = tableView.dequeueReusableCell() {
                    cell.setRoamingDetailsLayout(roamingDetails: aDetailsOrDescription?.roamingDetails)
                    return cell
                }
                
            case MBOfferType.FreeResourceValidity:
                
                if let cell:FreeResourceValidityCell = tableView.dequeueReusableCell() {
                    cell.setFreeResourceValidityValues(freeResourceValidity: aDetailsOrDescription?.freeResourceValidity)
                    return cell
                }
                
            case MBOfferType.TitleSubTitleValueAndDesc:
                
                if let cell:TitleSubTitleValueAndDescCell = tableView.dequeueReusableCell() {
                    cell.setTitleSubTitleValueAndDescLayoutValues(titleSubTitleValueAndDesc: aDetailsOrDescription?.titleSubTitleValueAndDesc)
                    return cell
                }
                
            default:
                break
            }
            
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - Helper fuction for data loading
    
    func containsInfoOfDetailOrDescriptionSection(aDetailsOrDescription : DetailsAndDescription?) -> Bool {
        
        if let aDetailsOrDescription =  aDetailsOrDescription {
            
            // 1
            if aDetailsOrDescription.price != nil {
                return true
            }
            // 2
            if aDetailsOrDescription.rounding != nil {
                return true
            }
            // 3
            if aDetailsOrDescription.textWithTitle != nil {
                return true
            }
            // 4
            if aDetailsOrDescription.textWithOutTitle != nil {
                return true
            }
            // 5
            if aDetailsOrDescription.textWithPoints != nil {
                return true
            }
            // 6
            if aDetailsOrDescription.date != nil {
                return true
            }
            // 7
            if aDetailsOrDescription.time != nil {
                return true
            }
            // 8
            if aDetailsOrDescription.roamingDetails != nil {
                return true
            }
            // 9
            if aDetailsOrDescription.freeResourceValidity != nil {
                return true
            }
            // 10
            if aDetailsOrDescription.titleSubTitleValueAndDesc != nil {
                return true
            }
            
        }
        return false
    }
    
    func typeOfDataInAOffer(aOffer : Offers?) -> [MBSectionType] {
        
        // Atleast one because there is one with offer name
        
        var dataTypes : [MBSectionType] = []
        
        if let aOffer =  aOffer {
            // 1
            if aOffer.header != nil {
                
                dataTypes.append(MBSectionType.Header)
            }
            // 2
            if aOffer.details != nil  {
                
                dataTypes.append(MBSectionType.Detail)
            }
            // 3
            if aOffer.description != nil {
                
                dataTypes.append(MBSectionType.Description)
            }
            
            dataTypes.append(MBSectionType.Subscription)
        }
        return dataTypes
    }
    
    func typeOfDataInHeaderSection(aHeader : Header?) -> [MBOfferType] {
        
        var dataTypeArray : [MBOfferType] = []
        if let aHeader =  aHeader {
            
            if selectedoffersType == MBOfferTabType.Internet ||
                selectedoffersType == MBOfferTabType.Roaming {
                
                if checkOfferContainsHeaderType(aHeader: aHeader) {
                    dataTypeArray.append(MBOfferType.TypeTitle)
                }
                
                if checkOfferContainsOfferGroup(aHeader: aHeader) {
                    dataTypeArray.append(MBOfferType.OfferGroup)
                }
            }
            dataTypeArray.append(MBOfferType.OfferValidity)
            
            aHeader.attributeList?.forEach({ (aAttribute) in
                dataTypeArray.append(MBOfferType.AttributeList)
            })
        }
        return dataTypeArray
    }
    
    func typeOfDataInDetailOrDescriptionSection(_ aDetailsOrDescription : DetailsAndDescription?) -> [MBOfferType] {
        
        var dataTypes : [MBOfferType] = []
        
        if let aDetailsOrDescription =  aDetailsOrDescription {
            // 1
            if aDetailsOrDescription.price != nil {
                
                dataTypes.append(MBOfferType.Price)
            }
            /*
             if let count = aDetailsOrDescription.price?.count {
             
             for _ in 0..<count {
             dataTypes.append(MBOfferType.Price)
             }
             }
             */
            // 2
            if aDetailsOrDescription.rounding != nil  {
                
                dataTypes.append(MBOfferType.Rounding)
            }
            // 3
            if aDetailsOrDescription.textWithTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithTitle)
            }
            // 4
            if aDetailsOrDescription.textWithOutTitle != nil {
                
                dataTypes.append(MBOfferType.TextWithOutTitle)
            }
            // 5
            if aDetailsOrDescription.textWithPoints != nil {
                
                dataTypes.append(MBOfferType.TextWithPoints)
            }
            // 6
            if aDetailsOrDescription.titleSubTitleValueAndDesc != nil {
                
                dataTypes.append(MBOfferType.TitleSubTitleValueAndDesc)
            }
            
            // 7
            if aDetailsOrDescription.date != nil {
                
                dataTypes.append(MBOfferType.Date)
            }
            // 8
            if aDetailsOrDescription.time != nil {
                
                dataTypes.append(MBOfferType.Time)
            }
            // 9
            if aDetailsOrDescription.roamingDetails != nil {
                
                dataTypes.append(MBOfferType.RoamingDetails)
            }
            // 10
            if aDetailsOrDescription.freeResourceValidity != nil {
                
                dataTypes.append(MBOfferType.FreeResourceValidity)
            }
        }
        return dataTypes
    }
    
    
    func checkOfferContainsOfferGroup(aHeader : Header?) -> Bool {
        
        // Atleast one because there is one with offer name
        var isContainsOfferGroup : Bool = false
        
        if selectedoffersType == MBOfferTabType.Internet || selectedoffersType == MBOfferTabType.Roaming {
            
            if let aHeader =  aHeader {
                
                if aHeader.offerGroup != nil {
                    isContainsOfferGroup = true
                }
            }
        }
        
        return isContainsOfferGroup
    }
    
    func checkOfferContainsHeaderType(aHeader : Header?) -> Bool {
        
        // Atleast one because there is one with offer name
        var isContainsOfferGroup : Bool = false
        
        if selectedoffersType == MBOfferTabType.Roaming {
            
            if let aHeader =  aHeader {
                
                if aHeader.type != nil {
                    isContainsOfferGroup = true
                }
            }
        }
        
        return isContainsOfferGroup
    }
    
    func containsOfferOfOfferId(offeringId : String?, mySubscriptions: MySubscription?) -> Bool {
        
        // return complete array if string is empty
        if offeringId?.isBlank == true {
            return false
        } else {
            
            if let mySubscriptions = mySubscriptions {
                
                var containsOffer : Bool = false
                if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.callOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.internetOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.smsOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.campaignOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.tmOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.hybridOffers) {
                    containsOffer = true
                } else if self.isOffersContainsOfferingId(offeringId: offeringId ?? "", offerArray: mySubscriptions.roamingOffers) {
                    containsOffer = true
                }
                return containsOffer
                
            } else {
                return false
            }
        }
    }
    
    func isOffersContainsOfferingId(offeringId : String , offerArray : [SubscriptionOffers]? ) -> Bool {
        // Filtering offers array
        let containsOfferingId = offerArray?.contains() { (aOffer) -> Bool in
            
            if let aOfferingId = aOffer.header?.offeringId {
                return aOfferingId.trimmWhiteSpace.containsSubString(subString: offeringId.trimmWhiteSpace)
            } else {
                return false
            }
        }
        
        return containsOfferingId ?? false
    }
    
    func containsOfferOfOfferLevelInSubscription(offeringLevel : String?, offeringId: String?, mySubscriptions: MySubscription?) -> Bool {
        
        // return complete array if string is empty
        if offeringLevel?.isBlank == true || offeringId?.isBlank == true {
            return false
        } else {
            if let mySubscriptions = mySubscriptions {
                
                var containsOffer : Bool = false
                
                if selectedoffersType == .Call {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.callOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                } else if selectedoffersType == .Internet {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.internetOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                } else if selectedoffersType == .SMS {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.smsOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                    //  } else if selectedoffersType == .Campaign {
                    //
                    //  if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.campaignOffers) {
                    //  containsOffer = true
                    //  } else {
                    //  containsOffer = false
                    //  }
                    //  } else if selectedoffersType == .TM {
                    //
                    //  if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.tmOffers) {
                    //                        containsOffer = true
                    //  } else {
                    //  containsOffer = false
                    //  }
                } else if selectedoffersType == .Hybrid {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.hybridOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                } else if selectedoffersType == .Roaming {
                    
                    if self.isOfferLevelMatch(offerLevel: offeringLevel ?? "", offeringId: offeringId ?? "", offerArray: mySubscriptions.roamingOffers) {
                        containsOffer = true
                    } else {
                        containsOffer = false
                    }
                }
                
                return containsOffer
                
            } else {
                return false
            }
        }
    }
    
    func isOfferLevelMatch(offerLevel : String, offeringId: String, offerArray : [SubscriptionOffers]? ) -> Bool {
        // Filtering offers array
        let offerLevelMatch = offerArray?.contains() { (aOffer) -> Bool in
            
            if let header = aOffer.header {
                
                guard let aOfferLevel = header.offerLevel?.trimmWhiteSpace else {
                    return false
                }
                guard let aOfferingId = header.offeringId?.trimmWhiteSpace else {
                    return false
                }
                
                if aOfferLevel.isEqual( offerLevel.trimmWhiteSpace, ignorCase: true) && aOfferingId.isEqual( offeringId.trimmWhiteSpace, ignorCase: true) != true {
                    
                    return true
                } else {
                    return false
                }
                
            } else {
                return false
            }
        }
        
        return offerLevelMatch ?? false
    }
}

// MARK: - <MBAccordionTableViewDelegate>

extension SuplementaryOffersVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.setExpandStateIcon(true)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if (selectedSectionViewType != headerView.viewType && isCardView == true) {
                
                selectedSectionViewType = headerView.viewType
                
            } else if (offers[tableView.tag].openedViewSection != headerView.viewType && isCardView == false) {
                
                offers[tableView.tag].openedViewSection = headerView.viewType;
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            headerView.setExpandStateIcon(false)
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? SupplementaryOffersHeaderView {
            
            if (selectedSectionViewType == headerView.viewType && isCardView == true) {
                selectedSectionViewType = MBSectionType.Header
                tableView.toggleSection(0)
                
            } else if (offers[tableView.tag].openedViewSection == headerView.viewType && isCardView == false) {
                
                offers[tableView.tag].openedViewSection = MBSectionType.Header;
                tableView.toggleSection(0)
            }
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        
        if let headerView = tableView.headerView(forSection: section) as? SubscribeButtonView {
            
            if headerView.viewType == MBSectionType.Subscription {
                
                return false
                
            } else {
                
                return true
            }
        } else {
            
            return true
        }
    }
}

// MARK: - Pageing logic
extension SuplementaryOffersVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let indexPath = collectionView.indexPathForItem(at: visiblePoint)
        
        if indexPath != nil {
            if offers.count > 0 {
                
                let index : Int = indexPath?.item ?? 0
                offerNumberLabel.text = "\(index + 1 )/\(offers.count)"
            } else {
                offerNumberLabel.text = "0/\(offers.count)"
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == collectionView {
            openSelectedSection()
        }
    }
}
