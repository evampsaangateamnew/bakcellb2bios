//
//  SupplementaryVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 5/24/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

import ObjectMapper

// Supplementary and Subscription Offer type
enum MBOfferTabType: String {
    case Call           = "Call"
    case Internet       = "Internet"
    case SMS            = "SMS"
    case Hybrid         = "Hybrid"
    case Campaign       = "Campaign"
    case TM             = "TM"
    case Roaming        = "Roaming"
    case AllInclusive   = "All Inclusive"
    
    func localizedString() -> String {
        return Localized(self.rawValue)
    }
    
    static func getTitleFor(title:MBOfferTabType) -> String {
        return title.localizedString()
    }
}

class SupplementaryVC: BaseVC {
    
    //MARK: - Properties
    var showSearch : Bool = true
    var showCardView : Bool = true
    var prepareVCForRoaming : Bool = false
    var redirectedFromNotification : Bool = false
    var offeringIdFromNotification : String = ""
    var tabMenuArray: [MBOfferTabType] = []
    var selectedTabType : MBOfferTabType = MBOfferTabType.Internet
    
    var supplementaryOffers : SupplementaryOfferings?
    var offers : [Offers] = []
    var filters : Filters?
    var selectedFilterOptions : [MBFilterType : [KeyValue]] = [:]
    var countryTitle : String = ""
    var countryImage : String = ""
    
    //MARK: - IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var searchBottomLineView: UIView!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var switchButton: UIButton!
    @IBOutlet var filterButton: UIButton!
    
    @IBOutlet var topOptionsView: UIView!
    @IBOutlet var mainView: UIView!
    
    @IBOutlet var countryLabel: MBLabel!
    @IBOutlet var countryIconImageView: UIImageView!
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var tabBarCollactionViewHeight: NSLayoutConstraint!
    
    //MARK: - ViewContoller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextField.delegate = self
        searchTextField.attributedPlaceholder = NSAttributedString(string: Localized("PlaceHolder_Search"),
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        if prepareVCForRoaming == true {
            
            // Selected Country information
            countryLabel.isHidden = false
            countryIconImageView.isHidden = false
            self.filterButton.isHidden = false
            topOptionsView.isHidden = false
            
            tabBarCollactionViewHeight.constant = 0
            // Setting top country information and flag.
            countryLabel.text = countryTitle
            if countryImage.isBlank == true {
                countryIconImageView.image = UIImage.imageFor(name:  "dummy_flag")
            } else {
                countryIconImageView.image = UIImage.imageFor(name:  countryImage)
            }
            
            tabMenuArray = []
            
            // Load information from selected offers
            loadRoamingOffersScreen(offers: offers)
            
        } else {
            
            setSwitchLayoutButtonImage()
            // Hiding Country information view
            countryLabel.isHidden = true
            countryIconImageView.isHidden = true
            
            tabBarCollactionViewHeight.constant = 50
            tabMenuArray = [MBOfferTabType.Call,
                            MBOfferTabType.Internet,
                            MBOfferTabType.SMS,
                            // MBOfferTabType.Campaign,
                // MBOfferTabType.TM,
                MBOfferTabType.Hybrid,
                MBOfferTabType.Roaming ]
            
            let index = tabMenuArray.firstIndex(of: selectedTabType)
            collectionView .selectItem(at: IndexPath(item:index ?? 0, section:0) , animated: true, scrollPosition: UICollectionView.ScrollPosition.left)
            
            //load Supplementary data
            getSupplementaryOfferings()
        }
        
        // Switch Layout button image
        self.setSwitchLayoutButtonImage()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        // Layout view lables
        loadViewLayout()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if prepareVCForRoaming == false {
            let index = tabMenuArray.firstIndex(of: selectedTabType)
            collectionView.scrollToItem(at: IndexPath(item:index ?? 0, section:0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
        }
    }
   
    //MARK: - Functions
    
    func loadViewLayout(){
        
        if prepareVCForRoaming == true {
            
            titleLabel.text = Localized("Title_Roaming");
        } else {
            titleLabel.text = Localized("Title_SupplementaryOffer");
            
        }
        searchBottomLineView.isHidden = true
    }
    
    func setSwitchLayoutButtonImage() {
        
        if showCardView == false {
            
            // CardView image
            switchButton.setImage(UIImage (named: "cardview"), for: UIControl.State.normal)
            
        } else {
            // List image
            switchButton.setImage(UIImage (named: "actionbar_sort"), for: UIControl.State.normal)
            
        }
    }
    
    func reDirectUserToSelectedScreens( selectedItem : inout MBOfferTabType) {
        
        var offerIndex : Int = 0
        
        // Check if user is redirected from notification
        if redirectedFromNotification {
            // Filter and find offer by offering Id
            let (type, index) = typeAndIndexOfOfferingId(offeringId: offeringIdFromNotification, allOffersData: supplementaryOffers)
            
            // Set selected type and index
            selectedItem = type
            offerIndex = index
            
            //  Set to false so user redirected to offer only once
            redirectedFromNotification = false
        }
        
        // Higlight selected collectionView cell color.
        let index = tabMenuArray.firstIndex(of: selectedItem)
        collectionView .selectItem(at: IndexPath(item:index ?? 0, section:0) , animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
        
        // Removing subview from main view then adding updated again
        self.removeChildViewController(childController: SuplementaryOffersVC())
        self.removeChildViewController(childController: RoamingVC())
        
        // Check if user selected offer type roaming
        if selectedItem == MBOfferTabType.Roaming {
            
            topOptionsView.isHidden = true
            
            if let roamingVC :RoamingVC = RoamingVC.instantiateViewControllerFromStoryboard() {
                roamingVC.isCardView = self.showCardView
                
                if let MySupplementaryOffers = supplementaryOffers {
                    
                    if let offers = MySupplementaryOffers.roaming?.offers {
                        roamingVC.offers = offers
                    }
                    
                    /*if let countries = MySupplementaryOffers.roaming?.countries {
                        roamingVC.countries = countries
                    }*/
                    roamingVC.countries = [String]()
                    for country in MySupplementaryOffers.roaming?.countriesFlags ?? [Country]() {
                        roamingVC.countries?.append(country.name ?? "")
                    }
                    
                    roamingVC.filters = MySupplementaryOffers.roaming?.filters
                }
                
                self.addChildViewController(childController: roamingVC, onView: mainView)
            }
            
        } else {
            
            topOptionsView.isHidden = false
            self.filterButton.isHidden = true
            
            if let suplementaryOffersVC :SuplementaryOffersVC = SuplementaryOffersVC.instantiateViewControllerFromStoryboard() {
                
                suplementaryOffersVC.isCardView = self.showCardView
                suplementaryOffersVC.selectedoffersType = selectedItem
                suplementaryOffersVC.selectedOfferIndex = offerIndex
                
                
                if let MySupplementaryOffers = supplementaryOffers {
                    
                    switch selectedItem {
                        
                    case MBOfferTabType.Call:
                        if let offers = MySupplementaryOffers.call?.offers {
                            suplementaryOffersVC.offers = offers
                        }
                        self.filters = MySupplementaryOffers.call?.filters
                        
                    case MBOfferTabType.Internet:
                        
                        self.filterButton.isHidden = false
                        if let offers = MySupplementaryOffers.internet?.offers {
                            suplementaryOffersVC.offers = offers
                        }
                        self.filters = MySupplementaryOffers.internet?.filters
                        
                    case MBOfferTabType.SMS:
                        if let offers = MySupplementaryOffers.sms?.offers {
                            suplementaryOffersVC.offers = offers
                        }
                        self.filters = MySupplementaryOffers.sms?.filters
                        
                    case MBOfferTabType.Campaign
                        :
                        if let offers = MySupplementaryOffers.campaign?.offers {
                            suplementaryOffersVC.offers = offers
                        }
                        self.filters = MySupplementaryOffers.campaign?.filters
                        
                    case MBOfferTabType.TM:
                        if let offers = MySupplementaryOffers.tm?.offers {
                            suplementaryOffersVC.offers = offers
                        }
                        self.filters = MySupplementaryOffers.tm?.filters
                        
                    case MBOfferTabType.Hybrid:
                        if let offers = MySupplementaryOffers.hybrid?.offers {
                            suplementaryOffersVC.offers = offers
                        }
                        self.filters = MySupplementaryOffers.hybrid?.filters
                        
                    default:
                        break
                    }
                    
                    self.offers = suplementaryOffersVC.offers
                    
                }
                
                self.addChildViewController(childController: suplementaryOffersVC, onView: mainView)
            }
            
        }
    }
    
    func reDirectUserToSelectedScreenWithFilterdOffers(filteredOffers : [Offers], selectedItem : MBOfferTabType, erroMessage: String) {
        
        // Removing subview from main view then adding updated again
        self.removeChildViewController(childController: SuplementaryOffersVC())
        self.removeChildViewController(childController: RoamingVC())
        
        if selectedItem == MBOfferTabType.Roaming  && prepareVCForRoaming {
            
            loadRoamingOffersScreen(offers: filteredOffers)
            
        } else {
            
            if let supp :SuplementaryOffersVC = SuplementaryOffersVC.instantiateViewControllerFromStoryboard() {
                supp.offers = filteredOffers
                supp.isCardView = self.showCardView
                supp.errorMessage = erroMessage
                supp.selectedoffersType = self.selectedTabType
                
                if let MySupplementaryOffers = supplementaryOffers {
                    
                    
                    switch selectedItem {
                        
                    case MBOfferTabType.Call:
                        
                        self.filters = MySupplementaryOffers.call?.filters
                        
                    case MBOfferTabType.Internet:
                        
                        self.filters = MySupplementaryOffers.internet?.filters
                        
                    case MBOfferTabType.SMS:
                        
                        self.filters = MySupplementaryOffers.sms?.filters
                        
                    case MBOfferTabType.Campaign:
                        
                        self.filters = MySupplementaryOffers.campaign?.filters
                        
                    case MBOfferTabType.TM:
                        
                        self.filters = MySupplementaryOffers.tm?.filters
                        
                    case MBOfferTabType.Hybrid:
                        
                        self.filters = MySupplementaryOffers.hybrid?.filters
                        
                    default:
                        break
                    }
                }
                
                self.addChildViewController(childController: supp, onView: mainView)
            }
        }
    }
    
    func loadRoamingOffersScreen (offers : [Offers]) {
        
        if let suplementaryOffersVC :SuplementaryOffersVC = SuplementaryOffersVC.instantiateViewControllerFromStoryboard() {
            suplementaryOffersVC.offers = offers
            suplementaryOffersVC.isCardView = self.showCardView
            suplementaryOffersVC.selectedoffersType = self.selectedTabType
            
            self.addChildViewController(childController: suplementaryOffersVC, onView: mainView)
        }
    }
    
    func typeAndIndexOfOfferingId(offeringId : String?, allOffersData : SupplementaryOfferings?) -> (MBOfferTabType, Int) {
        
        // check if found nil information
        if allOffersData == nil {
            return (.Call,0)
        }
        
        /* Filter offers of Call section */
        var filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.call?.offers)
        
        if filterResponse.isFoundValue {
            return (.Call,filterResponse.index)
        }
        
        /* Filter offers of Internet section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.internet?.offers)
        
        if filterResponse.isFoundValue {
            return (.Internet,filterResponse.index)
        }
        
        /* Filter offers of SMS section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.sms?.offers)
        
        if filterResponse.isFoundValue {
            return (.SMS,filterResponse.index)
        }
        
        /* Filter offers of Campaign section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.campaign?.offers)
        
        //        if filterResponse.isFoundValue {
        //            return (.Campaign,filterResponse.index)
        //        }
        //
        //        /* Filter offers of TM section */
        //        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.tm?.offers)
        //
        //        if filterResponse.isFoundValue {
        //            return (.TM,filterResponse.index)
        //        }
        
        /* Filter offers of Hybrid section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.hybrid?.offers)
        
        if filterResponse.isFoundValue {
            return (.Hybrid,filterResponse.index)
        }
        
        /* Filter offers of Roaming section */
        filterResponse = containsOfferByOfferingId(offeringId: offeringId, offerArray: allOffersData?.roaming?.offers)
        
        if filterResponse.isFoundValue {
            return (.Roaming,filterResponse.index)
        }
        
        // If no information is found then retrun call as default
        return (.Internet,0)
    }
    
    func containsOfferByOfferingId(offeringId : String? , offerArray : [Offers]? ) -> (isFoundValue : Bool, index : Int) {
        
        var foundOffer = false
        
        // return 0 if offeringId is blank
        if offeringId?.isBlank != false {
            return (foundOffer, 0)
        }
        // return 0 if offerArray is nil
        if offerArray == nil {
            return (foundOffer, 0)
        }
        
        // Filtering offeringId index from array
        let indexOfOffer = offerArray?.firstIndex() {
            
            if let aOfferingId = ($0 as Offers).header?.offeringId {
                
                let isFound = aOfferingId.isEqual( offeringId, ignorCase: true)
                
                if isFound {
                    foundOffer = true
                    return true
                } else {
                    return false
                }
                
            } else {
                return false
            }
        }
        
        return (foundOffer, (indexOfOffer ?? 0))
    }
    
 
    
    //MARK: - IBActions
    @IBAction func switchView(_ sender: Any) {
        
        if showCardView == false {
            showCardView = true
        } else {
            showCardView = false
        }
        self.setSwitchLayoutButtonImage()
        
        //        searchPressed(search_btn)
        
        // Resetting selected filter
        selectedFilterOptions = [:]
        
        // Reset search
        searchButton.setImage(UIImage.imageFor(name: "actionbar_search") , for: UIControl.State.normal)
        searchTextField.isHidden = true
        searchBottomLineView.isHidden = true
        titleLabel.isHidden = false
        showSearch = true
        
        searchTextField.text = ""
        searchTextField.resignFirstResponder()
        
        NotificationCenter.default.post(name: NSNotification.Name("switchHorizontalPaging"), object: nil, userInfo: nil)
    }
    
    @IBAction func searchPressed(_ sender: UIButton) {
        
        if showSearch == false {
            searchButton.setImage(UIImage.imageFor(name: "actionbar_search") , for: UIControl.State.normal)
            searchTextField.isHidden = true
            searchBottomLineView.isHidden = true
            titleLabel.isHidden = false
            showSearch = true
            
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
            searchOffersByName(errorMessage: Localized("Message_NoDataAvalible"))
            
        } else {
            searchButton.setImage(UIImage.imageFor(name: "Cross") , for: UIControl.State.normal)
            titleLabel.isHidden = true
            searchTextField.isHidden = false
            searchBottomLineView.isHidden = false
            showSearch = false
            
            searchTextField.becomeFirstResponder()
            
        }
    }
    
    @IBAction func filterPressed(_ sender: UIButton) {
        
        if let filters = self.filters {
            
            if let filterPopUpVC :FilterPopUpVC = FilterPopUpVC.instantiateViewControllerFromStoryboard() {
                
                filterPopUpVC.filters = filters
                filterPopUpVC.offers = offers
                filterPopUpVC.selectedFilterOptions = selectedFilterOptions
                filterPopUpVC.isRoamingFilter = prepareVCForRoaming
                
                filterPopUpVC.setCompletionHandler(completionBlock: { (filteredOffers,filterSelectedOption) in
                    
                    self.reDirectUserToSelectedScreenWithFilterdOffers(filteredOffers: filteredOffers, selectedItem: self.selectedTabType, erroMessage: Localized("Message_NothingFoundFilter"))
                    
                    self.selectedFilterOptions = filterSelectedOption
                })
                
                self.presentPOPUP(filterPopUpVC, animated: true, completion: nil)
            }
            
        } else {
            self.showAlertWithMessage(message: Localized("Message_NothingFoundFilter"))
        }
    }
    
    //MARK: - Functions
    func loadSupplementaryOfferingsData() {
        
        
        /* Load data from UserDefaults */
        if let supplementaryOffersHandler :SupplementaryOfferings = SupplementaryOfferings.loadFromUserDefaults(key: "\(APIsType.supplementaryOffer.localizedAPIKey())\(MBPICUserSession.shared.picUserInfo?.picAllowedOffers ?? "")") {
            
            // Set information in local object
            self.supplementaryOffers = supplementaryOffersHandler
            // Redirect user to selected screen
            self.reDirectUserToSelectedScreens(selectedItem: &self.selectedTabType)
            
            /* Load new data in background*/
            self.getSupplementaryOfferings(showIndicator: false)
            
        } else {
            self.getSupplementaryOfferings()
        }
    }
    
    //MARK: - API Calls
    
    /// Call 'getSupplementaryOffering' API .
    ///
    /// - returns: void
    func getSupplementaryOfferings(showIndicator :Bool = true) {
        
        if (showIndicator == true) {
            self.showActivityIndicator()
        }
        
        _ = MBAPIClient.sharedClient.getSupplementaryOfferings({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            if (showIndicator == true) {
                self.hideActivityIndicator()
            }
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let supplementaryOffersHandler = Mapper<SupplementaryOfferings>().map(JSONObject: resultData){
                        
                        // Saving Supplementary Offers data in user defaults
                        supplementaryOffersHandler.saveInUserDefaults(key: "\(APIsType.supplementaryOffer.localizedAPIKey())\(MBPICUserSession.shared.picUserInfo?.picAllowedOffers ?? "")")
                        
                        
                        // Set information in local object
                        self.supplementaryOffers = supplementaryOffersHandler
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            // Redirect user to selected screen
            self.reDirectUserToSelectedScreens(selectedItem: &self.selectedTabType)
        })
    }
    
    
    
}
//MARK: - COLLECTION VIEW Delegates
extension SupplementaryVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let str: String = tabMenuArray[indexPath.item].localizedString()
        
        return CGSize(width: ((str as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.mbArial(fontSize: 14)]).width + 30), height: 35)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tabMenuArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as? SupplementaryCollectionCell {
            
            // Tab Button boader color
            cell.titleLabel.text = tabMenuArray[indexPath.item].localizedString()
            
            cell.layer.borderColor = UIColor.mbBorderGray.cgColor
            cell.titleLabel.highlightedTextColor = UIColor.mbTextBlack
            
            let bgColorView = UIView()
            bgColorView.backgroundColor = UIColor.mbButtonBackgroundGray
            cell.selectedBackgroundView = bgColorView
            
            return cell
        }
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if supplementaryOffers != nil {
            
            // Check if user select same selected tab again if true then do nothing.
            if selectedTabType == tabMenuArray[indexPath.item] {
                return
            }
            
            // load selected tab information
            selectedTabType = tabMenuArray[indexPath.item]
            
            // Resetting selected filter
            selectedFilterOptions = [:]
            
            // Reset search
            searchButton.setImage(UIImage.imageFor(name: "actionbar_search") , for: UIControl.State.normal)
            searchTextField.isHidden = true
            searchBottomLineView.isHidden = true
            titleLabel.isHidden = false
            showSearch = true
            
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
            
            
        } else {
            // get all information
            selectedTabType = tabMenuArray[indexPath.item]
            self.loadSupplementaryOfferingsData()
        }
        
        // Redirect user to selected screen
        reDirectUserToSelectedScreens(selectedItem: &selectedTabType)
    }
    
}


//MARK:- UITextFieldDelegate

extension SupplementaryVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        if textField == searchTextField {
            // New text of string after change
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            // Filter offers with new name
            self.searchOffersByName(searchString: newString)
            
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    // Filter offers with offer name
    func searchOffersByName(searchString: String = "", errorMessage : String = Localized("Message_NothingFoundSearch")) {
        
        // find offers where name matches
        let filterdOffers = filterOfferBySearchString(searchString: searchString, offerArray: self.offers)
        
        // Redirect user to screen accourdingly
        self.reDirectUserToSelectedScreenWithFilterdOffers(filteredOffers: filterdOffers, selectedItem: selectedTabType, erroMessage: errorMessage)
        
    }
    
    func filterOfferBySearchString(searchString : String? , offerArray : [Offers]? ) -> [Offers] {
        
        // return complete array if string is empty
        if let searchString = searchString {
            
            if searchString == "" {
                
                if let offers = offerArray {
                    return offers
                }
            }
        }
        
        // Filtering offers array
        var filteredOffers : [Offers] = []
        if let offers = offerArray {
            
            filteredOffers = offers.filter() {
                
                if let offerName = ($0 as Offers).header?.offerName {
                    
                    return offerName.containsSubString(subString: searchString)
                    
                } else {
                    return false
                }
            }
        }
        
        return filteredOffers
    }
}



