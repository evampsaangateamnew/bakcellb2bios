//
//  RoamingVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class RoamingVC: UIViewController {

    var offers : [Offers]?
    var filters : Filters?
    var countries : [String]?
    var isCardView = true

    @IBOutlet var descriptionLabel: MBLabel!
    @IBOutlet var countriesTextField: SearchTextField!
    @IBOutlet var nextButton: UIButton!

    //MARK: - ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let countries = countries {
            countriesTextField.filterStrings(countries)
        }
        countriesTextField.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        descriptionLabel.text = Localized("Description_Roaming")
        countriesTextField.placeholder = Localized("PlaceHolder_EnterYourCountry")
        nextButton.setTitle(Localized("BtnTitle_NEXT"), for: UIControl.State.normal)

        countriesTextField.textColor = UIColor.mbTextGray
        countriesTextField.theme = SearchTextFieldTheme.MBLightTheme()
    }

    //MARK: - IBAction
    @IBAction func nextButtonAction(_ sender: Any) {

        if let countryName = countriesTextField.text {

            if countryName.isEmpty {

                self.showErrorAlertWithMessage(message: Localized("Message_PleaseEnterCountry"))
            } else {

                let (filterOffers, flagName) = filterOfferByCountryName(countryName: countryName, offerArray: offers)

                if filterOffers.count <= 0 {

                    self.showAlertWithMessage(message: Localized("Message_NoOfferWereFound"))

                } else {
                    if let supplementaryVCObject :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard() {
                    supplementaryVCObject.offers = filterOffers
                    supplementaryVCObject.filters = filters
                    supplementaryVCObject.prepareVCForRoaming = true
                    supplementaryVCObject.showCardView = self.isCardView
                    supplementaryVCObject.selectedTabType = MBOfferTabType.Roaming
                    supplementaryVCObject.countryTitle = countryName
                    supplementaryVCObject.countryImage = flagName

                    self.navigationController?.pushViewController(supplementaryVCObject, animated: true)
                    }
                }
            }
        }
    }

    //MARK: - Filtering offers data accourding to selectes country.
    func filterOfferByCountryName(countryName : String , offerArray : [Offers]? ) -> ([Offers], String) {
        // Filtering offers array
        var filteredOffers : [Offers] = []
        var imageName : String = ""

        if let offers = offerArray {

            filteredOffers = offers.filter() {

                if let roamingDetailsCountriesList = ($0 as Offers).details?.roamingDetails?.roamingDetailsCountriesList {

                    let  aRoamingDetailsCountry = roamingDetailsCountriesList.filter() {

                        if let aCountryName = ($0 as RoamingDetailsCountries).countryName {


                            if aCountryName.trimmWhiteSpace.lowercased() == countryName.trimmWhiteSpace.lowercased() {

                                if imageName.isBlank == true {
                                    imageName = ($0 as RoamingDetailsCountries).flag ?? ""
                                }
                                return true
                            } else {
                                return false
                            }

                        } else {

                            return false
                        }
                    }

                    if  aRoamingDetailsCountry.count > 0 {

                        return true
                        
                    } else {
                        
                        return false
                    }
                    
                }  else {
                    return false
                }
            }
        }

        return (filteredOffers, imageName)
    }

}

//MARK: Textfield delagates
extension RoamingVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == countriesTextField {
            nextButtonAction(UIButton())
            return false
        } else {
            return true
        }
    }
    
}
