//
//  SupplementaryCollectionCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 5/24/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SupplementaryCollectionCell: UICollectionViewCell {
    
    @IBOutlet var titleLabel: UILabel!
}
