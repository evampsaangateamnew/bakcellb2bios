//
//  LiveChatWebViewVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 7/27/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import WebKit

class LiveChatWebViewVC: BaseVC {
    //MARK:- Properties
    
    //MARK:- IBOutlet
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var webContrinerView: UIView!
    var myWebView: WKWebView!
    
    
    //MARK:- View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let config = WKWebViewConfiguration.init()
        config.preferences.javaScriptEnabled = true
        //config.preferences.javaEnabled = true
        config.preferences.javaScriptCanOpenWindowsAutomatically = true
        if #available(iOS 11.0, *) {
            config.setURLSchemeHandler(self, forURLScheme: "com.bakcell.carrierapp.portal")
        } else {
            // Fallback on earlier versions
        }
        
        myWebView = WKWebView.init(frame: webContrinerView.frame, configuration:config )
        
        myWebView.backgroundColor = .clear
        myWebView.navigationDelegate = self
        webContrinerView.addSubview(myWebView)
        myWebView.snp.makeConstraints { make in
            make.top.equalTo(webContrinerView.snp.top).offset(0.0)
            make.right.equalTo(webContrinerView.snp.right).offset(0.0)
            make.left.equalTo(webContrinerView.snp.left).offset(0.0)
            make.bottom.equalTo(webContrinerView.snp.bottom).offset(0.0)
        }
        
        WKWebsiteDataStore.default().removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
        
        if let myURLRequest = Constants.liveChatUrlRequest {
            
            myWebView.load(myURLRequest)
            
        } else {
            self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            self.showErrorAlertWithMessage(message: Localized("Message_FailureLiveChat"))
            
            MBPICUserSession.shared.liveChatObject = nil
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWebView), name: NSNotification.Name(Constants.kAppDidBecomeActive), object: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(true)
        
        titleLabel.text = Localized("Title_LiveChat")
        
        if let myURL = URL(string: Constants.liveChatURL) {
            
            if myWebView.url != myURL {
                myWebView.stopLoading()
                if let myURLRequest = Constants.liveChatUrlRequest {
                    
                    myWebView.load(myURLRequest)
                    
                } else {
                    self.hideActivityIndicator()
                    self.showErrorAlertWithMessage(message: Localized("Message_FailureLiveChat"))
                    MBPICUserSession.shared.liveChatObject = nil
                    self.myWebView.showDescriptionViewWithImage(description: Localized("Message_FailureLiveChat"))
                }
            }
        }
    }
    
    @objc func reloadWebView() {
        
        if let myURLRequest = Constants.liveChatUrlRequest {
            
            myWebView.load(myURLRequest)
        }
    }
    
    // Remove notification
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


extension LiveChatWebViewVC : WKNavigationDelegate, WKUIDelegate, WKURLSchemeHandler {

    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        if (error as NSError).code !=  -999 {
            
            self.hideActivityIndicator()
            self.showErrorAlertWithMessage(message: Localized("Message_FailureLiveChat"))
            MBPICUserSession.shared.liveChatObject = nil
            self.myWebView.showDescriptionViewWithImage(description: Localized("Message_FailureLiveChat"))
        }
        self.hideActivityIndicator()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.showActivityIndicator()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideActivityIndicator()
        //print("Finished navigating to url \(webView.url)")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        switch navigationAction.navigationType {
            case .linkActivated:
                if let newURL = navigationAction.request.url,
                UIApplication.shared.canOpenURL(newURL) {
                
                self.openURLInSafari(urlString: newURL.absoluteString)
                decisionHandler(.cancel)
                
            }
            default:
                break
        }

        var liveChatUrlRequest : URLRequest = navigationAction.request
        if liveChatUrlRequest.httpBody == nil {
            liveChatUrlRequest.httpMethod = Constants.liveChatUrlRequest?.httpMethod
            liveChatUrlRequest.httpBody = Constants.liveChatUrlRequest?.httpBody
            decisionHandler(.allow)
        }
    }
        
    @available(iOS 11.0, *)
    func webView(_ webView: WKWebView, start urlSchemeTask: WKURLSchemeTask) {
        
    }
    
    @available(iOS 11.0, *)
    func webView(_ webView: WKWebView, stop urlSchemeTask: WKURLSchemeTask) {
        
    }
}
