//
//  BaseVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 05/30/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper
import KYDrawerController
import FirebaseMessaging

class BaseVC: UIViewController {
    
//    var isPopGestureEnabled : Bool = false
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //MARK:- viewController Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting status bar color
        setStatusBarBackgroundColor(color: UIColor.mbStatusBarRed)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Enabling default back Gesture
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
    }
    
    
    //MARK:- Functions
    
    /**
     Set color of status bar.
     
     - parameter color: Color which will be set to status bar.
     
     - returns: void
     */
    func setStatusBarBackgroundColor(color: UIColor) {
        let tag = 13254
        if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
            statusBar.backgroundColor = color
        } else {
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            statusBarView.tag = tag
            UIApplication.shared.keyWindow?.addSubview(statusBarView)
            statusBarView.backgroundColor = color
        }
    }
    
    
    /**
     Clear user information from userDefaults and navigate user to login screen.
     
     - returns: void
     */
    class public func logout() {
        
        // try to get rootView controller and pop accordingly
        if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
            
            // Clear information in userDefaults
            MBPICUserSession.shared.clearUserSession()
            navigationController.popToLoginViewController()
        } else {
            UIApplication.shared.delegate?.window??.rootViewController?.navigationController?.popToRootViewController(animated: true)
        }
        MBActivityIndicator.shared.removeAllActivityIndicator()
    }
    
    /**
     Redirect user to notification screen if he tab on notification.
     
     - returns: void
     */
    class func redirectUserToNotificationScreen() {
        
        // check user if logged in
        if MBPICUserSession.shared.isLoggedIn() {
            
            // search for Notifications screen from naviagetion
            if let navigationController  = UIApplication.shared.delegate?.window??.rootViewController as? MainNavigationController {
                
                var isFoundNotificationController = false
                
                navigationController.viewControllers.reversed().forEach({ (aViewController) in
                    
                    if aViewController.isKind(of: NotificationsVC().classForCoder) {
                        
                        isFoundNotificationController = true
                        navigationController.popToViewController(aViewController, animated: true)
                    }
                })
                
                // check if Notification screen found in main navigation
                if isFoundNotificationController != true {
                    
                    // Search for DashBoard from current navigation
                    var isFoundController = false
                    navigationController.viewControllers.forEach({ (aViewController) in
                        
                        if aViewController.isKind(of: KYDrawerController(drawerDirection: .left,drawerWidth: ((UIScreen.main.bounds.width/3)*2.5)).classForCoder) {
                            isFoundController = true
                            
                        }
                    })
                    
                    // push notifiaction screen on KYDrawerController controller if found
                    if isFoundController {
                        
                        if let notifications :NotificationsVC = NotificationsVC.instantiateViewControllerFromStoryboard() {
                            navigationController.pushViewController(notifications, animated: true)
                        }
                        
                    } else {
                        // Enable check to redirect user to Notification screen
                        MBPICUserSession.shared.isPushNotificationSelected = true
                    }
                }
            }
        } else {
            // Enable check to redirect user to Notification screen
            MBPICUserSession.shared.isPushNotificationSelected = true
        }
    }
    
    /**
     Determine password complexity according to predefied rules.
     
     - parameter isForLogin: check calling when user login or from diffrent screen.
     
     - returns: void
     */
    static var numberOfTimeAddFCMCalled : Int = 1
    @objc static func addFCMId(isFromLogin: Bool) {
        
        // Check user logged in or not
        if MBPICUserSession.shared.isLoggedIn() {
            
            // Check if addFCMId API called sucessfull or not
            if UserDefaults.standard.bool(forKey: Constants.kIsFCMIdAdded) != true {
                
                // Get and check token is valid
                let token = Messaging.messaging().fcmToken
                
                if token?.isBlank == false {
                    
                    /* Call add FCMId API*/
                    _ = MBAPIClient.sharedClient.addFCMId(fcmKey: token ?? "", ringingStatus: .Tone, isEnable: true, isFromLogin: isFromLogin, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                        
                        // Check if error occur
                        if error != nil {
                            // Do nothing
                            // error?.showServerErrorInViewController(self)
                            
                        } else {
                            
                            // Check if API responded successfuly
                            if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                                
                                // Updating check related FCM id add or not.
                                UserDefaults.saveBoolForKey(boolValue: true, forKey: Constants.kIsFCMIdAdded)
                                // setting Notification is enabled.
                                UserDefaults.saveBoolForKey(boolValue: true, forKey: Constants.kIsNotificationEnabled)
                                
                                // Parssing response data
                                let extractedExpr: Mapper<AddFCM> = Mapper<AddFCM>()
                                if let addFCMResponseHandler = extractedExpr.map(JSONObject:resultData) {
                                    
                                    // Save Notification configration
                                    addFCMResponseHandler.saveInUserDefaults(key: APIsType.notificationConfigurations.localizedAPIKey())
                                }
                                
                            } else { // Call three time in case of failure
                                
                                if numberOfTimeAddFCMCalled <= 3 {
                                    
                                    // Increase number of times API called and all again after 10n seconds
                                    numberOfTimeAddFCMCalled += 1
                                    perform(#selector(addFCMId), with: nil, afterDelay: 10)
                                } else {
                                    // Reset value of count
                                    numberOfTimeAddFCMCalled = 1
                                }
                            }
                        }
                    })
                }
            }
        }
    }
    
    //MARK:- IBACTIONS
    @IBAction func backPressed(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuPressed(_ sender: AnyObject){
        if let drawerController = self.tabBarController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
}

extension BaseVC: UIGestureRecognizerDelegate {
    
    func interactivePop(_ isEnable:Bool = true) {
        if let navigationVC = self.navigationController as? MainNavigationController {
            navigationVC.interactivePop(isEnable) 
        }
        
    }
}

//MARK:- KYDrawerController extension for status color
extension KYDrawerController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


