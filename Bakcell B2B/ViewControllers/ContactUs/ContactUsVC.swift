//
//  ContactUsVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/23/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import MessageUI
import ObjectMapper

class ContactUsVC: BaseVC {

    //MARK: - Properties
    var contactUsDetail : ContactUs?

    //MARK: - IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var headOfficeTitleLabel: MBLabel!
    @IBOutlet var headOfficeAddressLabel: MBLabel!

    @IBOutlet var infoTitleLabel: MBLabel!
    @IBOutlet var helpLineLabel: MBLabel!
    @IBOutlet var phoneNumberLabel: MBLabel!
    @IBOutlet var secondPhoneNumberLabel: MBLabel!
    @IBOutlet var emailLabel: MBLabel!
    @IBOutlet var webSiteLabel: MBLabel!
    @IBOutlet var officesLabel: MBLabel!

    @IBOutlet var socialMediaTitleLabel: MBLabel!
    @IBOutlet var inviteFriendsLabel: MBLabel!
    @IBOutlet var contactDetailsParentView : UIView!
    @IBOutlet var sendButton: UIButton!

    //MARK: - View controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interactivePop()
        
        contactDetailsParentView.isHidden = true
        
        // loading user initial inforamtion
        layoutViewController()

        // API call to get contact us information
        getContactUsDetails()
    }

    

    //MARK: - Functions
    
    ///Set Localized for layouts
    func layoutViewController() {

        /* Setting View controller text*/
        titleLabel.text              = Localized("Title_ContactUs")
        headOfficeTitleLabel.text    = Localized("Title_HeadOffice")
        headOfficeAddressLabel.text  = Localized("Info_HeadOfficeAddress")
        infoTitleLabel.text          = Localized("Title_InfoCenter")
        helpLineLabel.text           = Localized("Info_HelpLine")
        phoneNumberLabel.text        = Localized("Info_PhoneNumber")
        secondPhoneNumberLabel.text  = Localized("Info_PhoneNumber2")
        emailLabel.text              = Localized("Info_EmailData")
        webSiteLabel.text            = Localized("Info_webSite")
        officesLabel.text            = Localized("Info_Offices")
        socialMediaTitleLabel.text   = Localized("Title_SocialMedia")
        inviteFriendsLabel.text      = Localized("Title_InviteFriends")

        sendButton.setTitle(Localized("BtnTitle_Send"), for: UIControl.State.normal)
    }

    func updateInformationOfLayout() {

        if contactUsDetail != nil{
            /* Setting View controller text*/
            headOfficeAddressLabel.text  = contactUsDetail?.address ?? ""
            helpLineLabel.text           = contactUsDetail?.customerCareNo ?? ""
            phoneNumberLabel.text        = contactUsDetail?.phone ?? ""
            secondPhoneNumberLabel.text  = contactUsDetail?.fax ?? ""
            emailLabel.text              = contactUsDetail?.email ?? ""
            webSiteLabel.text            = contactUsDetail?.website ?? ""
            contactDetailsParentView.isHidden = false
            self.view.hideDescriptionView()
            
        } else{
            contactDetailsParentView.isHidden = true
            self.view.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        }


    }
    
    //MARK: - IBAction
    @IBAction func headOfficeLocationBtnPressed(_ sender: UIButton) {
        
        let lattitude = contactUsDetail?.addressLat ?? ""
        let longitude = contactUsDetail?.addressLong ?? ""
        
        if lattitude.isBlank == false && longitude.isBlank == false {
            
            let cordinatesDict = ["lati": lattitude, "longi" : longitude, "title": Localized("Title_HeadOffice"), "address" : contactUsDetail?.address ?? "" ]
            
            if let store :StoreLocatorVC = StoreLocatorVC.instantiateViewControllerFromStoryboard() {
                store.selectedType = .StoreLocator
                
                store.shouldFocuseToSelectedLocation = true
                store.selectedLocationForFocuse = cordinatesDict
                
                self.navigationController?.pushViewController(store, animated: true)
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_NoDataAvalible"))
        }
    }
    
    @IBAction func helpLineBtnPressed(_ sender: UIButton) {
        dialNumber(number: contactUsDetail?.customerCareNo ?? "")
    }
    
    @IBAction func phoneBtnPressed(_ sender: UIButton) {
        dialNumber(number: contactUsDetail?.phone ?? "")
    }
    
    @IBAction func phone2BtnPressed(_ sender: UIButton) {
        dialNumber(number: contactUsDetail?.fax ?? "")
    }
    
    @IBAction func emialBtnPressed(_ sender: UIButton) {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([contactUsDetail?.email ?? ""])
            mail.setMessageBody("", isHTML: false)
            
            present(mail, animated: true)
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_SetEmail"))
        }
    }
    
    @IBAction func webSiteBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail?.website ?? "")
    }
    
    @IBAction func officesBtnPressed(_ sender: UIButton) {
        if let store :StoreLocatorVC = StoreLocatorVC.instantiateViewControllerFromStoryboard() {
            store.selectedType = .StoreLocator
            self.navigationController?.pushViewController(store, animated: true)
        }

    }
    
    @IBAction func faceBookBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail?.facebookLink)
    }
    
    @IBAction func twitterBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail?.twitterLink)
    }
    
    @IBAction func googleBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail?.googleLink)
    }
    
    @IBAction func youTubeBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail?.youtubeLink)
    }
    
    @IBAction func instagramBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail?.instagramLink)
    }
    
    @IBAction func linkedInBtnPressed(_ sender: UIButton) {
        openURLInSafari(urlString: contactUsDetail?.linkedinLink)
    }
    
    @IBAction func sendPressed(_ sender: UIButton) {
        
        let sharingItems:[AnyObject?] = ["\(contactUsDetail?.inviteFriendText ?? "")" as AnyObject]
        
        let activityViewController = UIActivityViewController(activityItems: sharingItems.compactMap({$0}), applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }

    //MARK: - APIs call

    /// Call 'getContactUsDetails' API.
    ///
    /// - returns: Void
    func getContactUsDetails() {
        // Parssing response data
        if let contactUsResponse :ContactUs = ContactUs.loadFromUserDefaults(key: APIsType.contactUsDetail.localizedAPIKey()) {
            
            self.contactUsDetail = contactUsResponse
            self.updateInformationOfLayout()
            
        } else {
            self.showActivityIndicator()
        }
        
        _ = MBAPIClient.sharedClient.getContactUsDetails({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let contactUsResponse = Mapper<ContactUs>().map(JSONObject:resultData) {
                        
                        /*Save data into user defaults*/
                        contactUsResponse.saveInUserDefaults(key: APIsType.contactUsDetail.localizedAPIKey())
                        
                        self.contactUsDetail = contactUsResponse
                        
                    }
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.updateInformationOfLayout()
        })
    }
}

//MARK: - MFMailComposeViewControllerDelegate
extension ContactUsVC : MFMailComposeViewControllerDelegate {

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
