//
//  SplashVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/06/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper
import LocalAuthentication


class SplashVC: BaseVC {
    
    //MARK:- Properties
    var isAppStarted = false
    /// An authentication context stored at class scope so it's available for use during UI updates.
    var context = LAContext()
    
    //MARK:- IBOutlet
    @IBOutlet var backcellImageView: UIImageView!
    
    //MARK:- ViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting user initial language
        MBLanguageManager.setInitialUserLanguage()
        
        /* Verifing current app version */
        verifyAppVersion()
        
        /* Delete Users info from userDefaults */
        MBUtilities.clearJSONStringFromUserDefaults(key: APIsType.usersData.localizedAPIKey())
        MBUtilities.clearJSONStringFromUserDefaults(key: APIsType.groupData.localizedAPIKey())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        backcellImageView.image = UIImage.imageFor(name: Localized("Img_BakcelLogo"))  
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /* Check if user previously opened application and redirected to Home Page then redirect without calling AppVersion API */
        if isAppStarted == true {
            navigateUserToAppropriateScreen()
        }
    }
    
    //MARK:- Functions
    
    /**
     Redirect user from splash screen to walkThrough, Login/SignUp and Dashboard ViewController.
     
     - returns: void
     */
    @objc func navigateUserToAppropriateScreen() {
        
        // Set to true, can verify user redirected first time from splash screen.
        isAppStarted = true
        
        // Check if user loggedIn and Customer information is loaded sucessfully.
        if MBPICUserSession.shared.isLoggedIn() && MBPICUserSession.shared.loadUserInfomation() {
            
            if MBPICUserSession.shared.isBiometricEnabled() {
                self.authenticateWithTouchID()
            } else {
                self.redirectToDashboardScreen()
            }
        } else {
            // Redirect user to login screen.
            self.redirectToLoginScreen()
        }
    }
    
    func redirectToLoginScreen() {
        DispatchQueue.main.async { [unowned self] in
            if let loginVC :LoginVC = LoginVC.instantiateViewControllerFromStoryboard() {
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }
    }
    
    func redirectToDashboardScreen() {
        if let mainViewController :TabbarController = TabbarController.instantiateViewControllerFromStoryboard() {
            
            if let drawerViewController :SideDrawerVC = SideDrawerVC.instantiateViewControllerFromStoryboard() {
                let drawerController = KYDrawerController(drawerDirection: .left,drawerWidth: ((UIScreen.main.bounds.width/3)*2.5))
                drawerController.mainViewController = mainViewController
                drawerController.drawerViewController = drawerViewController
                self.navigationController?.pushViewController(drawerController, animated: true)
            }
        }
    }
    
    
    /**
     Redirect user to AppStore
     
     - returns: void
     */
    func redirectUserToAppStore(appStoreURL urlString :String?, byForce: Bool = false) {
        
        var appStoreURL :URL?
        
        if let storeURLString = urlString,
            let responseStoreURL = URL(string: storeURLString),
            UIApplication.shared.canOpenURL(responseStoreURL) {
            
            appStoreURL = responseStoreURL
            
        } else {
            
            if let constaintStoreURL = URL(string: Localized("appStore_URL")),
                UIApplication.shared.canOpenURL(constaintStoreURL) {
                
                appStoreURL = constaintStoreURL
            }
        }
        
        if let storeURL = appStoreURL,
            UIApplication.shared.canOpenURL(storeURL) {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(storeURL, options: [:], completionHandler: { (success: Bool) in
                    
                    // If failded to open URL
                    if success == false {
                        UIApplication.shared.openURL(storeURL)
                    }
                    // Close application
                    exit(0)
                })
            } else {
                UIApplication.shared.openURL(storeURL)
                // Close application
                exit(0)
            }
            
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_CannotAbleToRedirect"), {
                if byForce == true {
                    // Close application
                    exit(0)
                } else {
                    self.navigateUserToAppropriateScreen()
                }
            })
        }
    }
    
    func authenticateWithTouchID() {
        
        // Get a fresh context for each login. If you use the same context on multiple attempts
        //  (by commenting out the next line), then a previously successful authentication
        //  causes the next policy evaluation to succeed without testing biometry again.
        //  That's usually not what you want.
        context = LAContext()
        
        if #available(iOS 10.0, *) {
            //  context.localizedCancelTitle = "Enter Username/Password"
        } else {
            // Fallback on earlier versions
        }
        
        //context.localizedFallbackTitle = "Use Passcode"
        
        // First check if we have the needed hardware support.
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            
            let reason = Localized("Biometric_Login")
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                DispatchQueue.main.async {
                    if success {
                        self.redirectToDashboardScreen()
                        
                    } else {
                        //print(error?.localizedDescription ?? "Failed to authenticate")
                        
                        // Fall back to a asking for username and password.
                        // ...
                        //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
                        let errorDescription = self.evaluateAuthenticationPolicyMessageForLA(errorCode: error?._code)
                        if errorDescription.isUserAction == false {
                            self.showErrorAlertWithMessage(message: errorDescription.message, {
                                self.redirectToLoginScreen()
                            })
                        } else {
                            self.redirectToLoginScreen()
                        }
                        
                        
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                // Fall back to a asking for username and password.
                // ...
                //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
                let errorDescription = self.evaluateAuthenticationPolicyMessageForLA(errorCode: error?._code)
                if errorDescription.isUserAction == false {
                    self.showErrorAlertWithMessage(message: errorDescription.message, {
                        self.redirectToLoginScreen()
                    })
                } else {
                    self.redirectToLoginScreen()
                }
            }
        }
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int?) -> (message:String, isUserAction:Bool) {
        
        var message :String?
        var userCanceled :Bool = false
        
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            message = Localized("Error_FailedCredentials")
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            userCanceled = true
            
        case LAError.userFallback.rawValue:
            message = "The user choose to use the fallback"
            userCanceled = true
            
        case LAError.systemCancel.rawValue:
            message = Localized("Error_SystemCancel")
            
        case LAError.passcodeNotSet.rawValue:
            message = Localized("Error_PasscodeNotSet")
            
        case LAError.appCancel.rawValue:
            message = Localized("Auth_AppCancel")
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = Localized("Error_NotInteractive")
            
        default:
            if #available(iOS 11.0, macOS 10.13, *) {
                switch errorCode {
                case LAError.biometryNotAvailable.rawValue:
                    message = Localized("Error_BiometryNotAvailable")
                    
                case LAError.biometryNotEnrolled.rawValue:
                    message = Localized("Error_BiometryNotEnrolled")
                    
                case LAError.biometryLockout.rawValue:
                    message = Localized("Error_BiometricLockout")
                    
                default:
                    break
                }
            } else {
                switch errorCode {
                case LAError.touchIDNotAvailable.rawValue:
                    message = Localized("Error_BiometryNotAvailable")
                    
                case LAError.touchIDNotEnrolled.rawValue:
                    message = Localized("Error_BiometryNotEnrolled")
                    
                case LAError.touchIDLockout.rawValue:
                    message = Localized("Error_BiometricLockout")
                default:
                    break
                }
            }
        }
        
        return ((message ?? Localized("Error_AuthGeneral")) ,userCanceled)
    }
    
    //MARK: - API Calls
    
    /**
     verify AppVersion API call
     
     - returns: void
     */
    func verifyAppVersion() {
        
        // Check if connected to internet then call API
        if Connectivity.isConnectedToInternet == true {
            
            _ = MBAPIClient.sharedClient.verifyAppVersion({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
                
                if let appVersionHandler = Mapper<VerifyAppVersion>().map(JSONObject: resultData) {
                    
                    // handling data from API response.
                    if resultCode == Constants.MBAPIStatusCode.succes.rawValue { /* Application is updated */
                        
                        /* Redirect user to appropriate screen */
                        self.navigateUserToAppropriateScreen()
                        
                    } else if resultCode == Constants.MBAPIStatusCode.forceUpdate.rawValue { /* New version of application is available */
                        
                        /* Save AppStore URL in User Defaults */
                        MBUtilities.updateAppStoreURL(appVersionHandler.appStore)
                        
                        /* Show force update alert. */
                        self.showAlert(title: Localized("Title_Update"), message: resultDesc, btnTitle: Localized("BtnTitle_UPDATE"), {
                            /* Redirect user to appStore in case user select update button */
                            self.redirectUserToAppStore(appStoreURL: appVersionHandler.appStore, byForce: true)
                        })
                        
                    } else if resultCode == Constants.MBAPIStatusCode.optionalUpdate.rawValue {
                        
                        /* SHOW Aler and then redirect according to user selection */
                        self.showConfirmationAlert(title: Localized("Title_Update"), message: resultDesc, okBtnTitle: Localized("BtnTitle_UPDATE"), cancelBtnTitle: Localized("BtnTitle_CANCEL"), alertType: .other, {
                            
                            /* Redirect user to appStore in case user select update button */
                            self.redirectUserToAppStore(appStoreURL: appVersionHandler.appStore)
                        }, {
                            /* Redirect user to appropriate screen */
                            self.navigateUserToAppropriateScreen()
                        })
                        
                    } else if resultCode == Constants.MBAPIStatusCode.serverDown.rawValue {
                        /* Redirect user to appropriate screen */
                        self.navigateUserToAppropriateScreen()
                        
                    } else {
                        /* Redirect user to appropriate screen */
                        self.navigateUserToAppropriateScreen()
                    }
                    
                } else {
                    /* Redirect user to appropriate screen */
                    self.navigateUserToAppropriateScreen()
                }
            })
            
        } else {
            
            /* Redirect user to appropriate screen with delay */
            if isAppStarted == false {
                perform(#selector(navigateUserToAppropriateScreen), with: nil, afterDelay: Constants.SPLASH_DURATION)
            } else {
                /* Redirect user to appropriate screen */
                self.navigateUserToAppropriateScreen()
            }
        }
    }
}
