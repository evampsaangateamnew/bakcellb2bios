//
//  NotificationsVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class NotificationsVC: BaseVC {
    
    // MARK: - Properties
    var notificationsData : [NotificationsList]?
    var filterdNotificationsData : [NotificationsList]?
    
    // MARK: - IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var tableView: UITableView!
    
    //MARK: - View controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        searchView.isHidden = true
        
        searchTextField.delegate = self
        searchTextField.attributedPlaceholder = NSAttributedString(string: Localized("PlaceHolder_Search"),
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        tableView.separatorColor = UIColor.clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180.0
        tableView.allowsSelection = false
        tableView.isScrollEnabled = true
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Get Notification data
        getNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        // Setting ViewController localized text
        layoutViewController()
        
        /* Reset Notification bage to 0 */
        UIApplication.shared.applicationIconBadgeNumber = 0

    }
    
    
    //MARK: - Functions
    func layoutViewController() {
        titleLabel.text = Localized("Title_Notifications")
    }
    
    func reloadNotificationTableView()  {
        
        if self.filterdNotificationsData?.count ?? 0 == 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"), centerYConstant: -40)
            
        } else {
            self.tableView.hideDescriptionView()
        }
        tableView.reloadData()
    }
    
    func redirectUserToSupplementaryController(actionID : String?) {
        
        if let supplementoryOfferVC :SupplementaryVC = SupplementaryVC.instantiateViewControllerFromStoryboard() {
            supplementoryOfferVC.offeringIdFromNotification = actionID ?? ""
            supplementoryOfferVC.redirectedFromNotification = true
            self.navigationController?.pushViewController(supplementoryOfferVC, animated: true)
        }
        
    }
    func redirectUserToTariffController(actionID : String?) {
        
        if let aViewController :TariffsMainVC = TariffsMainVC.instantiateViewControllerFromStoryboard() {
            aViewController.redirectedFromNotification = true
            aViewController.offeringIdFromNotification = actionID ?? ""
            self.navigationController?.pushViewController(aViewController, animated: true)
        }
        
    }
    
    //MARK: - IBActions
    
    @IBAction func searchPressed(_ sender: UIButton) {
        
        if searchView.isHidden == false {
            
            searchView.isHidden = true
            titleLabel.isHidden = false
            
            searchButton.setImage(UIImage.imageFor(name: "actionbar_search") , for: UIControl.State.normal)
            
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
            
            
        } else {
            titleLabel.isHidden = true
            searchView.isHidden = false
            
            searchButton.setImage(UIImage.imageFor(name: "Cross") , for: UIControl.State.normal)
            
            searchTextField.becomeFirstResponder()
            
        }
    }
    
    @IBAction func redirectionBtnPressed(_ sender: UIButton) {
        
        let selectedNotificationData = filterdNotificationsData?[sender.tag]
        
        if (selectedNotificationData?.actionType ?? "").isEqual("tariff", ignorCase: true) {
            
            redirectUserToTariffController(actionID: selectedNotificationData?.actionID)
            
        } else if (selectedNotificationData?.actionType ?? "").isEqual("supplementary", ignorCase: true) {
            
            redirectUserToSupplementaryController(actionID: selectedNotificationData?.actionID)
            
        }
    }
    
    
    //MARK: - API Calls
    /// Call 'getNotifications' API .
    ///
    /// - returns: Void
    func getNotifications() {
        
        /* Loading initial data from UserDefaults */
        if let notificationsHandler :Notifications = Notifications.loadFromUserDefaults(key: APIsType.notificationData.localizedAPIKey()) {
            self.notificationsData = notificationsHandler.notificationsList
            self.filterdNotificationsData = notificationsHandler.notificationsList
            self.reloadNotificationTableView()
            
        } else {
            self.showActivityIndicator()
        }
        
        /*API call to get data*/
        _ = MBAPIClient.sharedClient.getNotifications({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let notificationsHandler = Mapper<Notifications>().map(JSONObject: resultData){
                        
                        /*Save data into user defaults*/
                        notificationsHandler.saveInUserDefaults(key: APIsType.notificationData.localizedAPIKey())
                        
                        self.notificationsData = notificationsHandler.notificationsList
                        self.filterdNotificationsData = notificationsHandler.notificationsList
                        
                        
                    } else {
                        
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                } else {
                    
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            self.reloadNotificationTableView()
        })
    }
}

//MARK: - TableView Delegate
extension NotificationsVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdNotificationsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:NotificationsCell = tableView.dequeueReusableCell() {
            
            // setting side line for last notification data
            if (filterdNotificationsData?.count ?? 0) - 1 == indexPath.row {
                cell.lineHeightConstraint.isActive = true
                cell.lineHeightConstraint.constant = 16
                cell.lineBottomConstraint.isActive = false
            } else {
                cell.lineHeightConstraint.isActive = false
                cell.lineBottomConstraint.isActive = true
                
            }
            
            // Loading data
            if let aNotificationData = filterdNotificationsData?[indexPath.row] {
                
                /* Icon mapping is not added */
                cell.imgView?.image = UIImage (named:"icon1" )
                if aNotificationData.btnTxt?.isBlank ?? true {
                    cell.reNewBtnWidth.constant = 0
                    cell.reNewBtnWidth.isActive = true
                    cell.renewButton.isHidden = true
                    
                } else {
                    
                    cell.reNewBtnWidth.constant = 90
                    //  cell.reNewBtnWidth.isActive = false
                    
                    cell.renewButton.isHidden = false
                    cell.renewButton.roundAllCorners(radius: CGFloat(Constants.kButtonCornerRadius))
                    cell.renewButton.setTitle(aNotificationData.btnTxt ?? "", for: UIControl.State.normal)
                    cell.renewButton.addTarget(self, action: #selector(redirectionBtnPressed(_:)), for: UIControl.Event.touchUpInside)
                    cell.renewButton.tag = indexPath.row
                }
                cell.detailsLabel.text = aNotificationData.message
                
                let times = "\(aNotificationData.datetime ?? "")".offSetFirstValue()
                // Adding '.' for "ч"
                if times .range(of: "ч") != nil{
                    cell.timePassedLabel.text = "\(times)."
                } else {
                    cell.timePassedLabel.text = "\(times)"
                }
                
            }
            cell.layoutIfNeeded()
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension NotificationsVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        filterdNotificationsData = filterNotificationBySearchString(searchString: newString, notificationsArray: notificationsData)
        reloadNotificationTableView()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
    func filterNotificationBySearchString(searchString : String? , notificationsArray : [NotificationsList]? ) -> [NotificationsList] {
        
        // return complete array if string is empty
        if let searchString = searchString {
            
            if searchString == "" {
                
                if let offers = notificationsArray {
                    return offers
                }
            }
        }
        
        // Filtering offers array
        var filteredNotification : [NotificationsList] = []
        if let aNotification = notificationsArray {
            
            filteredNotification = aNotification.filter() {
                
                if let offerName = ($0 as NotificationsList).message {
                    
                    return offerName.containsSubString(subString: searchString)
                    
                } else {
                    return false
                }
            }
        }
        
        return filteredNotification
    }
}
