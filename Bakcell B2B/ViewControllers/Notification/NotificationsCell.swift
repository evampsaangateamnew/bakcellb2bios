//
//  NotificationsCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var timePassedLabel: MBLabel!
    @IBOutlet var detailsLabel: MBLabel!
    @IBOutlet var lineView: UIView!
    @IBOutlet var renewButton: UIButton!

    @IBOutlet var lineBottomConstraint: NSLayoutConstraint!
    @IBOutlet var lineHeightConstraint: NSLayoutConstraint!
    @IBOutlet var reNewBtnWidth: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
