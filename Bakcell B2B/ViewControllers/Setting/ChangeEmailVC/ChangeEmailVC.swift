//
//  ChangeEmailVCViewController.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ChangeEmailVC: BaseVC {
    
    //MARK:- Properties
    @IBOutlet var titleLable: MBLabel!
    @IBOutlet var descriptionLable: MBLabel!
    @IBOutlet var emailTextField: MBTextField!
    @IBOutlet var updateButton: MBButton!
    
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        titleLable.text = Localized("ChangeEmail_Title")
        descriptionLable.text = Localized("ChangeEmail_Info")
        emailTextField.placeholder = Localized("PlaceHolder_EnterNewEmail")
        updateButton.setTitle(Localized("BtnTitle_UpdateEmail"), for: UIControl.State.normal)
        
    }
    //MARK: - API Calls
    /// Call 'updateCustomerEmail' API .
    ///
    /// - returns: Void
    func updateCustomerEmail(newEmail : String?) {
        
        _ = emailTextField.resignFirstResponder()
        
        /*self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.updateCustomerEmail(NewEmail: newEmail ?? "" ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in

            self.hideActivityIndicator()

            if error != nil {
                error?.showServerErrorInViewController(self)

            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {

                    // Parssing response data
                    if let responseHandler = Mapper<MessageResponse>().map(JSONObject: resultData){
                    
                        // Update user email
                        MBSelectedUserSession.shared.userInfo?.email = newEmail
                        self.showSuccessAlertWithMessage(message: responseHandler.message, {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }

                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })*/
    }
    
    //MARK:- IBAction
    @IBAction func updatePressed(_ sender:AnyObject) {
        
        if emailTextField.text?.isValidEmail() ?? false {
            
            self.showConfirmationAlert(title: Localized("Title_Confirmation"), message: Localized("Message_EmailChangeConfirmation"), okBtnTitle: Localized("BtnTitle_OK"), cancelBtnTitle: Localized("BtnTitle_NO"), {
                // Chage User email
                self.updateCustomerEmail(newEmail: self.emailTextField.text)
            })
            
        } else {
            
            if emailTextField.text?.isBlank ?? false {
                
                self.showErrorAlertWithMessage(message: Localized("Message_EnterEmail"))
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_invalidEmail"))
            }
        }
    }
}

//MARK: Textfield delagates
extension ChangeEmailVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length
        
        if textField == emailTextField {
            if newLength <= 50 {
                return  true
            } else {
                return  false
            }
            
        }  else {
            return false // Bool
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            _ = textField.resignFirstResponder()
            updatePressed(updateButton)
            return false
        }
        
        return true
    }
}
