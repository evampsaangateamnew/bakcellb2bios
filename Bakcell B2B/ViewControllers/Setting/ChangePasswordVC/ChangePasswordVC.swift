//
//  ChangePasswordVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ChangePasswordVC: BaseVC {
    
    // MARK: - Properties
    var alertCheck = false
    var canUserNavigate :Bool = true
    var picUserInformation : PICUserInformation?
    
    // MARK: - IBOutlet
    @IBOutlet var backButton: UIButton!
    @IBOutlet var updateButton: MBButton!
    @IBOutlet var logoutButton: MBButton!
    @IBOutlet var errorLabel: MBLabel!
    @IBOutlet var infoLabel: MBLabel!
    @IBOutlet var changePassTitleLabel: MBLabel!
    @IBOutlet var passwordStrengthView : UIView!
    @IBOutlet var securityLabel: MBLabel!
    @IBOutlet var securityBar : UIProgressView!
    @IBOutlet var alertView: UIView!
    
    @IBOutlet var oldPasswordTextField: MBTextField!
    @IBOutlet var newPasswordTextField: MBTextField!
    @IBOutlet var confirmNewPasswordTextField: MBTextField!
    @IBOutlet var backButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet var newPasswordTopConstraint: NSLayoutConstraint!
    @IBOutlet var infoViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var updateButtonHorizontalCenterConstraint: NSLayoutConstraint!
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        oldPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        confirmNewPasswordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        loadViewLayout()
        
        
        if canUserNavigate {
            backButton.isHidden = false
            backButtonWidthConstraint.constant = 32
            updateButtonHorizontalCenterConstraint = updateButtonHorizontalCenterConstraint.setMultiplier(multiplier: 1)
            logoutButton.isHidden = true
        } else {
            backButton.isHidden = true
            backButtonWidthConstraint.constant = 0
            updateButtonHorizontalCenterConstraint = updateButtonHorizontalCenterConstraint.setMultiplier(multiplier: 1.5)
            logoutButton.isHidden = false
        }
    }
    
    //MARK:- IBActions
    @IBAction func updatePressed(_ sender: Any) {
        
        if MBPICUserSession.shared.changePasswordRequestCount < 5 {
            
            var oldPassword : String = ""
            var newPassword : String = ""
            var confirmNewPassword : String = ""
            
            // Current Password validation
            if let oldPasswordText = oldPasswordTextField.text {
                
                let passwordStrength:Constants.MBPasswordStrength = MBUtilities.determinePasswordStrength(Text: oldPasswordText)
                
                switch passwordStrength {
                    
                case Constants.MBPasswordStrength.didNotMatchCriteria :
                    self.showErrorAlertWithMessage(message: Localized("Message_InvalidCurrentPassword"))
                    return
                case Constants.MBPasswordStrength.Week, Constants.MBPasswordStrength.Medium, Constants.MBPasswordStrength.Strong:
                    oldPassword = oldPasswordText
                }
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidCurrentPassword"))
                return
            }
            
            // Password validation
            if let passwordText = newPasswordTextField.text {
                
                let passwordStrength:Constants.MBPasswordStrength = MBUtilities.determinePasswordStrength(Text: passwordText)
                
                switch passwordStrength {
                    
                case Constants.MBPasswordStrength.didNotMatchCriteria:
                    self.showErrorAlertWithMessage(message: Localized("Message_InvalidPasswordComplexity"))
                    return
                case Constants.MBPasswordStrength.Week, Constants.MBPasswordStrength.Medium, Constants.MBPasswordStrength.Strong:
                    newPassword = passwordText
                }
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidPasswordComplexity"))
                return
            }
            
            // Confirm Password validation
            if let confirmPasswordText = confirmNewPasswordTextField.text,
                confirmPasswordText == newPassword {
                
                confirmNewPassword = confirmPasswordText
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidConfirmPassword"))
                return
            }
            
            // Hiding Key board
            _ = oldPasswordTextField.resignFirstResponder()
            _ = newPasswordTextField.resignFirstResponder()
            _ = confirmNewPasswordTextField.resignFirstResponder()
            
            if MBPICUserSession.shared.aSecretKey.isBlank == false {
                
                var picUserName = ""
                if canUserNavigate {
                    picUserName = MBPICUserSession.shared.msisdn
                } else {
                    picUserName = self.picUserInformation?.msisdn ?? ""
                }
                changePasswordUserAPICall(picUserName, OldPassword: oldPassword.aesEncrypt(key: MBPICUserSession.shared.aSecretKey), NewPassword: newPassword.aesEncrypt(key: MBPICUserSession.shared.aSecretKey), ConfirmNewPassword: confirmNewPassword.aesEncrypt(key: MBPICUserSession.shared.aSecretKey))
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_GenralError"))
            }
        }
    }
    
    @IBAction func logoutPressed(_ sender: MBButton) {
        self.logOut()
    }
    
    @IBAction func infoPressed(_ sender: UIButton) {
        
        if alertCheck == false{
            alertView.isHidden = false
            alertCheck = true
            newPasswordTopConstraint.constant = infoViewHeightConstraint.constant + 10
            
        } else {
            alertView.isHidden = true
            alertCheck = false
            newPasswordTopConstraint.constant = 24
        }
        
    }
    
    
    //MARK: - FUNCTIONS
    func loadViewLayout(){
        changePassTitleLabel.text = Localized("Title_ChangePassword")
        errorLabel.text = Localized("Password_ErrorInfo")
        infoLabel.text = Localized("ChangePassword_Description")
        updateButton.setTitle(Localized("BtnTitle_UpdatePassword"), for: UIControl.State.normal)
        logoutButton.setTitle(Localized("BtnTitle_LOGOUT"), for: UIControl.State.normal)
        oldPasswordTextField.placeholder = Localized("CurrentPass_PlaceHolder")
        newPasswordTextField.placeholder = Localized("SetPassword_PlaceHolder")
        confirmNewPasswordTextField.placeholder = Localized("ConfirmPassword_PlaceHolder")
    }
    
    func setPasswordStrenghtBar(Text text:String) {
        let passwordStrength:Constants.MBPasswordStrength = MBUtilities.determinePasswordStrength(Text: text)
        
        switch passwordStrength {
        case Constants.MBPasswordStrength.didNotMatchCriteria:
            passwordStrengthView.isHidden = true
            securityLabel.text = ""
            securityBar.setProgress(0, animated: false)
            securityBar.tintColor = UIColor.clear
            break
            
        case Constants.MBPasswordStrength.Week:
            passwordStrengthView.isHidden = false
            securityLabel.isHidden = false
            securityLabel.text = Localized("Pass_Weak")
            securityBar.tintColor = UIColor.mbBarRed
            securityBar.setProgress(0.33, animated: false)
            break
            
        case Constants.MBPasswordStrength.Medium:
            passwordStrengthView.isHidden = false
            securityLabel.isHidden = false
            securityLabel.text = Localized("Pass_Medium")
            securityBar.tintColor = UIColor.mbBarOrange
            securityBar.setProgress(0.66, animated: false)
            break
            
        case Constants.MBPasswordStrength.Strong:
            passwordStrengthView.isHidden = false
            securityLabel.isHidden = false
            securityLabel.text = Localized("Pass_Strong")
            securityBar.tintColor = UIColor.mbBarGreen
            securityBar.setProgress(1, animated: false)
            
        }
    }
    
    //MARK: - API Calls
    /// Call 'changePassword' API with the specified `OldPassword`, 'NewPassword' and `ConfirmNewPassword`.
    ///
    /// - parameter OldPassword:     The OldPassword of user.
    /// - parameter NewPassword:  The User new Password for account.
    /// - parameter ConfirmNewPassword:  The User Confirm new Password for account.
    ///
    /// - returns: Void
    func changePasswordUserAPICall(_ userName :String, OldPassword oldPassword : String , NewPassword newPassword : String, ConfirmNewPassword confirmNewPassword : String) {
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.changePassword(userName, OldPassword: oldPassword, NewPassword:newPassword , ConfirmNewPassword: confirmNewPassword,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if resultCode == Constants.MBAPIStatusCode.wrongAttemptToChangePassword.rawValue {
                
                MBPICUserSession.shared.changePasswordRequestCount = MBPICUserSession.shared.changePasswordRequestCount + 1
                if MBPICUserSession.shared.changePasswordRequestCount > 4 {
                    
                    // Reset counter
                    MBPICUserSession.shared.changePasswordRequestCount = 0
                    // Clear user Data
                    BaseVC.logout()
                    
                }  else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            } else if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    
                    // Parssing response data
                    if let changePasswordHandler = Mapper<MessageResponse>().map(JSONObject:resultData) {
                        // User logout
                        
                        self.showAlertWithMessage(message: changePasswordHandler.message, actionBlock: {
                            // Reset counter
                            MBPICUserSession.shared.changePasswordRequestCount = 0
                            
                            // logout user
                            self.logOut()
                        })
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    //MARK: - API Calls
    /// Call 'logOut' API .
    ///
    /// - returns: Void
    func logOut() {
        
        /*API call to Logout usr*/
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.logOut({ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self, {
                    // Clear user data
                    BaseVC.logout()
                })
                
            } else {
                // handling data from API response.
                if resultCode != Constants.MBAPIStatusCode.succes.rawValue {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc,{
                        // Clear user data
                        BaseVC.logout()
                    })
                } else {
                    // Clear user data
                    BaseVC.logout()
                }
            }
            
            
        })
    }
}

//MARK: Textfield delagates
extension ChangePasswordVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length
        
        // Allow charactors check
        let allowedCharactors = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharacters
        let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
        let characterSet = CharacterSet(charactersIn: string)
        
        if allowedCharacters.isSuperset(of: characterSet) != true {
            
            return false
        }
        
        if textField == newPasswordTextField {
            
            let validLimit : Bool = newLength <= 15
            
            if validLimit {
                
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
                setPasswordStrenghtBar(Text: newString)
                
            }
            
            return validLimit // Bool
        } else {
            // For other then setPassword textfield.
            return newLength <= 15 // Bool
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        setPasswordStrenghtBar(Text:"")
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        setPasswordStrenghtBar(Text:"")
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == oldPasswordTextField {
            newPasswordTextField.becomeFirstResponder()
            return false
        } else if textField == newPasswordTextField {
            confirmNewPasswordTextField.becomeFirstResponder()
            return false
        }else if textField == confirmNewPasswordTextField {
            _ = textField.resignFirstResponder()
            updatePressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
