//
//  SettingsVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper
import FirebaseMessaging

class SettingsVC: BaseVC {
    
    // MARK: - Properties
    var ringingType : Constants.MBNotificationSoundType = .Tone
    var isNotificationEnabled : Bool = false
    
    // MARK: - IBOutlet
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var changeLanguageTitleLabel: MBLabel!
    @IBOutlet var languageLabel: MBLabel!
    @IBOutlet var notificationTitleLabel: MBLabel!
    @IBOutlet var notificationDiscriptionLabel: MBLabel!
    @IBOutlet var notificationSettingTitleLabel: MBLabel!
    @IBOutlet var notificationSettingTuneLabel: MBLabel!
    @IBOutlet var notificationSettingVibrateLabel: MBLabel!
    @IBOutlet var notificationSettingMuteLabel: MBLabel!
    @IBOutlet var changePasswordTitleLabel: MBLabel!

    @IBOutlet var biometricTitleLabel: MBLabel!
    @IBOutlet var biometricSwitch: MBSwitch!
    
    @IBOutlet var appVersionTitleLabel: MBLabel!
    @IBOutlet var appVersionLabel: MBLabel!
    
    @IBOutlet var notificationsSwitch: MBSwitch!
    @IBOutlet var notificationsVibrateButton: UIButton!
    @IBOutlet var notificationsTuneButton: UIButton!
    @IBOutlet var notificationsMuteButton: UIButton!
    
    //MARK: - View Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationsSwitch.addTarget(self, action:#selector(notificationSwitchValueChanged) , for: UIControl.Event.valueChanged)
        biometricSwitch.addTarget(self, action:#selector(biometricSwitchValueChanged) , for: UIControl.Event.valueChanged)
        
        // get Notifications Configurations from server
        loadNotificationsConfigurations()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        // Loading intial layout Text
        layoutViewController()
        biometricSwitch.isOn = MBPICUserSession.shared.isBiometricEnabled()
    }
    
    //MARK: - IBACTIONS
    
    @IBAction func changeLanguagePressed(_ sender: UIButton) {
        
        if let changelang :LanguageSelectionVC =  LanguageSelectionVC.fromNib() {
            
            changelang.setLanguageSelectionAlert(MBLanguageManager.userSelectedLanguage()) { (newSelectedLanguage) in
                
                if MBLanguageManager.userSelectedLanguage() != newSelectedLanguage {
                    // Setting user language
                    MBLanguageManager.setUserLanguage(selectedLanguage: newSelectedLanguage)
                    
                    // Setting check to define user has changed language.
                    MBPICUserSession.shared.isLanguageChanged = true
                    
                    // Reloading layout Text accourding to seletcted language
                    self.layoutViewController()
                }
            }
            
            self.presentPOPUP(changelang, animated: true, completion:  nil)
        }
    }
    
    @IBAction func notificationsTonePressed(_ sender: Any) {
        
        notificationSoundSetting(ringingType: .Tone)
    }
    
    @IBAction func vibratePressed(_ sender: UIButton) {
        
        notificationSoundSetting(ringingType: .Vibrate)
    }
    
    @IBAction func mutePressed(_ sender: UIButton) {
        
        notificationSoundSetting(ringingType: .Mute)
    }
    
    @IBAction func changePassword(_ sender: UIButton) {
        if let changePassword :ChangePasswordVC = ChangePasswordVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(changePassword, animated: true)
        }
    }
    
    //MARK: - Functions
    func layoutViewController() {
        titleLabel.text                      = Localized("Title_Settings")
        changeLanguageTitleLabel.text        = Localized("Title_ChangeLanguage")
        languageLabel.text                   = Localized("Info_Language")
        notificationTitleLabel.text          = Localized("Title_Notifications")
        notificationDiscriptionLabel.text    = Localized("Description_Notifications")
        notificationSettingTitleLabel.text   = Localized("Title_NotificationsSetting")
        notificationSettingTuneLabel.text    = Localized("Info_NotificationSettingTune")
        notificationSettingVibrateLabel.text = Localized("Info_NotificationSettingVibrate")
        notificationSettingMuteLabel.text    = Localized("Info_NotificationSettingMute")
        changePasswordTitleLabel.text        = Localized("Title_ChangePassword")
        biometricTitleLabel.text             = Localized("Title_Biometric")
        appVersionTitleLabel.text            = Localized("Title_AppVersion")
        appVersionLabel.text                 = UIDevice.appVersion()
    }
    
    func loadNotificationsConfigurations() {
        
        //let jsonString = "{\"ringingStatus\":\"tone\",\"isEnable\":\"1\"}"
        // Parssing response data
        if let responseHandler :AddFCM = AddFCM.loadFromUserDefaults(key: APIsType.notificationConfigurations.localizedAPIKey()){
            
            self.isNotificationEnabled = self.checkIsEnableFromString(string: responseHandler.isEnable)
            self.ringingType = Constants.MBNotificationSoundType(rawValue: responseHandler.ringingStatus) ?? .Tone
            
            // Set Notification ON/Off setting
            self.changeNotificationSwitchLayout(isEnable: self.isNotificationEnabled)
            // Set  Notification Sound setting
            self.notificationSoundSettingLayout(ringingType: self.ringingType)
        } else {
            // Set Notification ON/Off setting
            self.changeNotificationSwitchLayout(isEnable: false)
            // Set  Notification Sound setting
            self.notificationSoundSettingLayout(ringingType: .Tone)
            
            self.showActivityIndicator()
        }
        
        getNotificationSettings()
    }
    
    @objc func notificationSwitchValueChanged() {
        
        changeNotificationState(isEnable: notificationsSwitch.isOn, ringingStatus: ringingType)
        changeNotificationSwitchLayout(isEnable: notificationsSwitch.isOn)
        
    }
    
    @objc func biometricSwitchValueChanged() {
        UserDefaults.saveBoolForKey(boolValue: biometricSwitch.isOn, forKey: Constants.K_IsBiometricEnabled)
    }
    
    func changeNotificationSwitchLayout(isEnable: Bool) {
        
        if isEnable {
            
            notificationsSwitch.isOn = true
            notificationTitleLabel.isEnabled = true
            notificationDiscriptionLabel.isEnabled = true
            
            notificationSettingTuneLabel.isEnabled = true
            notificationSettingVibrateLabel.isEnabled = true
            notificationSettingMuteLabel.isEnabled = true
            
            notificationsVibrateButton.isEnabled = true
            notificationsTuneButton.isEnabled = true
            notificationsMuteButton.isEnabled = true
            
        } else {
            
            notificationsSwitch.isOn = false
            
            notificationSettingTuneLabel.isEnabled = false
            notificationSettingVibrateLabel.isEnabled = false
            notificationSettingMuteLabel.isEnabled = false
            
            
            notificationsVibrateButton.isEnabled = false
            notificationsTuneButton.isEnabled = false
            notificationsMuteButton.isEnabled = false
        }
    }
    
    func notificationSoundSetting(ringingType: Constants.MBNotificationSoundType) {
        
        // Check if user can chage notification sound type
        if !isNotificationEnabled {
            return
        } else {
            
            notificationSoundSettingLayout(ringingType: ringingType)
            changeNotificationState(isEnable: isNotificationEnabled, ringingStatus: ringingType)
        }
    }
    
    func notificationSoundSettingLayout(ringingType: Constants.MBNotificationSoundType) {
        
        notificationsTuneButton .setImage(#imageLiteral(resourceName: "checkbox_state2"), for: UIControl.State.normal)
        notificationsVibrateButton .setImage(#imageLiteral(resourceName: "checkbox_state2"), for: UIControl.State.normal)
        notificationsMuteButton .setImage(#imageLiteral(resourceName: "checkbox_state2"), for: UIControl.State.normal)
        
        switch ringingType {
            
        case .Tone:
            notificationsTuneButton .setImage(#imageLiteral(resourceName: "checkbox_state1"), for: UIControl.State.normal)
        case .Vibrate:
            notificationsVibrateButton .setImage(#imageLiteral(resourceName: "checkbox_state1"), for: UIControl.State.normal)
        case .Mute:
            notificationsMuteButton .setImage(#imageLiteral(resourceName: "checkbox_state1"), for: UIControl.State.normal)
        }
    }
    
    
    func checkIsEnableFromString(string: String) -> Bool {
        
        if string == "1" {
            return true
        } else {
            return false
        }
    }
    
    
    //MARK: - API Calls
    /// Call 'changeNotificationState' API .
    ///
    /// - returns: Void
    
    func changeNotificationState(isEnable : Bool, ringingStatus: Constants.MBNotificationSoundType) {
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.addFCMId(fcmKey: Messaging.messaging().fcmToken ?? "", ringingStatus: ringingStatus ,isEnable: isEnable, isFromLogin: false, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let addFCMResponseHandler = Mapper<AddFCM>().map(JSONObject:resultData) {
                        
                        // Save Notification configration
                        addFCMResponseHandler.saveInUserDefaults(key: APIsType.notificationConfigurations.localizedAPIKey())
                        
                        // Updating selected Values
                        self.isNotificationEnabled = self.checkIsEnableFromString(string: addFCMResponseHandler.isEnable)
                        self.ringingType = Constants.MBNotificationSoundType(rawValue: addFCMResponseHandler.ringingStatus) ?? .Tone
                        
                        // Showing alert
                        self.showSuccessAlertWithMessage(message: resultDesc, {
                            // Check if user enable or disabled notification swiftch
                            if self.isNotificationEnabled {
                                // Check for Notification setting
                                self.isUserEnabledNotification(completionHandler: { (isEnabled) in
                                    
                                    if isEnabled != true {
                                        self.showAccessAlert(message: Localized("Message_EnableRemoteNotification"))
                                    }
                                })
                            }
                        })
                        
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                } else {
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
            
            // Will change Notification ON/Off setting in case of failure
            self.changeNotificationSwitchLayout(isEnable: self.isNotificationEnabled)
            // Will change Notification Sound setting in case of failure
            self.notificationSoundSettingLayout(ringingType: self.ringingType)
        })
    }
    
    func getNotificationSettings() {
        
        _ = MBAPIClient.sharedClient.addFCMId(fcmKey: Messaging.messaging().fcmToken ?? "", ringingStatus: self.ringingType, isEnable: true, isFromLogin: true, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    let extractedExpr: Mapper<AddFCM> = Mapper<AddFCM>()
                    if let addFCMResponseHandler = extractedExpr.map(JSONObject:resultData) {
                        
                        // Save Notification configration
                        addFCMResponseHandler.saveInUserDefaults(key: APIsType.notificationConfigurations.localizedAPIKey())
                        // Updating selected Values
                        self.isNotificationEnabled = self.checkIsEnableFromString(string: addFCMResponseHandler.isEnable)
                        self.ringingType = Constants.MBNotificationSoundType(rawValue: addFCMResponseHandler.ringingStatus) ?? .Tone
                    } else {
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                    
                } else {
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}
