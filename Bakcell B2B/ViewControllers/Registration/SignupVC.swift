//
//  SignupVC.swift
//  Bakcell
//
//  Created by Shujat on 16/05/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ForgotVC: BaseVC {
    
    //MARK:- Properties
    
    //MARK:- IBOutlet
    @IBOutlet var userNameTextField: UITextField!
    @IBOutlet var accountLabel: UILabel!
    @IBOutlet var lineView: UIView!
    
    @IBOutlet var nextButtonView: UIView!
    @IBOutlet var sentPinLabel: UILabel!
    @IBOutlet var nextButtonTitleLabel: UILabel!
    @IBOutlet var signUpButtonTitleLabel: UILabel!
    @IBOutlet var backcellImageView: UIImageView!
    
    //MARK:- ViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userNameTextField.delegate = self
        
        //  Disabling
        //  setNextButtonSelection(buttonView: nextButtonView, isEnabled: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadViewLayout()
    }
    
    //MARK:- IBACTIONS
    @IBAction func nextPressed(_ sender: Any) {
        
        var userName : String = ""
        // UserName validation
        if let userNameText = userNameTextField.text {
            
            // UserName count validation
            if userNameText.isValidEmail() {
                userName = userNameText
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_EnterValidUserName"))
                return
            }
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidUserName"))
            return
        }
        
        // TextField resignFirstResponder
        userNameTextField.resignFirstResponder()
        
        //API Call
        signUpUserAPICall(userName)
    }
    
    @IBAction func signInPressed(_ sender: UIButton) {
        
        self.backPressed(sender)
    }
    
    //MARK: - FUNCTIONS
    
    /**
     Set localized text in viewController
     */
    func loadViewLayout(){
        sentPinLabel.font = UIFont.mbArial(fontSize: 14)
        signUpButtonTitleLabel.font = UIFont.mbArialBold(fontSize: 16)
        nextButtonTitleLabel.font = UIFont.mbArialBold(fontSize: 16)
        accountLabel.font = UIFont.mbArial(fontSize: 14)
        
        userNameTextField.placeholder = Localized("Placeholder_UserName")
        
        sentPinLabel.text = Localized("Info_PinSend")
        nextButtonTitleLabel.text = Localized("BtnTitle_NEXT")
        signUpButtonTitleLabel.text = Localized("Title_SignIn")
        accountLabel.text = Localized("Info_AlreadyHaveAcount")
        
        backcellImageView.image = UIImage(named: Localized("Img_BakcelLogo"))
    }
    
    
    //MARK: - API Calls
    
    /**
     Call 'signup' API with the specified `MSISDN`.
     
     - parameter MSISDN: MSISDN of current user.
     
     - returns: void
     */
    func signUpUserAPICall(_ userName: String) {
        
        // Redirecting user to Pin verification screen.
        if let pinVerify = self.myStoryBoard.instantiateViewController(withIdentifier: "SignUpPINVerifyVC") as? SignUpPINVerifyVC {
            pinVerify.userName = userName
            self.navigationController?.pushViewController(pinVerify, animated: true)
        }
    }
}

//MARK: - Textfield delagates

extension ForgotVC : UITextFieldDelegate {
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //  guard let text = textField.text else { return false } // Check that if text is nill then return false
        
        //  let newLength = text.count + string.count - range.length
        
        if textField == userNameTextField {
            return true
            
        } else  {
            return false
        }
    }
}
