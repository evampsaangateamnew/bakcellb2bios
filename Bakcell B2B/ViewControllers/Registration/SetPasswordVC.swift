//
//  SetPasswordVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/6/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper

class SetPasswordVC: BaseVC {
    
    //MARK:- Properties
    var isInfoTipViewShowing = false
    var userName : String = ""
    var verifiedOTP : String = ""
    
    //MARK:- IBOutlet
    
    @IBOutlet var backcellImageView: UIImageView!
    
    @IBOutlet var passwordTextField: MBTextField!
    @IBOutlet var confirmPasswordTextField: MBTextField!
    @IBOutlet var infoTipView: UIView!
    @IBOutlet var infoTipLabel: MBLabel!
    
    @IBOutlet var passwordStrengthView : UIView!
    @IBOutlet var passwordStrengthInfoLabel: MBLabel!
    @IBOutlet var passwordStrengthProgressBar : UIProgressView!
    @IBOutlet var spaceBetweenPasswordViewsConstraint: NSLayoutConstraint!
    
    @IBOutlet var signUpView: UIView!
    @IBOutlet var updateButtonTitleLabel: MBLabel!
    @IBOutlet var signupButton: MBButton!
    
    
    //MARK:- viewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        // Disabling SignUP/Update button
        signUpView.setNextButtonSelection(isEnabled: false)
        
        setPasswordStrenghtBar(Text: "")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewLayout()
        
    }
    
    //MARK: - Functions
    
    /**
     Set localized text in viewController
     */
    func loadViewLayout(){
        
        updateButtonTitleLabel.text = Localized("BtnTitle_Update")
        
        infoTipLabel.text = Localized("Password_ErrorInfo")
        
        passwordTextField.placeholder = Localized("SetPassword_PlaceHolder")
        confirmPasswordTextField.placeholder = Localized("ConfirmPassword_PlaceHolder")
        
        backcellImageView.image = UIImage.imageFor(name: Localized("Img_BakcelLogo"))
        
    }
    
    /**
     Set password strenght bar.
     
     - parameter text: text of textfield.
     
     - returns: void
     */
    func setPasswordStrenghtBar(Text text:String) {
        
        let passwordStrength:Constants.MBPasswordStrength = MBUtilities.determinePasswordStrength(Text: text)
        
        switch passwordStrength {
        case Constants.MBPasswordStrength.didNotMatchCriteria:
            passwordStrengthView.isHidden = true
            passwordStrengthInfoLabel.text = ""
            passwordStrengthProgressBar.setProgress(0, animated: false)
            passwordStrengthProgressBar.tintColor = UIColor.clear
            spaceBetweenPasswordViewsConstraint.constant = 20
            break
            
        case Constants.MBPasswordStrength.Week:
            spaceBetweenPasswordViewsConstraint.constant = 33
            passwordStrengthView.isHidden = false
            passwordStrengthInfoLabel.isHidden = false
            passwordStrengthInfoLabel.text = Localized("Pass_Weak")
            
            passwordStrengthProgressBar.tintColor = UIColor.mbBarRed
            passwordStrengthProgressBar.setProgress(0.33, animated: false)
            break
            
        case Constants.MBPasswordStrength.Medium:
            spaceBetweenPasswordViewsConstraint.constant = 33
            passwordStrengthView.isHidden = false
            passwordStrengthInfoLabel.isHidden = false
            passwordStrengthInfoLabel.text = Localized("Pass_Medium")
            passwordStrengthProgressBar.tintColor = UIColor.mbBarOrange
            passwordStrengthProgressBar.setProgress(0.66, animated: false)
            break
            
        case Constants.MBPasswordStrength.Strong:
            spaceBetweenPasswordViewsConstraint.constant = 33
            passwordStrengthView.isHidden = false
            passwordStrengthInfoLabel.isHidden = false
            passwordStrengthInfoLabel.text = Localized("Pass_Strong")
            passwordStrengthProgressBar.tintColor = UIColor.mbBarGreen
            passwordStrengthProgressBar.setProgress(1, animated: false)
        }
        
        self.view.setNeedsUpdateConstraints()
    }
    
    /**
     Redirect user to Dashboard.
     - returns: void
     */
    func redirectUserToHome() {
        
        // Redirecting user to dashboard screen.
        if let mainViewController :TabbarController = TabbarController.instantiateViewControllerFromStoryboard() {
            
            if let drawerViewController :SideDrawerVC = SideDrawerVC.instantiateViewControllerFromStoryboard() {
                let drawerController = KYDrawerController(drawerDirection: .left,drawerWidth: ((UIScreen.main.bounds.width/3)*2.5))
                drawerController.mainViewController = mainViewController
                drawerController.drawerViewController = drawerViewController
                self.navigationController?.pushViewController(drawerController, animated: true)
            }
        }
    }
    
    //MARK:- IBACTIONS
    
    @IBAction func signUpPressed(_ sender: Any) {
        
        var password : String = ""
        var confirmPassword : String = ""
        
        // Password validation
        if let passwordText = passwordTextField.text {
            
            let passwordStrength:Constants.MBPasswordStrength = MBUtilities.determinePasswordStrength(Text: passwordText)
            
            switch passwordStrength {
                
            case Constants.MBPasswordStrength.didNotMatchCriteria :
                
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidPasswordComplexity"))
                return
                
            case Constants.MBPasswordStrength.Week, Constants.MBPasswordStrength.Medium, Constants.MBPasswordStrength.Strong :
                
                password = passwordText
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidPassword"))
            return
        }
        
        // Confirm Password validation
        if let confirmPasswordText = confirmPasswordTextField.text,
            confirmPasswordText == password {
            
            confirmPassword = confirmPasswordText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidConfirmPassword"))
            return
        }
        
        if MBPICUserSession.shared.aSecretKey.isBlank == false {
            
            // TextField resignFirstResponder
            _ = passwordTextField.resignFirstResponder()
            _ = confirmPasswordTextField.resignFirstResponder()
            
            forgotPasswordAPICall(self.userName, Password: password.aesEncrypt(key: MBPICUserSession.shared.aSecretKey), ConfirmPassword: confirmPassword.aesEncrypt(key: MBPICUserSession.shared.aSecretKey), OTP: verifiedOTP)
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_GenralError"))
        }
        
    }
    
    @IBAction func infoPressed(_ sender: UIButton) {
        
        if isInfoTipViewShowing == false {
            infoTipView.isHidden = false
            isInfoTipViewShowing = true
        } else {
            infoTipView.isHidden = true
            isInfoTipViewShowing = false
        }
    }
 
    
    //MARK: - API Calls
    
    /**
     Call 'forgotPassword' API with the specified `Password` and `Conform Password`.
     
     - parameter Password:     The Password.
     - parameter ConformPassword:  The Conform Password.
     
     - returns: void
     */
    func forgotPasswordAPICall(_ userName:String, Password password : String , ConfirmPassword confirmPassword : String, OTP pin: String) {
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.forgotPassword(userName, Password: password, ConfirmPassword: password, OTP: pin ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let forgotPasswordHandler = Mapper<LoginData>().map(JSONObject:resultData) {
                        
                        MBUtilities.saveCustomerDataInUserDefaults(loggedInUserMSISDN: userName, picCustomerData: forgotPasswordHandler.picUserInformation)
                        
                        /* Save user loggeding time*/
                        MBUtilities.updateLoginTime()
                        
                        // Calling FCM API to add new user FCM id
                        UserDefaults.saveBoolForKey(boolValue: false, forKey: Constants.kIsFCMIdAdded)
                        UserDefaults.saveBoolForKey(boolValue: true, forKey: Constants.kIsFCMIdAddRequestFromLogin)
                        BaseVC.addFCMId(isFromLogin: true)
                        
                        // Redirecting user to dashboard screen.
                        self.redirectUserToHome()
                        
                        UserDefaults.saveBoolForKey(boolValue: false, forKey: Constants.K_IsBiometricEnabled)
                    }
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: Textfield delagates
extension SetPasswordVC : UITextFieldDelegate {
    
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newLength = text.count + string.count - range.length
        
        if textField == passwordTextField {
            
            // Allow charactors check
            let allowedCharactors = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharacters
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            let validLimit : Bool = newLength <= Constants.passwordLength
            
            if validLimit {
                
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
                setPasswordStrenghtBar(Text: newString)
                
                let passwordStrength:Constants.MBPasswordStrength = MBUtilities.determinePasswordStrength(Text: newString)
                
                if (passwordStrength == .Week || passwordStrength == .Medium || passwordStrength == .Strong)  {
                    
                    signUpView.setNextButtonSelection(isEnabled: true)
                    
                } else {
                    signUpView.setNextButtonSelection(isEnabled: false)
                }
            }
            
            return validLimit // Bool
        } else {
            // For other then setPassword textfield.
            return newLength <= Constants.passwordLength // Bool
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        if textField == passwordTextField {
            setPasswordStrenghtBar(Text:"")
            signUpView.setNextButtonSelection(isEnabled: false)
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if textField == passwordTextField {
            setPasswordStrenghtBar(Text:"")
            signUpView.setNextButtonSelection(isEnabled: false)
        }
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isInfoTipViewShowing {
            self.infoPressed(UIButton())
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            confirmPasswordTextField.becomeFirstResponder()
            return false
        }
        else if textField == confirmPasswordTextField {
            _ = textField.becomeFirstResponder()
            signUpPressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
