//
//  LoginVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/06/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import KYDrawerController
import ObjectMapper
import LocalAuthentication

class LoginVC: BaseVC {
    
    
    //MARK: - IBOutlet
    @IBOutlet var backcellImageView: UIImageView!
    @IBOutlet var userNameTextField: MBTextField!
    @IBOutlet var passwordTextField: MBTextField!
    @IBOutlet var forgotButton: UIButton!
    @IBOutlet var loginButtonTitleLabel: MBLabel!
    @IBOutlet var languageTitleLabel: UILabel!
    
    
    //MARK:- ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting UITextField delegate to self
        userNameTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop(false)
        
        loadViewLayout()
        
        #if DEBUG
        userNameTextField.text  = ""
        passwordTextField.text  = ""
//        userNameTextField.text  = "pic_new1"
//        passwordTextField.text  = "123456"
        #else
        userNameTextField.text  = ""
        passwordTextField.text  = ""
        #endif
        
        self.removeAllActivityIndicator()
    }
    
    
    
    //MARK: - FUNCTIONS
    
    /**
     Set localized text in viewController
     */
    func loadViewLayout() {
        backcellImageView.image = UIImage.imageFor(name: Localized("Img_BakcelLogo"))
        
        userNameTextField.placeholder = Localized("Placeholder_UserName")
        passwordTextField.placeholder = Localized("Placeholder_Password")
        forgotButton.setTitle(Localized("Title_Forgot"), for: UIControl.State.normal)
        loginButtonTitleLabel.text = Localized("Title_Login")
        
        languageTitleLabel.text = Localized("Info_Language_Short")
        
    }
    
    
    //MARK: - API Calls
    
    /**
     Call 'authenticateUser' API with the specified `MSISDN` and `Password`.
     
     - parameter MSISDN: MSISDN of current user.
     - parameter pasword: pasword of current user.
     
     - returns: void
     */
    func authenticateUserAPICall(_ msisdn : String , Password pasword : String){
        
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.gethomepage(msisdn , Password:pasword ,{ (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator(withAnimation: false)
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    
                    if let authanticateUser = Mapper<LoginData>().map(JSONObject: resultData) {
                        
                        MBUtilities.saveCustomerDataInUserDefaults(loggedInUserMSISDN: msisdn, picCustomerData: authanticateUser.picUserInformation)
                       /* Save user loggeding time*/
                        MBUtilities.updateLoginTime()
                        
                        // Calling FCM API to add new user FCM id
                        UserDefaults.saveBoolForKey(boolValue: false, forKey: Constants.kIsFCMIdAdded)
                        UserDefaults.saveBoolForKey(boolValue: true, forKey: Constants.kIsFCMIdAddRequestFromLogin)
                        BaseVC.addFCMId(isFromLogin: true)
                        
                        UserDefaults.saveBoolForKey(boolValue: false, forKey: Constants.K_IsBiometricEnabled)
                        
                        self.redirectUserToDashBoard()
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
    
    ///Redirect User to Dashboard
    func redirectUserToDashBoard()  {
        if let mainViewController :TabbarController = TabbarController.instantiateViewControllerFromStoryboard() {
            
            if let drawerViewController :SideDrawerVC = SideDrawerVC.instantiateViewControllerFromStoryboard() {
                let drawerController = KYDrawerController(drawerDirection: .left,drawerWidth: ((UIScreen.main.bounds.width/3)*2.5))
                drawerController.mainViewController = mainViewController
                drawerController.drawerViewController = drawerViewController
                self.navigationController?.pushViewController(drawerController, animated: true)
            }
        }
    }
    
    //MARK: - IBACTIONS
    @IBAction func changeLanguagePressed(_ sender:UIButton) {
        if let changelang :LanguageSelectionVC =  LanguageSelectionVC.fromNib() {
            
            changelang.setLanguageSelectionAlert(MBLanguageManager.userSelectedLanguage()) { (newSelectedLanguage) in
                
                if MBLanguageManager.userSelectedLanguage() != newSelectedLanguage {
                    // Setting user language
                    MBLanguageManager.setUserLanguage(selectedLanguage: newSelectedLanguage)
                    
                    // Setting check to define user has changed language.
                    MBPICUserSession.shared.isLanguageChanged = true
                    
                    // Reloading layout Text accourding to seletcted language
                    self.loadViewLayout()
                }
            }
            
            self.presentPOPUP(changelang, animated: true, completion:  nil)
        }
    }
    
    @IBAction func forgotPasswordPressed(_ sender:AnyObject) {
        _ = userNameTextField.resignFirstResponder()
        _ = passwordTextField.resignFirstResponder()
        
        if let signUp :ForgotPasswordVC = ForgotPasswordVC.instantiateViewControllerFromStoryboard() {
            self.navigationController?.pushViewController(signUp, animated: true)
        }
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {
        
        var userName : String = ""
        var password : String = ""
        
        // UserName validation
        if let userNameText = userNameTextField.text,
             userNameText.isValidUserName() {
          
            userName = userNameText
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidUserName"))
            return
        }
        
        // Password validation
        if let passwordText = passwordTextField.text {
            
            let passwordStrength:Constants.MBPasswordStrength = MBUtilities.determinePasswordStrength(Text: passwordText)
            
            switch passwordStrength {
                
            case Constants.MBPasswordStrength.didNotMatchCriteria:
                
                self.showErrorAlertWithMessage(message: Localized("Message_InvalidPassword"))
                return
            case Constants.MBPasswordStrength.Week, Constants.MBPasswordStrength.Medium, Constants.MBPasswordStrength.Strong:
                password = passwordText
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidPassword"))
            return
        }
        
        _ = userNameTextField.resignFirstResponder()
        _ = passwordTextField.resignFirstResponder()
        // API call for user authentication
        authenticateUserAPICall(userName, Password: password)
        
    }
    
}

//MARK: - Textfield delagates
extension LoginVC : UITextFieldDelegate {
    
    ///Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false } // Check that if text is nil then return false
        
        // Length of text in UITextField
        let newLength = text.count + string.count - range.length
        
        // Check for MobileNumber textField
        if textField == userNameTextField {
            
            // Allow charactors check
            let allowedCharactors = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharactersForPIC
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            return newLength <= Constants.maxUserLength /* Restriction for User length */
        } else if textField == passwordTextField {
            return newLength <= Constants.passwordLength /* Restriction for password length */
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userNameTextField {
            passwordTextField.becomeFirstResponder()
            return false
        } else if textField == passwordTextField {
            _ = textField.resignFirstResponder()
            loginPressed(UIButton())
            return false
        } else {
            return true
        }
    }
}

