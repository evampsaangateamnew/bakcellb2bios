//
//  ForgotPasswordVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/06/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ForgotPasswordVC: BaseVC {
    
    
    //MARK:- IBOutlet
    @IBOutlet var userNameTextField: MBTextField!
    @IBOutlet var accountLabel: MBLabel!
    @IBOutlet var lineView: UIView!
    
    @IBOutlet var nextButtonView: UIView!
    @IBOutlet var sentPinLabel: MBLabel!
    @IBOutlet var nextButtonTitleLabel: MBLabel!
    @IBOutlet var signUpButtonTitleLabel: MBLabel!
    @IBOutlet var backcellImageView: UIImageView!
    
    //MARK:- ViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userNameTextField.delegate = self
        
        //  Disabling
        nextButtonView.setNextButtonSelection(isEnabled: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        loadViewLayout()
    }
    
    
    //MARK: - FUNCTIONS
    
    /**
     Set localized text in viewController
     */
    func loadViewLayout(){
        
        userNameTextField.placeholder = Localized("Placeholder_UserName")
        
        sentPinLabel.text = Localized("Info_PinSend")
        nextButtonTitleLabel.text = Localized("BtnTitle_NEXT")
        signUpButtonTitleLabel.text = Localized("Title_SignIn")
        accountLabel.text = Localized("Info_AlreadyHaveAcount")
        
        backcellImageView.image = UIImage.imageFor(name: Localized("Img_BakcelLogo"))
    }
    
    //MARK:- IBACTIONS
    @IBAction func nextPressed(_ sender: Any) {
        
        var userName : String = ""
        // UserName validation
        if let userNameText = userNameTextField.text,
            userNameText.isValidUserName() {
            userName = userNameText
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_EnterValidUserName"))
            return
        }
        
        // TextField resignFirstResponder
        _ = userNameTextField.resignFirstResponder()
        
        //API Call
        verifyUserAndSendPin(userName)
    }
    
    @IBAction func signInPressed(_ sender: UIButton) {
        
        self.backPressed(sender)
    }
    //MARK: - API Calls
    
    /**
     Call 'sendPin' API for logged in user 'MSISDN'.
     
     - returns: void
     */
    func verifyUserAndSendPin(_ userName: String) {
        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.sendPinForSelectedUser(UserName: userName , { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            self.hideActivityIndicator()
            
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    if let sendPinInfo = Mapper<SendPin>().map(JSONObject: resultData){
                        // Redirecting user to Pin verification screen.
                        if let pinVerify :ForgotPasswordPINVerifyVC = ForgotPasswordPINVerifyVC.instantiateViewControllerFromStoryboard() {
                            pinVerify.userName = userName
                            pinVerify.channelViaPinSent = sendPinInfo.channel ?? ""
                            self.navigationController?.pushViewController(pinVerify, animated: true)
                        }
                    }
                    
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }
}

//MARK: - Textfield delagates

extension ForgotPasswordVC : UITextFieldDelegate {
    //Set max Limit for textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false } // Check that if text is nill then return false
        let newLength = text.count + string.count - range.length
        
        if textField == userNameTextField {
            // Allow charactors check
            let allowedCharactors = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharactersForPIC
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
            let characterSet = CharacterSet(charactersIn: string)
            
            if allowedCharacters.isSuperset(of: characterSet) != true {
                
                return false
            }
            
            nextButtonView.setNextButtonSelection(isEnabled: true)
            
            return newLength <= Constants.maxUserLength /* Restriction for User length */
            
        } else  {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userNameTextField {
            _ = textField.becomeFirstResponder()
            nextPressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
