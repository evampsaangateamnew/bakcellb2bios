//
//  ForgotPasswordPINVerifyVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 21/6/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class ForgotPasswordPINVerifyVC: BaseVC {

    //MARK:- Properties
    var userName : String = ""
    var channelViaPinSent : String  = ""
    //MARK:- IBOutlet
    @IBOutlet var pinTextField: MBTextField!

    @IBOutlet var nextButtonView: UIView!
    @IBOutlet var backButtonView: UIView!
    @IBOutlet var pinSentMessageLable: MBLabel!
    @IBOutlet var backInfoLabel: MBLabel!
    @IBOutlet var backButtonTitleLabel: MBLabel!
    @IBOutlet var nextButtonTitleLabel: MBLabel!
    @IBOutlet var resendButtonTitleLabel: MBLabel!
    @IBOutlet var pinNotRecievedInfoLabel: MBLabel!
    @IBOutlet var backcellImageView: UIImageView!

    //MARK:- ViewCintroller Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        pinTextField.delegate = self

        //  Disabling Back and Next button
        backButtonView.setBackButtonSelection(isEnabled: false)
        nextButtonView.setNextButtonSelection(isEnabled: false)

        loadViewLayout()

        self.animatebackButtonEnablingCountDown()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        pinSentMessageLable.text = String(format: Localized("Title_SendPINViaMessage"), channelViaPinSent);
    }
    //MARK: - FUNCTIONS

    /**
     Set localized text in viewController
     */
    func loadViewLayout() {
        
        pinNotRecievedInfoLabel.text = Localized("Info_PinNotRecieved")
        resendButtonTitleLabel.text = Localized("BtnTitle_Resend")
        nextButtonTitleLabel.text = Localized("BtnTitle_NEXT")
        backButtonTitleLabel.text = Localized("BtnTitle_Back")
        backInfoLabel.text = String(format: Localized("VerifyPin_Description"), "\(Constants.PINVerifyBackTime)")
        pinTextField.placeholder = Localized("EnterPin_PlaceHolder")

        backcellImageView.image = UIImage.imageFor(name:Localized("Img_BakcelLogo"))
    }

    /**
     Animate back button Enabling CountDown.

     - returns: void
     */
    func animatebackButtonEnablingCountDown() {

        let duration: Int = Constants.PINVerifyBackTime//seconds
        DispatchQueue.global().async {
            for i in (0...duration).reversed() {
                DispatchQueue.main.async {

                    self.backInfoLabel.text = String(format: Localized("VerifyPin_Description"), "\(i)")

                    if i <= 0 {
                        /* Enable Back button */
                        self.backButtonView.setBackButtonSelection(isEnabled: true)
                        self.interactivePop()
                    }
                }
                let sleepTime = UInt32(duration/duration * 1000000)
                usleep(sleepTime)
            }
        }
    }
    
    
    //MARK:- IBACTIONS
    @IBAction func nextPressed(_ sender: Any) {
        var otp : String = ""
        // OTP validation
        if let otpText = pinTextField.text,
            otpText.trimmWhiteSpace.count == Constants.OTPLength {
            
            otp = otpText.trimmWhiteSpace
           
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_InvalidOTP"))
            return
        }
        
        // TextField resignFirstResponder
        _ = pinTextField.resignFirstResponder()
        
        // API call for OTP verification
        verifyOTPAPICall(userName, OTP: otp)
    }
    
    @IBAction func reSendPinPressed(_ sender: Any) {
        
        // TextField resignFirstResponder
        _ = pinTextField.resignFirstResponder()
        
        // API call for resending OTP
        reSendPin(userName)
    }
    

    //MARK: - API Calls

    /**
     Call 'verifyOTP' API with the specified `MSISDN` and `OTP`.

     - parameter MSISDN: MSISDN of current user.
     - parameter OTP:  The One Time PIN user entered.
     - parameter isSignUpRequest:  set true in case of signup and set false in case of forgot.

     - returns: void
     */
    func verifyOTPAPICall(_ userName : String , OTP pin : String) {

        self.showActivityIndicator()
        
        _ = MBAPIClient.sharedClient.verifyPin(UserName: userName, RequestType: .ForgotPassword, Pin: pin.trimmWhiteSpace, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    
                    if let _ = Mapper<MessageResponse>().map(JSONObject: resultData) {
                        
                        // Redirecting user to Set Password screen.
                        if let setPassword :SetPasswordVC = SetPasswordVC.instantiateViewControllerFromStoryboard() {
                            
                            setPassword.userName = self.userName
                            setPassword.verifiedOTP = pin.aesEncrypt(key: MBPICUserSession.shared.aSecretKey)
                            self.navigationController?.pushViewController(setPassword, animated: true)
                        }
                        
                    } else {
                        
                        // Show error alert to user
                        self.showErrorAlertWithMessage(message: resultDesc)
                    }
                }
            }
        })
    }
    
    /**
     Call 'reSendPin' API with the specified `MSISDN`

     - parameter Username: Username of current user.
     - parameter ofType:  type of pin.

     - returns: void
     */

    func reSendPin(_ userName : String ) {
        self.showActivityIndicator()
        _ = MBAPIClient.sharedClient.historyReSendPIN(UserName: userName, ofType: .ForgotPassword, { (response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
//                    pinSentMessageLable.text =
                    self.showSuccessAlertWithMessage(message: resultDesc)
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }

}


//MARK: - Textfield delagates

extension ForgotPasswordPINVerifyVC : UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }

        //Set max Limit for textfields
        let newLength = text.count + string.count - range.length

        //  OTP textfield
        if textField == pinTextField {

            // limiting OTP lenght to 4 digits
            let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
            let characterSet = CharacterSet(charactersIn: string)
            
            if newLength <= Constants.OTPLength && allowedCharacters.isSuperset(of: characterSet) {
                // Next button enable disable
                if newLength == Constants.OTPLength {
                    nextButtonView.setNextButtonSelection(isEnabled: true)
                } else {
                   nextButtonView.setNextButtonSelection(isEnabled: false)
                }
                return  true
            } else {
                return  false
            }
        } else  {
            return false
        }
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
       nextButtonView.setNextButtonSelection(isEnabled: false)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == pinTextField {
            _ = textField.becomeFirstResponder()
            nextPressed(UIButton())
            return false
        } else {
            return true
        }
    }
}
