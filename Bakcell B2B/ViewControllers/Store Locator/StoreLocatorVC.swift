//
//  StoreLocatorVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import ObjectMapper

class StoreLocatorVC: BaseVC {

    enum MBStoreLocatorType : String {
        case StoreLocator = "Store locator"
        case StoreList = "Store list"
    }
    
    //MARK: - Properties
    var storeLocatorData : StoreLocatorHandler?
    var selectedType : MBStoreLocatorType = MBStoreLocatorType.StoreLocator
    var storeListVC :  StoreListVC?
    var storeMapVC :  StoreMapVC?
    var shouldFocuseToSelectedLocation : Bool = false
    var selectedLocationForFocuse : [String: String]? = [:]

    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet var storeMapButton: MBButton!
    @IBOutlet var storeListButton: MBButton!
    
    //MARK: - ViewControllers methods
    override func viewDidLoad() {
        super.viewDidLoad()

        if selectedType == MBStoreLocatorType.StoreLocator {
            self.StoreLocatorPressed(storeMapButton)
        } else {
            self.StoreListPressed(storeListButton)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.interactivePop()
        
        layoutViewController()
        
        
    }
    
    //MARK:- Functions
    
    ///Set Localized for labels
    func layoutViewController(){
        titleLabel.text = Localized("Title_StoreLocator")
        storeMapButton .setTitle(Localized("StoreLocator_buttonTitle"), for: UIControl.State.normal)
        storeListButton .setTitle(Localized("StoreList_buttonTitle"), for: UIControl.State.normal)
        
    }

    func focusUserToSelectedLocation(selectedLocationDetail : [String:String]?) {


        // Set True to focuse to specifice location
        self.shouldFocuseToSelectedLocation = true
        self.selectedLocationForFocuse = selectedLocationDetail

        // Load MapView
        StoreLocatorPressed(storeMapButton)
    }

    /**
     Add child view controller to parent view.
     
     - Conditions:
     
        1. if storeLocatorData != nil
            1. StoreMapVC: if selectedType == StoreLocator
            2. StoreListVC: if selectedType == StoreList
        2. GetStoresList
     
     
     - returns: void
     */
    func reDirectUserToSelectedScreen() {
        
        if storeLocatorData != nil {
            
            mainView.hideDescriptionView()
            
            if selectedType == .StoreLocator {

                self.removeChildViewController(childController: StoreListVC())

                if (storeMapVC == nil) {
                    storeMapVC = StoreMapVC.instantiateViewControllerFromStoryboard()
                }
                storeMapVC?.storeLocator = storeLocatorData

                storeMapVC?.shouldFocuseToSelectedLocation = self.shouldFocuseToSelectedLocation
                storeMapVC?.selectedLocationForFocuse = self.selectedLocationForFocuse

                self.addChildViewController(childController: storeMapVC ?? StoreMapVC(), onView: mainView)

                // Clear selected location once map loaded.
                self.shouldFocuseToSelectedLocation = false
                self.selectedLocationForFocuse = [:]


            } else {

                self.removeChildViewController(childController: StoreMapVC())
                
                if storeListVC == nil {
                    storeListVC = StoreListVC.instantiateViewControllerFromStoryboard()
                }

                storeListVC?.storeLocatorData = storeLocatorData

                self.addChildViewController(childController: storeListVC ?? StoreListVC(), onView: mainView)
                
            }
        } else {
            getStoresLocationDetails()
            mainView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
        }
        
    }
    
    //MARK:- IBACTIONS
    @IBAction func StoreLocatorPressed(_ sender: MBButton) {

        selectedType = MBStoreLocatorType.StoreLocator
        
        storeMapButton.setButtonLayoutType(.grayButton)
        storeListButton.setButtonLayoutType(.whiteButton)

        self.view.backgroundColor = UIColor.mbBackgroundGray
        self.reDirectUserToSelectedScreen()
    }

    @IBAction func StoreListPressed(_ sender: MBButton) {

        selectedType = MBStoreLocatorType.StoreList
        
        storeListButton.setButtonLayoutType(.grayButton)
        storeMapButton.setButtonLayoutType(.whiteButton)

        self.view.backgroundColor = UIColor.white
        self.reDirectUserToSelectedScreen()
    }
    
    
    //MARK: - APIs call

    /**
     Call 'storeDetails' API.
     
     - returns: void
     */
    func getStoresLocationDetails() {

        /*Loading initial data from UserDefaults*/
        //let jsonString = Constants.demoStoreJSONString

        if let storeLocatorResponse :StoreLocatorHandler = StoreLocatorHandler.loadFromUserDefaults(key: APIsType.storeDetails.localizedAPIKey()) {
            
            self.storeLocatorData = storeLocatorResponse
            self.reDirectUserToSelectedScreen()
            
        } else {
            self.showActivityIndicator()
        }
        
        _ = MBAPIClient.sharedClient.storeDetails({ ( response, resultData, error, isCancelled, status, resultCode, resultDesc) in
            
            self.hideActivityIndicator()
            if error != nil {
                error?.showServerErrorInViewController(self)
                
            } else {
                // handling data from API response.
                if resultCode == Constants.MBAPIStatusCode.succes.rawValue {
                    
                    // Parssing response data
                    if let storeLocatorResponse = Mapper<StoreLocatorHandler>().map(JSONObject:resultData) {

                        self.storeLocatorData = storeLocatorResponse

                        /*Save data into user defaults*/
                        storeLocatorResponse.saveInUserDefaults(key: APIsType.storeDetails.localizedAPIKey())
                        self.reDirectUserToSelectedScreen()
                    }
                    
                } else {
                    // Show error alert to user
                    self.showErrorAlertWithMessage(message: resultDesc)
                }
            }
        })
    }

}
