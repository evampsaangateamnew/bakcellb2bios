//
//  StoreListVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/7/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class StoreListVC: UIViewController {
    
    //MARK:- Properties
    var storeLocatorData : StoreLocatorHandler?
    var storeLocatorFilter : StoreLocatorHandler?
    
    var selectedCity : String = Localized("DropDown_All")
    var selectedType : String = Localized("DropDown_All")
    
    //MARK:- IBOutlet
    @IBOutlet var storeListDrop: UIDropDown!
    @IBOutlet var storeCenterDrop: UIDropDown!
    @IBOutlet var tableView: MBAccordionTableView!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storeLocatorFilter = storeLocatorData?.copy()
        
        tableView.allowMultipleSectionsOpen = true
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        
        
        setUPDropBox()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    
    
    //MARK: - FUNCTIONS
    
    ///Initialze / Setup dropdown
    func setUPDropBox() {
        
        // set storeCenterDrop
        
        storeCenterDrop.placeholder = Localized("DropDown_All")
        
        var optionsArray : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocatorData?.type {
            
            for center in selectedCenter{
                
                optionsArray.append(center)
            }
        }
        
        storeCenterDrop.dataSource = optionsArray
        
        storeCenterDrop.didSelectOption{ (index, option) in
            
            self.selectedType = option
            
            self.storeLocatorFilter?.stores = self.filterStoreBy(city: self.selectedCity, type: self.selectedType, stores: self.storeLocatorData?.stores?.copy())
            
            self.reloadTableViewData()
            
        }
        
        // set storeListDrop
        
        storeListDrop.placeholder = Localized("DropDown_All")
        
        var optionsArrayCity : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocatorData?.city {
            
            for center in selectedCenter{
                
                optionsArrayCity.append(center)
            }
        }
        
        storeListDrop.dataSource = optionsArrayCity
        
        storeListDrop.didSelectOption { (index, option) in
            
            self.selectedCity = option
            
            self.storeLocatorFilter?.stores = self.filterStoreBy(city: self.selectedCity, type: self.selectedType, stores: self.storeLocatorData?.stores?.copy())
            
            self.reloadTableViewData()
            
        }
    }
    
    ///Reload Table view data
    func reloadTableViewData() {
        if (storeLocatorFilter?.stores?.count ?? 0) <= 0 {
            self.tableView.showDescriptionViewWithImage(description: Localized("Message_NoDataAvalible"))
            
        } else {
            self.tableView.hideDescriptionView()
        }
        self.tableView.reloadData()
    }
    
    /**
     Filter Stores List.
     
     - parameter city: City name.
     - parameter type: Store type.
     - parameter stores: StoresList.
     
     - returns: stores
     */
    func filterStoreBy(city:String?, type:String?, stores : [Stores]? ) -> [Stores]? {
        
        guard let myType = type else {
            return nil
        }
        
        guard let myCity = city else {
            return nil
        }
        
        guard let StoreListDetails = stores else {
            return nil
        }
        
        if(myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == true && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == true) {
            
            return stores
            
        } else if(myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == false && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == false) {
            
            var  filterdStoreDetails : [Stores] = []
            
            for aStoreItem in StoreListDetails {
                
                if aStoreItem.city.lowercased() == myCity.lowercased() {
                    
                    if aStoreItem.type.lowercased() == myType.lowercased() {
                        
                        filterdStoreDetails.append(aStoreItem)
                    }
                }
            }
            return filterdStoreDetails
            
        } else if (myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == false && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == true) {
            let  filterdStoreDetails = StoreListDetails.filter() {
                if ($0 as Stores).city.lowercased() == myCity.lowercased() {
                    return true
                } else {
                    return false
                }
            }
            return filterdStoreDetails
            
        } else if (myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == true && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == false ) {
            let  filterdStoreDetails = StoreListDetails.filter() {
                if ($0 as Stores).type.lowercased() == myType.lowercased() {
                    return true
                } else {
                    return false
                }
                
            }
            return filterdStoreDetails
            
        } else {
            return stores
        }
        
    }
    
    //MARK:- IBACTIONS
    
    @objc func showLocationOnMap(sender:UIButton) {
        
        if let aStoreLocation = storeLocatorFilter?.stores?[sender.tag] {
            
            let cordinatesDict = ["lati": aStoreLocation.latitude,
                                  "longi" : aStoreLocation.longitude,
                                  "title": aStoreLocation.store_name,
                                  "address" : aStoreLocation.address,
                                  "type": aStoreLocation.type]
            
            if let parentController = self.parent as? StoreLocatorVC {
                
                parentController.focusUserToSelectedLocation(selectedLocationDetail: cordinatesDict)
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
            
        } else {
            
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
    }
    
    @objc func initiateCall(sender : UIButton) {
        dialNumber(number: sender.titleLabel?.text ?? "")
    }
    
}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate>
extension StoreListVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return storeLocatorFilter?.stores?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 74
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerView :StoreLocatorHeaderView = tableView.dequeueReusableHeaderFooterView() {
            
            headerView.locationButton.addTarget(self, action: #selector(showLocationOnMap), for: UIControl.Event.touchUpInside)
            
            headerView.setTileLabelText(storeLocatorFilter?.stores?[section], senterTag: section)
            
            return headerView
        }
        return MBAccordionTableViewHeaderView()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell :StoreLocatorExpandCell = tableView.dequeueReusableCell() {
            
            cell.setLayoutFor(aStore:  storeLocatorFilter?.stores?[indexPath.section])
            
            cell.call_btn1.addTarget(self, action: #selector(initiateCall(sender:)), for: UIControl.Event.touchUpInside)
            cell.call_btn2.addTarget(self, action: #selector(initiateCall(sender:)), for: UIControl.Event.touchUpInside)
            cell.call_btn3.addTarget(self, action: #selector(initiateCall(sender:)), for: UIControl.Event.touchUpInside)
            
            return cell
            
        } else {
            return UITableViewCell()
        }
    }
    
}

// MARK: - <MBAccordionTableViewDelegate>

extension StoreListVC : MBAccordionTableViewDelegate {
    
    func tableView(_ tableView: MBAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? StoreLocatorHeaderView {
            headerView.setExpandStatus(true)
            
            storeLocatorFilter?.stores?[section].isSectionExpanded = true
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        if let headerView = header as? StoreLocatorHeaderView {
            
            headerView.setExpandStatus(false)
            
            storeLocatorFilter?.stores?[section].isSectionExpanded = false
            
        }
    }
    
    func tableView(_ tableView: MBAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: ((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x), y: ((labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y))
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}
