//
//  StoreLocatorExpandCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class StoreLocatorExpandCell: UITableViewCell {

    //MARK: -  IBOutlets
    @IBOutlet var contactNumbersView: UIView!
    @IBOutlet var contactsNumberImageView: UIImageView!
    @IBOutlet var contactNumberViewHeight: NSLayoutConstraint!
    @IBOutlet var call_btn1: UIButton!
    @IBOutlet var call_btn2: UIButton!
    @IBOutlet var call_btn3: UIButton!
    
    @IBOutlet var timmingContainorView: UIView!
    @IBOutlet var timmingContainorViewHightConstraint: NSLayoutConstraint!
    
    //MARK: -  Functions
    func setLayoutFor(aStore:Stores?) {
        
        if let aStoreObject = aStore {
            
            
            timmingContainorView.subviews.forEach({ (aView) in
                
                if aView is TitleAndValueView {
                    aView.removeFromSuperview()
                }
            })
            
            var lastView : UIView?
            aStoreObject.timing?.forEach({ (aStoreOpenTiming) in
                let myTitleAndValueView: TitleAndValueView = TitleAndValueView.fromNib()
                
                myTitleAndValueView.translatesAutoresizingMaskIntoConstraints = false

                myTitleAndValueView.setTitleAndValue(aStoreOpenTiming.day, value: aStoreOpenTiming.timings)

                timmingContainorView.addSubview(myTitleAndValueView)
                
                if lastView == nil{

                    myTitleAndValueView.snp.makeConstraints { (make) -> Void in
                        make.top.equalTo(timmingContainorView.snp.top)
                        make.right.equalTo(timmingContainorView.snp.right)
                        make.left.equalTo(timmingContainorView.snp.left)
                        make.height.equalTo(22)
                    }

                } else {

                    myTitleAndValueView.snp.makeConstraints { (make) -> Void in
                        make.top.equalTo(lastView?.snp.bottom ?? timmingContainorView.snp.top)
                        make.right.equalTo(timmingContainorView.snp.right)
                        make.left.equalTo(timmingContainorView.snp.left)
                        make.height.equalTo(22)
                    }

                }

                lastView = myTitleAndValueView
            })
            
            
            if let count = aStoreObject.timing?.count{
                timmingContainorViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                timmingContainorViewHightConstraint.constant = 0.0
            }
            
            // Setting Contact numbers
            if let numbers = aStoreObject.contactNumbers {
                if numbers.isEmpty == false{
                    
                    contactNumbersView.isHidden = false
                    contactsNumberImageView.isHidden = false
                    contactNumberViewHeight.constant = 24
                    
                    
                    if numbers.count >= 1 {
                        call_btn1.setTitle(numbers[0], for: UIControl.State.normal)
                    }
                    
                    if numbers.count >= 2 {
                        call_btn2.setTitle(numbers[1], for: UIControl.State.normal)
                    }
                    
                    if numbers.count >= 3 {
                        call_btn3.setTitle(numbers[2], for: UIControl.State.normal)
                    }
                    
                } else {
                    contactNumbersView.isHidden = true
                    contactNumberViewHeight.constant = 0
                    contactsNumberImageView.isHidden = true
                }
                
            } else {
                contactNumbersView.isHidden = true
                contactNumberViewHeight.constant = 0
                contactsNumberImageView.isHidden = true
            }
            
        } else {
            
        }
    }
    
}
