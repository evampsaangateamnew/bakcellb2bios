//
//  StoreLocatorHeaderView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 6/8/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit


class StoreLocatorHeaderView: MBAccordionTableViewHeaderView {
    
    //MARK: - Properties
    var isViewSelected = false
    
    //MARK: - IBOutlets
    @IBOutlet var locationButton: UIButton!
    @IBOutlet var storeAddressLabel: MBLabel!
    @IBOutlet var storeNameLabel: MBLabel!
    @IBOutlet var expandCellImage: UIImageView!

    
    //MARK: - Functions
    func setTileLabelText(_ aStoreItem: Stores?, senterTag:Int) {
        if let aStoreLocation = aStoreItem {
            
            storeNameLabel.text = aStoreLocation.store_name
            storeAddressLabel.text = aStoreLocation.address
            
            locationButton.tag = senterTag
            
            locationButton.setImage(UIImage.markerIconImageFor(key: aStoreLocation.type, inSmallSize: true), for: UIControl.State.normal)
            
            // Check if section is expanded then set expaned image else set unexpended image
            setExpandStatus(aStoreLocation.isSectionExpanded )
            
        } else {
            storeNameLabel.text =  ""
            storeAddressLabel.text = ""
            locationButton.tag = 0
            
            locationButton.setImage(UIImage(), for: UIControl.State.normal)
            locationButton.removeTarget(nil, action: nil, for: .allEvents)
            setExpandStatus(false )
        }
        
    }
    
    func setExpandStatus(_ isExpanded : Bool) {
        
        if isExpanded == true {
            self.expandCellImage.image  = UIImage.imageFor(name: "minus_sign")
        } else {
            self.expandCellImage.image = UIImage.imageFor(name: "plus_sign")
        }
    }

}
