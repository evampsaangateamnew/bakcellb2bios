//
//  MapVC.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 7/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit
import GoogleMaps


class StoreMapVC: UIViewController, CLLocationManagerDelegate {
    
    //MARK: - Properties
    var storeLocator : StoreLocatorHandler?
    var storeLocatorFilter : StoreLocatorHandler?
    var locationManager = CLLocationManager()
    // var currentLocation: CLLocation?
    // let geocoder = GMSGeocoder()
    var selectedCity : String = Localized("DropDown_All")
    var selectedType : String = Localized("DropDown_All")
    
    var shouldFocuseToSelectedLocation : Bool = false
    var selectedLocationForFocuse : [String: String]? = [:]
    
    var threeNeariestStores : [Stores] = []
    
    //MARK: - IBOutlets
    @IBOutlet var thirdDistance: MBLabel!
    @IBOutlet var secondDistance: MBLabel!
    @IBOutlet var firstDistance: MBLabel!
    @IBOutlet var thirdNearest: MBMarqueeLabel!
    @IBOutlet var secondNearest: MBMarqueeLabel!
    @IBOutlet var nearestStore: MBLabel!
    @IBOutlet var distance: MBLabel!
    
    @IBOutlet var firstStoreButton: UIButton!
    @IBOutlet var secondStoreButton: UIButton!
    @IBOutlet var thiredStoreButton: UIButton!
    
    @IBOutlet var storeDropDown: UIDropDown!
    @IBOutlet var serviceCenterDrop: UIDropDown!
    @IBOutlet weak var googleMapView: GMSMapView!
    
    @IBOutlet weak var nearestStoreView: UIView!
    @IBOutlet var nearestStoreViewHeightConstranint: NSLayoutConstraint!
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        distance.setupMarqueeAnimation()
//        secondNearest_lbl.setupMarqueeAnimation()
//        thirdNearest_lbl.setupMarqueeAnimation()
        
        layoutViewController()
        
        googleMapView.isMyLocationEnabled = true
        googleMapView.settings.compassButton = true;
        googleMapView.settings.myLocationButton = true;
        
        self.storeLocatorFilter = storeLocator?.copy()
        
        setUPDropBox()
        SetUpMarker()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
            case .restricted, .denied:
                self.showAccessAlert(message: Localized("Message_EnableLocationAccess"))
            case .authorizedAlways, .authorizedWhenInUse, .notDetermined:
                loadCurrentLocation()
            default:
                break
            }
        } else {
            loadCurrentLocation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        focuseOnSelectedLocation()
    }
    
    deinit {
        if let locationManagerObject = self.locationManager as? CLLocationManager {
            locationManagerObject.stopUpdatingLocation()
        }
    }

    
    
    //MARK: - Functions
    
    ///Set localized for labels
    func layoutViewController(){
        nearestStore.text = Localized("NearestStore_Title")
    }
    
    func loadCurrentLocation() {
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    
    ///Initialze / Setup dropdown
    func setUPDropBox(){
        
        // set storeCenterDrop
        serviceCenterDrop.placeholder = Localized("DropDown_All")
        
        var optionsArray : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocator?.type {
            for center in selectedCenter{
                optionsArray.append(center)
            }
        }
        
        serviceCenterDrop.dataSource = optionsArray
        
        serviceCenterDrop.didSelectOption { (index, option) in
            self.selectedType = option
            
            self.storeLocatorFilter?.stores = self.filterStoreBy(city: self.selectedCity, type: self.selectedType, stores: self.storeLocator?.stores?.copy())
            self.SetUpMarker()
        }
        
        // set storeListDrop
        storeDropDown.placeholder = Localized("DropDown_All")
        
        
        
        var optionsArrayCity : [String] = [Localized("DropDown_All")]
        
        if let selectedCenter = storeLocator?.city {
            
            for center in selectedCenter {
                
                optionsArrayCity.append(center)
            }
        }
        
        storeDropDown.dataSource = optionsArrayCity
        
        storeDropDown.didSelectOption { (index, option) in
            self.selectedCity = option
            
            self.storeLocatorFilter?.stores = self.filterStoreBy(city: self.selectedCity, type: self.selectedType, stores: self.storeLocator?.stores?.copy())
            
            self.SetUpMarker()
            }
    }
    
    ///Set marks
    func SetUpMarker(){
        
        DispatchQueue.main.async {
            self.googleMapView.clear()
            
            var bounds = GMSCoordinateBounds()
            self.storeLocatorFilter?.stores?.forEach({ (aStoreLocation) in
                // Creates a marker in the center of the map.
                let aStoreMarker : GMSMarker = self.addMarkerOnLocation(storeLatitude: aStoreLocation.latitude, storeLongitude: aStoreLocation.longitude, storeName: aStoreLocation.store_name, storeAddress: aStoreLocation.address, storeType: aStoreLocation.type, focusOnLocation: false)
                
                bounds = bounds.includingCoordinate(aStoreMarker.position)
            })
            
            
            
            //  self.googleMapView.animate(toZoom: 15)
            let updateCamera = GMSCameraUpdate.fit(bounds, withPadding: 20)
            self.googleMapView.animate(with: updateCamera)
            
            if self.storeLocatorFilter?.stores?.count ?? 0 <= 1 {
                if let lastStoreLocation = self.storeLocatorFilter?.stores?.last {
                    
                    let camera = GMSCameraPosition.camera(withLatitude: lastStoreLocation.latitude.toDouble , longitude: lastStoreLocation.longitude.toDouble, zoom: 15)
                    self.googleMapView.camera = camera
                }
                
            }
            self.focuseOnSelectedLocation()
        }
    }
    
    /**
     Focus on selected Location.
     
     - returns: void
     */
    func focuseOnSelectedLocation() {
        
        if shouldFocuseToSelectedLocation == true {
            
            _ = self.addMarkerOnLocation(storeLatitude: selectedLocationForFocuse?["lati"], storeLongitude: selectedLocationForFocuse?["longi"], storeName: selectedLocationForFocuse?["title"], storeAddress: selectedLocationForFocuse?["address"], storeType: selectedLocationForFocuse?["type"])
            
            // Clear Data
            shouldFocuseToSelectedLocation = false
            selectedLocationForFocuse = [:]
        }
        
    }
    
    /**
     Focus on a specific store.
     
     - parameter Store : Store information.
     
     - returns: void
     */
    func focusOnLocationOf(Store stroreData: Stores?) {
        
        if let selectedStore = stroreData {
            
            _ = self.addMarkerOnLocation(storeLatitude: selectedStore.latitude, storeLongitude: selectedStore.longitude, storeName: selectedStore.store_name, storeAddress: selectedStore.address, storeType:selectedStore.type)
        }
    }
    
    /**
     Create and add a marker to map.
     
     - parameter storeLatitude: Lattitude
     - parameter storeLongitude: Longitude
     - parameter storeName:  Store Name
     - parameter storeAddress: Store Address
     - parameter storeType: Store Type
     - parameter focusOnLocation: Foucus on location or not. Bydefault it's true.
     
     - returns: GMSMarker (Marker)
     */
    func addMarkerOnLocation(storeLatitude: String?, storeLongitude: String?, storeName: String?, storeAddress: String?, storeType: String?, focusOnLocation: Bool = true) -> GMSMarker {
        
        let latitudeRecived = storeLatitude?.toDouble ?? 0.0
        let longitudeRecieved = storeLongitude?.toDouble ?? 0.0
        
        // Creates a marker in the center of the map.
        let aStoreMarker = GMSMarker()
        aStoreMarker.position = CLLocationCoordinate2D(latitude: latitudeRecived, longitude: longitudeRecieved)
        aStoreMarker.title = storeName
        aStoreMarker.snippet = storeAddress
        aStoreMarker.icon = UIImage.markerIconImageFor(key: storeType)
        
        aStoreMarker.map = self.googleMapView
        
        if focusOnLocation == true {
            //  let camera = GMSCameraPosition.camera(withLatitude: latitudeRecived , longitude: longitudeRecieved, zoom: 15)
            //  let updateCamera = GMSCameraUpdate.setCamera(camera)
            //  self.googleMapView.animate(with: updateCamera)
            
            let camera = GMSCameraPosition.camera(withLatitude: latitudeRecived , longitude: longitudeRecieved, zoom: 15)
            self.googleMapView.animate(to: camera)
        }
        
        return aStoreMarker
    }
    
    /**
     Filter Store by Parameters.
     
     - parameter city: City name
     - parameter type: Store Type
     - parameter stores: Stores List
     
     - returns: stores
     */
    func filterStoreBy(city: String?, type:String?, stores : [Stores]? ) -> [Stores]? {
        
        guard let myType = type else {
            return nil
        }
        
        guard let myCity = city else {
            return nil
        }
        
        guard let StoreListDetails = stores else {
            return nil
        }
        
        if(myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == true && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == true) {
            
            return stores
            
        } else if(myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == false && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == false){
            
            let  filterdStoreDetails = StoreListDetails.filter() {
                
                if ($0 as Stores).city.lowercased() == myCity.lowercased() {
                    
                    if ($0 as Stores).type.lowercased() == myType.lowercased() {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            }
            return filterdStoreDetails
            
        } else if (myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == false && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == true) {
            
            let  filterdStoreDetails = StoreListDetails.filter() {
                if ($0 as Stores).city.lowercased() == myCity.lowercased() {
                    return true
                } else {
                    return false
                }
                
            }
            return filterdStoreDetails
        } else if (myCity.isEqual( Localized("DropDown_All"), ignorCase: true) == true && myType.isEqual( Localized("DropDown_All"), ignorCase: true) == false) {
            
            let  filterdStoreDetails = StoreListDetails.filter() {
                if ($0 as Stores).type.lowercased() == myType.lowercased() {
                    return true
                } else {
                    return false
                }
                
            }
            return filterdStoreDetails
            
        } else {
            return stores
        }
        
    }
    
    /**
     Arrange stores in order of shortest distance from specific location.
     
     - parameter location: Location
     
     - returns: stores
     */
    private func shortestDistanceToOrganizationFromLocation(location:CLLocation) -> [Stores]? {
        
        var newStoreListWithDistance : [Stores] = []
        
        storeLocator?.stores?.forEach({ (aStore) in
            
            let aNewStore: Stores = aStore.copy()
            
            let latitude = aStore.latitude.trimmWhiteSpace
            let longitude = aStore.longitude.trimmWhiteSpace
            
            if latitude.isBlank == false &&
                longitude.isBlank == false {
                
                let fromLocation = CLLocation(latitude: latitude.toDouble, longitude: longitude.toDouble)
                aNewStore.distance = fromLocation.distance(from: location) /* * 0.00062137119*/
            }
            
            newStoreListWithDistance.append(aNewStore)
            
        })
        
        // Remove nagitive values
        for index in 0..<newStoreListWithDistance.count {
            if newStoreListWithDistance[index].distance < 0 {
                newStoreListWithDistance.remove(at: index)
            }
        }
        
        // Sorte objects
        newStoreListWithDistance = newStoreListWithDistance.sorted(by: {$0.distance < $1.distance})
        
        
        if newStoreListWithDistance.count >= 3 {
            
            return [newStoreListWithDistance[0],newStoreListWithDistance[1],newStoreListWithDistance[2]]
        } else if newStoreListWithDistance.count >= 2 {
            return [newStoreListWithDistance[0],newStoreListWithDistance[1]]
        }
        else if newStoreListWithDistance.count >= 1 {
            return [newStoreListWithDistance[0]]
        }
        else{
            return []
        }
    }
    
   
    ///Set Three nearest store details.
    func setThreeNeariestStoresDetail() {
        
        
        nearestStoreView.isHidden = false
        nearestStoreViewHeightConstranint.constant = 82
        
        // 1
        if threeNeariestStores.count >= 1 {
            self.distance.text = threeNeariestStores[0].store_name
            self.firstDistance.text = "\((threeNeariestStores[0].distance / 1000.00).rounded(toPlaces: 2)) \(Localized("Miles_Title"))"
            firstStoreButton.tag = 1
            firstStoreButton.addTarget(self, action: #selector(showSelectedNearestStore), for: UIControl.Event.touchUpInside)
            
        } else {
            self.distance.text = ""
            self.firstDistance.text = ""
        }
        
        // 2
        if threeNeariestStores.count >= 2 {
            self.secondNearest.text = threeNeariestStores[1].store_name
            self.secondDistance.text = "\((threeNeariestStores[1].distance / 1000.00).rounded(toPlaces: 2)) \(Localized("Miles_Title"))"
            secondStoreButton.tag = 2
            secondStoreButton.addTarget(self, action: #selector(showSelectedNearestStore), for: UIControl.Event.touchUpInside)
        } else {
            self.secondNearest.text = ""
            self.secondDistance.text = ""
        }
        
        // 3
        if threeNeariestStores.count >= 3 {
            self.thirdNearest.text = threeNeariestStores[2].store_name
            self.thirdDistance.text = "\((threeNeariestStores[2].distance / 1000.00).rounded(toPlaces: 2)) \(Localized("Miles_Title"))"
            thiredStoreButton.tag = 3
            thiredStoreButton.addTarget(self, action: #selector(showSelectedNearestStore), for: UIControl.Event.touchUpInside)
        } else {
            self.thirdNearest.text = ""
            self.thirdDistance.text = ""
        }
    }
    
    //MARK: - IBAction
    @objc func showSelectedNearestStore(sender:UIButton){
        
        var selectedStore : Stores?
        if sender.tag == 1 {
            
            if threeNeariestStores.count > 0 {
                selectedStore = threeNeariestStores[0]
            }
            
        } else if sender.tag == 2 {
            
            if threeNeariestStores.count > 1 {
                selectedStore = threeNeariestStores[1]
            }
        } else if sender.tag == 3 {
            
            if threeNeariestStores.count > 2 {
                selectedStore = threeNeariestStores[2]
            }
        }
        
        if selectedStore != nil {
            
            focusOnLocationOf(Store: selectedStore)
            
        }
    }
    
    //MARK: - Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let currentLocation = locations.last else {
            return
        }
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
        //Display nearest three stores distance
        threeNeariestStores = shortestDistanceToOrganizationFromLocation(location: currentLocation) ?? []
        
        DispatchQueue.main.async {
            self.setThreeNeariestStoresDetail()
        }
    }
    
}

