//
//  MBLanguageManager.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 05/30/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import Foundation

class MBLanguageManager: NSObject {

    ///Initialzes user language
    class func setInitialUserLanguage(){
        _ = MBLanguageManager.userSelectedLanguage()
    }

    ///Returns user selected language.
    class func userSelectedLanguage() -> Constants.MBLanguage {

        let userDefaults = UserDefaults.standard

        if let userSelectedLanguage : String = userDefaults.value(forKey: Constants.kUserSelectedLanguage) as? String {

            switch userSelectedLanguage {

            case "2":
                return Constants.MBLanguage.russian

            case "3":
                return Constants.MBLanguage.english

            case "4":
                return Constants.MBLanguage.azeri

            default:
                MBLanguageManager.setUserLanguage(selectedLanguage: Constants.defaultAppLanguage)
                return Constants.defaultAppLanguage
            }

        } else {
            MBLanguageManager.setUserLanguage(selectedLanguage: Constants.defaultAppLanguage)
            return Constants.defaultAppLanguage
        }
    }

    ///Change user language.
    class func setUserLanguage(selectedLanguage : Constants.MBLanguage) {

        let userDefaults = UserDefaults.standard

        userDefaults.set(selectedLanguage.rawValue, forKey: Constants.kUserSelectedLanguage)

        userDefaults.synchronize()

        switch selectedLanguage {

        case Constants.MBLanguage.english:
            Localize.setCurrentLanguage("en")
            break

        case Constants.MBLanguage.russian:
            Localize.setCurrentLanguage("ru")
            break

        case Constants.MBLanguage.azeri:
            Localize.setCurrentLanguage("az-Cyrl")
            break
            
        }

    }

    ///Returns user selected language name.
    class func userSelectedLanguageName() -> String {

        let languageName = MBLanguageManager.userSelectedLanguage()

        switch languageName {

        case Constants.MBLanguage.russian:
            return "Russian"

        case Constants.MBLanguage.english:
            return "English"

        case Constants.MBLanguage.azeri:
            return "Azeri"
            
        }
    }
    
    ///Returns user selected language short code.
    class func userSelectedLanguageShortCode() -> String {
        
        let languageName = MBLanguageManager.userSelectedLanguage()
        
        switch languageName {
            
        case Constants.MBLanguage.russian:
            return "ru"
            
        case Constants.MBLanguage.english:
            return "en"
            
        case Constants.MBLanguage.azeri:
            return "az"
            
        }
    }
}
