//
//  AlertMessageVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 5/22/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class AlertMessageVC: BaseVC {
    
    //MARK : - Properties
    fileprivate var cancelButtonCompletionBlock : MBButtonCompletionHandler = {}
    var alertTitle : String = ""
    var balanceAmountInfo : String?
    var alertMessage :NSAttributedString = NSAttributedString.init(string: "")
    var cancelButtonTitle : String = ""
    
    //MARK : - IBOutlets
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var alertTitleLabel: MBLabel!
    @IBOutlet weak var alertBalanceTitleLabel: MBLabel!
    @IBOutlet weak var alertMessageLabel: MBLabel!
    @IBOutlet weak var cancelButton: MBButton!
    
    

    //MARK : - ViewControllers Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        alertTitleLabel.text = alertTitle.isBlank ? Localized("Title_Alert") : alertTitle
        alertMessageLabel.attributedText = alertMessage.string.isBlank ? NSAttributedString.init(string: Localized("Message_UnexpectedError")) : alertMessage
        cancelButton.setTitle(cancelButtonTitle, for: UIControl.State.normal)
        
        if let ammountInfo = balanceAmountInfo {
            
            let attributedString = MBUtilities.createAttributedText(Localized("Title_BalanceWithColon"), withManatSign: false)
            attributedString.append(MBUtilities.createAttributedText(ammountInfo, textColor:.mbBrandRed, withManatSign: true))
            
            alertBalanceTitleLabel.attributedText = attributedString
        } else {
            alertBalanceTitleLabel.text = ""
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        detailView.roundAllCorners(radius: Constants.kViewCornerRadius)

    }
    
    
    //MARK:- Functions

    func setAlertWith(title:String = Localized("Title_Alert"), balanceAmount:String? = nil, attributedMessage : NSAttributedString? = NSAttributedString.init(string: ""), btnTitle:String = Localized("BtnTitle_OK"), cancelBlock : @escaping MBButtonCompletionHandler = {}) {
    
        alertTitle = title
        self.balanceAmountInfo = balanceAmount
        self.alertMessage = attributedMessage ?? NSAttributedString.init(string: "")
        cancelButtonTitle = btnTitle
        
        
        cancelButtonCompletionBlock = cancelBlock
    }
    
    //MARK:- IBActions
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion:{
            self.cancelButtonCompletionBlock()
        })
        
    }
    
}
