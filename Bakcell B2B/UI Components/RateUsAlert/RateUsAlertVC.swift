//
//  RateUsAlertVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/9/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class RateUsAlertVC: BaseVC {

    //MARK : - Properties
    fileprivate var rateUsButtonCompletionBlock : MBButtonCompletionHandler = {}
    
    //MARK: - IBOutlets
    @IBOutlet var alertTitleLabel: MBLabel!
    @IBOutlet var alertConfirmationsMessageLabel: MBLabel!
    @IBOutlet var alertMessageLabel: MBLabel!
    
    @IBOutlet var rateNowButton: MBButton!
    @IBOutlet var cancelButton: MBButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadViewLayout()
    }
    
    //MARK: - Functions
    func loadViewLayout()  {
        cancelButton.setTitle(Localized("BtnTitle_MaybeLater"), for: .normal)
        rateNowButton.setTitle(Localized("BtnTitle_RateNow"), for: .normal)
        alertTitleLabel.text = Localized("Title_RateUs")
        alertConfirmationsMessageLabel.text =  RateUsAlertVC.getTitle()
        alertMessageLabel.text = RateUsAlertVC.getMessage()
        
    }

    //MARK: - IBActions
    @IBAction func rateNowButtonAction(_ sender: UIButton) {
        
        /* Save that Rate Us screen presented to user */
        MBUtilities.updateRateUsShownStatus(isShow: true)
        
        /* Save User interaction time with rateus screen */
        MBUtilities.updateRateUsLaterTime()
        
        /* Call Rate Us API to update status*/
        self.callRateUs()
        
        self.dismiss(animated: false, completion:{
            
            self.rateUsButtonCompletionBlock()
        })
        
    }
    
    @IBAction func laterButtonAction(_ sender: UIButton) {
        
        /* Save that Rate Us screen presented to user */
        MBUtilities.updateRateUsShownStatus(isShow: true)
        
        /* Save User interaction time with rateus screen */
        MBUtilities.updateRateUsLaterTime()
        
        self.dismiss(animated: true, completion:nil)
    }
    
    //MARK:- Functions
    func setRateUs(_ block : @escaping MBButtonCompletionHandler = {}) {
        rateUsButtonCompletionBlock = block
    }
    
    func callRateUs() {
        
        _ = MBAPIClient.sharedClient.rateUs({ (response, resultData, error, isCancelled, status, resultCode, resultDesc)  in
            if error != nil {
                error?.showServerErrorInViewController(self)
            } else {
            }
        })
    }
    

}

// Public class methods
extension RateUsAlertVC {
    
    public class func getTitle() -> String {
        if let titleString = UserDefaults.standard.string(forKey: Constants.K_RateUsTitle),
            titleString.isBlank == false {
            return titleString
        }
        return Localized("Message_DoYouLikeOurApp")
    }
    
    public class func getMessage() -> String {
        if let messageString = UserDefaults.standard.string(forKey: Constants.K_RateUsMessage),
            messageString.isBlank == false {
            return messageString
        }
        return Localized("Message_RatingMessage")
    }
    
    public class func saveTitle(title :String?) {
        UserDefaults.standard.set(title, forKey: Constants.K_RateUsTitle)
        UserDefaults.standard.synchronize()
    }
    
    public class func saveMessage(message :String?) {
        UserDefaults.standard.set(message, forKey: Constants.K_RateUsMessage)
        UserDefaults.standard.synchronize()
    }
}
