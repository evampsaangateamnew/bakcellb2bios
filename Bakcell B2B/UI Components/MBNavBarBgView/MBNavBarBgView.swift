
//
//  MBNavBarBgView.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 8/1/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

//@IBDesignable
class MBNavBarBgView: UIView {
    //MARK : - Properties
    @IBInspectable var bgColor : UIColor = .mbBrandRed {
        didSet {
            setupLayout()
        }
    }
    
    //MARK : - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupLayout()
    }
    
    func setupLayout() {
        
        self.backgroundColor = bgColor
    }
}
