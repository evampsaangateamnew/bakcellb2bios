//
//  ShareAlertVC.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/17/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class ShareAlertVC: BaseVC {

    enum ShareType {
        case pdf
        case excel
    }
    
    //MARK: - Properties
    fileprivate var shareButtonCompletionBlock : MBButtonCompletionHandler = {}
    var selectedShareType : ShareType = .pdf
    
    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var shareLabel: MBLabel!
    @IBOutlet var shareButton: MBButton!
    @IBOutlet var cancelButton: MBButton!
    @IBOutlet var pdfButton: MBButton!
    @IBOutlet var excelButton: MBButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pdfButtonTapped(pdfButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadViewLayout()
    }
    
    //MARK: -  Functions
    func loadViewLayout()  {
        titleLabel.text = Localized("Title_ShareAlert")
        shareLabel.text = Localized("Title_Share")
        cancelButton.setTitle(Localized("BtnTitle_CANCEL"), for: .normal)
    }
    
    func setShareAlertAction( shareActionBlock: @escaping MBButtonCompletionHandler = {})  {
        shareButtonCompletionBlock =  shareActionBlock
    }
    
    
    //MAR: - IBActions
    @IBAction func shareButtonPressed(_ sender: UIButton){
        self.dismiss(animated: true, completion:nil)
        shareButtonCompletionBlock()
        
        
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton){
        self.dismiss(animated: true, completion:nil)
        
    }
    
    @IBAction func pdfButtonTapped(_ sender: UIButton){
       pdfButton.layer.borderColor = UIColor.mbBrandRed.cgColor
       excelButton.layer.borderColor = UIColor.mbBorderGray.cgColor
        selectedShareType = .pdf
    }
    
    @IBAction func excellButtonTapped(_ sender: UIButton){
        excelButton.layer.borderColor = UIColor.mbBrandRed.cgColor
        pdfButton.layer.borderColor = UIColor.mbBorderGray.cgColor
        selectedShareType = .excel
    }
}
