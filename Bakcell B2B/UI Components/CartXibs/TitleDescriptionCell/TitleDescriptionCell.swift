//
//  TitleDescriptionCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/20/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TitleDescriptionCell: UITableViewCell {


    //MARK: - IBOutlets
   
    //    title label
    @IBOutlet var titleLabel: UILabel!
    // image view
    @IBOutlet var iconImageView: UIImageView!
    //    Detail label
    @IBOutlet var detailLabel: UILabel!

    @IBOutlet var iconImageViewWidthConstraint: NSLayoutConstraint!
    
     //MARK: - Functions 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setTitleAndDescription(title : String?, description : String? , iconName : String?) {

        titleLabel.text = title ?? ""
        detailLabel.text = description ?? ""

        iconImageView.image = UIImage.imageFor(key: iconName)
        iconImageViewWidthConstraint.constant = 26

    }

    func setTitleAndDescriptionWithOutIcon(title : String?, description : String?) {

        titleLabel.text = title ?? ""
        detailLabel.text = description ?? ""
        iconImageViewWidthConstraint.constant = 0

    }
}
