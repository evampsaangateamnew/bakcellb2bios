//
//  IndividualTitleView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 11/27/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class IndividualTitleView: MBAccordionTableViewHeaderView {
    
    //MARK: - IBOutlets
    @IBOutlet var titleView: MBMarqueeLabel!
    @IBOutlet var detailLabel: MBMarqueeLabel!
    
    @IBOutlet var contenView: UIView!
    @IBOutlet var roundingCoverView: UIView!
    @IBOutlet var seperatorView: UIView!
    
    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /**
     Set Individual details.
     
     - parameter titleText: Title text.
     - parameter priceLabel: Price text.
     - parameter priceValue: Price value.
     
     - returns: void.
     */
    func setViewWith(titleText : String?, priceLabel : String?, priceValue: String?) {
        
        contenView.roundTopCorners(radius: 8)
        contenView.backgroundColor = UIColor.mbBackgroundGray
        roundingCoverView.backgroundColor = UIColor.mbBackgroundGray
        seperatorView.backgroundColor = UIColor.white
        
        titleView.labelTextColor = .mbTextBlack
        
        titleView.text = titleText
        
        if priceLabel?.isBlank == true ||
            priceValue?.isBlank == true {
            
            detailLabel.text = ""
        } else {
            detailLabel.attributedText = MBUtilities.createAttributedText("\(priceLabel ?? ""): \(priceValue ?? "")", textColor: .black)
        }
    }
}
