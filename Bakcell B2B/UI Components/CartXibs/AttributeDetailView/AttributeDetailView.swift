//
//  AttributeDetailView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class AttributeDetailView: UIView {

    //MARK: - IBOutlets
    @IBOutlet var titleLabel: MBMarqueeLabel!
    @IBOutlet var leftValueLabel: MBLabel!
    @IBOutlet var rightValueLabel: MBLabel!

    @IBOutlet var centerSepratorView: UIView!

    @IBOutlet var leftValueViewWidthConstraint: NSLayoutConstraint!

     //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // Using in supplementary offers
    func setAttributeValues(showManatSign : Bool , aAttribute : AttributeList?) {
        
        // Hiding left value
        leftValueViewWidthConstraint.constant = 0
        leftValueLabel.text = ""
        
        centerSepratorView.isHidden = true
        
        if let title = aAttribute?.title {
            titleLabel.text = title
        } else {
            titleLabel.text = ""
        }
        
        if showManatSign == true &&
            aAttribute?.value?.isBlank == false &&
            (aAttribute?.unit?.isEqual("manat", ignorCase: true) == true ||
                aAttribute?.value?.isStringAnNumber() == true ) {
            
            rightValueLabel.attributedText = MBUtilities.createAttributedText(aAttribute?.value, textColor: UIColor.mbTextBlack, withManatSign: true)
        } else {
             rightValueLabel.attributedText = MBUtilities.createAttributedText("\(aAttribute?.value ?? "") \(aAttribute?.unit ?? "")", textColor: UIColor.mbTextBlack, withManatSign: false)
        }
    }


    /// Using in tariff Header section for call type
    func setAttributeValuesForCall(showManatSign : Bool , aAttribute : CinCallAttributes?, cellType : MBOfferType, priceTemplate : String?, isFromKlassPostpaid: Bool = false) {

        centerSepratorView.isHidden = true
        
        // Hiding left value
        leftValueViewWidthConstraint.constant = 0
        leftValueLabel.text = ""
        if let title = aAttribute?.title {
          
            titleLabel.text = title
        } else {
            titleLabel.text = ""
        }

        var rightValue : String = ""

        //Showing left and right values both in case of call
        if cellType == .Call {
            // Setting left label Value (in middle middle of view)
            if priceTemplate?.isEqual( Constants.kDoubleTitlesKey, ignorCase: true) ?? false ||
                priceTemplate?.isEqual( Constants.kDoubleValueKey, ignorCase: true) ?? false {

                //set width
                leftValueViewWidthConstraint.constant = 80

                if  aAttribute?.valueLeft?.isBlank == false &&
                    aAttribute?.valueRight?.isBlank == false {

                    centerSepratorView.isHidden = false
                } else {
                    centerSepratorView.isHidden = true
                }


                // get Value
                if let leftValue = aAttribute?.valueLeft {

                    leftValueLabel.text = leftValue

                    // Check for free resource value
                    if leftValue.isHasFreeOrUnlimitedText() && isFromKlassPostpaid == false {

                        leftValueLabel.attributedText = MBUtilities.createAttributedText(leftValue, textColor: UIColor.mbBrandRed, withManatSign: false)

                    } else  {
                       leftValueLabel.attributedText = MBUtilities.createAttributedText(leftValue, textColor: UIColor.mbTextBlack, withManatSign: leftValue.isStringAnNumber())
                    }

                } else  {
                    leftValueLabel.text = ""
                }

            } else {

                centerSepratorView.isHidden = true
                leftValueLabel.text = ""
                leftValueViewWidthConstraint.constant = 0
            }

            // Setting right value
            rightValue = aAttribute?.valueRight ?? ""

        } else {
            leftValueViewWidthConstraint.constant = 0

            // Setting right value
            rightValue =  aAttribute?.value ?? ""
        }

        // Setting right value
        if rightValue.isBlank {
            rightValueLabel.text = ""

        } else {
            if rightValue.isHasFreeOrUnlimitedText() {

                rightValueLabel.attributedText = MBUtilities.createAttributedText(rightValue, textColor: UIColor.mbBrandRed, withManatSign: false)

            } else {
                rightValueLabel.attributedText = MBUtilities.createAttributedText(rightValue, textColor: UIColor.mbTextBlack, withManatSign: rightValue.isStringAnNumber())
            }

            // Setting color of right value
            if cellType == .Call &&
                aAttribute?.valueLeft?.isBlank == false &&
                aAttribute?.valueRight?.isBlank == false &&
                (priceTemplate?.isEqual( Constants.kDoubleTitlesKey, ignorCase: true) ?? false || priceTemplate?.isEqual( Constants.kDoubleValueKey, ignorCase: true) ?? false) {

                rightValueLabel.textColor = UIColor.mbBrandRed
            }

        }
    }

    /// Using in tariff Header section for Internet type
    func setAttributeValuesForInternet(subTitle : String?, subTitleValue : String?, isFromKlassPostpaid: Bool = false)  {

        centerSepratorView.isHidden = true
        // Hiding left value
        leftValueViewWidthConstraint.constant = 0
        leftValueLabel.text = ""
    
        titleLabel.text = subTitle ?? ""
        
        if subTitleValue?.isHasFreeOrUnlimitedText() == true &&
            isFromKlassPostpaid == false {

            rightValueLabel.attributedText = MBUtilities.createAttributedText(subTitleValue, textColor: UIColor.mbBrandRed, withManatSign: false)
        } else {
            rightValueLabel.attributedText = MBUtilities.createAttributedText(subTitleValue, textColor: UIColor.mbTextBlack, withManatSign: subTitleValue?.isStringAnNumber() ?? false)
        }
    }
    
}


