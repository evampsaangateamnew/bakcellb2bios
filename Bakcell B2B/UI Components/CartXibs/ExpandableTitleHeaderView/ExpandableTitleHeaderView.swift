//
//  ExpandableTitleHeaderView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class ExpandableTitleHeaderView: MBAccordionTableViewHeaderView {
    
    //MARK:- Properties
    var viewType : MBSectionType = MBSectionType.UnSpecified
    
    //MARK: - IBOutlet
    @IBOutlet var myContentView: UIView!
    @IBOutlet var titleLabel: MBMarqueeLabel!
    @IBOutlet var stateIcon: UIImageView!
    @IBOutlet var seperatorView: MBSeperatorView!
    
    //MARK: - View Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    /**
     Set Expanded header details.
     
     - parameter title: Title text.
     - parameter isSectionSelected: Wheter section is selected or not.
     - parameter headerType: Section type.
     
     - returns: void.
     */
    func setViewWithTitle(title : String?, isSectionSelected: Bool, headerType:MBSectionType ) {
        
        //View Type
        viewType = headerType
        
        if isSectionSelected {
            stateIcon.image = UIImage.imageFor(name: "minus_sign")
        } else {
            stateIcon.image = UIImage.imageFor(name: "plus_sign")
        }
        
        myContentView.backgroundColor = UIColor.mbBackgroundGray
        
        // Setting offer name
        if let title = title {
            titleLabel.text = title
        } else {
            titleLabel.text = ""
        }
        
        titleLabel.textColor = UIColor.mbTextBlack
        seperatorView.backgroundColor = UIColor.white
    }
}
