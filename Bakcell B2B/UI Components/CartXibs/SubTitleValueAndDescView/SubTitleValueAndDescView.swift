//
//  SubTitleValueAndDescView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SubTitleValueAndDescView: UIView {
    //MARK: - IBOutlets
    @IBOutlet var subTitleLabel: MBMarqueeLabel!
    @IBOutlet var subValueLable: MBLabel!
    @IBOutlet var subDescriptionLabel: MBMarqueeLabel!

    //MARK: - Functions
    
    /**
     Set Sub Title and values.
     
     - parameter aAttribute: AttributeList.
     
     - returns: CGFloat cell heigt.
     */
    func setSubTitleValueAndDescription(aAttribute : AttributeList?) -> CGFloat {

        if let aAttributeObject = aAttribute {

            subTitleLabel.text = aAttributeObject.title ?? ""

            if aAttributeObject.unit?.isEqual("AZN", ignorCase: true) ?? false {
                subValueLable.attributedText = MBUtilities.createAttributedText(aAttributeObject.value ?? "", textColor: UIColor.mbTextBlack, withManatSign: true)

            } else {
                
                subValueLable.text = "\(aAttributeObject.value ?? "") \(aAttributeObject.unit?.removeNullValues() ?? "")"

            }
            subValueLable.textColor = UIColor.mbTextBlack

            if let description = aAttributeObject.description {
                subDescriptionLabel.loadHTMLString(htmlString: description)
                
            } else {
                subDescriptionLabel.text = ""
            }
            return (subDescriptionLabel.frame.origin.y + subDescriptionLabel.frame.height + 1.0)
            
        } else {
            subTitleLabel.text = ""
            subValueLable.text = ""
            subDescriptionLabel.text = ""
        }
        return 0.0
    }

}
