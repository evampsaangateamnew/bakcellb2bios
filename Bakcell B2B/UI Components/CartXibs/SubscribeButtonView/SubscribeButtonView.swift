//
//  SubscribeButtonView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/17/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class SubscribeButtonView: MBAccordionTableViewHeaderView {

    //MARK: - Properties
    var viewType : MBSectionType = MBSectionType.UnSpecified

    //MARK: - IBOutlets
    @IBOutlet var myContentView: UIView!
    @IBOutlet var activateRenewButton: MBButton!
    @IBOutlet var subscribedButton: MBButton!

    @IBOutlet var activateRenewRightConstraint: NSLayoutConstraint!
    @IBOutlet var activateRenewCenterConstraint: NSLayoutConstraint!

    @IBOutlet var subscribedLeftConstraint: NSLayoutConstraint!
    @IBOutlet var subscribedCenterConstraint: NSLayoutConstraint!

    //MARK: - Functions
    
    
    /**
     Set Subscribe Button layout.
     
     - parameter tag: button tag.
     
     - returns: void.
     */
    func setSubscribeButtonLayout(tag: Int) {
        // Using in Supplementary offer
        myContentView.backgroundColor = UIColor.mbBackgroundGray

        // set layout
        subscribedButton.isHidden = true
        activateRenewButton.isHidden = false

        activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 999)
        activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 998)

        activateRenewButton.tag = tag

        activateRenewButton.setButtonLayoutType(.grayButton)
        activateRenewButton.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: UIControl.State.normal)
        
    }


    /**
     Set Subscribe Button.
     
     - parameter tag: button tag.
     
     - returns: void.
     */
    func setSubscribedButton(tag: Int) {

        myContentView.backgroundColor = UIColor.mbBackgroundGray
        // set layout
        subscribedButton.isHidden = false
        activateRenewButton.isHidden = true

        subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 999)
        subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 998)

        subscribedButton.tag = tag
        
        subscribedButton.setButtonLayoutType(.grayButton)
        subscribedButton.setTitle(Localized("BtnTitle_SUBSCRIBED"), for: UIControl.State.normal)
        subscribedButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
    }

    /**
     Set Renew and Subscribe Button.
     
     - parameter tag: button tag.
     
     - returns: void.
     */
    func setRenewAndSubscribedButton(tag: Int) {

        myContentView.backgroundColor = UIColor.mbBackgroundGray
        // set layout
        subscribedButton.isHidden = false
        activateRenewButton.isHidden = false

        subscribedButton.isEnabled = false

        activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 999)
        activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 998)

        subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 999)
        subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 998)

        activateRenewButton.tag = tag

        activateRenewButton.setTitle(Localized("BtnTitle_RENEW"), for: UIControl.State.normal)
        activateRenewButton.isEnabled = true
        activateRenewButton.setButtonLayoutType(.grayButton)
        activateRenewButton.setTitleColor(UIColor.mbBrandRed, for: UIControl.State.normal)

        subscribedButton.setButtonLayoutType(.grayButton)
        subscribedButton.setTitle(Localized("BtnTitle_SUBSCRIBED"), for: UIControl.State.normal)
        subscribedButton.setTitleColor(UIColor.white, for: UIControl.State.normal)

    }

    /**
     Set Renew/Deactive Button.
     
     - parameter tag: button tag.
     - parameter renewEnable: Whether button should be enabeled or not. Bydefault it's false.
     - parameter deactivateEnable: Whether button should be enabeled or not. Bydefault it's false.
     
     - returns: void.
     */
    func setRenewDeActivateButton(tag: Int, renewEnable: Bool = false, deactivateEnable: Bool = false) {
        // Using My Subscription
        myContentView.backgroundColor = UIColor.mbBackgroundGray
        // set layout
        subscribedButton.isHidden = true
        activateRenewButton.isHidden = true

        activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 999)
        activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 998)

        subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 999)
        subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 998)

        activateRenewButton.tag = tag
        subscribedButton.tag = tag

        activateRenewButton.setTitle(Localized("BtnTitle_RENEW"), for: UIControl.State.normal)
        activateRenewButton.setButtonLayoutType(.grayButton)
        activateRenewButton.isEnabled = renewEnable
        activateRenewButton.setTitleColor(UIColor.mbBrandRed, for: UIControl.State.normal)

        subscribedButton.setTitle(Localized("BtnTitle_DEACTIVATE"), for: UIControl.State.normal)
        subscribedButton.setButtonLayoutType(.whiteButton)
        subscribedButton.isEnabled = deactivateEnable

        if deactivateEnable {
            subscribedButton.setTitleColor(UIColor.mbTextGray, for: UIControl.State.normal)
        }
        
        if renewEnable == true && deactivateEnable == false {

            activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 999)
            activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 998)

            activateRenewButton.isHidden = false
            subscribedButton.isHidden = true

        } else if renewEnable == false && deactivateEnable == true {

            subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 999)
            subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 998)

            subscribedButton.isHidden = false
            activateRenewButton.isHidden = true
        } else if renewEnable == true && deactivateEnable == true {

            activateRenewRightConstraint.priority = UILayoutPriority(rawValue: 999)
            activateRenewCenterConstraint.priority = UILayoutPriority(rawValue: 998)

            subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 999)
            subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 998)

            subscribedButton.isHidden = false
            activateRenewButton.isHidden = false
        }
    }

    /**
     Set View for subscribe Button Tarrif.
     
     - parameter isSubscribed: sbuscribed button text.
     - parameter tag: button tag.
     
     - returns: void.
     */
    func setViewForSubscribeButtonForTariff(isSubscribed : String?, tag: Int = 0) {

        //View Type
        viewType = MBSectionType.Subscription

        myContentView.backgroundColor = UIColor.mbBackgroundGray

        subscribedButton.isHidden = false
        activateRenewButton.isHidden = true

        subscribedCenterConstraint.priority = UILayoutPriority(rawValue: 999)
        subscribedLeftConstraint.priority = UILayoutPriority(rawValue: 998)

        // 0 = Can not Subscribe
        // 1 = Can Subscribe
        // 2 = Already Subscribe/ Subscribed
        // 3 = Renewable

        subscribedButton.tag = tag

        subscribedButton.setButtonLayoutType(.grayButton)

        if isSubscribed == "2" {
            subscribedButton.isEnabled = false
            subscribedButton.setTitle(Localized("BtnTitle_SUBSCRIBED"), for: UIControl.State.normal)
            subscribedButton.setTitleColor(UIColor.white, for: UIControl.State.normal)

        } else if isSubscribed == "3" {
            subscribedButton.isEnabled = true
            subscribedButton.setTitle(Localized("BtnTitle_RENEW"), for: UIControl.State.normal)
            subscribedButton.setTitleColor(UIColor.mbBrandRed, for: UIControl.State.normal)

        } else if isSubscribed == "1" {
            subscribedButton.isEnabled = true
            subscribedButton.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: UIControl.State.normal)
            subscribedButton.setTitleColor(UIColor.black, for: UIControl.State.normal)

        } else if isSubscribed == "0" {
            subscribedButton.isEnabled = false
            subscribedButton.setTitle(Localized("BtnTitle_SUBSCRIBE"), for: UIControl.State.normal)
            subscribedButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
        }
    }
}

