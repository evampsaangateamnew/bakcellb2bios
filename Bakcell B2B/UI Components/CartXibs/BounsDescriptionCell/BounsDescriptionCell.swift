//
//  BounsDescriptionCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 1/1/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit

class BounsDescriptionCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var detailLabel: MBMarqueeLabel!

    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    /**
     Set bonus details.
     
     - parameter bonusTitle: Bonus text.
     - parameter bonusIconName: Bonus icon name.
     - parameter bonusDescription: Bonus description text.
     
     - returns: void.
     */
    func setTopDescriptionLayoutValues(bonusTitle : String?, bonusIconName : String?, bonusDescription :String? ) {

        self.containerView.backgroundColor = UIColor.mbBackgroundGray
        detailLabel.backgroundColor = UIColor.clear

        // create an NSMutableAttributedString that we'll append everything to
        let fullString = NSMutableAttributedString(string: "")


        var bonusTitleString = ""

        if bonusTitle?.isBlank == false {
            bonusTitleString = "\( bonusTitle ?? ""): "
        }

        let bonusTitle = NSAttributedString(string:bonusTitleString, attributes: [NSAttributedString.Key.foregroundColor: UIColor.mbBrandRed])

        let bonusDescription = NSAttributedString(string: bonusDescription ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.mbTextGray])
        
        fullString.append(bonusTitle)
        fullString.append(bonusDescription)

        if fullString.length > 0 {
            detailLabel.attributedText  = fullString
        } else {
            detailLabel.text = ""

        }
    }
    
}
