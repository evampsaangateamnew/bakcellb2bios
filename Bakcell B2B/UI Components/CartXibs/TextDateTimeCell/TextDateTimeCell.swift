//
//  TextDateTimeCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TextDateTimeCell: UITableViewCell {
    
    //MARK: - IBOutlets
    
    //    title label
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var titleLabelHeightConstraint: NSLayoutConstraint!
    
    //    Detail label
    @IBOutlet var detailLabel: MBLabel!
    
    //    Date and Time View
    @IBOutlet var dateTimeView: UIView!
    @IBOutlet var fromTitleValueLabel: MBLabel!
    @IBOutlet var toTitleValueLabel: MBLabel!
    @IBOutlet var dateTimeViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Functions 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /**
     Set setTextWithTitleLayoutValues.
     
     - parameter textWithTitle: TextWithTitle.
     
     - returns: Void.
     */
    func setTextWithTitleLayoutValues(textWithTitle : TextWithTitle?) {
        
        titleLabelHeightConstraint.constant = 22
        dateTimeViewHeightConstraint.constant = 0
        self.contentView.backgroundColor = UIColor.white
        
        if let textWithTitle = textWithTitle {
            
            titleLabel.text = textWithTitle.title ?? ""
            titleLabel.textColor = UIColor.mbTextBlack
            
            if let detailText = textWithTitle.text{
                detailLabel.loadHTMLString(htmlString: detailText)
            } else {
                detailLabel.text = ""
            }
            detailLabel.textColor = UIColor.mbTextGray
        }
    }
    
    /**
     Set setTextWithOutTitleLayoutValues.
     
     - parameter textWithOutTitle: String.
     
     - returns: Void.
     */
    func setTextWithOutTitleLayoutValues(textWithOutTitle : String?) {
        
        titleLabelHeightConstraint.constant = 0
        dateTimeViewHeightConstraint.constant = 0
        self.contentView.backgroundColor = UIColor.white
        
        if let textWithOutTitle = textWithOutTitle {
            
            detailLabel.loadHTMLString(htmlString: textWithOutTitle)
        } else {
            detailLabel.text = ""
        }
        detailLabel.textColor = UIColor.mbTextGray
    }
    
    /**
     Set setDateAndTimeLayoutValues.
     
     - parameter dateAndTime: DateAndTime.
     
     - returns: Void.
     */
    func setDateAndTimeLayoutValues(dateAndTime : DateAndTime?) {
        
        titleLabelHeightConstraint.constant = 0
        dateTimeViewHeightConstraint.constant = 23
        
        self.contentView.backgroundColor = UIColor.white
        detailLabel.backgroundColor = UIColor.clear
        
        if let dateAndTime = dateAndTime {
            
            if let description = dateAndTime.description {
                
                detailLabel.loadHTMLString(htmlString: description)
                
            } else {
                detailLabel.text = ""
            }
            detailLabel.textColor = UIColor.mbTextGray
            fromTitleValueLabel.textColor = UIColor.mbTextBlack
            toTitleValueLabel.textColor = UIColor.mbTextBlack
            
            fromTitleValueLabel.text = "\(dateAndTime.fromTitle ?? "") \(dateAndTime.fromValue ?? "")"
            toTitleValueLabel.text = "\(dateAndTime.toTitle ?? "") \(dateAndTime.toValue ?? "")"
            
        } else {
            detailLabel.text = ""
            fromTitleValueLabel.text = ""
            toTitleValueLabel.text = ""
        }
    }
    
    /**
     Set setTextWithPointsLayoutValues.
     
     - parameter textWithPoint: Array of strings.
     
     - returns: Void.
     */
    func setTextWithPointsLayoutValues(textWithPoint : [String?]?) {
        
        titleLabelHeightConstraint.constant = 0
        dateTimeViewHeightConstraint.constant = 0
        self.contentView.backgroundColor = UIColor.white
        
        if let textWithPoint = textWithPoint {
            
            let attributedText = NSMutableAttributedString()
            textWithPoint.forEach({ (aString) in
                
                let bulletPoint: String = "\u{2022}"
                let aAttribute = NSAttributedString(string: "\(bulletPoint) \(aString ?? "")\n", attributes: [NSAttributedString.Key.foregroundColor: UIColor.mbTextGray])
                
                
                
                attributedText.append(aAttribute)
                
            })
            
            detailLabel.attributedText  = attributedText
        } else {
            detailLabel.text = ""
        }
        detailLabel.textColor = UIColor.mbTextGray
    }
    
}
