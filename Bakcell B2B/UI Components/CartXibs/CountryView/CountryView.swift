//
//  CountryView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class CountryView: UIView {

    //MARK: - IBOutlets
   
    @IBOutlet var countryTitleLabel: MBLabel!
    @IBOutlet var countryIconImageView: UIImageView!
    @IBOutlet var roamingDetailsCountriesListView: UIView!

    @IBOutlet var roamingDetailsCountriesListHeightConstraint: NSLayoutConstraint!
   
     //MARK: - Functions
    
    /**
     Set setRoamingDetailsCountriesListLayout.
     
     - parameter aRoamingDetailsCountries: RoamingDetailsCountries.
     
     - returns: CGFloat cell heigt.
     */
    func setRoamingDetailsCountriesListLayout (aRoamingDetailsCountries : RoamingDetailsCountries?) -> CGFloat {

        if let aRoamingDetailCountry = aRoamingDetailsCountries {

            countryTitleLabel.text = aRoamingDetailCountry.countryName?.trimmWhiteSpace ?? ""
            countryIconImageView.image = UIImage.imageFor(name: aRoamingDetailCountry.flag ?? "dummy_flag")
            countryIconImageView.roundAllCorners(radius: 4)

            roamingDetailsCountriesListView.subviews.forEach({ (aLabel) in
                aLabel.removeFromSuperview()
            })


            if let operatorList = aRoamingDetailCountry.operatorList {

                var lastLabel : UILabel?

                operatorList.forEach({ (aOperator) in

                    let aLabel : UILabel = UILabel()
                    aLabel.textColor = countryTitleLabel.textColor
                    aLabel.font = countryTitleLabel.font

                    aLabel.translatesAutoresizingMaskIntoConstraints = false

                    aLabel.text = aOperator ?? ""
                    aLabel.textAlignment = NSTextAlignment.right

                    roamingDetailsCountriesListView.addSubview(aLabel)

                    let leading = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingDetailsCountriesListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    let trailing = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingDetailsCountriesListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                    var top : NSLayoutConstraint = NSLayoutConstraint()

                    if lastLabel == nil {

                        top = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingDetailsCountriesListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                    } else {
                        top = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastLabel, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                    }



                    let height = NSLayoutConstraint(item: aLabel, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 15)
                    
                    roamingDetailsCountriesListView.addConstraints([leading, trailing, top, height])
                    
                    lastLabel = aLabel
                })
            }

            var countryDetailListHeight : CGFloat = 0.0

            if let count = aRoamingDetailCountry.operatorList?.count {
                countryDetailListHeight = CGFloat(count * 15)
            }

            //Adding top content height
            countryDetailListHeight += 30
            roamingDetailsCountriesListHeightConstraint.constant = countryDetailListHeight


            return countryDetailListHeight

        }
        return 0.0
    }

}


