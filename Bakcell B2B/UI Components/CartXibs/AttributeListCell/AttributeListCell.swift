//
//  AttributeListCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/13/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class AttributeListCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet var AttributeListView: UIView!
    
    // AttributeList view iner outlets
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: MBMarqueeLabel!
    @IBOutlet var valueLabel: MBLabel!
    
    
    @IBOutlet var onnetView: UIView!
    @IBOutlet var offnetView: UIView!
    
    @IBOutlet var onnetTitleLabel: MBMarqueeLabel!
    @IBOutlet var onnetValueLabel: MBLabel!
    @IBOutlet var offnetTitleLabel: MBMarqueeLabel!
    @IBOutlet var offnetValueLabel: MBLabel!
    
    @IBOutlet var topSeparatorView: UIView!
    @IBOutlet var bottomSeparatorView: UIView!
    
    @IBOutlet var description_View: UIView!
    @IBOutlet var descriptionLabel: MBLabel!
    
    @IBOutlet var descriptionViewHeightGreaterRelation: NSLayoutConstraint!
    @IBOutlet var descriptionViewHeightEqualRelation: NSLayoutConstraint!
    
    @IBOutlet var onnetViewHeight: NSLayoutConstraint!
    @IBOutlet var offnetViewHeight: NSLayoutConstraint!
    
    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.AttributeListView.backgroundColor = UIColor.white
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    /**
     Using in supplementary offers.
     
     - parameter attributeList: Attributes details.
     - parameter offerType: Offer type.
     
     - returns: void.
     */
    func setAttributeList(attributeList : AttributeList?, offerType: String) {
        
        AttributeListView.isHidden = false
        description_View.isHidden = false
        topSeparatorView.isHidden = false
        
        if let aAttributeList = attributeList {
            
            iconImageView.image = UIImage.imageFor(key: aAttributeList.iconMap)
            titleLabel.text = aAttributeList.title ?? ""
            
            if let value = aAttributeList.value {
                
                if aAttributeList.unit?.isEqual("AZN", ignorCase: true) == true {
                    
                    valueLabel.attributedText = MBUtilities.createAttributedText(value, textColor: UIColor.mbTextGray, withManatSign: true)
                    
                } else {
                    valueLabel.text = "\(value) \(aAttributeList.unit?.removeNullValues() ?? "")"
                }
                
                if value.isHasFreeOrUnlimitedText() ||
                    offerType.isEqual("Call", ignorCase: true) == true ||
                    offerType.isEqual("SMS", ignorCase: true) == true ||
                    offerType.isEqual("Internet", ignorCase: true) == true ||
                    (offerType.isEqual("TM", ignorCase: true) == true && aAttributeList.title?.isHasGetText() ?? false) ||
                    offerType.isEqual("Звонок", ignorCase: true) == true ||
                    offerType.isEqual("Интернет", ignorCase: true) == true ||
                    (offerType.isEqual("Особенные", ignorCase: true) == true && aAttributeList.title?.isHasGetText() ?? false) ||
                    offerType.isEqual("Zəng", ignorCase: true) == true ||
                    offerType.isEqual("İnternet", ignorCase: true) == true ||
                    (offerType.isEqual("Xüsusilər", ignorCase: true) == true && aAttributeList.title?.isHasGetText() ?? false) {
                    
                    valueLabel.textColor = UIColor.mbBrandRed
                    
                    
                } else {
                    valueLabel.textColor = UIColor.mbTextGray
                }
            } else {
                
                titleLabel.text = ""
                valueLabel.text = ""
            }
            
            
            
            if aAttributeList.onnetValue?.isBlank == false {
                onnetView.isHidden = false
                onnetViewHeight.constant = 15
                
                onnetTitleLabel.text = aAttributeList.onnetLabel
                onnetValueLabel.text = aAttributeList.onnetValue
                
            } else {
                onnetView.isHidden = true
                onnetViewHeight.constant = 0
            }
            
            if aAttributeList.offnetValue?.isBlank == false {
                offnetView.isHidden = false
                offnetViewHeight.constant = 15
                
                offnetTitleLabel.text = aAttributeList.offnetLabel
                offnetValueLabel.text = aAttributeList.offnetValue
                
            } else {
                offnetView.isHidden = true
                offnetViewHeight.constant = 0
            }
            
            
            let description = aAttributeList.description ?? ""
            if description.isBlank == false {
                
                description_View.isHidden = false
                descriptionViewHeightEqualRelation.isActive = false
                descriptionViewHeightGreaterRelation.isActive = true
                
                descriptionViewHeightEqualRelation.priority = UILayoutPriority(rawValue: 990)
                descriptionViewHeightGreaterRelation.priority = UILayoutPriority(rawValue: 999)
                
                descriptionLabel.text = description
                
            } else {
                description_View.isHidden = true
                
                descriptionViewHeightGreaterRelation.isActive = false
                descriptionViewHeightEqualRelation.isActive = true
                
                
                descriptionViewHeightGreaterRelation.priority = UILayoutPriority(rawValue: 990)
                descriptionViewHeightEqualRelation.priority = UILayoutPriority(rawValue: 999)
                descriptionLabel.text = nil
                
            }
        }
    }
    
    /**
     Using in My Subscription.
     
     - parameter attributeList: Attributes details.
     
     - returns: void.
     */
    func setAttributeListOfMySubscription(attributeList : AttributeList?) {
        
        AttributeListView.isHidden = false
        description_View.isHidden = false
        
        onnetView.isHidden = true
        offnetView.isHidden = true
        onnetViewHeight.constant = 0
        offnetViewHeight.constant = 0
        
        self.contentView.backgroundColor = UIColor.mbBackgroundGray
        topSeparatorView.backgroundColor = UIColor.mbBackgroundGray
        bottomSeparatorView.backgroundColor = UIColor.mbBackgroundGray
        AttributeListView.backgroundColor = UIColor.mbBackgroundGray
        description_View.backgroundColor = UIColor.mbBackgroundGray
        
        if let aAttributeList = attributeList {
            
            titleLabel.text = aAttributeList.title ?? ""
            
            // Text color
            
            valueLabel.textColor = UIColor.black
            
            
            
            if let value = aAttributeList.value {

                if aAttributeList.unit?.isEqual("AZN", ignorCase: true) ?? false {
                    valueLabel.attributedText = MBUtilities.createAttributedText(value, textColor: UIColor.mbTextGray, withManatSign: true)
                    
                } else {
                    valueLabel.text = "\(value) \(aAttributeList.unit?.removeNullValues() ?? "")"
                    
                }
                
                if value.isHasFreeOrUnlimitedText() {
                    
                    valueLabel.textColor = UIColor.mbBrandRed
                    
                } else {
                    valueLabel.textColor = UIColor.mbTextBlack
                }
                
            } else {
                
                titleLabel.text = ""
                valueLabel.text = ""
            }
            
            iconImageView.image = UIImage.imageFor(key: aAttributeList.iconMap)
            
            let description = aAttributeList.description ?? ""
            if description.isBlank == false {
                description_View.isHidden = false
                descriptionViewHeightEqualRelation.isActive = false
                descriptionViewHeightGreaterRelation.isActive = true
                
                descriptionViewHeightEqualRelation.priority = UILayoutPriority(rawValue: 990)
                descriptionViewHeightGreaterRelation.priority = UILayoutPriority(rawValue: 999)
                
                descriptionLabel.text = description
                
            } else {
                description_View.isHidden = true
                
                descriptionViewHeightGreaterRelation.isActive = false
                descriptionViewHeightEqualRelation.isActive = true
                
                
                descriptionViewHeightGreaterRelation.priority = UILayoutPriority(rawValue: 990)
                descriptionViewHeightEqualRelation.priority = UILayoutPriority(rawValue: 999)
                descriptionLabel.text = ""
                
            }
        }
    }

}
