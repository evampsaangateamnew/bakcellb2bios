//
//  FreeResourceValidityCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class FreeResourceValidityCell: UITableViewCell {

    //MARK: - IBOutlets
   
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var titleValueLabel: MBLabel!
    @IBOutlet var subTitleLabel: MBLabel!
    @IBOutlet var subTilteValueLabel: MBLabel!
    @IBOutlet var descriptionLabel: MBLabel!

    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    /**
     Set FreeResourceValidityValues.
     
     - parameter freeResourceValidity: FreeResourceValidity.
     
     - returns: Void.
     */
    func setFreeResourceValidityValues(freeResourceValidity : FreeResourceValidity?) {

        if let freeResourceValidity = freeResourceValidity {
            titleLabel.text = freeResourceValidity.title ?? ""
            titleValueLabel.text = freeResourceValidity.titleValue ?? ""
            subTitleLabel.text = freeResourceValidity.subTitle ?? ""
            subTilteValueLabel.text = freeResourceValidity.subTitleValue ?? ""

            if let description = freeResourceValidity.description {
                descriptionLabel.loadHTMLString(htmlString: description)
            } else {
                descriptionLabel.text = ""
            }
        }
    }
    
}
