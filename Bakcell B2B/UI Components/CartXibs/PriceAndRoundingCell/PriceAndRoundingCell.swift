//
//  PriceAndRoundingCell.swift
//  TestForAddingViewRunTimeINTableViewCell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 AbdulRehman Warraich. All rights reserved.
//

import UIKit

class PriceAndRoundingCell: UITableViewCell {

    //MARK:- Properties
    var iconDefaultWidth : CGFloat {
        return 26
    }
    
    //MARK:- IBOutlets
    // Main Views
    @IBOutlet var titleAndValueView: UIView!
    @IBOutlet var attributeListView: UIView!
    @IBOutlet var descriptionView: UIView!

    // TitleAndValueView iner outlets
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: MBMarqueeLabel!
    @IBOutlet var rightValueLabel: MBMarqueeLabel!
    @IBOutlet var leftValueLabel: MBMarqueeLabel!
    @IBOutlet var leftValueLabelWidthConstraint: NSLayoutConstraint!

    @IBOutlet var centerSepratorView: UIView!

    @IBOutlet var iconImageViewWidthConstraint: NSLayoutConstraint!

    // AttributeListView iner outlets
    @IBOutlet var attributeListViewHightConstraint: NSLayoutConstraint!
    
    // DescriptionView iner outlets
    @IBOutlet var descriptionLabel: MBLabel!
    @IBOutlet var descriptionViewHightConstraint: NSLayoutConstraint!

    // seprator view
    @IBOutlet var sepratorView: UIView!
    @IBOutlet var sepratorViewLeading: NSLayoutConstraint!
    @IBOutlet var sepratorViewTrialing: NSLayoutConstraint!
    
    //MARK:- Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
    /**
     Using in supplementery offers.
     
     - parameter price: Price details.
     - parameter showIcon: Whether show icon or not. Bydefault it's true.
     
     - returns: void.
     */
    func setPriceLayoutValues (price : Price?, showIcon: Bool = true) {

        sepratorView.isHidden = false
        sepratorView.backgroundColor = UIColor.mbBackgroundGray

        centerSepratorView.isHidden = true
        
        sepratorViewLeading.constant = 8
        sepratorViewTrialing.constant = 8

        leftValueLabel.text = ""
        leftValueLabelWidthConstraint.constant = 0

        self.contentView.backgroundColor = UIColor.white

        //        offersCurrency : ""
        //        offersCurrency : "AZN
        //        offersCurrency : "manat"

        if let price = price {

            if showIcon {
                iconImageView.image = UIImage.imageFor(key: price.iconName)
                iconImageViewWidthConstraint.constant = iconDefaultWidth
                iconImageView.isHidden = false

            } else {
                iconImageViewWidthConstraint.constant = 0
                iconImageView.isHidden = true
            }

            titleLabel.text = price.title ?? ""
            rightValueLabel.textColor = .mbTextGray

            if price.value?.isBlank == false {
                if let offersCurrency = price.offersCurrency {

                    if offersCurrency.isEqual("manat", ignorCase: true) {
                        rightValueLabel.attributedText = MBUtilities.createAttributedText("\(price.value ?? "")", textColor: .mbTextGray, withManatSign: true)

                    } else {
                        rightValueLabel.text = "\(price.value ?? "") \(offersCurrency)"
                    }
                    
                } else {
                    rightValueLabel.text = price.value ?? ""
                }
            } else {
                rightValueLabel.text = price.value ?? ""
            }

            if let description = price.description {
//                mainDescription_lbl.text = description
                descriptionLabel.loadHTMLString(htmlString: description)
                descriptionViewHightConstraint.isActive = false
            } else {
                descriptionLabel.text = ""
                descriptionViewHightConstraint.isActive = true
            }


            attributeListView.subviews.forEach({ (aView) in

                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })

            var lastView : UIView?
            price.attributeList?.forEach({ (aAttribute) in

                let attributesView : AttributeDetailView = AttributeDetailView.fromNib()
                attributesView.translatesAutoresizingMaskIntoConstraints = false

                attributesView.setAttributeValues(showManatSign: true, aAttribute: aAttribute)

                attributeListView.addSubview(attributesView)

                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }



                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)

                attributeListView.addConstraints([leading, trailing, top, height])
                
                lastView = attributesView

            })

            if let count = price.attributeList?.count{
                attributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                attributeListViewHightConstraint.constant = 0.0
            }
        }
    }

    /**
     Set Rounding values.
     
     - parameter rounding: Rounding details.
     - parameter showIcon: Whether show icon or not. Bydefault it's true.
     
     - returns: void.
     */
    func setRoundingLayoutValues (rounding : Rounding?, showIcon: Bool = true) {

        sepratorView.isHidden = false
        sepratorView.backgroundColor = UIColor.mbBackgroundGray

        centerSepratorView.isHidden = true

        sepratorViewLeading.constant = 8
        sepratorViewTrialing.constant = 8
//        manatSignLabelWidthConstraint.constant = 0

        leftValueLabel.text = ""
        leftValueLabelWidthConstraint.constant = 0

        self.contentView.backgroundColor = UIColor.white

        if let rounding = rounding {

            if showIcon {
                iconImageView.image = UIImage.imageFor(key: rounding.iconName)
                iconImageViewWidthConstraint.constant = iconDefaultWidth
                iconImageView.isHidden = false

            } else {
                iconImageViewWidthConstraint.constant = 0
                iconImageView.isHidden = true
            }

            titleLabel.text = rounding.title ?? ""
          rightValueLabel.attributedText = MBUtilities.createAttributedText(rounding.value ?? "", textColor: .mbTextGray, withManatSign: rounding.value?.isStringAnNumber() ?? false)

            if let description = rounding.description {
//                mainDescription_lbl.text = description
                descriptionLabel.loadHTMLString(htmlString: description)
                descriptionViewHightConstraint.isActive = false
            } else {
                descriptionLabel.text = ""
                descriptionViewHightConstraint.isActive = true
            }

            attributeListView.subviews.forEach({ (aView) in

                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })
            var lastView : UIView?
            rounding.attributeList?.forEach({ (aAttribute) in

                let attributesView : AttributeDetailView = AttributeDetailView.fromNib()
                attributesView.translatesAutoresizingMaskIntoConstraints = false

                attributesView.setAttributeValues(showManatSign: false, aAttribute: aAttribute)

                attributeListView.addSubview(attributesView)

                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }



                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)

                attributeListView.addConstraints([leading, trailing, top, height])

                lastView = attributesView

            })
            
            if let count = rounding.attributeList?.count{
                attributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                attributeListViewHightConstraint.constant = 0.0
            }
        }
    }


    /**
     Using in Tariff screen.
     
     - parameter internetHeader: InternetPriceSection details.
     - parameter setBackgroundColorWhite: Whether set background color white or not. Bydefault it's true.
     
     - returns: void.
     */
    func setInternetLayoutValues (internetHeader : InternetPriceSection?, setBackgroundColorWhite: Bool = false) {
        sepratorView.isHidden = false

        centerSepratorView.isHidden = true

        descriptionLabel.text = ""
        descriptionViewHightConstraint.isActive = true

        sepratorViewLeading.constant = 8
        sepratorViewTrialing.constant = 8
        if setBackgroundColorWhite == true {
            self.contentView.backgroundColor = UIColor.white
            sepratorView.backgroundColor = UIColor.mbBackgroundGray
        } else {
            self.contentView.backgroundColor = UIColor.mbBackgroundGray
            sepratorView.backgroundColor = UIColor.white
        }

        leftValueLabel.text = ""
        leftValueLabelWidthConstraint.constant = 0

        //        iconName : ""
        //        title : "Internet"
        //        titleValue : "1 MB"
        //        subTitle : "Download & Upload"
        //        subTitleValue : "0.29"

        if let internetHeader = internetHeader {

            titleLabel.text = internetHeader.title ?? ""

            rightValueLabel.attributedText = MBUtilities.createAttributedText(internetHeader.titleValue ?? "", textColor: .mbTextGray, withManatSign: internetHeader.titleValue?.isStringAnNumber() ?? false)

            iconImageView.image = UIImage.imageFor(key: internetHeader.iconName)


            attributeListView.subviews.forEach({ (aView) in

                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })


            let attributesView : AttributeDetailView = AttributeDetailView.fromNib()
            attributesView.translatesAutoresizingMaskIntoConstraints = false

            attributesView.setAttributeValuesForInternet(subTitle: internetHeader.subTitle, subTitleValue: internetHeader.subTitleValue, isFromKlassPostpaid: setBackgroundColorWhite)

            attributeListView.addSubview(attributesView)

            let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

            let top : NSLayoutConstraint = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

            let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)

            attributeListView.addConstraints([leading, trailing, top, height])
            
            attributeListViewHightConstraint.constant = 22
        }
    }

    /**
     Set calls, smsm details.
     
     - parameter callHeaderType: CallSMSPriceSection details.
     - parameter cellType: MBOfferType details.
     - parameter setBackgroundColorWhite: Whether set background color white or not. Bydefault it's true.
     
     - returns: void.
     */
    func setCallAndSMSLayoutValues (callHeaderType : CallSMSPriceSection?, cellType : MBOfferType, setBackgroundColorWhite: Bool = false) {

        sepratorView.isHidden = false
        sepratorView.backgroundColor = UIColor.mbBackgroundGray

        centerSepratorView.isHidden = true

        descriptionLabel.text = ""
        descriptionViewHightConstraint.isActive = true
        
        sepratorViewLeading.constant = 8
        sepratorViewTrialing.constant = 8

        if cellType == MBOfferType.Call {
            self.contentView.backgroundColor = UIColor.white
            sepratorView.backgroundColor = UIColor.mbBackgroundGray

        } else if cellType == MBOfferType.SMS {

            if setBackgroundColorWhite == true {
                self.contentView.backgroundColor = UIColor.white
                sepratorView.backgroundColor = UIColor.mbBackgroundGray
            } else {
                self.contentView.backgroundColor = UIColor.mbBackgroundGray
                sepratorView.backgroundColor = UIColor.white
            }

        }
        if let callHeaderType = callHeaderType {

            titleLabel.text = callHeaderType.title ?? ""

            if cellType == MBOfferType.Call {
                rightValueLabel.text = callHeaderType.titleValue ?? ""

                // Show double titles and values
                if callHeaderType.priceTemplate?.isEqual( Constants.kDoubleTitlesKey, ignorCase: true) ?? false{

                    leftValueLabelWidthConstraint.constant = 80
                    centerSepratorView.isHidden = false

                } else {
                    leftValueLabelWidthConstraint.constant = 0
                    centerSepratorView.isHidden = true
                }

                // id "Double Values" then left title will not be displed
                if callHeaderType.priceTemplate?.isEqual( Constants.kDoubleTitlesKey, ignorCase: true) ?? false {
                    leftValueLabel.text = callHeaderType.titleValueLeft ?? ""

                } else {

                    leftValueLabel.text = ""
                }

                rightValueLabel.text = callHeaderType.titleValueRight ?? ""

            } else {
                rightValueLabel.text = callHeaderType.titleValue ?? ""
                leftValueLabelWidthConstraint.constant = 0
            }

            rightValueLabel.attributedText = MBUtilities.createAttributedText(rightValueLabel.text, textColor: .mbTextGray, withManatSign: rightValueLabel.text?.isStringAnNumber() ?? false)
 
            iconImageView.image = UIImage.imageFor(key: callHeaderType.iconName)


            attributeListView.subviews.forEach({ (aView) in

                if aView is AttributeDetailView {
                    aView.removeFromSuperview()
                }
            })

            var lastView : UIView?
            callHeaderType.attributes?.forEach({ (aAttribute) in

                let attributesView : AttributeDetailView = AttributeDetailView.fromNib()
                attributesView.translatesAutoresizingMaskIntoConstraints = false

                attributesView.setAttributeValuesForCall(showManatSign: true, aAttribute: aAttribute, cellType: cellType, priceTemplate: callHeaderType.priceTemplate, isFromKlassPostpaid: setBackgroundColorWhite)

                attributeListView.addSubview(attributesView)

                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: attributeListView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
                }



                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 22)

                attributeListView.addConstraints([leading, trailing, top, height])

                lastView = attributesView
                
            })
            
            if let count = callHeaderType.attributes?.count {
                attributeListViewHightConstraint.constant = CGFloat(count * 22)
            } else {
                attributeListViewHightConstraint.constant = 0.0
            }
        }
    }

}

