//
//  RoamingDetailsCell.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class RoamingDetailsCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet var descriptionAboveLabel: MBLabel!
    @IBOutlet var descriptionAboveLableHeightConstraint: NSLayoutConstraint!

    @IBOutlet var roamingCountriesDetailsView: UIView!
    @IBOutlet var roamingCountriesDetailsViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet var descriptionBelowLabel: MBLabel!
    @IBOutlet var descriptionBelowLableHeightConstraint: NSLayoutConstraint!


    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    /**
     Set setRoamingDetailsLayout.
     
     - parameter roamingDetails: RoamingDetails.
     
     - returns: Void.
     */
    func setRoamingDetailsLayout(roamingDetails : RoamingDetails?) {

        if let roamingDetails = roamingDetails {

            descriptionAboveLabel.loadHTMLString(htmlString: roamingDetails.descriptionAbove?.removeNullValues() ?? "")
            
            descriptionBelowLabel.loadHTMLString(htmlString: roamingDetails.descriptionBelow?.removeNullValues() ?? "")

            var lastView : UIView?
            var allCountryViewHeight : CGFloat = 0.0
            
            roamingDetails.roamingDetailsCountriesList?.forEach({ (aRoamingCountryDetail) in

                let attributesView : CountryView = CountryView.fromNib()
                attributesView.translatesAutoresizingMaskIntoConstraints = false

                let countryViewHeight = attributesView.setRoamingDetailsCountriesListLayout(aRoamingDetailsCountries: aRoamingCountryDetail)

                allCountryViewHeight = allCountryViewHeight + countryViewHeight

                roamingCountriesDetailsView.addSubview(attributesView)

                let leading = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingCountriesDetailsView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)

                let trailing = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingCountriesDetailsView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)

                var top : NSLayoutConstraint = NSLayoutConstraint()

                if lastView == nil{

                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: roamingCountriesDetailsView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

                } else {
                    top = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lastView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 8)

                    allCountryViewHeight += 8
                }



                let height = NSLayoutConstraint(item: attributesView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: countryViewHeight)
                
                roamingCountriesDetailsView.addConstraints([leading, trailing, top, height])

                lastView = attributesView

            })

            roamingCountriesDetailsViewHeightConstraint.constant = allCountryViewHeight

        }

    }
    
}
