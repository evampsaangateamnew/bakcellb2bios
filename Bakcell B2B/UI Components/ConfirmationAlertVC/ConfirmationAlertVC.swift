//
//  ConfirmationAlertVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 5/22/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class ConfirmationAlertVC: BaseVC {
    
    enum ConfirmationLayoutType {
        case logout
        case groupSelection
        case other
    }
    //MARK: - Properties
    fileprivate var okButtonCompletionBlock : MBButtonCompletionHandler = {}
    fileprivate var cancelButtonCompletionBlock : MBButtonCompletionHandler = {}
    var alertTitle : String = ""
    var balanceAmountInfo : String?
    var alertMessage :NSAttributedString = NSAttributedString.init(string: "")
    var okButtonTitle : String = ""
    var cancelButtonTitle : String = ""
    
    
    var alertLayoutType: ConfirmationLayoutType = ConfirmationLayoutType.other
    
    //MARK : - IBOutlets
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var alertTitleLabel: MBLabel!
    @IBOutlet weak var alertBalanceTitleLabel: MBLabel!
    @IBOutlet weak var alertMessageLabel: MBLabel!
    @IBOutlet weak var okButton: MBButton!
    @IBOutlet weak var cancelButton: MBButton!
    
    
    //MARK: - ViewControllers Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertTitleLabel.text = alertTitle
        alertMessageLabel.attributedText = alertMessage
       
        okButton.setTitle(okButtonTitle, for: UIControl.State.normal)
        cancelButton.setTitle(cancelButtonTitle, for: UIControl.State.normal)
        
        if alertLayoutType == .logout {
            alertBalanceTitleLabel.text = ""
            
            okButton.setButtonLayoutType(.redButton)
            cancelButton.setButtonLayoutType(.grayButton)
            
        } else {
            okButton.setButtonLayoutType(.grayButton)
            cancelButton.setButtonLayoutType(.whiteButton)
            
            if let ammountInfo = balanceAmountInfo {
                
                let attributedString = MBUtilities.createAttributedText(Localized("Title_BalanceWithColon"), withManatSign: false)
                attributedString.append(MBUtilities.createAttributedText(ammountInfo, textColor:.mbBrandRed, withManatSign: true))
                
                alertBalanceTitleLabel.attributedText = attributedString
            } else {
                alertBalanceTitleLabel.text = ""
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        detailView.roundAllCorners(radius: Constants.kViewCornerRadius)
        
    }
    
    
    
    //MARK:- Functions
    func setAlertWith(title:String = Localized("Title_Alert"), balanceAmount:String? = nil, popupType : ConfirmationLayoutType? = .other, message : NSAttributedString? = NSAttributedString.init(string: ""), okBtnTitle:String = Localized("BtnTitle_OK"),caneclBtnTitle:String = Localized("BtnTitle_CANCEL"), okBlock : @escaping MBButtonCompletionHandler = {}, cancelBlock : @escaping MBButtonCompletionHandler = {}) {
        
        alertTitle = title
        alertLayoutType = popupType ?? .other
        
        self.balanceAmountInfo = balanceAmount
        
        okButtonTitle = okBtnTitle
        cancelButtonTitle = caneclBtnTitle
        
        okButtonCompletionBlock = okBlock
        cancelButtonCompletionBlock = cancelBlock
        
       
        alertMessage = message ?? NSAttributedString.init(string: "")
        
        
    }
    
    func setLogoutAlert(_ okBlock : @escaping MBButtonCompletionHandler = {}, cancelBlock : @escaping MBButtonCompletionHandler = {}) {
         alertLayoutType = .logout
        alertTitle = Localized("Title_Logout")
        self.balanceAmountInfo = nil
        alertMessage =  NSAttributedString.init(string: Localized("Message_LogoutMessage"))
        okButtonTitle = Localized("BtnTitle_LOGOUT")
        cancelButtonTitle = Localized("BtnTitle_CANCEL")
        
        okButtonCompletionBlock = okBlock
        cancelButtonCompletionBlock = cancelBlock
        
       
        
    }
    
    //MARK: - IBActions
    @IBAction func okButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion:nil)
        self.okButtonCompletionBlock()
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion:nil)
        self.cancelButtonCompletionBlock()
    }
    
}
