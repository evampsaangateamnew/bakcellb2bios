//
//  MBSlider.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 7/2/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit


fileprivate class MBSliderTooltipView: UIView {
    
    // MARK: properties
    var font: UIFont = UIFont.mbArial(fontSize: 16) {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var text: String? {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var value: Float {
        get {
            if let text = text {
                return Float(text) ?? 0
            }
            return 0.0
        }
        set {
            text = "\(newValue.toRoundedInt)"
        }
    }
    
    var fillColor = UIColor.mbBackgroundGray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var textColor = UIColor.mbBrandRed {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override open func draw(_ rect: CGRect) {
        fillColor.setFill()
        
        let roundedRect = CGRect(x:bounds.origin.x, y:bounds.origin.y, width:bounds.size.width, height:bounds.size.height * 0.8)
        let roundedRectPath = UIBezierPath(roundedRect: roundedRect, cornerRadius: 6.0)
        
        // create arrow
        let arrowPath = UIBezierPath()
        
        let p0 = CGPoint(x: bounds.midX, y: bounds.maxY - 2.0 )
        arrowPath.move(to: p0)
        arrowPath.addLine(to: CGPoint(x:bounds.midX - 6.0, y: roundedRect.maxY))
        arrowPath.addLine(to: CGPoint(x:bounds.midX + 6.0, y: roundedRect.maxY))
        
        roundedRectPath.append(arrowPath)
        roundedRectPath.fill()
    
        // draw text
        if let text = self.text {
            
            let size = text.size(withAttributes: [NSAttributedString.Key.font: font])
            let yOffset = (roundedRect.size.height - size.height) / 2.0
            let textRect = CGRect(x:roundedRect.origin.x, y: yOffset, width: roundedRect.size.width, height: size.height)
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            let attrs = [NSAttributedString.Key.font: font,
                         NSAttributedString.Key.paragraphStyle: paragraphStyle,
                         NSAttributedString.Key.foregroundColor: textColor]
            text.draw(in:textRect, withAttributes: attrs)
        }
    }
}


public protocol MBSliderDelegate: class {
    func startDragging(slider: MBSlider)
    func endDragging(slider: MBSlider)
    func markSlider(slider: MBSlider, dragged to: Float)
}

//MARK: - MBSlider
//@IBDesignable
open class MBSlider: UISlider {
    
    @IBInspectable
    open var handlerImage: UIImage? {
        didSet {
            setNeedsDisplay()
        }
    }
    open weak var delegate: MBSliderDelegate?
    
    open var markPositions: [Float]?
    
    
    private var toolTipView: MBSliderTooltipView!
    
    var thumbRect: CGRect {
        let rect = trackRect(forBounds: bounds)
        return thumbRect(forBounds: bounds, trackRect: rect, value: value)
    }
    
    // MARK: view life circle
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        setup()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame);
        setup()
    }
    
    // MARK: local functions
    func setup() {
        if (toolTipView == nil) {
            toolTipView = MBSliderTooltipView(frame: CGRect.zero)
            toolTipView.backgroundColor = UIColor.clear
            self.addSubview(toolTipView)
        }
        setSlider()
    }
    ///Set Slider
    func setSlider() {
        let gradientLayer = CAGradientLayer()
        let frame = CGRect.init(x:0, y:0, width:self.frame.size.width, height:6)
        gradientLayer.frame = frame
        gradientLayer.colors = [ UIColor(hexString:"#8C002B").cgColor, UIColor.mbBrandRed.cgColor ]
        
        gradientLayer.startPoint = CGPoint.init(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint.init(x:1.0, y:0.5)
        
        gradientLayer.cornerRadius = 3
        
        UIGraphicsBeginImageContextWithOptions(gradientLayer.frame.size, gradientLayer.isOpaque, 0.0);
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
       
        if let image = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            
            image.resizableImage(withCapInsets: UIEdgeInsets.zero)
            
            self.setMinimumTrackImage(image, for: .normal)
        }
        setThumbImage(UIImage.imageFor(name: "slider_image"), for: .normal)
        
    }
    // MARK: UIControl touch event tracking
    
    /**
     Check wheter touch should begin or not.
     
     - parameter touch: UITocuh.
     - parameter with event: Touch event.
     
     - returns: Bool.
     */
    open override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        
        delegate?.startDragging(slider: self)
        // Fade in and update the popup view
        let touchPoint = touch.location(in: self)
        // Check if the knob is touched. Only in this case show the popup-view
       checkAndShowPopupView(touchPoint)
        return super.beginTracking(touch, with: event)
    }
    
    /**
     Check wheter continue slider tracking or not.
     
     - parameter touch: UITocuh.
     - parameter with event: Touch event.
     
     - returns: Bool.
     */
    open override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
    
        if toolTipView.alpha != 1.0 {
            // Fade in and update the popup view
            let touchPoint = touch.location(in: self)
            // Check if the knob is touched. Only in this case show the popup-view
            checkAndShowPopupView(touchPoint)
        
        } else {
            // Update the popup view as slider knob is being moved
            positionAndUpdatePopupView()
        }
        
        return super.continueTracking(touch, with: event)
    }
    
    /**
     Cancel slider tracking.
     
     - parameter with event: Touch event.
     
     - returns: Void.
     */
    open override func cancelTracking(with event: UIEvent?) {
        delegate?.endDragging(slider: self)
        super.cancelTracking(with: event)
    }
    
    /**
     End slider tracking.
    
     - parameter touch: UITocuh.
     - parameter with event: Touch event.
     
     - returns: Void.
     */
    open override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        // Fade out the popoup view
        delegate?.endDragging(slider: self)
        delegate?.markSlider(slider: self, dragged: value)
        fadePopupViewInAndOut(fadeIn: false)
        super.endTracking(touch, with: event)
    }
    
    private func positionAndUpdatePopupView() {
        
        let tRect = thumbRect
        let popupRect = tRect.offsetBy(dx: 0, dy: -(tRect.size.height * 1.5))
        toolTipView.frame = popupRect.insetBy(dx: -28, dy: -10)
        toolTipView.value = value
    }
    
    /**
     Fade in/out popupview.
     
     - parameter fadeIn: Should fade in or not.
   
     
     - returns: Void.
     */
    private func fadePopupViewInAndOut(fadeIn: Bool) {
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        if fadeIn {
            toolTipView.alpha = 1.0
        } else {
            toolTipView.alpha = 0.0
        }
        UIView.commitAnimations()
    }
    
    /**
     Show popup view for point.
     
     - parameter touchPoint: touch point.
     
     - returns: Void.
     */
    private func checkAndShowPopupView(_ touchPoint : CGPoint){
        // Check if the knob is touched. Only in this case show the popup-view
        if thumbRect.contains(touchPoint) && toolTipView.alpha != 1.0 {
            fadePopupViewInAndOut(fadeIn: true)
        }
        positionAndUpdatePopupView()
    }
}
