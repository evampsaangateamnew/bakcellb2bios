//
//  MBSeperatorView.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/25/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class MBSeperatorView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.mbSeperator
    }

}
