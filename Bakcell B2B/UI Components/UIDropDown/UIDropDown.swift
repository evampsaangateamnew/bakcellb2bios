//
//  UIDropDown.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 6/1/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

//@IBDesignable
class UIDropDown: UIControl {
    
    fileprivate var title: UILabel = UILabel()
    fileprivate var imageView: UIImageView = UIImageView()
    fileprivate let dropDown = DropDown()
    fileprivate var didSelectOption: SelectionClosure?
    
    
    @IBInspectable public var placeholder: String? {
        didSet {
            title.text = placeholder ?? ""
        }
    }
    @IBInspectable public var text: String? {
        set {
            title.text = newValue
        }
        get {
            return title.text
        }
    }
    
    
    @IBInspectable public var rightImage: UIImage? {
        didSet {
            setup()
        }
    }
    
    @IBInspectable public var applyTintColor: Bool = true {
        didSet {
            setup()
        }
    }
    // Init
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    public var dataSource = [String]() {
        didSet {
            dropDown.dataSource = self.dataSource
        }
    }
    
    func reloadAllComponents() {
        dropDown.reloadAllComponents()
        title.text = placeholder ?? ""
    }
    
    fileprivate func setup() {
        //self.layer.borderColor = UIColor.mbBorderGray.cgColor
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 1
        
        title.textAlignment = .center
        self.addSubview(title)
        
        if rightImage != nil {
            
            imageView.image = rightImage
            imageView.contentMode = .scaleAspectFit
            
            if self.applyTintColor == true {
                imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
                self.imageView.tintColor = UIColor.mbArrowTintGray
            } else {
                self.imageView.tintColor = UIColor.clear
            }
            
            self.addSubview(imageView)
            
            imageView.snp.remakeConstraints { (make) in
                make.right.equalTo(self.snp.right).offset(-4)
                make.top.equalTo(self.snp.top).offset(4)
                make.bottom.equalTo(self.snp.bottom).offset(-4)
                make.width.equalTo(self.snp.height).offset(-8)
            }
        } else {
            
          imageView.removeFromSuperview()
        }
        
        
        title.snp.remakeConstraints { (make) in
            make.left.equalTo(self.snp.left).offset(2)
            make.top.equalTo(self.snp.top)
            make.bottom.equalTo(self.snp.bottom)
            if rightImage != nil {
                make.right.equalTo(self.imageView.snp.left).offset(-4)
            } else {
                make.right.equalTo(self.snp.right)
            }
        }
        self.addTarget(self, action: #selector(touchAction), for: .touchUpInside)
        
        
        
        // The view to which the drop down will appear on
        dropDown.anchorView = self
        dropDown.dismissMode = .automatic
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.dataSource = []
        
        title.font = UIFont.mbArial(fontSize: 14)
        title.textColor = UIColor.mbTextGray
        
        dropDown.textFont = UIFont.mbArial(fontSize: 14)
        dropDown.textColor = UIColor.mbTextGray
        dropDown.backgroundColor = UIColor.mbBackgroundLightGray
        dropDown.cellHeight = 44
        dropDown.separatorColor = UIColor.mbDarkSeperator
        
        //        if let aCount = self.faq?.count {
        //
        //            self.dropDown.tableHeight = CGFloat(35 * aCount)
        //        } else {
        //            self.dropDown.tableHeight = 35 * 0
        //        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.title.text = item
            if self.didSelectOption != nil {
                self.didSelectOption?(index,item)
            }
        }
        
        dropDown.cancelAction = { [unowned self] in
            if self.applyTintColor == true {
                self.imageView.tintColor = UIColor.mbArrowTintGray
            } else {
                self.imageView.tintColor = .clear
            }
        }
        
        dropDown.willShowAction = { [unowned self] in
            if self.applyTintColor == true {
                self.imageView.tintColor = UIColor.mbBrandRed
            } else {
                self.imageView.tintColor = .clear
            }
        }
        
    
    }
    
    @objc fileprivate func touchAction() {
        dropDown.show()
        DropDown.startListeningToKeyboard()
    }
    
    public func didSelectOption(_ selectionBlock: @escaping SelectionClosure) {
        didSelectOption = selectionBlock
    }
    
    
}
