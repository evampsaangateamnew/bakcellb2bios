//
//  ExpandableUsersGroupView.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/28/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class ExpandableUsersGroupView: MBAccordionTableViewHeaderView {

    //MARK: - IBOutlets
    @IBOutlet var containorView: UIView!
    @IBOutlet var myContentView: UIView!
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var groupCountLabel: MBLabel!
    @IBOutlet var nextIndicatorImageView: UIImageView!
    
    //MARK: - Functions
    ///Set Values
    func setLayout(aGroup:UsersGroupData?) {
        
        if let aGroupObject = aGroup {
            titleLabel.text = aGroupObject.groupName
            groupCountLabel.text = "\(aGroupObject.usersData?.count ?? 0)"
            setExpandStatus(aGroupObject.isSectionExpanded)
        } else {
            titleLabel.text = ""
            groupCountLabel.text = "0"
            setExpandStatus(false)
        }
    }
    
    /**
     Sets expand image based on expanded status.
     
     - parameter isExpanded: Wheter expanded or not.
     
     - returns: void.
     */
    func setExpandStatus(_ isExpanded : Bool) {
        
        if isExpanded == true {
            self.nextIndicatorImageView.image  = UIImage.imageFor(name: "up_arrow")
        } else {
            self.nextIndicatorImageView.image = UIImage.imageFor(name: "down_arrow")
        }
    }
}
