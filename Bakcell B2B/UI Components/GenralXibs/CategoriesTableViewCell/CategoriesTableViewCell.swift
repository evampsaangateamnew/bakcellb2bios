//
//  CategoriesTableViewCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/21/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
class CategoriesTableViewCell: UITableViewCell {
 
    //MARK: - IBOutlets
    @IBOutlet weak var categoryTitleLabel: MBLabel?
    @IBOutlet weak var categoryImageView: UIImageView?
    
    //MARK: - Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /**
     Sets Item labels values.
     
     - parameter aItem: Category details.
     
     - returns: void.
     */
    func setItemInfo(aItem: Items) {
        categoryTitleLabel?.text = aItem.title
        categoryImageView?.image = UIImage.imageFor(name: self.imageNameFor(Identifier: aItem.identifier))
    }
    
    /**
     Sets category labels values.
     
     - parameter aCategory: Category details.
     
     - returns: void.
     */
    func setCategoryInfo(aCategory: Category) {
        categoryTitleLabel?.text = aCategory.name
        categoryImageView?.image = UIImage.imageFor(name: aCategory.image)
    }
    
    /**
     Provide image name against identifiers. which are mapped.
     
     - parameter identifier: key for mapped image.
     
     - returns: image name for mapped identifier.
     */
    func imageNameFor(Identifier identifier : String) -> String {
        
        switch identifier.lowercased() {
            
        case "packages":
            return "packages"
            
        case "change_group":
            return "change-group"
            
        case "change_sim":
            return "change-sim"
            
        case "invoice_information":
            return "invoiceinfo"
            
        case "action_history":
            return "action-history"
            
        case "broadcast_sms":
            return "broadcast-SMS"
            
        case "change_limit":
            return "change-limit"
            
        case "top_up":
            return "services_topup"
            
        case "core_services":
            return "core-services"
            
        case "tarrifs":
            return "tarrifs"
            
        case "cug":
            return "cug"
            
        default:
            return ""
            
        }
    }
}
