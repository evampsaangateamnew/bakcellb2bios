//
//  UsersCell.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/28/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class UsersCell: UITableViewCell {
    
    @IBOutlet var nameLabel: MBMarqueeLabel!
    @IBOutlet var msisdnLabel: MBLabel!
    @IBOutlet var groupTypeLabel: MBLabel!
    @IBOutlet var indicatorImageView: UIImageView!
    @IBOutlet weak var myContentView: UIView!
    
    @IBOutlet var myContentViewLeadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setLayout(aUser : UsersData?, showGroupName:Bool = true, showIndicator:Bool = true) {
        
        if let aUserObject = aUser {
            nameLabel.text = aUserObject.custFullName
            msisdnLabel.text = aUserObject.msisdn
            
            if showGroupName {
                groupTypeLabel.text = aUserObject.groupNameDisp
                myContentViewLeadingConstraint.constant = 0
            } else {
                groupTypeLabel.text = ""
                myContentViewLeadingConstraint.constant = 16
            }
            
            if showIndicator {
                indicatorImageView.isHidden = false
                indicatorImageView.image = UIImage.imageFor(name: "cell_arrow")
            } else {
                indicatorImageView.isHidden = true
            }
        } else {
            nameLabel.text = ""
            msisdnLabel.text = ""
            groupTypeLabel.text = ""
        }
    }
    
}
