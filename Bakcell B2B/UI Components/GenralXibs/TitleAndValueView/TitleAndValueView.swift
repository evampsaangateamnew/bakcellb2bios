//
//  TitleAndValueView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/30/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class TitleAndValueView: UIView {

    //MARK: - IBOutlets
    //    AttributeListView iner outlets
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var valueLabel: MBLabel!
  
    //MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /**
     Sets value for Title and value.
     
     - parameter title: title text.
     - parameter value: value text.
     - parameter titleTextColor: title text color.
     - parameter valueTextColor: value text color.
     
     - returns: void.
     */
    func setTitleAndValue(_ title:String?, titleTextColor:UIColor = .mbTextGray, value:String?, valueTextColor:UIColor = .mbTextGray) {
        titleLabel.text = title ?? ""
        valueLabel.text = value ?? ""
        
        titleLabel.textColor = titleTextColor
        valueLabel.textColor = valueTextColor
        
        
    }
}


