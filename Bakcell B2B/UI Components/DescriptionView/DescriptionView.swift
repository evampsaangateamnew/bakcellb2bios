//
//  DescriptionView.swift
//  NoDataFoundView
//
//  Created by AbdulRehman Warraich on 4/30/18.
//  Copyright © 2018 AbdulRehman Warraich. All rights reserved.
//

import UIKit

class DescriptionView: UIView {
    
    @IBOutlet weak var descriptionImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: MBLabel!
    
    func setDescriptionViewWithImage(_ imageName:String = "step-3-info", description: String?) {
        
        self.tag = 998877
        descriptionImageView.image = UIImage.imageFor(name: imageName)
        descriptionLabel.text = description ?? ""
        self.layoutIfNeeded()
    }
}

