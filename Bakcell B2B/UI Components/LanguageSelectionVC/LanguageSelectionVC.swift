//
//  LanguageSelectionVC.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 6/27/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

class LanguageSelectionVC: BaseVC {
    
    typealias MBLanguageCompletionHandler = (_ currentLanguage:Constants.MBLanguage) -> ()
    
    //MARK: - Properties
    var selectedLanguage : Constants.MBLanguage = Constants.defaultAppLanguage
    fileprivate var doneBlock : MBLanguageCompletionHandler?
    
    //MARK: - IBOutlet
    
    @IBOutlet var titleLabel: MBLabel!
    @IBOutlet var check1Button: UIButton!
    @IBOutlet var check2Button: UIButton!
    @IBOutlet var check3Button: UIButton!
    
    @IBOutlet var doneButton: UIButton!
    
    
    //MARK: - View Controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localized("PleaseSelect_title")
        doneButton.setTitle(Localized("BtnTitle_Done"), for: UIControl.State.normal)
 
        // Selecting current selected language
        selectLanguage(senderType : selectedLanguage)
    }
    
    
    
    //MARK: - FUNCTIONS
    
    private func selectLanguage(senderType : Constants.MBLanguage) {
        
        check1Button .setImage(UIImage.imageFor(name: "checkbox_state2"), for: UIControl.State.normal)
        check2Button .setImage(UIImage.imageFor(name: "checkbox_state2"), for: UIControl.State.normal)
        check3Button .setImage(UIImage.imageFor(name: "checkbox_state2"), for: UIControl.State.normal)
        
        switch senderType {
        case .azeri:
            check1Button .setImage(UIImage.imageFor(name: "checkbox_state1"), for: UIControl.State.normal)
            selectedLanguage = Constants.MBLanguage.azeri
            
        case .russian:
            check2Button .setImage(UIImage.imageFor(name: "checkbox_state1"), for: UIControl.State.normal)
            selectedLanguage = Constants.MBLanguage.russian
            
        case .english:
            
            check3Button .setImage(UIImage.imageFor(name: "checkbox_state1"), for: UIControl.State.normal)
            selectedLanguage = Constants.MBLanguage.english
            
        }
    }
    
    func setLanguageSelectionAlert(_ currentLanguage:Constants.MBLanguage, completionBlock : @escaping MBLanguageCompletionHandler ) {
        
        selectedLanguage = currentLanguage
        self.doneBlock = completionBlock
        
    }
    
    //MARK: - IBACTIONS
    
    @IBAction func donePressed(_ sender: UIButton) {
        
        self.doneBlock?(selectedLanguage)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func check1Pressed(_ sender: Any) {
        selectLanguage(senderType : Constants.MBLanguage.azeri)
    }
    
    @IBAction func check2Pressed(_ sender: Any) {
        selectLanguage(senderType : Constants.MBLanguage.russian)
    }
    
    @IBAction func check3Pressed(_ sender: Any) {
        selectLanguage(senderType : Constants.MBLanguage.english)
        
    }
}
