//
//  MBSwitch.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/15/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class MBSwitch: UISwitch {

    var sectionTag : Int = 0
    var rowTag : Int = 0

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.onTintColor = UIColor.mbBrandRed
        self.backgroundColor = UIColor.mbButtonBackgroundGray
        self.layer.cornerRadius = 16.0;
    }
}
