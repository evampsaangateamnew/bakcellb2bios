//
//  MBLabel.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 5/23/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

//@IBDesignable
class MBLabel: UILabel {
 
    enum LabelType: Int {
        case other = 0
        case header = 1
        case subheader = 2
        case title = 3
        case body = 4
        case buttonTitleLarge = 5
        case buttonTitleSmall = 6
        case description = 7
        case viewControllerTitle = 8
    }
    
    // MARK: - Life Cycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        adjustsFontSizeToFitDevice()
        self.textColor = labelTextColor
    }
    
    // Programmatically: use the enum
    var labelFontType:LabelType = .body
    
    // IB: use the adapter
    @IBInspectable var labelType:Int {
        get {
            return self.labelFontType.rawValue
            
        } set( labelTypeIndex) {
            
            self.labelFontType = LabelType(rawValue: labelTypeIndex) ?? .body
            adjustsFontSizeToFitDevice()
        }
    }
    @IBInspectable var labelTextColor: UIColor = .mbTextGray {
        didSet {
            self.textColor = labelTextColor
        }
    }
    
    func adjustsFontSizeToFitDevice() {
        
        var labelNewFont : UIFont = UIFont.mbArial(fontSize: 14)
        switch labelFontType {
        case .header:
            labelNewFont = UIFont.mbArialBold(fontSize: 40)
        case .subheader:
            labelNewFont = UIFont.mbArialBold(fontSize: 18)
        case .title:
            labelNewFont = UIFont.mbArialBold(fontSize: 14)
        case .body:
            labelNewFont = UIFont.mbArial(fontSize: 14)
        case .buttonTitleLarge:
            labelNewFont = UIFont.mbArialBold(fontSize: 16)
        case .buttonTitleSmall:
            labelNewFont = UIFont.mbArialBold(fontSize: 14)
        case .description:
            labelNewFont = UIFont.mbArial(fontSize: 12)
        case .viewControllerTitle:
            labelNewFont = UIFont.mbArial(fontSize: 20)
        case .other:
            labelNewFont = self.font
//        default:
//            labelNewFont = UIFont.mbArial(fontSize: 14)
        }
        
        font = labelNewFont.withSize(labelNewFont.pointSize + UIDevice().additionalFontSizeAccourdingToScreen)
    }
    
    func setLabelFontType(_ type: LabelType?) {
        if let newLabelFontType = type {
            self.labelType = newLabelFontType.rawValue
        }
    }
    
    func loadHTMLString(htmlString: String) {
        if let htmlData = htmlString.data(using: String.Encoding.unicode) {
            do {
                let attributedString = try NSMutableAttributedString(data: htmlData,
                                                                     options: [.documentType: NSAttributedString.DocumentType.html],
                                                                     documentAttributes: nil)
                
                attributedString.addAttribute(NSAttributedString.Key.font,
                                              value: self.font ?? UIFont.systemFont(ofSize: UIFont.systemFontSize),
                                              range: NSRange(location: 0 , length: attributedString.length))
                
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor,
                                              value: self.textColor ?? UIColor.mbTextGray,
                                              range: NSRange(location: 0 , length: attributedString.length))
                
                self.attributedText = attributedString
                
                
            } catch let e as NSError {
                print("Couldn't parse \(htmlString): \(e.localizedDescription)")
                
                self.text = ""
            }
        }
    }
}



