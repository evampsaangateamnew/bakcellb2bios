//
//  MBTextField.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 5/23/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import SnapKit

//@IBDesignable
class MBTextField: UITextField {
    
    
    
    enum TextFieldLayoutType: Int {
        case grayTextField = 1
        case whiteTextField = 2
        case other = 0
    }
    fileprivate var rightButtonCompletionBlock : MBButtonCompletionHandler = {}
    
    
    // MARK: - Properties
    
    weak var textFieldTipView: EasyTipView?
    
    @IBInspectable var tipViewText : String?
    
    @IBInspectable var placeHolderColor : UIColor = .mbPlaceholderGray {
        didSet {
            
            if let placeholderText = self.placeholder {
                
                self.attributedPlaceholder = NSAttributedString(string: placeholderText,
                                                                attributes: [NSAttributedString.Key.foregroundColor: placeHolderColor])
            }
        }
    }
    
    
    
    var textFieldLayoutTypeInfo : TextFieldLayoutType = .other
    
    
    @IBInspectable var textFieldLayoutType:Int {
        get {
            return self.textFieldLayoutTypeInfo.rawValue
            
        } set( typeIndex) {
            
            self.textFieldLayoutTypeInfo = TextFieldLayoutType(rawValue: typeIndex) ?? .other
            adjustsTextFieldLayout()
        }
    }
    
    @IBInspectable var leftImageName : String? {
        didSet {
            
            let containorView = UIView()
            
            if let image = UIImage(named: leftImageName ?? "") {
                containorView.frame.size = CGSize(width:self.frame.height + 20, height: self.frame.height)
                let imageView = UIImageView(image: image);
                imageView.contentMode = .scaleAspectFit
                containorView.addSubview(imageView)
                imageView.snp.makeConstraints { (make) -> Void in
                    make.top.equalTo(containorView.snp.top)
                    make.bottom.equalTo(containorView.snp.bottom)
                    make.right.equalTo(containorView.snp.right)
                    make.left.equalTo(containorView.snp.left).offset(20)
                    make.width.equalTo(self.frame.height)
                    make.height.equalTo(self.frame.height)
                }
                
            } else {
                containorView.frame.size = CGSize(width:12, height: self.frame.height)
            }
            self.leftView = containorView;
            self.leftViewMode = .always
        }
    }
    
    @IBInspectable var rightImageName : String? {
        didSet {
            
            let containorView = UIView()
            
            
            if let image = UIImage(named: rightImageName ?? "") {
                containorView.frame.size = CGSize(width:self.frame.height + 20, height: self.frame.height)
                let button = UIButton(type: .custom)
                button.setImage(image, for: .normal)
                button.addTarget(self, action: #selector(self.rightButtonAction), for: .touchUpInside)
                //  let imageView = UIImageView(image: image);
                button.imageView?.contentMode = .scaleAspectFit
                containorView.addSubview(button)
                button.snp.makeConstraints { (make) -> Void in
                    make.top.equalTo(containorView.snp.top)
                    make.bottom.equalTo(containorView.snp.bottom)
                    make.right.equalTo(containorView.snp.right).offset(-20)
                    make.left.equalTo(containorView.snp.left)
                    make.width.equalTo(self.frame.height)
                    make.height.equalTo(self.frame.height)
                }
                
                
            } else {
                containorView.frame.size = CGSize(width:12, height: self.frame.height)
            }
            self.rightView = containorView;
            self.rightViewMode = .always
        }
    }
    
    // MARK: - Life Cycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.adjustsFontSizeToFitDevice()
    }
 
    func adjustsTextFieldLayout() {
        
        switch textFieldLayoutTypeInfo {
        case .grayTextField:
            self.backgroundColor = UIColor.mbBackgroundGray
            self.borderStyle = .roundedRect
            self.textColor = UIColor.mbTextFieldTextGray
            self.placeHolderColor = .mbPlaceholderGray
            //  self.layer.borderColor = UIColor.red.cgColor
            //  self.layer.borderWidth = 1;
            //  self.layer.masksToBounds = true;
            //  self.layer.cornerRadius = Constants.kTextFieldCornerRadius
            
            
            break
            
        case .whiteTextField:
            self.backgroundColor = UIColor.white
            self.borderStyle = .roundedRect
            self.textColor = UIColor.mbTextFieldTextGray
            self.placeHolderColor = .mbPlaceholderGray
            
            break
            
        default:
            break
        }
    }
    
    @IBAction func rightButtonAction(_ sender: UIButton) {
        
        if tipViewText != nil {
            if let tipView = textFieldTipView {
                tipView.dismiss(withCompletion: {
                    self.textFieldTipView = nil
                })
            } else {
                var myPreferences = EasyTipView.globalPreferences
                myPreferences.drawing.backgroundColor = UIColor.mbBrandRed
                myPreferences.animating.showInitialAlpha = 0
                myPreferences.positioning.bubbleHInset = 20
                myPreferences.drawing.arrowPosition = .bottom
                
                let newTipView = EasyTipView(text: tipViewText ?? "", preferences: myPreferences)
                newTipView.show(animated: true,
                                forView: sender,
                                withinSuperview: self.superview)
                
                textFieldTipView = newTipView
            }
        }
        
        if self.isFirstResponder == true {
            _ = self.resignFirstResponder()
        } else {
            self.becomeFirstResponder()
        }
        
        // Call completion block
        rightButtonCompletionBlock()
        
    }
    
    override func resignFirstResponder() -> Bool {
        
        textFieldTipView?.dismiss(withCompletion: {
            self.textFieldTipView = nil
        })
        
        return super.resignFirstResponder()
    }
    
    func setRightButtonCompletionBlock(_ rightButtonBlock : @escaping MBButtonCompletionHandler) {
        rightButtonCompletionBlock = rightButtonBlock
    }
}

