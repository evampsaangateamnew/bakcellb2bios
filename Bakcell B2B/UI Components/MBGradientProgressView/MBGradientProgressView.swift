//
//  MBGradientProgressView.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/15/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import UIKit

class MBGradientProgressView: UIProgressView {

    lazy private var gradientLayer: CAGradientLayer! = self.initGradientLayer()
    lazy private var alphaLayer: CALayer! = self.initAlphaLayer()

    override init (frame : CGRect) {
        super.init(frame : frame)

        self.initColors()
        self.layer.insertSublayer(self.gradientLayer, at: 0)
        
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initColors()
        self.layer.insertSublayer(self.gradientLayer, at: 0)
    }

    func initColors() {
        self.backgroundColor = UIColor(hexString:"#e5e9eb")
        self.trackTintColor = UIColor.clear
        self.progressTintColor = UIColor.clear
    }

    // MARK: Lazy initializers

    /**
     Create and returns a CAGradientLayer.
     
     - parameter gradientColors: Gradiant colors array.
     
     - returns: CAGradientLayer.
     */
    func initGradientLayer(gradientColors: Array<CGColor> = [ UIColor(hexString:"#8C002B").cgColor, UIColor.mbBrandRed.cgColor ]) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds

        gradientLayer.anchorPoint = CGPoint(x: 0, y: 0)
        gradientLayer.position = CGPoint(x: 0, y: 0)

        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0);
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0);

        gradientLayer.colors = gradientColors

        gradientLayer.mask = self.alphaLayer
        gradientLayer.cornerRadius = 3

        return gradientLayer
    }

    /**
     Change Gradiant layer colors.
     
     - parameter newGradientColors: Gradiant colors array.
     - parameter newBackgroundColor: View back ground color.
     
     - returns: Void.
     */
    func changeGradientLayerColors(newGradientColors: Array<CGColor> = [ UIColor(hexString:"#8C002B").cgColor, UIColor.mbBrandRed.cgColor ], newBackgroundColor: UIColor = UIColor(hexString:"#e5e9eb")) {

        self.backgroundColor = newBackgroundColor

        let newGradientLayer: CAGradientLayer = self.initGradientLayer(gradientColors: newGradientColors)

        self.layer.replaceSublayer(gradientLayer, with: newGradientLayer)

        gradientLayer = newGradientLayer

    }

    /**
     Create and return a layer.
     
     - returns: CALayer.
     */
    func initAlphaLayer() -> CALayer {
        let alphaLayer = CALayer()
        alphaLayer.frame = self.bounds

        alphaLayer.anchorPoint = CGPoint(x: 0, y: 0)
        alphaLayer.position = CGPoint(x: 0, y: 0)

        alphaLayer.backgroundColor = UIColor.white.cgColor

        return alphaLayer
    }

    // MARK: Layout

    func updateAlphaLayerWidth() {
        self.alphaLayer.frame =
            self.bounds.sizeByPercentage(width: CGFloat(self.progress))
    }

    override public func layoutSubviews() {
        super.layoutSubviews()

        self.gradientLayer.frame = self.bounds
        self.updateAlphaLayerWidth()
    }

    /**
     Set progress bar value.
     
     - parameter prgress : prgress value.
     - parameter animated: Animation or not.
     
     - returns: Void.
     */
    override public func setProgress(_ progress: Float, animated: Bool) {

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            // Your code with delay
            super.setProgress(progress, animated: animated)
            self.updateAlphaLayerWidth()
        }
    }
}

