//
//  Constants.swift
//  WebservicesWrapper
//
//  Created by AbdulRehman Warraich on 05/30/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

typealias MBButtonCompletionHandler = () -> ()
typealias MBButtonWithParamCompletionHandler = (_ gernralString : String) -> Void

enum UserSelectedType {
    case individualList
    case groupList
}
enum GroupTypes :String {
    case paybySubs  = "PaybySubs"
    case partPay    = "PartPay"
    case fullPay    = "FullPay"
    
    func localizedString() -> String {
        return Localized(self.rawValue)
    }
    
    static func getTitleFor(title:GroupTypes) -> String {
        return title.localizedString()
    }
}

//APIs Key
enum APIsType : String {
    //Home page
    case homePage = "Home_Page"
    case PICUserInformation = "PIC_User_Information"
    case usersData = "User_Data"
    case groupData = "Group_Data"
    case queryInvoiceResponseData = "Query_Invoice_Response"
    case queryBalanceResponseData = "Query_Balance_Response"
    
    //MySubscriptions
    case mySubscriptions = "My_Subscriptions"
    case supplementaryOffer = "Supplementary_Offer"
    
    //Tarrifs
    case tariffDetails = "Tariff_Details"
    
    // StoreLocator
    case storeDetails = "Store_Details"
    
    case faqDetails = "FAQ_Details"
    case contactUsDetail = "Contact_Us_Detail"
    case notificationConfigurations = "notification_Configurations"
    case notificationData = "notification_Data"
    case notificationCount = "notification_Count"
    case horizontalMenu = "Horizontal_Menu"
    case VerticalMenu = "Vertical_Menu"
    case appMenu = "App_Menu"
    case terms_Conditions = "terms_Conditions"
    
    func localizedAPIKey() -> String {
        return self.rawValue.localizedAPIKey()
    }
    func keyValue() -> String {
        return self.rawValue
    }
}

class Constants {
    
    class var kMBAPIClientBaseURL:String {
        
        //  Development URL
        
        //  return "http://10.220.48.130:8080/bakcellappserver/api/"
        //  Staging URL
//         return "https://myappstg.bakcell.com:16444/bakcellappserverv2/api/"
        
        //https://myappstg.bakcell.com:16444/bakcellappserverv2
        
        //  Production URL
        return "https://myapp.bakcell.com/bakcellappserverv2/api/"
    }
    
    class var kRequestHeaders : [String : String] {
        return ["Content-Type": "application/json",
                "deviceID": UIDevice.deviceID(),
                "UserAgent": "iPhone",
                "isFromB2B": "true",
                "lang": MBLanguageManager.userSelectedLanguage().rawValue,
                "token": MBPICUserSession.shared.token,
                "subscriberType": "",
                "tariffType": "",
                "msisdn" :MBPICUserSession.shared.msisdn]
    }
    
    class var liveChatURL :String {
        
        if let savedLiveChatURL = UserDefaults.standard.string(forKey: kLiveChatURLString),
            savedLiveChatURL.isEmpty == false {
            print("\n\nLivechat URL: \(savedLiveChatURL)\n\n")
            return savedLiveChatURL.urlEncode
        }
        
        return ""
    }
    
    //Live chat
    class var liveChatUrlRequest : URLRequest? {
        // prepare json data
        let json: [String: String] = ["authenticate": "Ev@mp1iv3Ch@t" ]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json)
            
            // create post request
            if let url = URL(string: liveChatURL) {
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                
                // insert json data to the request
                request.httpBody = jsonData
                return request
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    class var MBAPIClientDefaultTimeOut: Double {
        return 20.0
    }
    
    // API Status code
    enum MBAPIStatusCode: String {
        case succes                         = "00"
        case forceUpdate                    = "8"
        case optionalUpdate                 = "9"
        case serverDown                     = "10"
        case sessionExpired                 = "7"
        case wrongAttemptToChangePassword   = "104"
    }
    
    enum MBPasswordStrength: String {
        case didNotMatchCriteria    = "0"
        case Week                   = "1"
        case Medium                 = "2"
        case Strong                 = "3"
    }
    
    
    enum OrderStatus : String {
        case processed = "P"
        case completed = "C"
        case new = "N"
        case failed = "F"
        case unknow = "unknow"
        
        func localizedString() -> String {
            return Localized(self.rawValue)
        }
    }
    
    // Resend Type
    enum MBResendType : String {
        case ForgotPassword     = "forgotpassword"
        case SignUp             = "signup"
        case UsageHistory       = "usagehistory-details-view"
    }
    
    // Language types of Application
    enum MBLanguage: String {
        case russian = "2"
        case english = "3"
        case azeri   = "4"
    }
    
    // Notification sound setting
    enum MBNotificationSoundType : String {
        case Tone       = "tone"
        case Vibrate    = "vibrate"
        case Mute       = "mute"
    }
    
    enum MBSubscriberType : String {
        case PrePaid = "Prepaid"
        case PostPaid = "Postpaid"
        case UnSpecified = ""
    }
    
    enum MBCustomerType : String {
        case Individual = "Individual Customer"
        case Corporate = "Corporate Customer"
        case UnSpecified = ""
    }
    
    enum MBBrandName : String {
        case CIN = "CIN"
        case Klass = "Klass"
        case Business = "Business"
        case UnSpecified = ""
    }
    
    class var K_PayBySUBs :String {
        return "PayBySUBs"
    }
    class var K_PayBySUB :String {
        return "PayBySUB"
    }
    //Date formates
    
    
    class var kDisplayFormat: String {
        return "dd/MM/yy"
    }
    
    class var kAPIFormat: String {
        return "yyyy-MM-dd"
    }
    
    class var kHomeDateFormate: String {
        return "yyyy-MM-dd HH:mm:ss"
    }
    
    class var kNumberDateFormate: String {
        return  "dd/MM/yy"
    }
    
    class var kInviceDateFormate: String {
        return  "dd/MM/yyyy"
    }
    
    //  Constant values
    
    class var defaultAppLanguage: MBLanguage {
        return .azeri
    }
    
    class var kButtonCornerRadius: CGFloat {
        return 16.0
    }
    
    class var kViewCornerRadius: CGFloat {
        return 6.0
    }
    
    class var kTextFieldCornerRadius: CGFloat {
        return 4.0
    }
    
    class var SPLASH_DURATION: Double {
        return 1.0
    }
    
    class var minUserLength: Int {
        return 8
    }
    class var maxUserLength: Int {
        return 15
    }
    
    class var passwordLength: Int {
        return 15
    }
    
    class var OTPLength: Int {
        return 4
    }
    
    class var PINVerifyBackTime: Int {
        return 25
    }
    
    class var AzriAndRussianFreeSMSLength : Int {
        return 70
    }
    
    class var EnglishFreeSMSLength : Int  {
        return 160
    }
    
    class var MSISDNLength : Int  {
        return 9
    }
    
    class var ValidCardNumberLength : Int  {
        return 15
    }
    
    class var VerifyPinLenght : Int  {
        return 4
    }
    
    class var SevenDigitPinLength : Int  {
        return 7
    }
    
    class var SerialNumberMinLength : Int  {
        return 8
    }
    
    class var SerialNumberMaxLength : Int  {
        return 9
    }
    
    class var ICCIDLength : Int  {
        return 20
    }
    
    class var allowedNumbers: String {
        return "0123456789"
    }
    
    class var allowedAlphbeats: String {
        return allowedCapitalAlphabets + allowedSmallAlphabets
    }
    
    class var allowedSmallAlphabets: String {
        return allowedCapitalAlphabets.lowercased()
    }
    
    class var allowedCapitalAlphabets: String {
        return "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    }
    
    class var allowedSpecialCharacters: String {
        return "~`!@#$%^&*()_+=-]}[{|'\";:/?.>,<"
    }
    class var allowedSpecialCharactersForPIC: String {
        return "._-"
    }
    
    //New_App_Check Keys
    class var kIsLaunchedBefore: String {
        return "kIsLaunchedBefore"
    }
    
    class var kIsUserLoggedInKey: String {
        return "isUserLoggedIn"
    }
    
    class var kUserSelectedLanguage: String {
        return "UserSelectedLanguage"
    }
    
    class var kSingleValueKey: String {
        return "Single value"
    }
    
    class var kDoubleValueKey: String {
        return "Double values"
    }
    
    class var kDoubleTitlesKey: String {
        return "Double titles"
    }
    
    class var kIsFCMIdAdded: String {
        return "IsFCMIdAdded"
    }
    
    class var kIsFCMIdAddRequestFromLogin: String {
        return "IsFCMIdAddedRequestFromLogin"
    }
    
    class var kIsNotificationEnabled: String {
        return "IsNotificationEnabled"
    }
    
    //MARK:- Notifications Keys
    class var kAppDidBecomeActive: String {
        return "appDidBecomeActive"
    }
    
    class var KN_ReloadHomePage :String {
        return "ReloadHomePage"
    }
    
    class var kLiveChatURLString: String {
        return "LiveChatURLString"
    }
    
    class var kUsersCount: String {
        return "UsersCountOfPIC"
    }
    
    class var K_I_Called_You_Turned_Off :String {
        return "984855324"
    }
    
    class var K_I_Am_Back :String {
        return "102341307"
    }
    
    class var K_Call_Forward :String {
        return "165811270"
    }
    
    class var K_RateUsTitle :String {
        return "RateUsTitle"
    }
    
    class var K_RateUsMessage :String {
        return "RateUsMessage"
    }
    
    class var K_AppStoreURL :String {
        return "AppStoreURL"
    }
    
    class var K_UserLoggedInTime :String {
        return "UserLoggedInTime"
    }
    
    class var K_RateUsLaterTime :String {
        return "RateUsLaterTime"
    }
    
    class var K_IsRateUsShownBefore :String {
        return "IsRateUsShownBefore"
    }
    class var K_IsBiometricEnabled :String {
        return "IsBiometricEnabled"
    }
    
}
