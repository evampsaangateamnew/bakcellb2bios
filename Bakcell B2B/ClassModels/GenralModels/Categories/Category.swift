//
//  Category.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/21/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

/** Category contain its name an image name. */

class Category: Mappable {

    var name = ""
    var image  = ""
    var identifier = ""
    
    init(Name newName:String, ImageName imageName:String, Idenitifier identifier: String) {
        name = newName
        image = imageName
        self.identifier = identifier
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["Name"]
        image <- map["ImageName"]
        identifier <- map["identifier"]
    }
    

}
