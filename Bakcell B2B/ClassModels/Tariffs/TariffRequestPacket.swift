//
//  TariffRequestPacket.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 1/17/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct TariffRequestPacket : Mappable {
    var tariffId : String?
    var msisdn : String?
    var groupType : String?
    var corpCrmCustName :String?
    
    init?(map: Map) {
        
    }
    init(tariffId:String?, msisdn:String?, groupType:String?, corpCrmCustName:String?) {
        self.tariffId = tariffId
        self.msisdn = msisdn
        self.groupType = groupType
        self.corpCrmCustName = corpCrmCustName
    }
    
    mutating func mapping(map: Map) {
        
        tariffId    <- map["tariffIds"]
        msisdn      <- map["msisdn"]
        groupType   <- map["groupType"]
        corpCrmCustName <- map["corpCrmCustName"]
        
        /*
        tariffId    <- map["tariffIds"]
        msisdn      <- map["msisdn"]
        groupType   <- map["groupType"]
        */
    }
    
}
