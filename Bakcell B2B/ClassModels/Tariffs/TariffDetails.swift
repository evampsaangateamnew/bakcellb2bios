//
//  TariffDetails.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/14/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

enum MBTariffType : String {
    case klass = "klass"
    case cin = "Cin"
    case individual = "individual"
    case corporate = "BusinessCorporate"
    case klassPostpaid = "klassPostpaid"
    case unSpecified = "unSpecified"
}

enum MBSectionType: String {
    case Header         = "Header"
    case PackagePrice   = "PackagePrice"
    case PaygPrice      = "PaygPrice"
    case Price          = "Price"
    case Detail         = "Detail"
    case Description    = "Description"
    case Subscription   = "Subscription"
    case DeActivate     = "DeActivate"
    case UnSpecified    = ""
}



////////////////////////////////////////////////////////////////////////////////////////
struct  TariffsData: Mappable {
    
    var klassTariffData : [NewKlass]?
    var cinTariffData : [NewCin]?
    var businessCorporateTariffData : [NewCorporate]?
    var businessIndividualTariffData : [NewIndividual]?
    var klassPostpaidTariffData : [NewKlassPostpaid]?
    
    var tarrifs : [KeyAny] = []
    
    init?(map : Map) {
        
    }
    
    mutating func mapping(map: Map) {
        klassTariffData                     <- map["klassTariffData"]
        cinTariffData                       <- map["cinTariffData"]
        businessCorporateTariffData         <- map["businessCorporateTariffData"]
        businessIndividualTariffData        <- map["businessIndividualTariffData"]
        klassPostpaidTariffData             <- map["klassPostpaidTariffData"]
        
        mapAndSortAllTariffs()
        
    }
    
    mutating func mapAndSortAllTariffs() {
        klassTariffData?.forEach({ (aKlassObject) in
            let aTariffObject : KeyAny = KeyAny.init(tariffType: .klass, sortOrder: aKlassObject.sortOrder, value: aKlassObject.klass)
            
            tarrifs.append(aTariffObject)
        })
        
        cinTariffData?.forEach({ (aCINObject) in
            let aTariffObject : KeyAny = KeyAny.init(tariffType: .cin, sortOrder: aCINObject.sortOrder, value: aCINObject.cin)
            
            tarrifs.append(aTariffObject)
        })
        
        businessCorporateTariffData?.forEach({ (aCorporateObject) in
            let aTariffObject : KeyAny = KeyAny.init(tariffType: .corporate, sortOrder: aCorporateObject.sortOrder, value: aCorporateObject.corporate)
            
            tarrifs.append(aTariffObject)
        })
        
        businessIndividualTariffData?.forEach({ (aIndividualObject) in
            let aTariffObject : KeyAny = KeyAny.init(tariffType: .individual, sortOrder: aIndividualObject.sortOrder, value: aIndividualObject.individual)
            
            tarrifs.append(aTariffObject)
        })
        
        klassPostpaidTariffData?.forEach({ (aklassPostpaidObject) in
            let aTariffObject : KeyAny = KeyAny.init(tariffType: .klassPostpaid, sortOrder: aklassPostpaidObject.sortOrder, value: aklassPostpaidObject.klasspostpaid)
            
            tarrifs.append(aTariffObject)
        })
        
        
        tarrifs.sort{ $0.sortOrder < $1.sortOrder }
    }
    
}


struct NewKlass: Mappable {
    
    var sortOrder : Int = 0
    var tariffType : String?
    var klass : Klass?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        sortOrder       <- map["sortOrder"]
        tariffType      <- map["tariffType"]
        klass           <- map["klass"]
    }
}

struct NewCin: Mappable {
    
    var sortOrder : Int = 0
    var tariffType : String?
    var cin : Cin?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        sortOrder       <- map["sortOrder"]
        tariffType      <- map["tariffType"]
        cin             <- map["cin"]
    }
}


struct NewKlassPostpaid: Mappable {
    
    var sortOrder : Int = 0
    var tariffType : String?
    var klasspostpaid : KlassPostpaid?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        sortOrder       <- map["sortOrder"]
        tariffType      <- map["tariffType"]
        klasspostpaid   <- map["klass"]
    }
}

struct NewCorporate: Mappable {
    
    var sortOrder : Int = 0
    var tariffType : String?
    var corporate : Corporate?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        sortOrder       <- map["sortOrder"]
        tariffType      <- map["tariffType"]
        corporate       <- map["corporate"]
    }
}

struct NewIndividual: Mappable {
    
    var sortOrder : Int = 0
    var tariffType : String?
    var individual : Individual?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        sortOrder       <- map["sortOrder"]
        tariffType      <- map["tariffType"]
        individual      <- map["individual"]
    }
}



////////////////////////////////////////////////////////////////////////////////////////

/*
class TariffsData: Mappable {
    
    var tarrifs : [KeyAny] = []
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        tarrifs  <- (map["data"],TariffTransform())
    }
    
}

open class TariffTransform: TransformType {
    
    
    public typealias Object = [KeyAny]
    public typealias JSON = Array<Any>

    open func transformFromJSON(_ value: Any?) -> [KeyAny]? {
        
        var processedObject:[KeyAny] = []
        
        if let aValue = value as? [AnyObject] {
            
            aValue.forEach { (aObject) in
                
                if let aObjectType = MBTariffType(rawValue: (aObject["tariffType"] as? String) ?? "" ) {
                    
                    if aObjectType == .klass {
                        if let klassObject = Mapper<Klass>().map(JSONObject: aObject) {
                            processedObject.append( KeyAny(tariffType: aObjectType, sortOrder: 0, value: klassObject))
                        }
                        
                    } else if aObjectType == .cin {
                        
                        if let cinObject = Mapper<Cin>().map(JSONObject: aObject) {
                            processedObject.append( KeyAny(tariffType: aObjectType, sortOrder: 0, value: cinObject))
                        }
                        
                    } else if aObjectType == .corporate {
                        
                        if let corporateObject = Mapper<Corporate>().map(JSONObject: aObject) {
                            processedObject.append( KeyAny(tariffType: aObjectType, sortOrder: 0, value: corporateObject))
                        }
                        
                    } else if aObjectType == .individual {
                        
                        if let individualObject = Mapper<Individual>().map(JSONObject: aObject) {
                            processedObject.append( KeyAny(tariffType: aObjectType, sortOrder: 0, value: individualObject))
                        }
                        
                    } else if aObjectType == .klassPostpaid {
                        
                        if let klassPostpaidObject = Mapper<KlassPostpaid>().map(JSONObject: aObject) {
                            processedObject.append( KeyAny(tariffType: aObjectType, sortOrder: 0, value: klassPostpaidObject))
                        }
                    }
                }
            }
        }
        
        return processedObject
    }
    
    open func transformToJSON(_ value: [KeyAny]?) -> Array<Any>? {
        
        var results = [Any]()
        value?.forEach({ (aObject) in
            
            if aObject.tariffType == .klass {
                if let klassObject = aObject.valueObject as? Klass {
                    results.append(klassObject.toJSON() as Any)
                }
                
            } else if aObject.tariffType == .cin {
                
                if let cinObject = aObject.valueObject as? Cin {
                    results.append(cinObject.toJSON() as Any)
                }
            } else if aObject.tariffType == .corporate {
                
                if let corporateObject = aObject.valueObject as? Corporate {
                    results.append(corporateObject.toJSON() as Any)
                }
            } else if aObject.tariffType == .individual {
                
                if let individualObject = aObject.valueObject as? Individual {
                    results.append(individualObject.toJSON() as Any)
                }
            } else if aObject.tariffType == .klassPostpaid {
                
                if let klassPostpaidObject = aObject.valueObject as? KlassPostpaid {
                    results.append(klassPostpaidObject.toJSON() as Any)
                }
            }
        })
        
        
        return results
        
    }
}*/

open class KeyAny {
    var tariffType : MBTariffType = .unSpecified
    var sortOrder : Int = 0
    var valueObject : AnyObject?
    var openedViewSection : MBSectionType = MBSectionType.Header
    
    init(tariffType :MBTariffType, sortOrder :Int, value :AnyObject?) {
        self.tariffType = tariffType
        self.sortOrder = sortOrder
        self.valueObject = value
        
    }
}




class Klass: Mappable {
    var myType : String?
    var header : TariffHeader?
    var packagePrice: PackagePrice?
    var paygPrice : PaygPrice?
    var details : DetailsWithPriceArray?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        myType          <- map["type"]
        header          <- map["header"]
        packagePrice    <- map["packagePrice"]
        paygPrice       <- map["paygPrice"]
        details         <- map["details"]
        
        
    }
    
}

class Cin: Mappable {
    //var myType : String?
    var header : CinHeader?
    var details: DetailsWithPriceArray?
    var description : TariffDescription?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        //myType          <- map["myType"]
        header          <- map["header"]
        details         <- map["details"]
        description     <- map["description"]
        
    }
    
}


class KlassPostpaid: Mappable {
    
    var header : TariffHeader?
    var packagePrice: PackagePriceKlassPostpaid?
    var details: DetailsWithPriceArray?
    
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        header          <- map["header"]
        details         <- map["details"]
        packagePrice    <- map["packagePrice"]
        
    }
}

class PackagePriceKlassPostpaid: Mappable {
    
    var packagePriceLabel : String = ""
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        packagePriceLabel   <- map["packagePriceLabel"]
        call                <- map["call"]
        sms                 <- map["sms"]
        internet            <- map["internet"]
        
    }
    
}


class DetailsWithPriceArray: Mappable {
    var detailLabel : String?
    var price : [Price]?
    var rounding : Rounding?
    var textWithTitle : TextWithTitle?
    var textWithOutTitle : String?
    var textWithPoints : [String?]?
    var date : DateAndTime?
    var time : DateAndTime?
    var roamingDetails : RoamingDetails?
    var freeResourceValidity : FreeResourceValidity?
    var titleSubTitleValueAndDesc : TitleSubTitleValueAndDesc?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        detailLabel                 <- map["detailLabel"]
        price                       <- map["price"]
        rounding                    <- map["rounding"]
        textWithTitle               <- map["textWithTitle"]
        textWithOutTitle            <- map["textWithOutTitle.description"]
        textWithPoints              <- map["textWithPoints.pointsList"]
        date                        <- map["date"]
        time                        <- map["time"]
        roamingDetails              <- map["roamingDetails"]
        freeResourceValidity        <- map["freeResourceValidity"]
        titleSubTitleValueAndDesc   <- map["titleSubTitleValueAndDesc"]
        
    }
}

class TariffHeader: Mappable {
    
    var id: String?
    var offeringId: String?
    var currency: String?
    var name: String?
    var priceLabel: String?
    var priceValue: String?
    var subscribable: String?
    var attributes: [HeaderAttributes]?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        id                           <- map["id"]
        offeringId                   <- map["offeringId"]
        currency                     <- map["currency"]
        name                         <- map["name"]
        priceLabel                   <- map["priceLabel"]
        priceValue                   <- map["priceValue"]
        subscribable                 <- map["subscribable"]
        attributes                   <- map["attributes"]
        
    }
}

class HeaderAttributes: Mappable {
    
    var iconName : String?
    var title : String?
    var value : String?
    var metrics : String?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        iconName              <- map["iconName"]
        title                 <- map["title"]
        value                 <- map["value"]
        metrics               <- map["metrics"]
        
    }
    
}

class PackagePrice: Mappable {
    
    var packagePriceLabel : String?
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        packagePriceLabel           <- map["packagePriceLabel"]
        call                        <- map["call"]
        sms                         <- map["sms"]
        internet                    <- map["internet"]
        
        
    }
    
}


class Attributes: Mappable {
    
    var title : String?
    var valueLeft : String?
    var valueRight : String?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        title               <- map["title"]
        valueLeft           <- map["valueLeft"]
        valueRight          <- map["valueRight"]
    }
    
}



class PaygPrice: Mappable {
    var paygPriceLabel : String?
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        paygPriceLabel              <- map["paygPriceLabel"]
        call                        <- map["call"]
        sms                         <- map["sms"]
        internet                    <- map["internet"]
    }
    
}


class CinHeader : Mappable {
    
    var id : String?
    var offeringId : String?
    var bonusDescription : String?
    var bonusIconName : String?
    var bonusTitle : String?
    var name : String?
    var subscribable : String?
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        id                                      <- map["id"]
        offeringId                              <- map["offeringId"]
        bonusDescription                        <- map["bonusDescription"]
        bonusIconName                           <- map["bonusIconName"]
        bonusTitle                              <- map["bonusTitle"]
        name                                    <- map["name"]
        subscribable                            <- map["subscribable"]
        call                                    <- map["call"]
        internet                                <- map["internet"]
        sms                                     <- map["sms"]
        
    }
    
}

class CallSMSPriceSection: Mappable {
    
    var iconName : String?
    var title : String?
    var titleValue : String?
    var titleValueLeft : String?
    var titleValueRight : String?
    var priceTemplate : String?
    var attributes : [CinCallAttributes]?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        iconName            <- map["iconName"]
        title               <- map["title"]
        titleValue          <- map["titleValue"]
        titleValueLeft      <- map["titleValueLeft"]
        titleValueRight     <- map["titleValueRight"]
        priceTemplate       <- map["priceTemplate"]
        attributes          <- map["attributes"]
        
    }
    
}

class CinCallAttributes: Mappable {
    
    var title : String?
    var value : String?
    var valueLeft : String?
    var valueRight : String?
    
    required init?(map : Map) {
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        valueLeft       <- map["valueLeft"]
        valueRight      <- map["valueRight"]
        
    }
    
}

class InternetPriceSection: Mappable {
    
    var iconName : String?
    var subTitle : String?
    var subTitleValue : String?
    var title : String?
    var titleValue : String?
    var attributes : [CinInternetAttributes]?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        iconName                           <- map["iconName"]
        subTitle                           <- map["subTitle"]
        subTitleValue                      <- map["subTitleValue"]
        title                              <- map["title"]
        titleValue                         <- map["titleValue"]
        
    }
    
}
class CinInternetAttributes: Mappable {
    
    var title : String?
    var value : String?
    
    required init?(map : Map) {
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        
    }
    
}


class CinSMSAttributes: Mappable {
    
    var title : String?
    var value : String?
    
    required init?(map : Map) {
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        
    }
    
}


class CinMMSAttributes: Mappable {
    
    var title : String?
    var value : String?
    
    required init?(map : Map) {
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        
    }
    
}

class DetailMMSCallDestination: Mappable {
    
    
    var attributes : [CinCallDetailsAttribute]?
    var iconName : String?
    var title : String?
    var titleValue : String?
    
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        attributes           <- map["attributes"]
        iconName             <- map["iconName"]
        title                <- map["title"]
        titleValue           <- map["titleValue"]
        
    }
}

class CinCallDetailsAttribute: Mappable {
    
    var title : String?
    var value : String?
    
    required init?(map : Map) {
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        
    }
    
}

class CinDestinationDetailsAttribute: Mappable {
    
    var title : String?
    var value : String?
    
    required init?(map : Map) {
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        
    }
    
}

class TariffDescription: Mappable {
    
    var descriptionTitle : String?
    var advantages : Advantages?
    var classification : Classification?
    
    //Useing Corporate user
    var descLabel : String?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        descriptionTitle        <- map["descriptionTitle"]
        advantages              <- map["advantages"]
        classification          <- map["classification"]
        descLabel               <- map["descLabel"]
        
    }
    
}

class Advantages: Mappable {
    
    var description : String?
    var title : String?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        description                         <- map["description"]
        title                               <- map["title"]
        
    }
    
}

class Classification: Mappable {
    
    var description : String?
    var title : String?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        description                         <- map["description"]
        title                               <- map["title"]
        
    }
    
}

class PostpaidTariff: Mappable {
    
    var corporate : [Corporate]?
    var individual : [Individual]?
    var klassPostpaid : [KlassPostpaid]?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        corporate           <- map["corporate"]
        individual          <- map["individual"]
        klassPostpaid       <- map["klassPostpaid"]
    }
    
}

class Corporate: Mappable {
    
    var header : CinPostpaidHeader?
    var price : CinPostpaidPrice?
    var details : DetailsWithPriceArray?
    var description : TariffDescription?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        header          <- map["header"]
        price           <- map["price"]
        details         <- map["details"]
        description     <- map["description"]
        
    }
    
}

class CinPostpaidHeader: Mappable {
 
    var id : String?
    var offeringId : String?
    var name : String?
    var priceLabel : String?
    var priceValue : String?
    var subscribable : String?
    var attributes : [HeaderAttributes]?
    
    required init?(map: Map) {
    }
    
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        offeringId              <- map["offeringId"]
        name                    <- map["name"]
        priceLabel              <- map["priceLabel"]
        priceValue              <- map["priceValue"]
        subscribable            <- map["subscribable"]
        attributes              <- map["attributes"]
        
    }
    
}

class CinPostpaidPrice: Mappable {
    
    var priceLabel : String?
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        priceLabel              <- map["priceLabel"]
        call                    <- map["call"]
        sms                     <- map["sms"]
        internet                <- map["internet"]
        
    }
    
}

class CinPostpaidDetails: Mappable {
    
    var detailLabel : String?
    var destination : CinPostpaidDestination?
    var internationalOffPeak : CinInternationalOffPeak?
    var internationalOnPeak : CinInternationalOnPeak?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        detailLabel                             <- map["detailLabel"]
        destination                             <- map["destination"]
        internationalOffPeak                    <- map["internationalOffPeak"]
        internationalOnPeak                     <- map["internationalOnPeak"]
    }
    
}

class CinPostpaidDestination: Mappable {
    var iconName : String?
    var title : String?
    var titleValue : String?
    var attributes : [CinDestinationAttribute]?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        iconName                 <- map["iconName"]
        title                    <- map["title"]
        titleValue               <- map["titleValue"]
        attributes               <- map["attributes"]
        
        
    }
    
}

class CinDestinationAttribute: Mappable {
    
    var title : String?
    var value : String?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        title               <- map["title"]
        value               <- map["value"]
        
    }
    
}

class CinInternationalOffPeak: Mappable {
    
    var description : String?
    var iconName : String?
    var title : String?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        description                 <- map["description"]
        iconName                    <- map["iconName"]
        title                       <- map["title"]
    }
    
}

class CinInternationalOnPeak: Mappable {
    
    var description : String?
    var iconName : String?
    var title : String?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        description                 <- map["description"]
        iconName                    <- map["iconName"]
        title                       <- map["title"]
    }
    
}

class Individual: Mappable {
    
    var header : IndividualHeader?
    var details : DetailsWithPriceArray?
    var description : TariffDescription?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        header              <- map["header"]
        details             <- map["details"]
        description         <- map["description"]
        
    }
}

class IndividualHeader : Mappable {
    
    var id : String?
    var offeringId : String?
    var name : String?
    var priceLabel : String?
    var priceValue : String?
    var subscribable : String?
    
    var call : CallSMSPriceSection?
    var sms : CallSMSPriceSection?
    var internet : InternetPriceSection?
    
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        id                                      <- map["id"]
        offeringId                              <- map["offeringId"]
        name                                    <- map["name"]
        priceLabel                              <- map["priceLabel"]
        priceValue                              <- map["priceValue"]
        subscribable                            <- map["subscribable"]
        call                                    <- map["call"]
        sms                                     <- map["sms"]
        internet                                <- map["internet"]
        
    }
}

class DestinationIndividual : Mappable {
    
    var iconName : String?
    var title : String?
    var titleValue : String?
    var attributes : [Attributes]?
    
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        iconName        <- map["iconName"]
        title           <- map["title"]
        titleValue      <- map["titleValue"]
        attributes      <- map["attributes"]
        
    }
}

class International : Mappable {
    
    var iconName : String?
    var title : String?
    var description : String?
    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        iconName        <- map["iconName"]
        title           <- map["title"]
        description     <- map["description"]
    }
}


class SupplementaryOfferings: Mappable {
    var internet : Internet?
    var campaign : Campaign?
    var sms : SMS?
    var call : Call?
    var tm : TM?
    var hybrid : Hybrid?
    var roaming : Roaming?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        internet        <- map["internet"]
        campaign        <- map["campaign"]
        sms             <- map["sms"]
        call            <- map["call"]
        tm              <- map["tm"]
        hybrid          <- map["hybrid"]
        roaming         <- map["roaming"]
        
    }
}

class Internet: Mappable {
    var offers : [Offers]?
    var filters : Filters?
    
    required init?(map: Map) {
        
    }
    //    offers
    //    filters
    
    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class Campaign: Mappable {
    var offers : [Offers]?
    var filters : Filters?
    
    required init?(map: Map) {
        
    }
    //    offers
    //    filters
    
    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class SMS: Mappable {
    var offers : [Offers]?
    var filters : Filters?
    
    required init?(map: Map) {
        
    }
    //    offers
    //    filters
    
    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class Call: Mappable {
    var offers : [Offers]?
    var filters : Filters?
    
    required init?(map: Map) {
        
    }
    //    offers
    //    filters
    
    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class TM: Mappable {
    var offers : [Offers]?
    var filters : Filters?
    
    required init?(map: Map) {
        
    }
    //    offers
    //    filters
    
    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class Hybrid: Mappable {
    var offers : [Offers]?
    var filters : Filters?
    
    required init?(map: Map) {
        
    }
    //    offers
    //    filters
    
    func mapping(map: Map) {
        offers         <- map["offers"]
        filters        <- map["filters"]
    }
}

class Roaming: Mappable {
    var countries : [String]?
    var offers : [Offers]?
    var filters : Filters?
    var countriesFlags  :[Country]?
    
    required init?(map: Map) {
        
    }
    
    //    countries
    //    offers
    //    filters
    
    func mapping(map: Map) {
        
        //countries      <- map["countries"]
        offers         <- map["offers"]
        filters        <- map["filters"]
        countriesFlags <- map["countries"]
    }
}

class Country: Mappable {
    var name : String?
    var flag : String?

    required init?(map: Map) {

    }

    //    countries
    //    offers
    //    filters

    func mapping(map: Map) {

        name      <- map["name"]
        flag         <- map["flag"]
    }
}

class Offers: Mappable {
    var details : DetailsAndDescription?
    var description : DetailsAndDescription?
    var header : Header?
    
    var openedViewSection : MBSectionType = MBSectionType.Header
    
    required init?(map: Map) {
        
    }
    
    //    details
    //    description
    //    header
    
    func mapping(map: Map) {
        
        details             <- map["details"]
        description         <- map["description"]
        header              <- map["header"]
    }
}


class Header: Mappable {
    
    var id : String?
    var type : String?
    var offerName : String?
    var stickerLabel : String?
    var stickerColorCode : String?
    var validityTitle : String?
    var validityInformation : String?
    var validityValue : String?
    var price : String?
    var appOfferFilter : String?
    var offeringId : String?
    var offerLevel : String?
    var btnDeactivate : String?
    var btnRenew : String?
    var status : String?
    var offerGroup : OfferGroup?
    var attributeList : [AttributeList]?
    var usage : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        type                    <- map["type"]
        offerName               <- map["offerName"]
        stickerLabel            <- map["stickerLabel"]
        stickerColorCode        <- map["stickerColorCode"]
        validityTitle           <- map["validityTitle"]
        validityInformation     <- map["validityInformation"]
        validityValue           <- map["validityValue"]
        price                   <- map["price"]
        appOfferFilter          <- map["appOfferFilter"]
        offeringId              <- map["offeringId"]
        offerLevel              <- map["offerLevel"]
        btnDeactivate           <- map["btnDeactivate"]
        btnRenew                <- map["btnRenew"]
        status                  <- map["status"]
        offerGroup              <- map["offerGroup"]
        attributeList           <- map["attributeList"]
        usage                   <- map["usage"]
    }
}

class OfferGroup: Mappable {
    var groupName : String?
    var groupValue : String?
    required init?(map: Map) {
        
    }

    
    func mapping(map: Map) {
        
        groupName       <- map["groupName"]
        groupValue      <- map["groupValue"]
        
    }
}


class DetailsAndDescription: Mappable {
    var price : Price?
    var rounding : Rounding?
    var textWithTitle : TextWithTitle?
    var textWithOutTitle : String?
    var textWithPoints : [String?]?
    var date : DateAndTime?
    var time : DateAndTime?
    var roamingDetails : RoamingDetails?
    var freeResourceValidity : FreeResourceValidity?
    var titleSubTitleValueAndDesc : TitleSubTitleValueAndDesc?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        price                       <- map["price"]
        rounding                    <- map["rounding"]
        textWithTitle               <- map["textWithTitle"]
        textWithOutTitle            <- map["textWithOutTitle.description"]
        textWithPoints              <- map["textWithPoints"]
        date                        <- map["date"]
        time                        <- map["time"]
        roamingDetails              <- map["roamingDetails"]
        freeResourceValidity        <- map["freeResourceValidity"]
        titleSubTitleValueAndDesc   <- map["titleSubTitleValueAndDesc"]
        
    }
}

class RoamingDetails: Mappable {
    var descriptionAbove : String?
    var descriptionBelow : String?
    var roamingDetailsCountriesList : [RoamingDetailsCountries]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        descriptionAbove                <- map["descriptionAbove"]
        descriptionBelow                <- map["descriptionBelow"]
        roamingDetailsCountriesList     <- map["roamingDetailsCountriesList"]
    }
}

class RoamingDetailsCountries: Mappable {
    var countryName : String?
    var flag : String?
    var operatorList : [String?]?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        countryName         <- map["countryName"]
        flag                <- map["flag"]
        operatorList        <- map["operatorList"]
    }
}


class TitleSubTitleValueAndDesc: Mappable {
    var title : String?
    var attributeList : [AttributeList]?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title               <- map["title"]
        attributeList       <- map["attributeList"]
    }
}

class FreeResourceValidity: Mappable {
    var title : String?
    var titleValue : String?
    var subTitle : String?
    var subTitleValue : String?
    var description : String?
  
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title               <- map["title"]
        titleValue          <- map["titleValue"]
        subTitle            <- map["subTitle"]
        subTitleValue       <- map["subTitleValue"]
        description         <- map["description"]
    }
}

class DateAndTime: Mappable {
    var fromTitle : String?
    var fromValue : String?
    var toTitle : String?
    var toValue : String?
    var description : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        fromTitle           <- map["fromTitle"]
        fromValue           <- map["fromValue"]
        toTitle             <- map["toTitle"]
        toValue             <- map["toValue"]
        description         <- map["description"]
    }
}

class TextWithTitle: Mappable {
    var title : String?
    var isBullet : Bool = false
    var text : String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        isBullet        <- map["isBullet"]
        text            <- map["text"]
    }
}


class Rounding: Mappable {
    var title : String?
    var value : String?
    var iconName : String?
    var description : String?
    var attributeList : [AttributeList]?
  
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        iconName        <- map["iconName"]
        description     <- map["description"]
        attributeList   <- map["attributeList"]
    }
}

class Price: Mappable {
    var title : String?
    var value : String?
    var iconName : String?
    var description : String?
    var offersCurrency : String?
    var attributeList : [AttributeList]?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        iconName        <- map["iconName"]
        description     <- map["description"]
        offersCurrency  <- map["offersCurrency"]
        attributeList   <- map["attributeList"]
    }
}


class AttributeList: Mappable {
    var title : String?
    var value : String?
    var iconMap : String?
    var description : String?
    var unit : String?
    var onnetLabel : String?
    var onnetValue : String?
    var offnetLabel : String?
    var offnetValue : String?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title           <- map["title"]
        value           <- map["value"]
        iconMap         <- map["iconMap"]
        description     <- map["description"]
        unit            <- map["unit"]
        onnetLabel      <- map["onnetLabel"]
        onnetValue      <- map["onnetValue"]
        offnetLabel     <- map["offnetLabel"]
        offnetValue     <- map["offnetValue"]
    }
}

class Filters: Mappable {
    var app : [KeyValue]?
    var desktop : [KeyValue]?
    var tab : [KeyValue]?
  
    init() {
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        app           <- map["app"]
        desktop       <- map["desktop"]
        tab           <- map["tab"]
    }
}

class KeyValue: Mappable, Copyable {
    var key : String?
    var value : String?
    
    required init?(map: Map) {
        
    }
    
    required init(instance: KeyValue) {
        self.key = instance.key
        self.value = instance.value
    }
    
    init(key : String , value : String) {
        self.key = key
        self.value = value
    }
    
    func mapping(map: Map) {
        
        key             <- map["key"]
        value           <- map["value"]
    }
}



