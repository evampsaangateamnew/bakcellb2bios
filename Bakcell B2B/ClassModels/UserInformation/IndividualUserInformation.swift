//
//  UserInformation.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 8/13/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct IndividualUserInformation : Mappable {
    var customerData : CustomerData?
    var primaryOffering : PrimaryOffering?
    var supplementaryOfferingList : [SupplementaryOfferingList]?
    var predefinedData : PredefinedData?
    var homePageData : HomePageData?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        customerData                <- map["customerInfo"]
        primaryOffering             <- map["primaryOffering"]
        supplementaryOfferingList   <- map["supplementaryOfferingList"]
        predefinedData              <- map["predefinedData"]
        homePageData                <- map["homePageData"]
    }
    
}


// Customer Data

struct CustomerData : Mappable {
    var title : String?
    var firstName : String?
    var middleName : String?
    var lastName : String?
    var customerType : String?
    var gender : String?
    var dob : String?
    var accountId : String?
    var effectiveDate : String?
    var expiryDate : String?
    var subscriberType : String?
    var status : String?
    var statusCode : String?
    var statusDetails : String?
    var brandId : String?
    var brandName : String?
    var loyaltySegment : String?
    var offeringId : String?
    var msisdn : String?
    var token : String?
    var imageURL : String?
    var billingLanguage : String?
    var language : String?
    var customerId : String?
    var entityId : String?
    var customerStatus : String?
    var magCustomerId : String?
    var groupType : String?
    var email : String?
    var simNumber : String?
    var pinCode : String?
    var pukCode : String?
    var offeringNameDisplay : String?
    var offeringName : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        title           <- map["title"]
        firstName       <- map["firstName"]
        middleName      <- map["middleName"]
        lastName        <- map["lastName"]
        customerType    <- map["customerType"]
        gender          <- map["gender"]
        dob             <- map["dob"]
        accountId       <- map["accountId"]
        effectiveDate   <- map["effectiveDate"]
        expiryDate      <- map["expiryDate"]
        subscriberType  <- map["subscriberType"]
        status          <- map["status"]
        statusCode      <- map["statusCode"]
        statusDetails   <- map["statusDetails"]
        brandId         <- map["brandId"]
        brandName       <- map["brandName"]
        loyaltySegment  <- map["loyaltySegment"]
        offeringId      <- map["offeringId"]
        msisdn          <- map["msisdn"]
        token           <- map["token"]
        imageURL        <- map["imageURL"]
        billingLanguage <- map["billingLanguage"]
        language        <- map["language"]
        customerId      <- map["customerId"]
        entityId        <- map["entityId"]
        customerStatus  <- map["customerStatus"]
        magCustomerId   <- map["magCustomerId"]
        groupType       <- map["groupType"]
        email           <- map["email"]
        simNumber       <- map["simNumber"]
        pinCode         <- map["pinCode"]
        pukCode         <- map["pukCode"]
        offeringNameDisplay <- map["offeringNameDisplay"]
        offeringName    <- map["offeringName"]
    }
    
}



//Primary Offers

struct PrimaryOffering : Mappable {
    var offeringId : String?
    var offeringName : String?
    var offeringCode : String?
    var offeringShortName : String?
    var status : String?
    var networkType : String?
    var effectiveTime : String?
    var expiredTime : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        offeringId          <- map["OfferingId"]
        offeringName        <- map["OfferingName"]
        offeringCode        <- map["OfferingCode"]
        offeringShortName   <- map["OfferingShortName"]
        status              <- map["Status"]
        networkType         <- map["NetworkType"]
        effectiveTime       <- map["EffectiveTime"]
        expiredTime         <- map["ExpiredTime"]
    }
    
}


struct SupplementaryOfferingList : Mappable {
    var offeringId : String?
    var offeringName : String?
    var offeringCode : String?
    var offeringShortName : String?
    var status : String?
    var networkType : String?
    var effectiveTime : String?
    var expiredTime : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        offeringId          <- map["offeringId"]
        offeringName        <- map["offeringName"]
        offeringCode        <- map["offeringCode"]
        offeringShortName   <- map["offeringShortName"]
        status              <- map["status"]
        networkType         <- map["networkType"]
        effectiveTime       <- map["effectiveTime"]
        expiredTime         <- map["expiredTime"]
    }
    
}


//PredefinedData

struct PredefinedData : Mappable {
    var callStatus : String?
    var resultCode : String?
    var resultDesc : String?
    var topup : Topup?
    var fnf : Fnf?
    var redirectionLinks : RedirectionLinks?
    var tariffMigrationPrices : [KeyValue]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        callStatus              <- map["callStatus"]
        resultCode              <- map["resultCode"]
        resultDesc              <- map["resultDesc"]
        topup                   <- map["topup"]
        fnf                     <- map["fnf"]
        redirectionLinks        <- map["redirectionLinks"]
        tariffMigrationPrices   <- map["tariffMigrationPrices"]
    }
    
}


struct Topup : Mappable {
    var moneyTransfer : MoneyTransfer?
    var getLoan : GetLoan?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        moneyTransfer   <- map["moneyTransfer"]
        getLoan         <- map["getLoan"]
    }
    
}


struct Fnf : Mappable {
    var maxFNFCount : String?
    var fnFAllowed : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        maxFNFCount <- map["maxFNFCount"]
        fnFAllowed  <- map["fnFAllowed"]
    }
    
}



struct RedirectionLinks : Mappable {
    var onlinePaymentGatewayURL_AZ : String?
    var onlinePaymentGatewayURL_EN : String?
    var onlinePaymentGatewayURL_RU : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        onlinePaymentGatewayURL_AZ <- map["onlinePaymentGatewayURL_AZ"]
        onlinePaymentGatewayURL_EN <- map["onlinePaymentGatewayURL_EN"]
        onlinePaymentGatewayURL_RU <- map["onlinePaymentGatewayURL_RU"]
    }
    
}

struct GetLoan : Mappable {
    var selectAmount : SelectAmount?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        selectAmount <- map["selectAmount"]
    }
    
}

struct MoneyTransfer : Mappable {
    var selectAmount : SelectAmount?
    var termsAndCondition : TermsAndCondition?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        selectAmount        <- map["selectAmount"]
        termsAndCondition   <- map["termsAndCondition"]
    }
    
}


struct SelectAmount : Mappable {
    var cin : [Cin]?
    var klass : [Klass]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        cin     <- map["cin"]
        klass   <- map["klass"]
    }
    
}


struct TermsAndCondition : Mappable {
    var cin : [CinTermsnCondition]?
    var klass : [KlassTermsnCondition]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        cin     <- map["cin"]
        klass   <- map["klass"]
    }
    
}


struct CinTermsnCondition : Mappable {
    var currency : String?
    var amount : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        currency    <- map["currency"]
        amount      <- map["amount"]
    }
    
}

struct KlassTermsnCondition : Mappable {
    var currency : String?
    var amount : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        currency    <- map["currency"]
        amount      <- map["amount"]
    }
    
}

struct HomePageData : Mappable {
    var notificationUnreadCount : String?
    var balance : Balance?
    var installments : Installments?
    var mrc : Mrc?
    var credit : Credit?
   
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        notificationUnreadCount <- map["notificationUnreadCount"]
        balance                 <- map["balance"]
        installments            <- map["installments"]
        mrc                     <- map["mrc"]
        credit                  <- map["credit"]

    }
    
}



struct Balance : Mappable {
    var prepaid : Prepaid?
    var postpaid : Postpaid?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        prepaid     <- map["prepaid"]
        postpaid    <- map["postpaid"]
    }
    
}

struct BounusWallet : Mappable {
    var balanceTypeName : String?
    var currency : String?
    var amount : String?
    var effectiveTime : String?
    var expireTime : String?
    var lowerLimit : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        balanceTypeName     <- map["balanceTypeName"]
        currency            <- map["currency"]
        amount              <- map["amount"]
        effectiveTime       <- map["effectiveTime"]
        expireTime          <- map["expireTime"]
        lowerLimit          <- map["lowerLimit"]
    }
    
}

struct CountryWideWallet : Mappable {
    var balanceTypeName : String?
    var currency : String?
    var amount : String?
    var effectiveTime : String?
    var expireTime : String?
    var lowerLimit : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        balanceTypeName <- map["balanceTypeName"]
        currency        <- map["currency"]
        amount          <- map["amount"]
        effectiveTime   <- map["effectiveTime"]
        expireTime      <- map["expireTime"]
        lowerLimit      <- map["lowerLimit"]
    }
    
}

struct Credit : Mappable {
    var creditTitleLabel : String?
    var creditTitleValue : String?
    var creditCurrency : String?
    var creditDateLabel : String?
    var creditDate : String?
    var creditInitialDate : String?
    var creditLimit : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        creditTitleLabel    <- map["creditTitleLabel"]
        creditTitleValue    <- map["creditTitleValue"]
        creditCurrency      <- map["creditCurrency"]
        creditDateLabel     <- map["creditDateLabel"]
        creditDate          <- map["creditDate"]
        creditInitialDate   <- map["creditInitialDate"]
        creditLimit         <- map["creditLimit"]
    }
    
}





struct MainWallet : Mappable {
    var balanceTypeName : String?
    var currency : String?
    var amount : String?
    var effectiveTime : String?
    var expireTime : String?
    var lowerLimit : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        balanceTypeName <- map["balanceTypeName"]
        currency        <- map["currency"]
        amount          <- map["amount"]
        effectiveTime   <- map["effectiveTime"]
        expireTime      <- map["expireTime"]
        lowerLimit      <- map["lowerLimit"]
    }
    
}

struct Mrc : Mappable {
    var mrcTitleLabel : String?
    var mrcTitleValue : String?
    var mrcCurrency : String?
    var mrcDateLabel : String?
    var mrcDate : String?
    var mrcInitialDate : String?
    var mrcLimit : String?
    var mrcType : String?
    var mrcStatus : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        mrcTitleLabel   <- map["mrcTitleLabel"]
        mrcTitleValue   <- map["mrcTitleValue"]
        mrcCurrency     <- map["mrcCurrency"]
        mrcDateLabel    <- map["mrcDateLabel"]
        mrcDate         <- map["mrcDate"]
        mrcInitialDate  <- map["mrcInitialDate"]
        mrcLimit        <- map["mrcLimit"]
        mrcType         <- map["mrcType"]
        mrcStatus       <- map["mrcStatus"]
    }
    
}

struct Postpaid : Mappable {
    var availableBalanceCorporateValue : String?
    var availableBalanceIndividualValue : String?
    var availableCreditLabel : String?
    var balanceCorporateValue : String?
    var balanceIndividualValue : String?
    var balanceLabel : String?
    var corporateLabel : String?
    var currentCreditCorporateValue : String?
    var currentCreditIndividualValue : String?
    var currentCreditLabel : String?
    var individualLabel : String?
    var outstandingIndividualDebt : String?
    var outstandingIndividualDebtLabel : String?
    var template : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        availableBalanceCorporateValue  <- map["availableBalanceCorporateValue"]
        availableBalanceIndividualValue <- map["availableBalanceIndividualValue"]
        availableCreditLabel            <- map["availableCreditLabel"]
        balanceCorporateValue           <- map["balanceCorporateValue"]
        balanceIndividualValue          <- map["balanceIndividualValue"]
        balanceLabel                    <- map["balanceLabel"]
        corporateLabel                  <- map["corporateLabel"]
        currentCreditCorporateValue     <- map["currentCreditCorporateValue"]
        currentCreditIndividualValue    <- map["currentCreditIndividualValue"]
        currentCreditLabel              <- map["currentCreditLabel"]
        individualLabel                 <- map["individualLabel"]
        outstandingIndividualDebt       <- map["outstandingIndividualDebt"]
        outstandingIndividualDebtLabel  <- map["outstandingIndividualDebtLabel"]
        template                        <- map["template"]
    }
    
}

struct Prepaid : Mappable {
    var mainWallet : MainWallet?
    var countryWideWallet : CountryWideWallet?
    var bounusWallet : BounusWallet?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        mainWallet          <- map["mainWallet"]
        countryWideWallet   <- map["countryWideWallet"]
        bounusWallet        <- map["bounusWallet"]
    }
    
}


struct LoginData : Mappable {
    var picUserInformation : PICUserInformation?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        picUserInformation          <- map["loginData"]
    }
    
}

struct AppResumeData : Mappable {
    var returnCode : String?
    var queryInvoiceResponseData : [QueryInvoiceResponseData]?
    var returnMsg : String?
    var groupData : GroupData?
    var picUserInformation : PICUserInformation?
    var users : [UsersData]?
    var queryBalancePicResponseData : PICBalanceData?
    var predefinedData : NewPredefinedData?
    var liveChat :String?
    var userCount :String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        returnCode                  <- map["returnCode"]
        queryInvoiceResponseData    <- map["queryInvoiceResponseData"]
        returnMsg                   <- map["returnMsg"]
        groupData                   <- map["groupData"]
        picUserInformation          <- map["loginData"]
        users                       <- map["users"]
        queryBalancePicResponseData <- map["queryBalancePicResponseData"]
        predefinedData              <- map["predefinedData"]
        liveChat                    <- map["liveChat"]
        userCount                   <- map["userCount"]
    }
    
}

struct NewPredefinedData : Mappable {
    var content : String?
    var firstPopup : String?
    var lateOnPopup : String?
    var rateUsPopupTitle : String?
    var rateUsPopupContent : String?
    var changeTariffPopUpMessage : String?
    var documentTypes : [DocumentType]?
  
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        content                     <- map["content"]
        firstPopup                  <- map["firstPopup"]
        lateOnPopup                 <- map["lateOnPopup"]
        rateUsPopupTitle            <- map["popupTitle"]
        rateUsPopupContent          <- map["popupContent"]
        changeTariffPopUpMessage    <- map["changeTariffPopUpMessage"]
        documentTypes               <- map["documentTypes"]
    }
    
}

struct DocumentType : Mappable {
    var id      : String?
    var typeValue   : String?
  
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id             <- map["id"]
        typeValue          <- map["value"]
    }
    
}


//MARK: Query Inovice Data
struct QueryInvoiceResponseData : Mappable {
    var invoiceAmount : String?
    var invoiceDate : String?
    var dueDate : String?
    var settleDate : String?
    var invoiceDateDisp : String?
    var dueDateDisp : String?
    var status : String?
    var settleDateDisp : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        invoiceAmount           <- map["invoiceAmount"]
        invoiceDate             <- map["invoiceDate"]
        dueDate                 <- map["dueDate"]
        settleDate              <- map["settleDate"]
        invoiceDateDisp         <- map["invoiceDateDisp"]
        dueDateDisp             <- map["dueDateDisp"]
        status                  <- map["status"]
        settleDateDisp          <- map["settleDateDisp"]
    }
    
}


//MARK: Groups Data


struct GroupData : Mappable {
    var usersGroupData : [UsersGroupData]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        usersGroupData <- map["usersGroupData"]
    }
    
}


struct UsersGroupData : Mappable {
    var usersData : [UsersData]?
    var groupName : String = ""
    var userCount : String = ""
    var isSectionExpanded : Bool = false
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        usersData       <- map["usersData"]
        groupName       <- map["groupName"]
        userCount       <- map["userCount"]
    }
    
}

//MARK: UserData
struct UsersData : Mappable {
    var msisdn : String = ""
    var tarifId : String = ""
    var brandId : String = ""
    var corpId : String = ""
    var groupCode : String = ""
    var groupCustName : String = ""
    var groupId : String = ""
    var corpCrmCustName : String = ""
    var picName : String = ""
    var corpAcctCode : String = ""
    var corpCrmAcctId : String = ""
    var custFstName : String = ""
    var custLastName : String = ""
    var brandName : String = ""
    var groupName : String = ""
    var custFullName: String = ""
    var groupNameDisp: String = ""
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        msisdn              <- map["msisdn"]
        tarifId             <- map["tarif_id"]
        brandId             <- map["brand_id"]
        corpId              <- map["corp_id"]
        groupCode           <- map["group_code"]
        groupCustName       <- map["group_cust_name"]
        groupId             <- map["group_id"]
        corpCrmCustName     <- map["corp_crm_cust_name"]
        picName             <- map["pic_name"]
        corpAcctCode        <- map["corp_acct_code"]
        corpCrmAcctId       <- map["corp_crm_acct_id"]
        custFstName         <- map["cust_fst_name"]
        custLastName        <- map["cust_last_name"]
        brandName           <- map["brand_name"]
        groupName           <- map["group_name"]
        groupNameDisp       <- map["group_name_disp"]
        
        custFullName = custFstName + " " + custLastName
        
    }
    
}


//MARK: - User Informaiton
struct PICUserInformation : Mappable {
    var defaultBilling : String?
    var customerGender : String?
    var otpSource : String?
    var picVEON : String?
    var defaultShipping : String?
    var brandName : String?
    var picTariffsPermissions : String?
    var picPIN : String?
    var companyName : String?
    var failuresNum : String?
    var rewardWarningNotification : String?
    var websiteID : String?
    var accountID : String?
    var groupID : String?
    var brandID : String?
    var storeID : String?
    var ngbssStatus : String?
    var contactNumber : String?
    var disableAutoGroupChange : String?
    var picAllowedTariffs : String?
    var updatedAt : String?
    var rewardUpdateNotification : String?
    var email : String?
    var prefix : String?
    var offeringName : String?
    var language : String?
    var customProfileImage : String?
    var passwordHash : String?
    var picAttribute2 : String?
    var picAttribute1 : String?
    var lastName : String?
    var entityID : String?
    var picAttribute4 : String?
    var customerType : String?
    var picAttribute3 : String?
    var title : String?
    var rpTokenCreatedAt : String?
    var picStatus : String?
    var createdAt : String?
    var picAllowedOffers : String?
    var isActive : String?
    var picTNC : String?
    var passwordUnhashed : String?
    var msisdn : String?
    var firstName : String?
    var taxvat : String?
    var suffix : String?
    var subscriberType : String?
    var otpMSISDN : String?
    var offeringID : String?
    var picNewReset : String?
    var picSerialNo : String?
    var rpToken : String?
    var channel : String?
    var createdIn : String?
    var customerID : String?
    var token : String?
    var rateUsIOS : String?
    var picGroupsPermissions : String?
    var sun : String?
    var document_type : String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        defaultBilling              <- map["default_billing"]
        customerGender              <- map["customer_gender"]
        otpSource                   <- map["otp_source"]
        picVEON                     <- map["pic_veon"]
        defaultShipping             <- map["default_shipping"]
        brandName                   <- map["brand_name"]
        picTariffsPermissions       <- map["pic_tariffs_permissions"]
        picPIN                      <- map["pic_pin"]
        companyName                 <- map["company_name"]
        failuresNum                 <- map["failures_num"]
        rewardWarningNotification   <- map["reward_warning_notification"]
        websiteID                   <- map["website_id"]
        accountID                   <- map["account_id"]
        groupID                     <- map["group_id"]
        brandID                     <- map["brand_id"]
        storeID                     <- map["store_id"]
        ngbssStatus                 <- map["ngbss_status"]
        contactNumber               <- map["contact_number"]
        disableAutoGroupChange      <- map["disable_auto_group_change"]
        picAllowedTariffs           <- map["pic_allowed_tariffs"]
        updatedAt                   <- map["updated_at"]
        rewardUpdateNotification    <- map["reward_update_notification"]
        email                       <- map["email"]
        prefix                      <- map["prefix"]
        offeringName                <- map["offering_name"]
        language                    <- map["language"]
        customProfileImage          <- map["custom_profile_image"]
        passwordHash                <- map["password_hash"]
        picAttribute2               <- map["pic_attribute2"]
        picAttribute1               <- map["pic_attribute1"]
        lastName                    <- map["lastname"]
        entityID                    <- map["entity_id"]
        picAttribute4               <- map["pic_attribute4"]
        customerType                <- map["customer_type"]
        picAttribute3               <- map["pic_attribute3"]
        title                       <- map["title"]
        rpTokenCreatedAt            <- map["rp_token_created_at"]
        picStatus                   <- map["pic_status"]
        createdAt                   <- map["created_at"]
        picAllowedOffers            <- map["pic_allowed_offers"]
        isActive                    <- map["is_active"]
        picTNC                      <- map["pic_tnc"]
        passwordUnhashed            <- map["password_unhashed"]
        msisdn                      <- map["msisdn"]
        firstName                   <- map["firstname"]
        taxvat                      <- map["taxvat"]
        suffix                      <- map["suffix"]
        subscriberType              <- map["subscriber_type"]
        otpMSISDN                   <- map["otp_msisdn"]
        offeringID                  <- map["offering_id"]
        picNewReset                 <- map["pic_new_reset"]
        picSerialNo                 <- map["pic_serial_no"]
        rpToken                     <- map["rp_token"]
        channel                     <- map["channel"]
        createdIn                   <- map["created_in"]
        customerID                  <- map["customer_id"]
        token                       <- map["token"]
        rateUsIOS                   <- map["rateus_ios"]
        picGroupsPermissions        <- map["pic_groups_permissions"]
        sun                         <- map["sun"]
        document_type               <- map["document_type"]
    }
    
}



//MARK: - PIC Balance
struct PICBalanceData : Mappable {
    var totalLimit : String?
    var availableCredit : String?
    var outstandingDebt : String?
    var unbilledBalance : String?
    var balanceCredit : String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        totalLimit          <- map["total_limit"]
        availableCredit     <- map["available_credit"]
        outstandingDebt     <- map["outstanding_debt"]
        unbilledBalance     <- map["unbilled_balance"]
        balanceCredit       <- map["balanceCredit"]
    }
}


struct Installments: Mappable {
    var installmentDescription: String?
    var installmentTitle: String?
    var installments: [Installment]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        installmentDescription          <- map["installmentDescription"]
        installmentTitle                <- map["installmentTitle"]
        installments                    <- map["installments"]
        
    }
}

struct Installment: Mappable {
    
    
    var amountLabel: String = ""
    var amountValue: String = ""
    var currency: String = ""
    var installmentFeeLimit: String = ""
    var installmentStatus: String = ""
    var name: String = ""
    var nextPaymentInitialDate: String = ""
    var nextPaymentLabel: String = ""
    var nextPaymentValue: String = ""
    var purchaseDateLabel: String = ""
    var purchaseDateValue: String = ""
    var remainingAmountCurrentValue: String = ""
    var remainingAmountLabel: String = ""
    var remainingAmountTotalValue: String = ""
    var remainingCurrentPeriod: String = ""
    var remainingPeriodBeginDateLabel: String = ""
    var remainingPeriodBeginDateValue: String = ""
    var remainingPeriodEndDateLabel: String = ""
    var remainingPeriodEndDateValue: String = ""
    var remainingPeriodLabel: String = ""
    var remainingTotalPeriod: String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        amountLabel                         <- map["amountLabel"]
        amountValue                         <- map["amountValue"]
        currency                            <- map["currency"]
        installmentFeeLimit                 <- map["installmentFeeLimit"]
        installmentStatus                   <- map["installmentStatus"]
        name                                <- map["name"]
        nextPaymentInitialDate              <- map["nextPaymentInitialDate"]
        nextPaymentLabel                    <- map["nextPaymentLabel"]
        nextPaymentValue                    <- map["nextPaymentValue"]
        purchaseDateLabel                   <- map["purchaseDateLabel"]
        purchaseDateValue                   <- map["purchaseDateValue"]
        remainingAmountCurrentValue         <- map["remainingAmountCurrentValue"]
        remainingAmountLabel                <- map["remainingAmountLabel"]
        remainingAmountTotalValue           <- map["remainingAmountTotalValue"]
        remainingCurrentPeriod              <- map["remainingCurrentPeriod"]
        remainingPeriodBeginDateLabel       <- map["remainingPeriodBeginDateLabel"]
        remainingPeriodBeginDateValue       <- map["remainingPeriodBeginDateValue"]
        remainingPeriodEndDateLabel         <- map["remainingPeriodEndDateLabel"]
        remainingPeriodEndDateValue         <- map["remainingPeriodEndDateValue"]
        remainingPeriodLabel                <- map["remainingPeriodLabel"]
        remainingTotalPeriod                <- map["remainingTotalPeriod"]
        
    }
    
}



struct SimSwap: Mappable {
    
    
    var message: String = ""
    var transactionId: String = ""
    var responseMsg: String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        message                 <- map["message"]
        transactionId           <- map["transactionId"]
        responseMsg                 <- map["responseMsg"]
        
    }
    
}





