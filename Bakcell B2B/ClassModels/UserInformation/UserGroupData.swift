//
//  UserGroupData.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 11/26/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct UserGroupData : Mappable {
    
    var groupData : GroupData?
    var users : [UsersData]?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        groupData   <- map["groupData"]
        users       <- map["users"]
    }
    
}
