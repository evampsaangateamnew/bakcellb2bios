//
//  ChangeBillingLanguageHandler.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/11/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class ChangeBillingLanguage : Mappable {
    
    var message :  String = ""
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        
        message      <- map["message"]
    }
}
