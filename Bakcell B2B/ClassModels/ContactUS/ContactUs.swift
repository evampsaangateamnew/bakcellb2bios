//
//  ContactUs.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/3/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class ContactUs : Mappable{

    var address : String?
    var phone : String?
    var fax : String?
    var email : String?
    var website : String?
    var customerCareNo : String?
    var facebookLink : String?
    var twitterLink : String?
    var googleLink : String?
    var youtubeLink : String?
    var instagramLink : String?
    var linkedinLink : String?
    var addressLat : String?
    var addressLong : String?
    var inviteFriendText : String?

    init() {
    }
    required init?(map: Map) {
    }

    func mapping(map: Map) {

        address                 <- map["address"]
        phone                   <- map["phone"]
        fax                     <- map["fax"]
        email                   <- map["email"]
        website                 <- map["website"]
        customerCareNo          <- map["customerCareNo"]
        facebookLink            <- map["facebookLink"]
        twitterLink             <- map["twitterLink"]
        googleLink              <- map["googleLink"]
        youtubeLink             <- map["youtubeLink"]
        instagramLink           <- map["instagram"]
        linkedinLink            <- map["linkedinLink"]
        addressLat              <- map["addressLat"]
        addressLong             <- map["addressLong"]
        inviteFriendText        <- map["inviteFriendText"]


    }
}
/*
"data": {
    "address": "Port Baku Tower South, Neftchiler eve 153, Baku, AZ1010", 
 "phone": "+9944654286594",
    "fax": "+9944654286595",
    "email": "contact@bakcell.com",
    "website": "https://www.bakcell.com", 
 "customerCareNo": "+9944654286596", 
 "facebookLink": "https://www.facebook.com/bakcell",
 "twitterLink": "https://www.twitter.com/bakcell",
 "googleLink": "https://www.plus.google.com/bakcell",
 "youtubeLink": "https://www.youtube.com/bakcell", 
 "linkedinLink": "https://www.linkedin.com/bakcell",
 "addressLat": "33.6843° N",
 "addressLong": "72.9885° E",
    "inviteFriendText": "Hi Friend, I am inviting you to Bakcell APP. https://www.bakcell.com"
}*/

