//
//  FAQListHandler.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper


class FAQListHandler: Mappable {

    var faqList : [FAQList]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        faqList          <- map["faqlist"]
    }
}

//title : "Category 2"
//qalist : [...]
class FAQList: Mappable {
    var title : String = ""
    var qaList : [QAList]?

    init() {
        qaList = []
    }
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        title           <- map["title"]
        qaList          <- map["qalist"]
    }
}

//question : "Why do we use it?"
//answer : "It is a long established fact that....."
//sort_order : "1"

class QAList: Mappable {
    var question : String = ""
    var answer : String = ""
    var sortOrder : String = ""
    var isSectionExpanded:Bool = false

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        question      <- map["question"]
        answer        <- map["answer"]
        sortOrder     <- map["sort_order"]
    }
}
