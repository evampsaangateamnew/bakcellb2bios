//
//  MySubscriptionResponse.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/25/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct SupplementaryMySubscription : Mappable{
    
    var message: String?
    var mySubscriptionsData : MySubscription?
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        message                 <- map["message"]
        mySubscriptionsData     <- map["mySubscriptionsData"]
        
    }
}


struct MySubscription : Mappable{

    var message: String?
    var internetOffers              : [SubscriptionOffers]?
    var campaignOffers              : [SubscriptionOffers]?
    var smsOffers                   : [SubscriptionOffers]?
    var callOffers                  : [SubscriptionOffers]?
    var tmOffers                    : [SubscriptionOffers]?
    var hybridOffers                : [SubscriptionOffers]?
    var roamingOffers               : [SubscriptionOffers]?
    
    var internetInclusiveOffers     : [SubscriptionOffers]?
    var voiceInclusiveOffers        : [SubscriptionOffers]?
    var smsInclusiveOffers          : [SubscriptionOffers]?
   
    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        message                     <- map["message"]
        internetOffers              <- map["internet.offers"]
        campaignOffers              <- map["campaign.offers"]
        smsOffers                   <- map["sms.offers"]
        callOffers                  <- map["call.offers"]
        tmOffers                    <- map["tm.offers"]
        hybridOffers                <- map["hybrid.offers"]
        roamingOffers               <- map["roaming.offers"]
        
        internetInclusiveOffers     <- map["internetInclusiveOffers.offers"]
        voiceInclusiveOffers        <- map["voiceInclusiveOffers.offers"]
        smsInclusiveOffers          <- map["smsInclusiveOffers.offers"]

    }
}


struct SubscriptionOffers : Mappable {

//    header
//    details

    var header : SubscriptionHeader?
    //Using detail class  used in supplementery offers
    var details: DetailsAndDescription?
    
    var openedViewSection : MBSectionType = MBSectionType.Header

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {

        header          <- map["header"]
        details         <- map["details"]
        
    }
}

class SubscriptionHeader: Mappable {

    var id : String?
    var type : String?
    var offerName : String?
    var stickerLabel : String?
    var stickerColorCode : String?
    var validityTitle : String?
    var validityInformation : String?
    var validityValue : String?
    var price : String?
    var appOfferFilter : String?
    var offeringId : String?
    var offerLevel : String?
    var btnDeactivate : String?
    var btnRenew : String?
    var status : String?
    var isFreeResource: String?

    var offerGroup : OfferGroup?
    var attributeList : [AttributeList]?
//    var action : Action?
    var usage : [Usage]?


    required init?(map: Map) {

    }

//    id : "35"
//    type : "TM"
//    offerName : "TM Offer 2"
//    stickerLabel : "Discounted"
//    stickerColorCode : "#3E9E0D"
//    validityTitle : ""
//    validityInformation : ""
//    validityValue : ""
//    price : "0.00"
//    appOfferFilter : ""
//    offeringId : "922355794"
//    offerLevel : ""
//    btnDeactivate : "1"
//    btnRenew : "1"
//    status : null

    func mapping(map: Map) {

        id                      <- map["id"]
        type                    <- map["type"]
        offerName               <- map["offerName"]
        stickerLabel            <- map["stickerLabel"]
        stickerColorCode        <- map["stickerColorCode"]
        validityTitle           <- map["validityTitle"]
        validityInformation     <- map["validityInformation"]
        validityValue           <- map["validityValue"]
        price                   <- map["price"]
        appOfferFilter          <- map["appOfferFilter"]
        offeringId              <- map["offeringId"]
        offerLevel              <- map["offerLevel"]
        btnDeactivate           <- map["btnDeactivate"]
        btnRenew                <- map["btnRenew"]
        status                  <- map["status"]
        isFreeResource          <- map["isFreeResource"]
        offerGroup              <- map["offerGroup"]
        attributeList           <- map["attributeList"]
//        action                  <- map["action"]
        usage                   <- map["usage"]
        
    }
}

class Action : Mappable{


    var iconName: String = ""
    var title: String = ""
    var value: String = ""
    var desc: String = ""

    required init?(map: Map) {
    }

    func mapping(map: Map) {

        iconName            <- map["iconName"]
        title               <- map["title"]
        value               <- map["value"]
        desc                <- map["desc"]
        
    }
}

class Usage : Mappable {

    var iconName: String = ""
    var remainingTitle: String = ""
    var remainingUsage: String = ""
    var totalUsage: String = ""
    var unit: String = ""
    var renewalTitle: String = ""
    var activationDate: String?
    var renewalDate: String?
    var type: String = ""
    
    required init?(map: Map) {
    }

    func mapping(map: Map) {

        iconName                <- map["iconName"]
        remainingTitle          <- map["remainingTitle"]
        remainingUsage          <- map["remainingUsage"]
        totalUsage              <- map["totalUsage"]
        unit                    <- map["unit"]
        renewalTitle            <- map["renewalTitle"]
        activationDate          <- map["activationDate"]
        renewalDate             <- map["renewalDate"]
        type                    <- map["type"]
        
    }
}
