//
//  AppMenu.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

/*class AppMenues: Mappable {
    
    var appMenues : [AppMenu]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        appMenues        <- map["appMenues"]
    }
}

//menuHeader : "app_hor"
//menuList : [...]
class AppMenu: Mappable {
    var menuHeader : String = ""
    var menuList : [MenuList]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        menuHeader      <- map["menuHeader"]
        menuList        <- map["menuList"]
    }
}


//identifier : "dashboard"
//title : "Dashboard"
//sortOrder : "1"

class MenuList: NSObject,Mappable,NSCoding {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    var iconName : String = ""
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        
    }
    
    init(identifier:String, title:String, sortOrder:String, iconName:String ) {
        self.identifier = identifier
        self.title = title
        self.sortOrder = sortOrder
        self.iconName = iconName
    }
    
    func mapping(map: Map) {
        
        identifier      <- map["identifier"]
        title           <- map["title"]
        sortOrder       <- map["sortOrder"]
        iconName        <- map["iconName"]
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        
        identifier = aDecoder.decodeObject(forKey: "identifier") as? String ?? ""
        title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        sortOrder = aDecoder.decodeObject(forKey: "sortOrder") as? String ?? ""
        iconName = aDecoder.decodeObject(forKey: "iconName") as? String ?? ""
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(identifier, forKey: "identifier")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(sortOrder, forKey: "sortOrder")
        aCoder.encode(iconName, forKey: "iconName")
    }
}*/

struct AppMenus : Mappable {
    var menuHorizontal : [MenuHorizontal]?
    var menuVertical : [MenuVertical]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        menuHorizontal <- map["menuHorizontal"]
        menuVertical <- map["menuVertical"]
    }
    
}


struct MenuVertical : Mappable {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    var dataV2s : String = ""
    var items : String = ""
    var iconName : String = ""
    
    init?(map: Map) {
        
    }
    
    init(identifier:String, title:String, sortOrder:String, iconName:String ) {
        self.identifier = identifier
        self.title = title
        self.sortOrder = sortOrder
        self.iconName = iconName
    }
    
    mutating func mapping(map: Map) {
        
        identifier <- map["identifier"]
        title <- map["title"]
        sortOrder <- map["sortOrder"]
        dataV2s <- map["dataV2s"]
        items <- map["items"]
    }
    
}


struct MenuHorizontal : Mappable {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    var dataV2s : [DataV2s]?
    var items : [Items]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        identifier <- map["identifier"]
        title <- map["title"]
        sortOrder <- map["sortOrder"]
        dataV2s <- map["dataV2s"]
        items <- map["items"]
    }
    
}

struct DataV2s : Mappable {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        identifier <- map["identifier"]
        title <- map["title"]
        sortOrder <- map["sortOrder"]
    }
    
}


struct Items : Mappable {
    var identifier : String = ""
    var title : String = ""
    var sortOrder : String = ""
    var iconName : String = ""
    
    init?(map: Map) {
        
    }
    
    init(identifier:String, title:String, sortOrder:String, iconName:String ) {
        self.identifier = identifier
        self.title = title
        self.sortOrder = sortOrder
        self.iconName = iconName
    }
    
    mutating func mapping(map: Map) {
        
        identifier <- map["identifier"]
        title <- map["title"]
        sortOrder <- map["sortOrder"]
    }
    
}
