//
//  AddFCM.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/12/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class AddFCM : Mappable {
    var isEnable :  String          = ""
    var ringingStatus :  String     = ""

//"isEnable": "1", "ringingStatus": "mute"
    init() {
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {

        isEnable        <- map["isEnable"]
        ringingStatus   <- map["ringingStatus"]
    }
}
