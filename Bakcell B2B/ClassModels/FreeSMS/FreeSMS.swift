//
//  FreeSMS.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/6/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct FreeSMS : Mappable {
    var responseMsg :   String = ""
    
    
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
        responseMsg <- map["responseMsg"]
    }
}



struct MSISDNs:Mappable {
    var msisdn:String = ""
    
    init(_ msisdn :String) {
        self.msisdn = msisdn
    }
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        msisdn  <- map["msisdn"]
    }
}
