//
//  ReSendPin.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/21/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//


import Foundation
import ObjectMapper

class ReSendPin: Mappable{
    var pin : String = ""

    required init?(map: Map) {

    }

    func mapping(map: Map) {

        pin     <- map["pin"]
    }
}
