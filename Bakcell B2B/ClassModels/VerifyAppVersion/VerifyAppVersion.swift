//
//  VerifyAppVersion.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/20/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

struct VerifyAppVersion : Mappable {
    var versionConfig : String?
    var message : String?
    var timeStamps : [TimeStamps]?
    var appStore : String?
    var playStore : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        versionConfig <- map["versionConfig"]
        message <- map["message"]
        timeStamps <- map["timeStamps"]
        appStore <- map["appStore"]
        playStore <- map["playStore"]
    }
}


struct TimeStamps : Mappable {
    var cacheType : String?
    var timeStamp : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        cacheType <- map["cacheType"]
        timeStamp <- map["timeStamp"]
    }
}
