

import Foundation
import ObjectMapper

//struct SendPin : Mappable {
//    var callStatus : String?
//    var resultCode : String?
//    var resultDesc : String?
//    var data : PinData?
//
//    init?(map: Map) {
//
//    }
//
//    mutating func mapping(map: Map) {
//
//        callStatus <- map["callStatus"]
//        resultCode <- map["resultCode"]
//        resultDesc <- map["resultDesc"]
//        data <- map["data"]
//    }
//
//}


struct SendPin : Mappable {
    var responseMsg : String?
    var channel : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        responseMsg <- map["responseMsg"]
        channel <- map["channel"]
    }
    
}

struct BulkCoreServices : Mappable {
    var responseMsg : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        responseMsg <- map["responseMsg"]
    }
    
}



