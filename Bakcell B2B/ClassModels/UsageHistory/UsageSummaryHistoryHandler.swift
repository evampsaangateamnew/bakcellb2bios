//
//  UsageSummaryHistoryHandler.swift
//  Bakcell
//
//  Created by Awais Shehzad on 25/09/2017.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

class UsageSummaryHistoryHandler: Mappable {
    
    var summaryList : [SummaryList]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        summaryList     <- map["summaryList"]
    }
}

//SummaryList : [...]
class SummaryList: Mappable {
    var name : String = ""
    var totalUsage : String = ""
    var totalCharge : String = ""
    var records : [Records]?

    
    required init?(map : Map) {
        
    }
    
    func mapping(map: Map) {
        
        name            <- map["name"]
        totalUsage      <- map["totalUsage"]
        totalCharge     <- map["totalCharge"]
        records         <- map["records"]
    }
}

class Records: Mappable {
    
    var service_type : String = ""
    var unit : String = ""
    var total_usage : String = ""
    var chargeable_amount : String = ""
    var count : String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        service_type        <- map["service_type"]
        unit                <- map["unit"]
        total_usage         <- map["total_usage"]
        chargeable_amount   <- map["chargeable_amount"]
        count               <- map["count"]
    }
}
