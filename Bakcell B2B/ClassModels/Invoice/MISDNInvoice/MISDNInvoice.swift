//
//  MISDNInvoice.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/3/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper

class MISDNInvoice: Mappable {
    var misdn : String = ""
    var limit: String = ""
    var invoiceAmount : String = ""
    var userName : String = ""
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        misdn <- map["misdn"]
        limit <- map["limit"]
        invoiceAmount <- map["invoiceAmount"]
        userName <- map["userName"]
    }
    
    init(MISDN misdn: String, Limit limit: String, InvoiceAmount invoiceAmount: String, UserName userName: String) {
        self.misdn = misdn
        self.limit = limit
        self.invoiceAmount = invoiceAmount
        self.userName = userName
    }
}

class MISDNInvoiceModel : Mappable {
    
    var msisdnSummary : [MISDNInvoice]?
    var totalLabel : String = ""
    var totalValue : String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        msisdnSummary <- map["msisdnSummary"]
        totalLabel <- map["totalLabel"]
        totalValue <- map["totalValue"]
    }
}
