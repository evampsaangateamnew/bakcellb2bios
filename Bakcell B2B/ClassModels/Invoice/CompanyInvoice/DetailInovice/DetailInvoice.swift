//
//  DetailInvoice.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
class DetailInvoice: Mappable {
    var service : String = ""
    var quantity : String = ""
    var vahidi : String = ""
    var mablaq: String = ""
    var discount : String = ""
    var cami : String = ""

    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        service <- map["service"]
        quantity <- map["quantity"]
        vahidi <- map["vahidi"]
        mablaq <- map["mablaq"]
        discount <- map["discount"]
        cami <- map["cami"]
    }
    init(ServiceName service: String , Quantity quantity: String, Vahid vahidi: String, Mablaq mablaq: String, Discount discount: String, Cami cami: String) {
        self.service = service
        self.quantity = quantity
        self.vahidi = vahidi
        self.mablaq = mablaq
        self.discount = discount
        self.cami = cami
    }
}
