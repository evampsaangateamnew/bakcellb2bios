/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

class CompanyInvoiceDetail : Mappable {
    
	var companyDetail : BillSubItemCodes?

    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        companyDetail <- map["companyDetail"]
    }
}

class MSISDNInvoiceDetail : Mappable {
    
    var companyDetail : BillSubItemCodes?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        companyDetail <- map["msisdnDetail"]
    }
}

class BillSubItemCodes : Mappable {
    var billSubItemDetail : [BillSubItemDetail]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        billSubItemDetail <- map["BillSubItemCodes"]
    }
}

class BillSubItemDetail : Mappable {
    
    var details : BillSubItemData?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        details <- map["details"]
    }
}

class BillSubItemData : Mappable {
    
    var detailInvoice : String?
    var detailDesc : String?
    var detailTotal : String?
    var detailUsage : String?
    var subitemTitle : String?
    var detailUnit : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        detailInvoice <- map["detailInvoice"]
        detailDesc <- map["detailDesc"]
        detailTotal <- map["detailTotal"]
        detailUsage <- map["detailUsage"]
        subitemTitle <- map["subitemTitle"]
        detailUnit <- map["detailUnit"]
    }
}
