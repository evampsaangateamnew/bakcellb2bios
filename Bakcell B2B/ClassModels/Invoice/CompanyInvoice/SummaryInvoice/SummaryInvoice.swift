//
//  SummaryInvoice.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/4/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
class SummaryInvoice: Mappable {
    
    var title : String = ""
    var amount : String = ""
    required init?(map: Map) {
    }
    init(Name title: String, Amount amount: String) {
        self.title = title
        self.amount = amount
    }
    func mapping(map: Map) {
        title <- map["title"]
        amount <- map["amount"]
    }
}

class SummaryInvoiceModel : Mappable {
    
    var totalDiscountLable : String = ""
    var totalDiscountValue : String = ""
    var companySummary : [SummaryItem]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        totalDiscountLable <- map["totalDiscountLable"]
        totalDiscountValue <- map["totalDiscountValue"]
        companySummary <- map["companySummary"]
    }    
}

class SummaryItem : Mappable {
    
    var label : String?
    var value : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        label <- map["label"]
        value <- map["value"]
    }
}
