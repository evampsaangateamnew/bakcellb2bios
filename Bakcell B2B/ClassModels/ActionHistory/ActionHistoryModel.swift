//
//  ActionHistoryModel.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/17/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import ObjectMapper
struct OrderList: Mappable {
    var orderId : String?
    var date : String?
    var orderStatusN : String?
    var orderStatus : Constants.OrderStatus = Constants.OrderStatus.unknow
    var orderKey : String?
    var orderType : String?
    var totalCount : String?
    var success : String?
    var pending : String?
    var failed : String?
    var cancelled : String?
    
    var isSectionOpened : Bool = false
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        orderId <- map["orderId"]
        date <- map["date"]
        orderStatusN <- map["orderStatus"]
        orderStatus <- (map["orderStatus"],transform)
        orderKey <- map["orderKey"]
        orderType <- map["orderType"]
        totalCount <- map["totalCount"]
        success <- map["success"]
        pending <- map["pending"]
        failed <- map["failed"]
        cancelled <- map["cancelled"]
    }
    
    // MARK: Transforms
    
    let transform = TransformOf<Constants.OrderStatus, String>(fromJSON: { (value: String?) -> Constants.OrderStatus in
        
        if let newValue = value,
            newValue.isBlank == false {
            
            return Constants.OrderStatus(rawValue: newValue) ?? Constants.OrderStatus.unknow
        }
        return Constants.OrderStatus.unknow
        
    }, toJSON: { (value: Constants.OrderStatus? ) -> String? in
   
        if let newValue = value{
           
            return newValue.rawValue
        }
       return Constants.OrderStatus.unknow.rawValue
        
    })
    
}


struct ActionHistoryModel : Mappable {
    var orderList : [OrderList]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        orderList <- map["orderList"]
    }
    
}



struct RerequetActionHistory : Mappable {
    var responseMsg : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        responseMsg <- map["responseMsg"]
    }
    
}


