//
//  ActionHistoryDetailModel.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 1/28/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import Foundation
import ObjectMapper


struct ActionHistoryDetailModel : Mappable {
    var orderdetails : [OrderDetailModel]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        orderdetails <- map["orderdetails"]
    }
    
}

struct OrderDetailModel : Mappable {
    var msisdn : String?
    var resultCode : String?
    var resultDescription : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        msisdn              <- map["msisdn"]
        resultCode          <- map["resultCode"]
        resultDescription   <- map["resultDescription"]
    }
    
}



