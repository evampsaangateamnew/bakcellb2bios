//
//  ChangeLimitModel.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 2/28/19.
//  Copyright © 2019 Evamp&Saanga. All rights reserved.
//

import ObjectMapper

struct LimitMaxMinModel : Mappable {
    var initialLimit : String?
    var maxLimit : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        initialLimit <- map["initialLimit"]
        maxLimit <- map["maxLimit"]
    }
    
}
