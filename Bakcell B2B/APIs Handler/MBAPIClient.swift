//
//  MBAPIClient.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 05/30/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}

class MBAPIClient: MBAPIClientHandler {
    
    
    static var sharedClient: MBAPIClient = {
        
        let baseURL = URL(string: Constants.kMBAPIClientBaseURL)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = Constants.MBAPIClientDefaultTimeOut
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        
        // Configure the trust policy manager
        // With SSL Pining certificate
        /*
         let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
         certificates: ServerTrustPolicy.certificates(),
         validateCertificateChain: true,
         validateHost: true
         )
         */
        
        // With Public key
        let serverTrustPolicy = ServerTrustPolicy.pinPublicKeys(
            publicKeys: ServerTrustPolicy.publicKeys(),
            validateCertificateChain: true,
            validateHost: true)
        
        // With out SSL Pining
        // let serverTrustPolicy = ServerTrustPolicy.disableEvaluation
        
        let serverTrustPolicies = [baseURL?.host ?? "": serverTrustPolicy]
        let serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
        
        // Configure session manager with trust policy
        let instance = MBAPIClient (
            baseURL: baseURL!,
            configuration: configuration,
            serverTrustPolicyManager: serverTrustPolicyManager
        )
        
        // SessionManager instance.
        return instance
    }()
    
    // MARK: -  < APIs WITHOUT TOKEN START >
    
    // MARK: - verifyAppVersion Request
    
    func verifyAppVersion(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/verifyappversion"
        
        let params = ["appversion": UIDevice.appVersion()]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: -  < APIs WITHOUT TOKEN END >
    
    // MARK: - HomePage Request
    func gethomepage(_ msisdn: String, Password password: String,  _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "dashboardservices/gethomepage"
        
        let params: [String : String] = ["msisdn" : msisdn, "password" : password]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    // MARK: - updateCustomerEmail Request
    
    func getSubscriptions(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "mysubscriptionsV2/getsubscriptions"
        
        var headers : [String : String] = Constants.kRequestHeaders
        
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        //headers.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "msisdn")
        
        let params = ["offeringName": MBSelectedUserSession.shared.userInfo?.offeringName ?? "",
                      "brandName": MBSelectedUserSession.shared.userInfo?.brandName ?? "",
                      "individualMsisdn":MBSelectedUserSession.shared.msisdn]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - App Resume Update Dashboard data
    
    func gethomepageResume(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "dashboardservices/gethomepageResume"
        
        let params = ["msisdn": MBPICUserSession.shared.picUserInfo?.msisdn ?? "", "password":""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getsupplementaryofferings Request
    func getSupplementaryOfferings(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "supplementaryofferingsV2/getsupplementaryofferingsV2"
        
        let params = ["offeringIds": MBPICUserSession.shared.picUserInfo?.picAllowedOffers ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - changeSupplementaryOffering Request
    
    func changeSupplementaryOffering(msisdn:String, actionType:Bool, offeringId:String, offerName:String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "supplementaryofferingsV2/changesupplementaryoffering"
        
        let headers : [String : String] = Constants.kRequestHeaders
        //headers.updateValue(msisdn, forKey: "msisdn")
        
        
        var params = ["offeringId":offeringId,
                      "offerName":offerName,
                      "individualMsisdn":msisdn]
        
        if offerName.isBlank == false {
            if actionType {
                params.updateValue("1", forKey: "actionType")
            } else {
                params.updateValue("3", forKey: "actionType")
            }
        }
        
        //  {"actionType":"1","offeringId":"1376368122","offerName":"Internet"}
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - changeSupplementaryOffering Request
    
    func changeSupplementaryOfferingBulk(msisdns:[String : UsersData]?, offeringId :String, price :String, actionType :Bool, _ completionBlock :@escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ordermanagement/insertorderbulk"
        
        var params :[String :AnyObject] = ["offeringId":offeringId as AnyObject,
                                           "orderKey" : "C003" as AnyObject,
                                           "actPrice" : price as AnyObject]
        
        if actionType {
            params.updateValue("1" as AnyObject, forKey: "actionType")
        } else {
            params.updateValue("3" as AnyObject, forKey: "actionType")
        }
        
        var selectedMSISDNs : [[String :String]] = []
        msisdns?.forEach { (key, aMSISDNDetailObject) in
            
            var aMSISDNDetail :[String :String] = [:]
            aMSISDNDetail.updateValue(aMSISDNDetailObject.msisdn, forKey: "msisdn")
            aMSISDNDetail.updateValue(aMSISDNDetailObject.groupName, forKey: "groupType")
            
            selectedMSISDNs.append(aMSISDNDetail)
        }
        
        params.updateValue(selectedMSISDNs as AnyObject, forKey: "recieverMsisdn")
        
        return sendRequest(serviceName, parameters: params, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getTariffDetails Request
    
    func getTariffDetails(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "tariffservicesV2/gettariffdetails"
        
        var params : [String : AnyObject] = [:]
        
        if let tariffOfferIds = MBPICUserSession.shared.picUserInfo?.picAllowedTariffs?.components(separatedBy: ",") {
            params = ["offeringId":tariffOfferIds ,] as [String : AnyObject]
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    // MARK: - changeTariff Request
    
    func changeTariff(offeringId:String, selectedUsersList : [String : UsersData], _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        //        let serviceName = "tariffservicesV2/changetariffbulk"
        let serviceName = "ordermanagement/insertorderbulk"
        
        var params : [String : Any] = ["orderKey": "CH06",
                                       "destinationTariff":offeringId,
                                       "tariffPermissions" : MBPICUserSession.shared.picUserInfo?.picTariffsPermissions ?? "",
                                       "customerId" : MBPICUserSession.shared.picUserInfo?.customerID ?? ""]
        
        var recieversMSISDN : [TariffRequestPacket] = []
        selectedUsersList.forEach { (aSeletedUser) in
            let aUser = TariffRequestPacket(tariffId: aSeletedUser.value.tarifId,
                                            msisdn: aSeletedUser.value.msisdn,
                                            groupType: aSeletedUser.value.groupName,
                                            corpCrmCustName: aSeletedUser.value.corpCrmCustName)
            recieversMSISDN.append(aUser)
        }
        
        params.updateValue(recieversMSISDN.toJSON(), forKey: "recieverMsisdn")
        
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    // MARK: - Get Selected User Information Request
    
    func getUserInformation(userMSISDN : String ,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerdataservice/getcustomerdata"
        
        let params : [String : String] = ["msisdn":userMSISDN]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    
    // MARK: - getUsageSummaryHistory Request
    func getUsageSummaryHistory(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "historyV2/getusagesummary"
        
        
        let params : [String : String] = ["startDate":startDate,
                                          "endDate":endDate,
                                          "accountId":MBSelectedUserSession.shared.userInfo?.accountId ?? "",
                                          "customerId":MBSelectedUserSession.shared.userInfo?.customerId ?? "",
                                          "individualMsisdn":MBSelectedUserSession.shared.msisdn]
        
        var headers : [String : String] = Constants.kRequestHeaders
        //headers.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - Update Limit for User
    func updateLimitsForUsers(amount :String,
                              balance :String,
                              companyValue :String,
                              totalLimit :String,
                              lowerLimit :String,
                              maxLimit :String,
                              Users selectedUsers : [String : UsersData]?,
                              _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ordermanagement/insertorderbulk"
        
        var params :[String :Any] = ["orderKey" : "CPR007",
                                     "amount" : amount,
                                     "balance" : balance,
                                     "companyValue" : companyValue,
                                     "totalLimit" : totalLimit,
                                     "lowerLimit" : lowerLimit,
                                     "maxLimit" : maxLimit,
                                     "accountId" :MBPICUserSession.shared.picUserInfo?.accountID ?? ""]
        
        
        var selectedMSISDNs : [[String :String]] = []
        selectedUsers?.forEach { (key, aMSISDNDetailObject) in
            
            var aMSISDNDetail :[String :String] = [:]
            aMSISDNDetail.updateValue(aMSISDNDetailObject.tarifId, forKey: "tariffIds")
            aMSISDNDetail.updateValue(aMSISDNDetailObject.msisdn, forKey: "msisdn")
            aMSISDNDetail.updateValue(aMSISDNDetailObject.groupName, forKey: "groupType")
            
            
            selectedMSISDNs.append(aMSISDNDetail)
        }
        
        params.updateValue(selectedMSISDNs as AnyObject, forKey: "recieverMsisdn")
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - Update Limit for User
    func changeGroupForUsers(groupIdFrom :String,
                             groupIdTo :String,
                             groupTypeTo :GroupTypes?,
                             Users selectedUsers : [String : UsersData]?,
                             _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ordermanagement/insertorderbulk"
        
        var params :[String :Any] = ["orderKey" : "CG008",
                                     "groupIdTo" : groupIdTo,
                                     "groupTypeTo" : groupTypeTo?.rawValue ?? "",
                                     "tariffPermissions" : MBPICUserSession.shared.picUserInfo?.picGroupsPermissions ?? "",
                                     "accountId" :MBPICUserSession.shared.picUserInfo?.accountID ?? "",
                                     "customerId" : MBPICUserSession.shared.picUserInfo?.customerID ?? ""]
        
        
        var selectedMSISDNs : [[String :String]] = []
        selectedUsers?.forEach { (key, aMSISDNDetailObject) in
            
            var aMSISDNDetail :[String :String] = [:]
            aMSISDNDetail.updateValue(aMSISDNDetailObject.msisdn, forKey: "msisdn")
            aMSISDNDetail.updateValue(aMSISDNDetailObject.groupName, forKey: "groupType")
            aMSISDNDetail.updateValue(aMSISDNDetailObject.tarifId, forKey: "tariffIds")
            
            aMSISDNDetail.updateValue(groupIdFrom, forKey: "groupIdFrom")
            aMSISDNDetail.updateValue(aMSISDNDetailObject.corpCrmCustName, forKey: "corpCrmCustName")
            
            selectedMSISDNs.append(aMSISDNDetail)
        }
        params.updateValue(selectedMSISDNs as AnyObject, forKey: "recieverMsisdn")
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - Update Limit for User
    func getChangeLimitMinMax(groupType :String,
                              companyStandardValue :String,
                              offeringId :String,
                              msisdnuser :String,
                              _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ordermanagement/changelimitminmax"
        
        let params :[String :Any] = ["groupType" : groupType,
                                     "companyStandardValue" : companyStandardValue,
                                     "offeringId" : offeringId,
                                     "msisdnuser" : msisdnuser]
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - Get MSISDN Summary
    func getMSISDNInvoice(monthIndex : String, userMSISDN : String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/msisdnSummary"
        
        let params : [String : Any] = ["billMonth":monthIndex,
                                       "selectedMsisdn":userMSISDN,
                                       "customerId":MBPICUserSession.shared.picUserInfo?.customerID ?? "",
                                       "acctCode":MBPICUserSession.shared.picUserInfo?.picAttribute1 ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - Get MSISDN Detail
    func getMSISDNInvoiceDetail(monthIndex : String, userMSISDN : String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/msisdndetail"
        
        let params : [String : Any] = ["billMonth":monthIndex,
                                       "selectedMsisdn":userMSISDN,
                                       "customerId":MBPICUserSession.shared.picUserInfo?.customerID ?? "",
                                       "acctCode":MBPICUserSession.shared.picUserInfo?.picAttribute1 ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - Company Invoice Summary Request
    
    func companyInvoiceSummary(monthIndex : String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/companysummary"
        
        let params : [String : Any] = ["billMonth":monthIndex,
                                       "customerId":MBPICUserSession.shared.picUserInfo?.customerID ?? "",
                                        "acctCode":MBPICUserSession.shared.picUserInfo?.picAttribute1 ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - Company Invoice Detail Request
    
    func companyInvoiceDetail(monthIndex : String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/companydetail"
        
        let params : [String : Any] = ["billMonth":monthIndex,
                                       "customerId":MBPICUserSession.shared.picUserInfo?.customerID ?? "",
                                       "acctCode":MBPICUserSession.shared.picUserInfo?.picAttribute1 ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - Cancel Action History Request
    func cancelActionHistory(cancelOrderId: String , _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ordermanagement/cancel"
        
        let params : [String : String] = ["orderId": cancelOrderId, "orderKey" : "U005"]
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - Retry Action History Request
    func retryActionHistory(retryOrderId: String ,  _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ordermanagement/retryfailed"
        
        let params : [String : String] = ["orderId": retryOrderId, "orderKey" : "R004"]
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - Detail Action History Request
    func actionHistoryDetail(detailOrderId: String,  _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ordermanagement/orderdetails"
        
        let params : [String : String] = ["orderId": detailOrderId]
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - Get Action History Request
    func getActionHistory(StartDate startDate: String, EndDate endDate: String, orderRequestType : String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "actionhistory/getactionhistory"
        
        let params : [String : String] = ["startDate":startDate, "endDate":endDate, "orderType" : orderRequestType]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getUsageDetails History Request
    func getUsageDetailsHistory(StartDate startDate: String, EndDate endDate: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "historyV2/getusagedetails"
        
        let params : [String : String] = ["startDate":startDate,
                                          "endDate":endDate,
                                          "accountId":MBSelectedUserSession.shared.userInfo?.accountId ?? "",
                                          "customerId":MBSelectedUserSession.shared.userInfo?.customerId ?? "",
                                          "individualMsisdn": MBSelectedUserSession.shared.msisdn]
        
        var headers : [String : String] = Constants.kRequestHeaders
        //headers.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - SendPin Request
    func sendPinForSelectedUser(UserName userName: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "historyV2/sendpin"
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        //headers.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "msisdn")
        
        let params = ["userName": userName,
                      "individualMsisdn": MBSelectedUserSession.shared.msisdn]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject] , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    // MARK: - verifypin Request
    func verifyPin(UserName userName: String, RequestType requestType : Constants.MBResendType, Pin pin: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "historyV2/verifypin"
        
        var params : [String : String] = ["userName" : userName,
                                          "otp":pin,
                                          "requestPlatform":requestType.rawValue]
        
        if requestType == .UsageHistory {
            params.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "individualMsisdn")
        }
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(userName, forKey: "msisdn")
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    // MARK: - History ReSendPin Request
    func historyReSendPIN(UserName userName: String, ofType: Constants.MBResendType, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "historyV2/historyresendpin"
        
        var params : [String : String] = ["cause" : ofType.rawValue,
                                          "userName": userName]
        
        var headers : [String : String] = Constants.kRequestHeaders
        if ofType == .ForgotPassword {
            headers.updateValue(userName, forKey: "msisdn")
        } else if ofType == .UsageHistory {
            
            headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
            headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
            
            params.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "individualMsisdn")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - getOperationsHistory Request
    func getOperationsHistory(StartDate startDate: String, EndDate endDate: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "historyV2/getoperationshistory"
        
        var headers : [String : String] = Constants.kRequestHeaders
        //headers.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        let params : [String : String] = ["startDate":startDate,
                                          "endDate":endDate,
                                          "accountId":MBSelectedUserSession.shared.userInfo?.accountId ?? "",
                                          "customerId":MBSelectedUserSession.shared.userInfo?.customerId ?? "",
                                          "individualMsisdn":MBSelectedUserSession.shared.msisdn]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - getCoreServices Request
    
    func getCoreServices(accountType : String, groupType : String, tariffType: String, msisdn: String, subscriberType: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "coreservicesV2/getcoreservices"
        
        var headers : [String : String] = Constants.kRequestHeaders
        //        headers.updateValue(msisdn, forKey: "msisdn")
        headers.updateValue(tariffType, forKey: "tariffType")
        headers.updateValue(subscriberType, forKey: "subscriberType")
        
        let params : [String : String] = ["accountType": accountType,
                                          "groupType": groupType,
                                          "individualMsisdn": msisdn]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - getCoreServices Request
    
    func getCoreServicesBulk(accountType : String, groupType : String, selectedUserMSISDN : String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "coreservicesV2/getcoreservicesbulk"
        
        let headers : [String : String] = Constants.kRequestHeaders
        //headers.updateValue(selectedUserMSISDN, forKey: "msisdn")
        
        let params : [String : String] = ["accountType": accountType, "groupType": groupType]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    
    
    // MARK: - reportLostSim Request
    // HARDCODED 1 will be sent in any case.
    //TODO: Verify Packet.
    func reportLostSim(userMSISDN :String, suspendMSISDN :String, reasonCode:String = "1", _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/reportlostsim"
        
        var headers : [String : String] = Constants.kRequestHeaders
        //headers.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        let params = ["reasonCode":reasonCode,
                      "msisdn":suspendMSISDN,
                      "individualMsisdn":userMSISDN]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    // MARK: - processcoreservicesbulk Request
    
    func processBulkCoreServices(msisdns :[String], offeringId:String, forwardNumber :String = "", actionType:Bool, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        //        let serviceName = "coreservicesV2/processcoreservicesbulk"
        let serviceName = "ordermanagement/insertorderbulk"
        
        /*
         S001 = broadcastsms
         P002 = process core services
         C003 = change supplementary
         R004 = retry failed
         U005 = cancel pending orders
         */
        
        var selectedMSISDNs : [MSISDNs] = []
        msisdns.forEach { (aMSISDN) in
            selectedMSISDNs.append(MSISDNs(aMSISDN))
        }
        
        /*
         var params :[String : Any] = ["offeringId" :offeringId,
         "number": forwardNumber,
         "users" :msisdns.joined(separator: ","),
         "orderKey":"P002" ]
         */
        var params :[String : Any] = ["offeringId" :offeringId,
                                      "number": forwardNumber,
                                      "recieverMsisdn" :selectedMSISDNs.toJSON(),
                                      "orderKey":"P002" ]
        
        if actionType {
            params.updateValue("1", forKey: "actionType")
        } else {
            params.updateValue("3", forKey: "actionType")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    ///MARK: - billingLanguage Request
    func changeBillingLanguage(language : String ,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request{
        
        let serviceName = "customerservices/changebillinglanguage"
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        let params : [String : String] = ["language": language]
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - uploadImage Request
    
    func uploadImage(image : UIImage?, isUploadImage : Bool = true, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/uploadimage"
        var params : [String : String] = [:]
        
        if isUploadImage {
            params.updateValue("1", forKey: "actionType")
            
            if let myImage = image {
                let (bs64String,imageFormate) = myImage.base64(format: .JPEG(0.5))
                
                //  let rImage = bs64String.convertBase64ToImage(base64String: bs64String)
                params.updateValue(bs64String, forKey: "image")
                params.updateValue(imageFormate, forKey: "ext")
            } else {
                params.updateValue("", forKey: "image")
                params.updateValue(".jpg", forKey: "ext")
            }
            
            
        } else {
            params.updateValue("3", forKey: "actionType")
            params.updateValue("", forKey: "image")
            params.updateValue(".jpg", forKey: "ext")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - updateCustomerEmail Request
    
    func updateCustomerEmail(NewEmail newEmail: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservices/updatecustomeremail"
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(MBSelectedUserSession.shared.msisdn, forKey: "msisdn")
        headers.updateValue(MBSelectedUserSession.shared.subscriberTypeLowerCaseStr, forKey: "subscriberType")
        headers.updateValue(MBSelectedUserSession.shared.brandNameLowerCaseStr, forKey: "tariffType")
        
        let params : [String : String] = ["email":newEmail]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - storeDetails Request
    func storeDetails(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/getstoresdetails"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getFAQs Request
    func getFAQs(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/getfaqs"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getContactUsDetails Request
    func getContactUsDetails(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/getcontactusdetails"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - addFCMId Request
    
    func addFCMId(fcmKey : String, ringingStatus : Constants.MBNotificationSoundType, isEnable: Bool, isFromLogin: Bool, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "notifications/addfcm"
        
        var params : [String : String] = ["fcmKey": fcmKey, "ringingStatus":ringingStatus.rawValue]
        
        // On Of notification
        if isEnable {
            params.updateValue("1", forKey: "isEnable")
        } else {
            params.updateValue("0", forKey: "isEnable")
        }
        
        // set cause tyoe
        if isFromLogin {
            params.updateValue("login", forKey: "cause")
        } else {
            params.updateValue("settings", forKey: "cause")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getTopUp Request
    func getTopUp(SubscriberType subscriberType: String, CardPin cardPinNumber: String, TopupNumber topupnum : String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "financialservicesV2/requesttopup"
        
        let params : [String : String] = ["subscriberType":subscriberType,"cardPinNumber":cardPinNumber, "topupnum": topupnum]
        
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - sendFreeSMS Request
    
    func sendFreeSMS(recieverMsisdns: [String], textMessage: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        //  let serviceName = "quickservicesV2/broadcastsms"
        let serviceName = "ordermanagement/insertorderbulk"
        
        
        var selectedMSISDNs : [MSISDNs] = []
        recieverMsisdns.forEach { (aMSISDN) in
            selectedMSISDNs.append(MSISDNs(aMSISDN))
        }
        
        /*
         var params : [String : String] = ["recieverMsisdn": recieverMsisdns.joined(separator: ",") ,
         "textmsg":textMessage.toBase64(),
         "orderKey":"S001",
         "senderName":MBPICUserSession.shared.picUserInfo?.otpMSISDN ?? ""]
         */
        
        var params : [String : Any] = ["recieverMsisdn": selectedMSISDNs.toJSON() ,
                                       "textmsg":textMessage.toBase64(),
                                       "orderKey":"S001",
                                       "senderName":MBPICUserSession.shared.picUserInfo?.otpMSISDN ?? ""]
        
        if textMessage.containsOtherThenAllowedCharactersForFreeSMSInEnglish()  == true {
            params.updateValue("NE", forKey: "msgLang")
        } else {
            params.updateValue("EN", forKey: "msgLang")
        }
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - changePassword Request
    func changePassword(_ userName: String, OldPassword oldPassword: String, NewPassword newPassword: String, ConfirmNewPassword confirmNewPassword: String,_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservicesV2/changepassword"
        
        //  {"oldPassword":"1234", "newPassword":"12345", "confirmNewPassword":"12345" }
        
        let params : [String : String] = ["userName":userName,
                                          "oldPassword":oldPassword,
                                          "newPassword":newPassword,
                                          "confirmNewPassword":confirmNewPassword]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - forgotPassword Request
    func forgotPassword(_ userName: String, Password password: String, ConfirmPassword confirmPassword: String, OTP pin: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservicesV2/forgotpassword"
        
        let params : [String : String] = ["userName":userName,"password":password, "confirmPassword":confirmPassword, "temp": pin]
        
        var headers : [String : String] = Constants.kRequestHeaders
        headers.updateValue(userName, forKey: "msisdn")
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: headers, completionBlock: completionBlock)
    }
    
    // MARK: - getAppMenu Request
    func getAppMenu(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        let serviceName = "menuV2/getappmenu"
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getAppMenu Request
    func acceptTnC(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        let serviceName = "generalservicesV2/acceptTnC"
        
        let params : [String : String] = ["userName":MBPICUserSession.shared.msisdn]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject], isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - getNotifications Request
    
    func getNotifications(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "notificationsV2/getnotifications"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    // MARK: - getNotificationsCount Request
    func getNotificationsCount(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "notificationsV2/getnotificationscount"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - logOut Request
    
    func logOut(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "customerservicesV2/logout"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - rateIUs Request
    
    func rateUs(_ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "rateV2/rateus"
        
        return sendRequest(serviceName, parameters: nil, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - updateCustomerEmail Request
    
    func getUserGroupData(customerID: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "dashboardservices/getusergroupdata"
        
        let params : [String : String] = ["customerID":customerID]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - DuplicateSim Request
    
    func verifyDuplicateSim(documentType: String, documentNumber: String, documentPin: String, voen: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/verify"
        
        let params : [String : String] = ["documentType":documentType, "documentNumber":documentNumber, "documentPin":documentPin, "sun" : MBPICUserSession.shared.picUserInfo?.sun ?? "", "voen" : voen, "customerId" : MBPICUserSession.shared.picUserInfo?.customerID ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    func duplicateSimNow(msisdn:String, iccId :String, contactnumber :String, transactionId :String, _ completionBlock :@escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "ordermanagement/insertorderbulk"
        
        var params :[String :AnyObject] = [
            "iccid":iccId as AnyObject,
            "orderKey" : "SW009" as AnyObject,
            "transactionId" : transactionId as AnyObject,
            "customerId" : (MBPICUserSession.shared.picUserInfo?.customerID ?? "")   as AnyObject,
            "contactNumber" : contactnumber as AnyObject
        ]
        
        
        
        let selectedMSISDNs  =  [["msisdn" :msisdn]]
        params.updateValue(selectedMSISDNs as AnyObject, forKey: "recieverMsisdn")
        
        return sendRequest(serviceName, parameters: params, isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
    // MARK: - VaidateDuplicateSim Request
    
    func vaidateDuplicateSim(msisdnNumber: String, iccidNumber: String, contactNumber: String, _ completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let serviceName = "generalservicesV2/documentValid"
        
        let params : [String : String] = ["gsmNumber":msisdnNumber, "iccid":iccidNumber, "contact":contactNumber, "customerId" : MBPICUserSession.shared.picUserInfo?.customerID ?? ""]
        
        return sendRequest(serviceName, parameters: params as [String : AnyObject]? , isPostRequest: true, headers: Constants.kRequestHeaders, completionBlock: completionBlock)
    }
    
}


