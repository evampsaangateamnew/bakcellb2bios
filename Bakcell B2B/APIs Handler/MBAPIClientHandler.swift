//
//  MBAPIClientHandler.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 05/30/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireNetworkActivityIndicator
import ObjectMapper

typealias APIClientCompletionBlock = (_ response: HTTPURLResponse?, _ result: AnyObject?, _ error: NSError?) -> Void

typealias MBAPIClientCompletionHandler = (_ response: HTTPURLResponse?, _ resultData: AnyObject?, _ error: NSError?, _ isCancelled: Bool, _ callStatus: Bool, _ resultCode: String, _ resultDesc: String?) -> Void

enum MBAPIClientHandlerErrorCode: Int {
    case general = 30001
    case noNetwork = 30002
    case timeOut = 30003
}


// MARK: -

class MBAPIClientHandler: SessionManager {
    
    private let kMBAPIClientHandlerErrorDomain = "com.es.backcell.webserviceerror"
    private let showLogs : Bool = true
    
    
    // MARK: - Properties methods
    
    fileprivate var serviceURL: URL?
    
    // MARK: - init & deinit methods
    
    init(baseURL: URL,
         configuration: URLSessionConfiguration = URLSessionConfiguration.default,
         delegate: SessionDelegate = SessionDelegate(),
         serverTrustPolicyManager: ServerTrustPolicyManager? = nil){
        
        super.init(configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
        var aURL = baseURL
        
        // Ensure terminal slash for baseURL path, so that NSURL relativeToURL works as expected
        
        if aURL.path.count > 0 && !aURL.absoluteString.hasSuffix("/") {
            aURL = baseURL.appendingPathComponent("/")
        }
        
        serviceURL = baseURL
        NetworkActivityIndicatorManager.shared.isEnabled = true
    }
    
    // MARK: - Public methods
    
    func serverRequest(_ methodName: String,
                       parameters: [String : AnyObject]? ,
                       isPostRequest: Bool,
                       headers: [String : String]?,
                       completionBlock: @escaping APIClientCompletionBlock) -> Request {
        
        let url = URL(string: methodName, relativeTo: serviceURL)
        
        let HTTPCallMethod : HTTPMethod
        if !isPostRequest {
            HTTPCallMethod = HTTPMethod.get
        } else {
            HTTPCallMethod = HTTPMethod.post
        }
        
        let request = self.request(url!, method: HTTPCallMethod , parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseJSON(queue: DispatchQueue.global(qos: .background)) { response in
                
                switch response.result {
                    
                case .success:
                    if self.showLogs {
                        self.showRequestDetailForSuccess(responseObject: response)
                    }
                    completionBlock(response.response, response.result.value as AnyObject?, nil)
                    break
                    
                case .failure(let error):
                    if self.showLogs {
                        self.showRequestDetailForFailure(responseObject: response, error: error as NSError)
                    }
                    completionBlock(response.response, nil, error as NSError?)
                    break
                }
        }
        
        return request
    }
    
    func sendRequest(_ methodName: String,
                     parameters: [String : AnyObject]?,
                     isPostRequest: Bool,
                     headers: [String : String]?,
                     completionBlock: @escaping MBAPIClientCompletionHandler) -> Request {
        
        let request = self.serverRequest(methodName, parameters: parameters, isPostRequest: isPostRequest, headers: headers) { (response, result, error) in
            
            var resultCode: String = "9999"
            
            if error != nil {
                var isCancelled = false
                var apiError = error
                
                if error?.code == NSURLErrorCancelled {
                    isCancelled = true
                }
                
                if error?.code == NSURLErrorNotConnectedToInternet {
                    let userInfo : [String: Any] = [NSLocalizedDescriptionKey : Localized("Message_NoInternet")]
                    apiError = self.createErrorWithErrorCode(MBAPIClientHandlerErrorCode.noNetwork.rawValue, andErrorInfo: userInfo)
                    
                } else {
                    let userInfo : [String: Any] = [NSLocalizedDescriptionKey : Localized("Message_GenralError")]
                    apiError = self.createErrorWithErrorCode(MBAPIClientHandlerErrorCode.timeOut.rawValue, andErrorInfo: userInfo)
                }
                DispatchQueue.main.async {
                    completionBlock(response, nil, apiError, isCancelled, true, resultCode, "")
                }
                
            } else {
                
                var sendError = false
                var callStatus = false
                var resultDesc = ""
                var resultError: NSError?
                var resultData: AnyObject?
                
                if let responseHandler = Mapper<MBResponseHandler>().map(JSONObject:result) {
                    
                    callStatus = responseHandler.callStatus
                    
                    if let messageDiscription = responseHandler.resultDesc {
                        resultDesc = messageDiscription
                    }
                    
                    if let responseResultCode = responseHandler.resultCode {
                        resultCode = responseResultCode
                        
                        // Logout user and show error discription
                        if responseResultCode == Constants.MBAPIStatusCode.sessionExpired.rawValue {
                            DispatchQueue.main.async {
                                BaseVC.logout()
                            }
                            self.cancelAllRequests()
                            
                            return
                        }
                    }
                    
                    resultData = responseHandler.data
                    
                    if !callStatus {
                        
                        if resultData == nil {
                            resultData = true as AnyObject?
                        }
                        sendError = true
                    }
                    
                } else {
                    sendError = true
                }
                
                
                if sendError {
                    
                    resultError = self.createError(resultDesc)
                    DispatchQueue.main.async {
                        completionBlock(response, resultData, resultError, false, false, resultCode, resultDesc )
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        completionBlock(response, resultData, nil, false, true, resultCode, resultDesc )
                    }
                }
            }
        }
        
        return request
    }
    
    // MARK: - methods
    
    func createError(_ errorDescription: String) -> NSError {
        var description = Localized("Message_GenralError")
        
        
        if errorDescription.count > 0 {
            description = errorDescription
        }
        
        let userInfo : [String: Any] = [NSLocalizedDescriptionKey : description]
        
        return createErrorWithErrorCode(MBAPIClientHandlerErrorCode.general.rawValue, andErrorInfo: userInfo)
    }
    
    func createErrorWithErrorCode(_ code: Int, andErrorInfo info: [String: Any]?) -> NSError {
        return NSError(domain: kMBAPIClientHandlerErrorDomain, code: code, userInfo: info)
    }
    
    func cancelAllRequests() {
        session.getAllTasks { tasks in
            
            for task in tasks {
                task.cancel()
            }
        }
    }
    
    func showRequestDetailForSuccess(responseObject response : DataResponse<Any>) {
        
        #if DEBUG
        print("\n\n\n✅✅✅✅ ------- Success Response Start ------- ✅✅✅✅\n")
        print(""+(response.request?.url?.absoluteString ?? ""))
        print("\n=========   allHTTPHeaderFields   ========== \n")
        print("%@",response.request!.allHTTPHeaderFields!)
    
        if let bodyData : Data = response.request?.httpBody {
            let bodyString = String(data: bodyData, encoding: String.Encoding.utf8)
            print("\n=========   Request httpBody   ========== \n" + (bodyString ?? ""))
        } else {
            print("\n=========   Request httpBody   ========== \n" + "Found Request Body Nil")
        }
        
        print("\n=========   Request Total Duration   ========== \n\(response.timeline.totalDuration)")
        
        
        if let responseData : Data = response.data {
            let responseString = String(data: responseData, encoding: String.Encoding.utf8)
            print("\n=========   Response Body   ========== \n" + (responseString ?? ""))
        } else {
            print("\n=========   Response Body   ========== \n" + "Found Response Body Nil")
        }
        print("\n✅✅✅✅ ------- Success Response End ------- ✅✅✅✅\n\n\n")
        
        #endif
    }
    
    func showRequestDetailForFailure(responseObject response : DataResponse<Any>, error:NSError) {
        
        #if DEBUG
        print("\n\n\n❌❌❌❌ ------- Failure Response Start ------- ❌❌❌❌\n")
        print(""+(response.request?.url?.absoluteString ?? ""))
        print("\n=========   allHTTPHeaderFields   ========== \n")
        print("%@",response.request!.allHTTPHeaderFields!)
        
        if let bodyData : Data = response.request?.httpBody {
            let bodyString = String(data: bodyData, encoding: String.Encoding.utf8)
            print("\n=========   Request httpBody   ========== \n" + (bodyString ?? ""))
        } else {
            print("\n=========   Request httpBody   ========== \n" + "Found Request Body Nil")
        }
        
        print("\n=========   Request Total Duration   ========== \n\(response.timeline.totalDuration) Secs")
        
        if let responseData : Data = response.data {
            let responseString = String(data: responseData, encoding: String.Encoding.utf8)
            print("\n=========   Response Body   ========== \n" + (responseString ?? ""))
        } else {
            print("\n=========   Response Body   ========== \n" + "Found Response Body Nil")
        }
        
        if error.description.isBlank == false {
            print("\n=========   Error   ========== \n" + error.description)
        }
        print("\n❌❌❌❌ ------- Failure Response End ------- ❌❌❌❌\n\n\n")
        
        #endif
        
    }
}
