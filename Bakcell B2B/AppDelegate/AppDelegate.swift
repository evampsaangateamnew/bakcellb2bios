//
//  AppDelegate.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 5/21/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit
import GoogleMaps
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import Firebase
import FirebaseMessaging
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarBarTintColor = UIColor.mbBackgroundGray
        
        //Tab Bar Selection Color set
        UITabBar.appearance().tintColor = UIColor.mbBrandRed
        
        GMSServices.provideAPIKey("AIzaSyDn9gSf7fg32oLQc0HR6QimsLn7UCtWdj0")
        
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        registerForPushNotifications(application)
        
        Messaging.messaging().delegate = self
        
        if (launchOptions != nil) {
            // opened from a push notification when the app is closed
            let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]
            if (userInfo != nil)  {
                
                // Redirect user to notification screen
                BaseVC.redirectUserToNotificationScreen()
            }
        }
        
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getDeliveredNotifications { notifications in
                
                print(notifications)
                print("notifications")
                
            }
        } else {
            // Fallback on earlier versions
        }
        
        return true
    }
    
    func registerForPushNotifications(_ application: UIApplication) {
        
        // Notification Setting
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (granted, error) in
                
                guard granted else { return }
                
                // register remote notification
                UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                    print("Notification settings: \(settings)")
                    
                    if granted {
                        DispatchQueue.main.async {
                            application.registerForRemoteNotifications()
                        }
                    } else {
                        // handle the error
                    }
                }
                
            })
            
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        MBSelectedUserSession.shared.isOTPVerified = false
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        // Try to add FCM id if this is not added previosly.
        BaseVC.addFCMId(isFromLogin: UserDefaults.standard.bool(forKey: Constants.kIsFCMIdAddRequestFromLogin))
        
        //Removing Usage History when app comes from background
        NotificationCenter.default.post(name: NSNotification.Name(Constants.kAppDidBecomeActive), object: nil, userInfo: nil)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        MBSelectedUserSession.shared.isOTPVerified = false
    }
    
    
}

//MARK: - Notifications delegate
extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    //  @available(iOS 10.0, *)
    func application(received remoteMessage: MessagingRemoteMessage) {
        
        print(remoteMessage.appData);
        
        // Redirect user to notification screen
        BaseVC.redirectUserToNotificationScreen()
    }
    
    // For iOS 9 and above
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print(userInfo)
        
        // Redirect user to notification screen
        BaseVC.redirectUserToNotificationScreen()
    }
    
    // Received notification on background
    /* Note:
     if this is implemented then above application:didReceiveRemoteNotification will not be called
     */
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        // Let FCM know about the message for analytics etc.
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Redirect user to notification screen
        BaseVC.redirectUserToNotificationScreen()
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print(response)
        
        // Redirect user to notification screen
        BaseVC.redirectUserToNotificationScreen()
        
    }
}

