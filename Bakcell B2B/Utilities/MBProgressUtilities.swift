//
//  MBProgressUtilities.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 8/10/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation

public class MBProgressUtilities {
    
    /**
     Calculate Progress.
     
     - parameter total: Total Value
     - parameter remaining: Remaining Value.
     
     - returns: Progress (Float Value).
     */
    class func calculateProgress(total: Double = 0, remaining: Double = 0) -> Float {
        
        if total <= 0 || remaining <= 0 {
            return 1.0
        } else {
            return Float(total - remaining)/Float(total)
        }
    }
    
    /*/**
     Calculate Progress.
     
     - parameter total: Total Value
     - parameter remaining: Remaining Value.
     
     - returns: Progress (Float Value).
     */
    class func calculateProgress(total: Double = 0, remaining: Double = 0) -> Float {
        
        if total <= 0 || remaining <= 0 {
            return 1.0
        } else {
            return Float(remaining)/Float(total)
        }
    }*/
    
    
    /**
     Calculate Progress Bar valus and other variables.
     
     - parameter startDateString: Start Date.
     - parameter endDateString: End Date.
     - parameter dateFormate: Date Formate.
     - parameter AdditionalValueInEndDate: Addition in end date. By default it's value is '0'.
     
     - returns: (startDate, endDate, daysLeft, daysLeftDisplayValue, totalDays, progressValue ).
     */
    class func calculateProgressValues(from startDateString: String, to endDateString: String, dateFormate:String = Constants.kHomeDateFormate, AdditionalValueInEndDate additionalValue: Int = 0)
        ->
        (startDate: String, endDate: String, daysLeft: Int, daysLeftDisplayValue: String, totalDays: Int, progressValue: Float ) {
            
            MBPICUserSession.shared.dateFormatter.timeZone = TimeZone.appTimeZone()
            
            let startDateDisplayValue = MBDateUtilities.displayFormatedDateString(dateString: startDateString, dateFormate: dateFormate)
            let endDateDisplayValue = MBDateUtilities.displayFormatedDateString(dateString: endDateString, dateFormate: dateFormate)
            
            let dayBeforeEndDate =  MBDateUtilities.displayFormatedDateString(from: endDateString, additionalValue: additionalValue, dateFormate: dateFormate, returnDateFormate: dateFormate)
            
            
            let daysLeft = MBDateUtilities.calculateDaysLeft (startingDate:startDateString, endingDate: dayBeforeEndDate, dateFormate: dateFormate)
            
            let totalDays = MBDateUtilities.calculateTotalDaysBetween(startingDate: startDateString, endingDate: dayBeforeEndDate, dateFormate: dateFormate)
            
            var daysLeftUnit = ""
            
            if daysLeft <= 1 {
                daysLeftUnit = Localized("Info_days")
            } else {
                daysLeftUnit = Localized("Info_days")
            }
            let daysLeftDisplayValue = "\(daysLeft) \(daysLeftUnit)"
            
            let progress : Float =  MBProgressUtilities.calculateProgress(total: totalDays.toDouble(), remaining: daysLeft.toDouble())
            
            return (startDateDisplayValue, endDateDisplayValue, daysLeft, daysLeftDisplayValue, totalDays, progress)
    }
    
    /**
     Calculate Progress Bar valus and other variables for MRC.
     
     - parameter startDateString: Start Date.
     - parameter endDateString: End Date.
     - parameter type:  MRC Type Daily or other.
     
     - returns: (startDate, endDate, daysLeft, daysLeftDisplayValue, totalDays, progressValue )
     */
    class func calculateProgressValuesForMRC(from startDateString: String, to endDateString: String, type: String = "") -> (startDate: String, endDate: String, daysLeft: Int, daysLeftDisplayValue: String, totalDays: Int, progressValue: Float ) {
        
        if type.isEqual("daily", ignorCase: true) {
            
            MBPICUserSession.shared.dateFormatter.timeZone = TimeZone.appTimeZone()
            
            let startDateDisplayValue = MBDateUtilities.displayFormatedDateString(dateString: startDateString)
            let endDateDisplayValue = MBDateUtilities.displayFormatedDateString(dateString: endDateString)
            
            let totalHours = MBDateUtilities.calculateTotalHoursBetweenTwoDates(startingDate: startDateString, endingDate: endDateString, dateFormate: Constants.kHomeDateFormate)
            
            var passedHours = MBDateUtilities.calculateTotalHoursBetweenTwoDates(startingDate: startDateString, endingDate: Date().todayDateString(dateFormate: Constants.kHomeDateFormate), dateFormate: Constants.kHomeDateFormate)
            
            if passedHours > 0 {
                passedHours = passedHours + 1
            }
            
            //  let currentHourValue = Date().currentHour()
            var remainingHours = totalHours - passedHours
            
            var hoursLeftUnit = ""
            
            if remainingHours <= 1 {
                hoursLeftUnit = Localized("title_Hours")
                
                if remainingHours <= 0 {
                    remainingHours = 0
                }
                
            } else {
                hoursLeftUnit = Localized("title_Hours")
            }
            let hoursLeftDisplayValue = "\(remainingHours) \(hoursLeftUnit)"
            
            let progress : Float =  MBProgressUtilities.calculateProgress(total: totalHours.toDouble(), remaining: remainingHours.toDouble())
            
            
            return (startDateDisplayValue,endDateDisplayValue,remainingHours,hoursLeftDisplayValue, totalHours,progress)
            
        } else {
            
            return MBProgressUtilities.calculateProgressValues(from: startDateString, to: endDateString, AdditionalValueInEndDate: -1)
        }
    }
    
    /**
     Calculate Progress Bar valus and other variables for Installments.
     
     - parameter startDateString: Start Date.
     - parameter endDateString: End Date.
     
     
     - returns: (startDate, endDate, daysLeft, daysLeftDisplayValue, totalDays, progressValue )
     */
    class func calculateProgressValuesForInstallment(from startDateString: String, to endDateString: String) -> (startDate: String, endDate: String, daysLeft: Int, daysLeftDisplayValue: String, totalDays: Int, progressValue: Float ) {
        
        
        MBPICUserSession.shared.dateFormatter.timeZone = TimeZone.appTimeZone()
        
        let startDateDisplayValue = MBDateUtilities.displayFormatedDateString(dateString: startDateString)
        let endDateDisplayValue = MBDateUtilities.displayFormatedDateString(dateString: endDateString)
        
        let startDateOfMonth = Date().todayDate().startOfMonth()
        let endDateOfMonth = Date().todayDate().endOfMonth()
        
        let startDateStringOfMonth = MBPICUserSession.shared.dateFormatter.createString(from: startDateOfMonth, dateFormate: Constants.kHomeDateFormate)
        let endDateStringOfMonth = MBPICUserSession.shared.dateFormatter.createString(from:  endDateOfMonth, dateFormate: Constants.kHomeDateFormate)
        
        //  let dayBeforeEndDate =  MBUtilities.displayFormatedDateString(from: endDateString, additionalValue: 0)
        
        let daysLeft = MBDateUtilities.calculateDaysLeft(startingDate: startDateStringOfMonth, endingDate: endDateStringOfMonth, dateFormate: Constants.kHomeDateFormate)
        
        let totalDays = MBDateUtilities.calculateTotalDaysBetween(startingDate: startDateStringOfMonth, endingDate: endDateStringOfMonth, dateFormate: Constants.kHomeDateFormate)
        
        var daysLeftUnit = ""
        
        if daysLeft <= 1 {
            daysLeftUnit = Localized("Info_days")
        } else {
            daysLeftUnit = Localized("Info_days")
        }
        let daysLeftDisplayValue = "\(daysLeft) \(daysLeftUnit)"
        
        let progress : Float =  MBProgressUtilities.calculateProgress(total: totalDays.toDouble(), remaining: daysLeft.toDouble())
        
        return (startDateDisplayValue,endDateDisplayValue,daysLeft,daysLeftDisplayValue, totalDays,progress)
    }
    
}
