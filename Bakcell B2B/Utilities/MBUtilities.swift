//
//  MBUtilities.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/24/17.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper


//MARK: - < User information related Utilities >
class MBUtilities : NSObject {
    
    class func isLaunchedBefore() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.kIsLaunchedBefore)
    }
    
    class func updateLaunchedBeforeStatus() {
        UserDefaults.standard.set("YES", forKey:Constants.kIsLaunchedBefore)
    }
        
    public class func getAppStoreURL() -> String {
        
        if let appStroreURL = UserDefaults.standard.string(forKey: Constants.K_AppStoreURL),
            appStroreURL.isBlank == false {
            return appStroreURL
        } else {
            return Localized("appStore_URL")
        }
    }
    
    public class func updateAppStoreURL(_ URLString :String?) {
        UserDefaults.standard.set(URLString, forKey: Constants.K_AppStoreURL)
        UserDefaults.standard.synchronize()
    }
    
    public class func updateLoginTime() {
        UserDefaults.standard.set(Date().todayDateString(), forKey: Constants.K_UserLoggedInTime)
    }
    
    public class func getLoginTime() -> String {
        if let loginTime = UserDefaults.standard.string(forKey: Constants.K_UserLoggedInTime),
            loginTime.isBlank == false {
            return loginTime
        }
        return ""
    }
    
    public class func updateRateUsLaterTime() {
        UserDefaults.standard.set(Date().todayDateString(), forKey: Constants.K_RateUsLaterTime)
    }
    
    public class func getRateUsLaterTime() -> String {
        if let rateUsTime = UserDefaults.standard.string(forKey: Constants.K_RateUsLaterTime),
            rateUsTime.isBlank == false {
            return rateUsTime
        }
        
        return ""
    }
    
    public class func updateRateUsShownStatus(isShow :Bool) {
        UserDefaults.standard.set(isShow, forKey: Constants.K_IsRateUsShownBefore)
    }
    
    public class func isRateUsShownBefore() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.K_IsRateUsShownBefore)
    }
    
    /**
     Create attributed text with currency symbol and specific color.
     
     - parameter text:   String to be formatted.
     - parameter textColor: Color for attributed text. By default color is mbTextGray(Gray).
     - parameter withManatSign: Bool value for adding Manat(₼) sign to attributed text. By default it's true.
     
     - returns: Attributed text with specific color with or without Manat(₼) sign.
     */
    
    class func createAttributedText(_ text: String?, textColor: UIColor = .mbTextGray, withManatSign addSign: Bool = true) -> NSMutableAttributedString {
        
        // Return Empty string is text is emty
        if text?.isBlank == true || text == nil {
            return NSMutableAttributedString(string: "" ,attributes: [ NSAttributedString.Key.font: UIFont.mbArial(fontSize: 14)])
        }
        var newText = text ?? ""
        if addSign == true && newText.isBlank == false {
            newText.append("₼")
        }
        
        let amountAttributedText = NSMutableAttributedString(string: newText ,attributes: [ NSAttributedString.Key.font: UIFont.mbArial(fontSize: Float(14 + UIDevice().additionalFontSizeAccourdingToScreen)), NSAttributedString.Key.foregroundColor: textColor])
        
        if addSign == true && newText.isBlank == false {
            
            amountAttributedText.addAttribute(NSAttributedString.Key.font,
                                              value: UIFont.systemFont(ofSize: 10),
                                              range: NSRange(location: (amountAttributedText.length - 1), length: 1))
            
            amountAttributedText.addAttribute(NSAttributedString.Key.baselineOffset,
                                              value:3.0,
                                              range: NSRange(location:(amountAttributedText.length - 1),length:1))
            
            amountAttributedText.addAttribute(NSAttributedString.Key.expansion,
                                              value:0.25,
                                              range: NSRange(location:(amountAttributedText.length - 1),length:1))
            
            //            amountAttributedText.addAttribute(NSAttributedString.Key.ligature,
            //                                              value:0.6,
            //                                              range: NSRange(location:(amountAttributedText.length - 1),length:1))
        }
        
        return amountAttributedText
        
        
    }
    
    /**
     Determines the enterd password strength.
     
     - parameter Text:   Password string
     
     - Conditions:
     
     1. Weak: ( Password length is greater or equal than '6', and password lengt is less than '8', and contains special characters or contains alphbeats or contains digits)
     2. Weak: ( Password length is greater or equal than '6')
     3. Medium: ( Password length is greater or equal than '6', and password lengt is less or equal than '8', and contains special characters, and constains capital alphabets, and constains small alphabets, and contains digits)
     4. Medium: ( Password lengt is greater or equal than '8', and contains special characters, and constains alphabets, and contains digits)
     5. Medium: ( Password lengt is greater or equal than '8', and  ( ( Constains small alphabets, and contains digits, and constains capital alphabets ) or( Constains small alphabets, and contains special character, and constains capital alphabets ) ) )
     6. Strong: ( Password length is less or equal than '15', and password lengt greater than '8', and contains special characters, and constains capital alphabets, and constains small alphabets, and contains digits)
     7. didNotMatchCriteria: ( Password is less or equal to '0', or password is greater than '15', or password is les than '6')
     8. didNotMatchCriteria: ( If all above conditions does not match. )
     
     - returns: Password strenght based on above condition.
     */
    
    class func determinePasswordStrength(Text passwordString:String) -> Constants.MBPasswordStrength {
        
        if passwordString.count <= 0 ||
            passwordString.count > 15 ||
            passwordString.count < 6 {
            return Constants.MBPasswordStrength.didNotMatchCriteria
            
        }
            // Strong
        else if passwordString.count > 8 &&
            passwordString.count <= 15 &&
            passwordString.containsSpecialCharacters() &&
            passwordString.constainsSmallAlphabets() && passwordString.constainsCapitalAlphabets() &&
            passwordString.containsDigits() {
            
            return Constants.MBPasswordStrength.Strong
            
        }
            // Medium
        else if passwordString.count >= 6 &&
            passwordString.count <= 8 &&
            passwordString.containsSpecialCharacters() &&
            passwordString.constainsCapitalAlphabets() && passwordString.constainsSmallAlphabets() &&
            passwordString.containsDigits() {
            
            return Constants.MBPasswordStrength.Medium
            
        } else if passwordString.count >= 8 &&
            (passwordString.containsSpecialCharacters() && passwordString.containsAlphabets() && passwordString.containsDigits()) {
            
            return Constants.MBPasswordStrength.Medium
            
        }
        else if passwordString.count >= 8 && (passwordString.containsDigits() && passwordString.constainsSmallAlphabets() && passwordString.constainsCapitalAlphabets()) || (passwordString.constainsSmallAlphabets() && passwordString.constainsCapitalAlphabets() && passwordString.containsSpecialCharacters()){
            
            return Constants.MBPasswordStrength.Medium
            
        }
            // Weak
        else if passwordString.count >= 6 &&
            passwordString.count < 8 &&
            (passwordString.containsAlphabets() || passwordString.containsDigits() || passwordString.containsSpecialCharacters()) {
            
            return Constants.MBPasswordStrength.Week
            
        } else if passwordString.count >= 6 {
            
            return Constants.MBPasswordStrength.Week
            
        } else {
            
            return Constants.MBPasswordStrength.didNotMatchCriteria
            
        }
    }
    
    class func getMonthNames(totalNumberOfMonths : Int) -> [String] {
        
        var monthNames : [String] = []
        var j : Int = 0
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "M"
        dateFormatter.locale = Locale(identifier: Localized("date_locale"))
        let currentMonth : Int = Int(dateFormatter.string(from: Date())) ?? 0
        
        if totalNumberOfMonths <= 12 {
            
            for n in 2...totalNumberOfMonths+1 {
                if (currentMonth - n) >= 0 {
                    monthNames.append(dateFormatter.monthSymbols[currentMonth - n])
                } else {
                    monthNames.append(dateFormatter.monthSymbols[11 - j])
                    j+=1
                }
            }
        }
        
        return monthNames
    }
    
}


extension MBUtilities {
    
    ///Save PIC user in userdefault.
    class func saveCustomerDataInUserDefaults(loggedInUserMSISDN :String, picCustomerData:PICUserInformation?) {
        let userDefaults = UserDefaults.standard
        
        if var customerInfo = picCustomerData {
            customerInfo.msisdn = loggedInUserMSISDN
            MBPICUserSession.shared.picUserInfo = customerInfo
            customerInfo.saveInUserDefaults(key: APIsType.PICUserInformation.localizedAPIKey())
            
            userDefaults.set(true, forKey: Constants.kIsUserLoggedInKey)
        } else {
            userDefaults.set(false, forKey: Constants.kIsUserLoggedInKey)
        }
        userDefaults.synchronize()
    }
    
    ///Load customer information from userdefault.
    class func loadCustomerDataFromUserDefaults() -> Bool {
        
        if let userInformation : PICUserInformation = PICUserInformation.loadFromUserDefaults(key: APIsType.PICUserInformation.localizedAPIKey()) {
            MBPICUserSession.shared.picUserInfo = userInformation
            return true
        }
        return false
    }
    
    class func saveAppResumeInfo(loggedInUserMSISDN :String, appResumeData :AppResumeData) {
        
        /* Save PIC login information */
        MBUtilities.saveCustomerDataInUserDefaults(loggedInUserMSISDN: loggedInUserMSISDN, picCustomerData: appResumeData.picUserInformation)
        
        /* Save Terms and conditions information */
        appResumeData.predefinedData?.saveInUserDefaults(key: APIsType.terms_Conditions.localizedAPIKey())
        
        /* PIC Balance and invoice information */
        appResumeData.queryBalancePicResponseData?.saveInUserDefaults(key: APIsType.queryBalanceResponseData.localizedAPIKey())
        appResumeData.queryInvoiceResponseData?.saveInUserDefaults(key: APIsType.queryInvoiceResponseData.localizedAPIKey())
        
        /* PIC Users group and list information */
        /*
         appResumeData.users?.saveInUserDefaults(key: APIsType.usersData.localizedAPIKey())
         appResumeData.groupData?.saveInUserDefaults(key: APIsType.groupData.localizedAPIKey())
         */
        
        /* Save live chat URL in UserDefaults */
        UserDefaults.saveStringForKey(stringValue: appResumeData.liveChat ?? "", forKey: Constants.kLiveChatURLString)
        
        /* Save number of users count */
        UserDefaults.saveStringForKey(stringValue: appResumeData.userCount ?? "", forKey: Constants.kUsersCount)
        MBPICUserSession.shared.userDocumentTypes = appResumeData.predefinedData?.documentTypes ?? [DocumentType]()
        MBPICUserSession.shared.tarifPopUpStr = appResumeData.predefinedData?.changeTariffPopUpMessage ?? ""
    }
    
    class func saveUserGroupData(userGroupData :UserGroupData) {
        /* PIC Users group and list information */
        userGroupData.users?.saveInUserDefaults(key: APIsType.usersData.localizedAPIKey())
        userGroupData.groupData?.saveInUserDefaults(key: APIsType.groupData.localizedAPIKey())
    }
    
    /// Save JSON string into user defaults
    class func saveJSONStringInToUserDefaults(jsonString:String?, key : String) {
        let userDefaults = UserDefaults.standard
        
        if let jsonString = jsonString {
            
            userDefaults.set(jsonString, forKey: key)
            
        } else {
            userDefaults.set("", forKey: key)
        }
        userDefaults.synchronize()
    }
    
    /// Load JSON string from UserDefaults
    class func loadJSONStringFromUserDefaults(key : String) -> String {
        
        let userDefaults = UserDefaults.standard
        
        if let jsonString = userDefaults.object(forKey:key) as? String {
            return jsonString
            
        } else {
            return ""
        }
    }
    
    /// Clear JSON string from UserDefault for key
    class func clearJSONStringFromUserDefaults(key : String) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: key)
        userDefaults.synchronize()
        
    }
    
    
}





