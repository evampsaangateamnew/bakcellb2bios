//
//  MBSelectedUserSession.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 8/15/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation

// MARK: - Singleton
final class MBSelectedUserSession: NSObject {
    
    // Can't init is singleton
    override private init() {
        super.init()
    }
    // MARK:- Shared Instance
    static var shared = MBSelectedUserSession()
    
    // MARK:- Properties
    var userInfo : CustomerData?
    var isOTPVerified : Bool = false
    
    var token : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let token = self.userInfo?.token {
            return token
        } else {
            return ""
        }
    }
    
    var msisdn : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let msisdn = self.userInfo?.msisdn {
            return msisdn
        } else {
            return ""
        }
    }
    
    var subscriberTypeLowerCaseStr : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let subscriberType = self.userInfo?.subscriberType {
            return subscriberType.lowercased()
        } else {
            return ""
        }
    }
    
    var brandNameLowerCaseStr : String {
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let brandName = self.userInfo?.brandName {
            return brandName.lowercased()
        } else {
            return ""
        }
    }
    
    var groupType : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let groupType = self.userInfo?.groupType {
            return groupType
        } else {
            return ""
        }
    }
    
    var customerType : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let customerType = self.userInfo?.customerType {
            return customerType
        } else {
            return ""
        }
    }
    
    func UpdateGroupType(_ type :String) {
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
     self.userInfo?.groupType = type
    }
    
    // MARK:- Functions
    
    ///Returns Selected Billing Language
    func selectedBillingLanguageID() -> Constants.MBLanguage {
        
        
        if let language : String = userInfo?.billingLanguage {
            
            let billingLanguage = Constants.MBLanguage(rawValue: language)
            
            if billingLanguage == nil {
                return Constants.MBLanguage.azeri
            } else {
                return billingLanguage ?? Constants.MBLanguage.azeri
            }
            
        } else {
            return Constants.MBLanguage.azeri
        }
    }
    
    ///Returns Selected Billing Language
    func userSelectedBillingLanguageLanguage() -> String {
        
        switch self.selectedBillingLanguageID() {
            
        case .english:
            return "English"
            
        case .azeri:
            return "Azərbaycan"
            
        case .russian:
            return "Pу́сский"
        }
    }
    
    ///Clear User information
    func clearUserSession() {
        userInfo = nil
        isOTPVerified = false
    }
    
    ///Concatinating UserName
    func userName() -> String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        var name : String = ""
        
        if let firstName = userInfo?.firstName {
            name = firstName
        }
        
        if let middleName = userInfo?.middleName {
            name = name + " " + middleName
        }
        
        if let lastName = userInfo?.lastName {
            name = name + " " + lastName
        }
        name = name.replacingOccurrences(of: "  ", with: " ")
        
        if name.isBlank == false {
            return name
        } else {
            return ""
        }
    }
    
    
    ///Returns Subscriber type.
    func subscriberType() -> Constants.MBSubscriberType {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let subscriberType = self.userInfo?.subscriberType {
            
            if subscriberType.isEqual(Constants.MBSubscriberType.PrePaid.rawValue, ignorCase: true) {
                return Constants.MBSubscriberType.PrePaid
                
            } else if subscriberType.isEqual(Constants.MBSubscriberType.PostPaid.rawValue, ignorCase: true){
                
                return Constants.MBSubscriberType.PostPaid
            } else {
                return Constants.MBSubscriberType.UnSpecified
            }
            
        } else {
            return Constants.MBSubscriberType.UnSpecified
        }
    }
    
    ///Returns Customer type.
    func userCustomerType() -> Constants.MBCustomerType {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let customerType = self.userInfo?.customerType {
            
            if customerType.isEqual(Constants.MBCustomerType.Individual.rawValue, ignorCase: true) {
                return Constants.MBCustomerType.Individual
                
            } else if customerType.isEqual(Constants.MBCustomerType.Corporate.rawValue, ignorCase: true) {
                
                return Constants.MBCustomerType.Corporate
            } else {
                return Constants.MBCustomerType.UnSpecified
            }
            
        } else {
            return Constants.MBCustomerType.UnSpecified
        }
    }
    
    ///Returns Brand name.
    func brandName() -> Constants.MBBrandName {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let brandName = self.userInfo?.brandName {
            
            if brandName.isEqual(Constants.MBBrandName.CIN.rawValue, ignorCase: true) {
                return Constants.MBBrandName.CIN
                
            } else if brandName.isEqual(Constants.MBBrandName.Klass.rawValue, ignorCase: true) {
                
                return Constants.MBBrandName.Klass
                
            } else if brandName.isEqual(Constants.MBBrandName.Business.rawValue, ignorCase: true) {
                
                return Constants.MBBrandName.Business
                
            }else {
                return Constants.MBBrandName.UnSpecified
            }
            
        } else {
            return Constants.MBBrandName.UnSpecified
        }
    }
    
    
    
    ///Returns wheter to load user information or not..
    func loadUserInfomation() -> Bool {
        
        if self.userInfo == nil {
            return false
            
        } else {
            return true
        }
    }
    
    ///Returns wheter user status is active or not..
    func isStatusActive() -> Bool {
        let statusText = self.userInfo?.statusCode ?? ""
        
        if statusText.isEqual("B01", ignorCase: true) {
            return true
        } else {
            return false
        }
    }
    
    ///Returns Color accourding to user status
    func isStatusColor() -> UIColor {
        let statusText = self.userInfo?.statusCode ?? ""
        
        if statusText.isEqual("B01", ignorCase: true) { /* Status is Active */
            return UIColor.mbGreen
        } else if statusText.isEqual("B04", ignorCase: true) {/* Status is BLOCK_ONE_WAY */
            return UIColor.mbBarOrange
        } else {/* Status is BLOCK_TWO_WAY */
            return UIColor.mbBrandRed
        }
    }
}
