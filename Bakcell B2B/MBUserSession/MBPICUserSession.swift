//
//  MBPICUserSession.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 05/30/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit

// MARK: - Singleton
final class MBPICUserSession: NSObject {
    
    // Can't init is singleton
    override private init() {
        super.init()
    }
    // MARK: Shared Instance
    static let shared = MBPICUserSession()
    
    //MARK:- Properties
    
    var liveChatObject:LiveChatWebViewVC?
    
    var changePasswordRequestCount : Int = 0
    
    var picUserInfo : PICUserInformation?
    var appMenu : AppMenus?
    
    var userDocumentTypes = [DocumentType]()
    var tarifPopUpStr : String?
    
    let dateFormatter = DateFormatter()
    
    var isPushNotificationSelected : Bool = false
    var isLanguageChanged : Bool = false
    
    
    
    var msisdn : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let msisdn = self.picUserInfo?.msisdn {
            return msisdn
        } else {
            return ""
        }
    }
    
    var token : String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        if let token = self.picUserInfo?.token {
            return token
        } else {
            return ""
        }
    }
    
    //MARK:- Funtions
    ///Check Wheter user in logged in or not.
    func isLoggedIn() -> Bool {
        // Get Data from UserDefaults
        return UserDefaults.standard.bool(forKey: Constants.kIsUserLoggedInKey)
    }
    
    func isBiometricEnabled() -> Bool {
        // Get Data from UserDefaults
        return UserDefaults.standard.bool(forKey: Constants.K_IsBiometricEnabled)
    }
    
    func loadUserInfomation() -> Bool {
        
        if self.picUserInfo == nil {
            
            if self.isLoggedIn() {
                // Load customer information
                return MBUtilities.loadCustomerDataFromUserDefaults()
            }
        }
        return true
    }
    
    ///Concatinating UserName
    func userName() -> String {
        
        // Load user info if user loggedIn
        _ = self.loadUserInfomation()
        
        var name : String = ""
        
        if let firstName = picUserInfo?.firstName {
            name = firstName
        }
        
        //        if let middleName = picUserInfo?.middleName {
        //            name = name + " " + middleName
        //        }
        
        if let lastName = picUserInfo?.lastName {
            name = name + " " + lastName
        }
        name = name.replacingOccurrences(of: "  ", with: " ")
        
        if name.isBlank == false {
            return name
        } else {
            return ""
        }
    }
    
    
    ///Clear user informaiton from userdefault.
    func clearUserSession() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(false, forKey: Constants.kIsUserLoggedInKey)
        
        let userSelectedLanguage = MBLanguageManager.userSelectedLanguage()
        
        // Remove all data from UserDefaults
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier ?? "" )
        userDefaults.synchronize()
        
        // Set values in user defaults which are changed only once
        MBUtilities.updateLaunchedBeforeStatus()
        MBLanguageManager.setUserLanguage(selectedLanguage: userSelectedLanguage)
        
        changePasswordRequestCount = 0
        isPushNotificationSelected = false
        isLanguageChanged = false
        appMenu = nil
        picUserInfo = nil
        
        liveChatObject = nil
        
    }
    
    
    var aSecretKey : String {
        
        let key = "hbnnV6jRcxEUEcdKHs4jMuTbpQyTfEdt+7+Zloen5KU=".aesDecrypt(key: "s@@ng@tibe*&~~~jkm@@st@r")
        return key
        
    }
    
    
}
