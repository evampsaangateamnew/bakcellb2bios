//
//  MainNavigationController.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/23/17.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    var isPopGestureEnabled : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.interactivePopGestureRecognizer?.isEnabled = true
        self.interactivePopGestureRecognizer?.delegate = self
    }
}

extension MainNavigationController: UIGestureRecognizerDelegate {
    
    func interactivePop(_ isEnable:Bool = true) {
        isPopGestureEnabled = isEnable
        
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {print("gestureRecognizerShouldBegin: \(isPopGestureEnabled)")
        
        return isPopGestureEnabled
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        //  print("shouldBeRequiredToFailBy: \(isPopGestureEnabled)")
        return isPopGestureEnabled
    }
    /*
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive press: UIPress) -> Bool {
        
        print("press")
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        print("touch")
        return true
    }
    */
    
}
