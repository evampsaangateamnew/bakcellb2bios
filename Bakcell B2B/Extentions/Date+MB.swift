//
//  Date+MB.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 7/18/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation
extension Date {
    
    //MARK: - Properties
    var millisecondsSince1970:Double {
        return (self.timeIntervalSince1970 * 1000.0).rounded()
    }
    
    init(milliseconds:Double) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    /**
     Returns today date String.
     
     - parameter dateFormate: Date format.
     
     - returns: Todaydate(String).
     */
    func todayDateString(dateFormate: String = Constants.kHomeDateFormate ) -> String {
        
        return MBPICUserSession.shared.dateFormatter.createString(from: self.todayDate(dateFormate: dateFormate), dateFormate: dateFormate)
    }
    
    /**
     Returns today date.
     
     - parameter dateFormate: Date format.
     
     - returns: Todaydate(Date).
     */
    func todayDate(dateFormate: String = Constants.kHomeDateFormate ) -> Date {
        
        let now = Date()
        let currentDateString = MBPICUserSession.shared.dateFormatter.createString(from: now, dateFormate: dateFormate)
        
        return MBPICUserSession.shared.dateFormatter.createDate(from: currentDateString, dateFormate: dateFormate) ?? now
    }
    
    /**
     Returns today date with addition of days.
     
     - parameter withAdditionalValue: number of days to add to current date.
     
     - returns: Todaydate(String).
     */
    func todayDateString(withAdditionalValue additionalValue: Int, dateFormate: String = Constants.kAPIFormat) -> String {
        
        // ADD/Subtract value from date
        if let newDate = Calendar.current.date(byAdding: .day, value: additionalValue, to: Date().todayDate()) {
            
            let myDateString  = MBPICUserSession.shared.dateFormatter.createString(from: newDate, dateFormate: dateFormate)
            
            return myDateString
            
        } else {
            return ""
        }
    }
    
    /**
     Returns previous month.
     
     - returns: Previous month(Date).
     */
    func getPreviousMonth() -> Date? {
        var currentCalendar = Calendar.current
        
        currentCalendar.timeZone = TimeZone.appTimeZone()
        
        return currentCalendar.date(byAdding: .month, value: -1, to: self)
    }
    
    /**
     Returns end of month.
     
     - returns: end of month(Date).
     */
    func endOfMonth() -> Date {
        var currentCalendar = Calendar.current
        
        currentCalendar.timeZone = TimeZone.appTimeZone()
        
        return currentCalendar.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    /**
     Returns month wit addition of monts in date.
     
     - parameter withAdditionalValue: number of monts to add to current date.
     
     - returns: end of month(Date).
     */
    func getMonth(WithAdditionalValue additionalValue: Int = 0) -> Date? {
        var currentCalendar = Calendar.current
        
        currentCalendar.timeZone = TimeZone.appTimeZone()
        
        return currentCalendar.date(byAdding: .month, value: additionalValue, to: self)
    }
    
    /**
     Returns Strat of month.
     
     - returns: Start of month(Date).
     */
    func startOfMonth() -> Date {
        var currentCalendar = Calendar.current
        
        currentCalendar.timeZone = TimeZone.appTimeZone()
        
        if let newDate = currentCalendar.date(from: currentCalendar.dateComponents([.year, .month], from: currentCalendar.startOfDay(for: self))) {
            
            return newDate
        } else {
            return todayDate()
        }
        
    }
    
}

