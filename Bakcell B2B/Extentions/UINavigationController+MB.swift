//
//  UINAvigationController+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/28/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {

    ///Navigates user to login screen
    func popToLoginViewController() {

        var isFoundLoginController = false

        self.viewControllers.forEach({ (aViewController) in

            if aViewController.isKind(of: LoginVC().classForCoder) {
                isFoundLoginController = true
                self.popToViewController(aViewController, animated: true)
                
            }

        })

        if !isFoundLoginController {

            if let splashVCObj = self.viewControllers.first {
                self.popToViewController(splashVCObj, animated: true)
            } else {
                self.popToRootViewController(animated: false)
            }

        }
    }
}
