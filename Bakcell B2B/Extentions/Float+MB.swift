//
//  Float+MB.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 7/2/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation

extension Float {
    //MARK: - Properties
    var toFloorInt: Int {
     return Int(self.rounded(.down))
    }
    
    var toCeilInt: Int {
        return Int(self.rounded(.up))
    }
    
    var toRoundedInt: Int {
        return Int(roundf(self))
    }
    //MARK: - Functions
    /// Converts float to string
    func toString() -> String {
        return "\(self)"
    }
}
