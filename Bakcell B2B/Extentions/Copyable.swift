//
//  Copyable.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 6/28/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation

protocol Copyable {
    init(instance: Self)
}

extension Copyable {
    func copy() -> Self {
        return Self.init(instance: self)
    }
}
