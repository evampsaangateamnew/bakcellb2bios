//
//  UIImage+MB.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 6/21/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    /**
     Load image from a Assets.
     
     - parameter key: Name of  image.
     
     - returns: UIImage.
     */
    public class func imageFor(key: String?) -> UIImage {
        
        
        let iconKeyString = key?.lowercased() ?? ""
        var iconName = ""
        
        switch (iconKeyString) {
        case "calls":
            iconName = "ic_calls"
            
        case "internet":
            iconName = "ic_internet"
            
        case "whatsapp":
            iconName = "ic_whatsapp"
            
        case "phone":
            iconName = "ic_calls"
            
        case "sms":
            iconName = "ic_sms"
            
            //Tariff
            
        case "discount":
            iconName = ""
            
        case "free":
            iconName = ""
            
        case "tsdcall":
            iconName = "ic_calls"
            
        case "tsdsms":
            iconName = "ic_sms"
            
        case "tsdmms":
            iconName = "ic_mms"
            
        case "tsdinternet":
            iconName = "ic_internet"
            
        case "tsddistination":
            iconName = "ic_destination"
            
        case "tsdwhatsapp":
            iconName = "ic_whatsapp"
            
        case "tsdrounding":
            iconName = "ic_rounding-off"
            
        case "countrywidecall":
            iconName = "ic_calls"
            
        case "internationalcalls":
            iconName = "ic_calls_international"
            
        case "roamingcalls":
            iconName = "ic_calls_roaming"
            
        case "countrywidesms":
            iconName = "ic_sms"
            
        case "countrywideinternet":
            iconName = "ic_internet"
            
        case "roaminginternet":
            iconName = "ic_internet"
            
        case "whatsappusing":
            iconName = "ic_whatsapp"
            
        case "youtube":
            iconName = "ic_youtube"
            
        case "instagram":
            iconName = "ic_insta"
            
        case "facebook":
            iconName = "ic_facebook"
            
        case "mms":
            iconName = "ic_mms"
            
        case "campaign":
            iconName = "ic_campaing"
            
        case "tmrequired":
            iconName = "ic_tmrequired"
            
        case "tmpresented":
            iconName = "ic_tmpresented"
            
        case "campaigndiscountedcalls":
            iconName = "ic_campaigndiscountedcall"
            
        case "campaigndiscountedinternet":
            iconName = "ic_campaigndiscountedinternet"
            
        case "campaigndiscountedsms":
            iconName = "ic_campaigndiscountedsms"
            
        default:
            break
        }
        
        return UIImage.imageFor(name:iconName)
    }
    
    /**
     Load image from a Assets.
     
     - parameter key: Name of  image.
     
     - returns: UIImage.
     */
    public class func imageFor(name: String?) -> UIImage {
        if name?.isBlank != true {
            return UIImage(named: name ?? "placeHolder") ?? UIImage(named: "placeHolder") ?? UIImage()
        } else {
            return UIImage(named: "placeHolder") ?? UIImage()
        }
    }
    
    /**
     Load marker image from a Assets.
     
     - parameter key: Name of  image.
     - parameter inSmallSize: Size of marker image whether small or default size.
     
     - returns: UIImage.
     */
    public class func markerIconImageFor(key: String?, inSmallSize:Bool = false) -> UIImage{
        
        
        if key != nil {
            
            let iconKeyString = key?.lowercased() ?? ""
            var iconName = ""
            
            switch (iconKeyString) {
            case "Head Office".lowercased(), "Mərkəzi Ofis".lowercased(), "Центральный Офис".lowercased() :
                iconName = "headOfficeMarker"
                
            case "Customer care offices".lowercased(), "Müştəri xidmətləri mərkəzləri".lowercased(), "Офисы обслуживания".lowercased() :
                iconName = "serviceCenterMarker"
                
            case "Bakcell-IM".lowercased(), "Bakcell-IM".lowercased(), "Bakcell-IM".lowercased() :
                iconName = "bakcellimsMarker"
                
            case "Dealer stores".lowercased(), "Diler mağazaları".lowercased(), "Дилерские магазины".lowercased() :
                iconName = "dealerShopMarker"
                
            default:
                iconName = "bakcellimsMarker"
            }
            
            if inSmallSize == true {
                iconName += "_Small"
            }
            return UIImage.imageFor(name: iconName)
            
        } else {
            return UIImage()
        }
    }
}


// MARK: - UIImage (Base64 Encoding)

extension UIImage {
    
    public enum ImageFormat {
        case PNG
        case JPEG(CGFloat)
    }
    
    /**
     Convert image to base64.
     
     - parameter format: Image format PNG or JPEG.
     
     - returns: base64,imageformate(String,String).
     */
    public func base64(format: ImageFormat) -> (String,String) {
        var imageData: Data
        
        switch format {
            
        case .PNG:
            imageData = self.pngData() ?? Data()
            
        case .JPEG(let compression):
            
            imageData = self.jpegData(compressionQuality: compression)  ?? Data()
        }
        
        let imageFormate = imageData.format()
        
        return (imageData.base64EncodedString(), imageFormate)
    }
}
