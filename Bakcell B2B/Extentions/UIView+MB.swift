
//
//  UIView+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 05/30/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    /**
     Round corner of UIView.
     
     - parameter corners: topLeft, topRight, bottomLeft, bottomRight and allCorners.
     - parameter radius: how much we want to round corners.
     - returns: void.
     */
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    /**
     Round top corners of UIView.
     
     - parameter radius: how much we want to round corners.
     - returns: void.
     */
    func roundTopCorners(radius: CGFloat) {
        DispatchQueue.main.async {
            self.roundCorners(corners:[.topLeft, .topRight], radius: radius)
        }
    }
    
    /**
     Round all corners of UIView.
     
     - parameter radius: how much we want to round corners.
     - returns: void.
     */
    func roundAllCorners(radius: CGFloat) {
        //  DispatchQueue.main.async {
        //  self.roundCorners(corners:[.topLeft, .topRight, .bottomLeft, .bottomRight], radius: radius)
        //  }
        
        self.layer.cornerRadius = radius
    }
    
    ///Remove all subviews from a UIView.
    func removeAllSubViews() {
        self.subviews.forEach { (aView) in
            aView.removeFromSuperview()
        }
    }
    
    public class func fromNib() -> Self {
        return fromNib(nibName: nil)
    }
    
    public class func fromNib(nibName: String?) -> Self {
        func fromNibHelper<T>(nibName: String?) -> T where T : UIView {
            let bundle = Bundle(for: T.self)
            let name = nibName ?? String(describing: T.self)
            return bundle.loadNibNamed(name, owner: nil, options: nil)?.first as? T ?? T()
        }
        return fromNibHelper(nibName: nibName)
    }
    
    ///Adds ripple animation to UIView.
    public func showRippleAnimation() {
        let pulse = PulsingAnimation(numberOfPulses: 1, radius: self.frame.width, position: self.center, viewToAnimate: self)
        pulse.animationDuration = 0.5
        pulse.backgroundColor = UIColor.mbBrandRed.cgColor
        
        self.layer.insertSublayer(pulse, above: self.layer)
    }
    
    /**
     Enable and disable next button accourding to isEnabled value.
     
     - parameter isEnabled: set ture for enabling and false for disabling.
     
     - returns: void
     */
    func enableDisableAllSubView(isEnabled: Bool) {
        
        var alphaValue:CGFloat = 1.0
        if isEnabled == true {
            alphaValue = 1.0
        } else {
            alphaValue = 0.7
        }
        self.alpha = alphaValue
        // Loop through all subviews of buttonView
        self.subviews.forEach { (aView) in
           if aView is UIButton { // Check if subView is UIButton
                
                // Convert view to UIButton and Enable and Disable
                if let aButton = aView as? UIButton {
                    aButton.isEnabled = isEnabled
                    aButton.alpha = alphaValue
                }
            } else if aView is UITextField { // Check if subView is UITextField
                
                // Convert view to UIButton and Enable and Disable
                if let aTextField = aView as? UITextField {
                    aTextField.isEnabled = isEnabled
                }
            
            } else if aView is UIDropDown { // Check if subView is UITextField
                
                // Convert view to UIButton and Enable and Disable
                if let aDropDown = aView as? UIDropDown {
                    aDropDown.isEnabled = isEnabled
                }
                
            } else if aView is MBView && aView.subviews.count > 0 {
                aView.enableDisableAllSubView(isEnabled: isEnabled)
            }
        }
    }
    
    /**
     Enable and disable next button accourding to isEnabled value.
     
     - parameter isEnabled: set ture for enabling and false for disabling.
     
     - returns: void
     */
    func setNextButtonSelection(isEnabled: Bool) {
        
        // Loop through all subviews of buttonView
        self.subviews.forEach { (aView) in
            
            // Check if subView is UIImageView
            if aView is UIImageView {
                
                // Convert view to UIImageView
                if let imageView = aView as? UIImageView {
                    
                    // Set UIImageView image
                    if isEnabled {
                        imageView.image = UIImage.imageFor(name:"arrow_sign_forward")
                    } else {
                        imageView.image = UIImage.imageFor(name: "forward-arrow-disabled")
                    }
                }
            } else if aView is UILabel { // Check if subView is UILabel
                
                // Convert view to UILabel
                if let tiltLable = aView as? UILabel {
                    
                    // Change label textColor
                    if isEnabled {
                        tiltLable.textColor = UIColor.white
                    } else {
                        tiltLable.textColor = UIColor(hexString: "EC4B75")
                    }
                }
            } else if aView is UIButton { // Check if subView is UIButton
                
                // Convert view to UIButton and Enable and Disable
                if let aButton = aView as? UIButton {
                    aButton.isEnabled = isEnabled
                }
            }
        }
    }
    
    /**
     Enable and disable Back button accourding to isEnabled value.
     
     - parameter isEnabled: set ture for enabling and false for disabling.
     
     - returns: void
     */
    func setBackButtonSelection(isEnabled: Bool) {
        
        // Loop through all subviews of buttonView
        self.subviews.forEach { (aView) in
            
            // Check if subView is UIImageView
            if aView is UIImageView {
                
                // Convert view to UIImageView
                if let imageView = aView as? UIImageView {
                    
                    // Set UIImageView image
                    if isEnabled {
                        imageView.image = UIImage.imageFor(name: "arrow_sign_back")
                    } else {
                        imageView.image = UIImage.imageFor(name: "step-2-back-arrow-disabled")
                    }
                }
            } else if aView is UILabel { // Check if subView is UILabel
                
                // Convert view to UILabel
                if let tiltLable = aView as? UILabel {
                    
                    // Change label textColor
                    if isEnabled {
                        tiltLable.textColor = UIColor.white
                    } else {
                        tiltLable.textColor = UIColor(hexString: "EC4B75")
                    }
                }
                
            } else if aView is UIButton { // Check if subView is UIButton
                
                // Convert view to UIButton and Enable and Disable
                if let aButton = aView as? UIButton {
                    aButton.isEnabled = isEnabled
                }
            }
        }
    }

}

//MARK:- UIView extention for DescriptionView
extension UIView {
    
    /**
     Adds description view as sub view to UIView.
     
     - parameter imageName: Image name for description view.
     - parameter description: Text to be displayed in description view.
     
     - returns: void.
     */
    func showDescriptionViewWithImage(_ imageName:String = "step-3-info", description: String?, centerYConstant :CGFloat = 0) {
        
        if let myDescriptionView = self.viewWithTag(998877) as? DescriptionView {
            
            myDescriptionView.setDescriptionViewWithImage(imageName, description: description)
            
        } else {
            
            let myDescriptionView: DescriptionView = DescriptionView.fromNib()
            myDescriptionView.setDescriptionViewWithImage(imageName, description: description)
            myDescriptionView.translatesAutoresizingMaskIntoConstraints = false
            
            self.addSubview(myDescriptionView)
            self.bringSubviewToFront(myDescriptionView)
            
            let leadingConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
            
            let trailingConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            
            let verticalCenterConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: centerYConstant)
            
            let horizantalCenterConstraint = NSLayoutConstraint(item: myDescriptionView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
            
            self.addConstraints([leadingConstraint, trailingConstraint, verticalCenterConstraint, horizantalCenterConstraint])
            
        }
    }
    
    ///Hide description view
    func hideDescriptionView() {
        if let myDescriptionView = self.viewWithTag(998877) as? DescriptionView {
            
            myDescriptionView.isHidden = true
            myDescriptionView.removeFromSuperview()
        }
    }
    
    /**
     Adds Dashed border to a UIView.
     
     - parameter color: Border color.
     
     - returns: void.
     */
    func dashedBorderLayerWithColor(color:CGColor) {
        
        let  borderLayer = CAShapeLayer()
        borderLayer.name  = "borderLayer"
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

        borderLayer.bounds=shapeRect
        borderLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = color
        borderLayer.lineWidth=1
        borderLayer.lineJoin = CAShapeLayerLineJoin.round
        borderLayer.lineDashPattern = NSArray(array: [NSNumber(value: 8),NSNumber(value:4)]) as? [NSNumber]
        
        let path = UIBezierPath.init(roundedRect: shapeRect, cornerRadius: 0)
        
        borderLayer.path = path.cgPath
        
        borderLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: self.layer.cornerRadius).cgPath

        self.layer.addSublayer(borderLayer)

    }
}
