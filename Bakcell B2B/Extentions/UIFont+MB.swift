//
//  UIFont+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/18/17.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    /**
     Returns a Airal font with custom size.
     
     - parameter fontSize: Font size.
     
     - returns: UIFont.
     */
    class func mbArial(fontSize: Float) -> UIFont {
        return UIFont(name: "ArialMT", size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize))
    }
    
    /**
     Returns a Airal Bold font with custom size.
     
     - parameter fontSize: Font size.
     
     - returns: UIFont.
     */
    class func mbArialBold(fontSize: Float) -> UIFont {
        return UIFont(name: "Arial-BoldMT", size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize), weight: UIFont.Weight.bold)
    }
}
