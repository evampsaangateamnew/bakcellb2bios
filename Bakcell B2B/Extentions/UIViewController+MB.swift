//
//  UIViewController+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 05/30/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

extension UIViewController {
    
    //MARK: -  Properties
    var myStoryBoard: UIStoryboard {
        
        if let currentStoryBoard = self.storyboard  {
            return currentStoryBoard
        } else {
            return UIStoryboard(name: "Main", bundle: nil)
        }
    }
    //MARK: -  Functions
    
    /**
     Presents ViewController with animation.
     
     - parameter viewControllerToPresent: ViewController to present.
     - parameter animated: With animation or not.
     - parameter modalTransitionStyle: Presentation style. Bydefault its 'coverVertical'.
     
     - returns: void.
     */
    open func presentPOPUP(_ viewControllerToPresent: UIViewController, animated flag: Bool, modalTransitionStyle:UIModalTransitionStyle = .coverVertical, completion: (() -> Swift.Void)? = nil) {
        DispatchQueue.main.async {
            viewControllerToPresent.modalPresentationStyle = .overCurrentContext
            viewControllerToPresent.modalTransitionStyle = modalTransitionStyle
            
            self.present(viewControllerToPresent, animated: flag, completion: completion)
        }
        
    }
}

extension UIViewController {
    
    /**
     Initialize a nib.
     
     
     - returns: UIViewController.
     */
    public class func fromNib<T>() -> T? where T : UIViewController {
        return fromNib(nibName: nil)
    }
    
    /**
     Initialize a nib.
     
     - parameter nibName: Nib name.
     
     - returns: UIViewController.
     */
    public class func fromNib<T>(nibName: String?) -> T? where T : UIViewController {
        
        let name = nibName ?? String(describing: self)
        return self.init(nibName: name, bundle: Bundle.main) as? T
    }
    
    /**
     Initialize a UIViewController from Storyboard.
     
     
     - returns: UIViewController.
     */
    class func instantiateViewControllerFromStoryboard<T>() -> T? where T : UIViewController {
        return instantiateViewController()
    }
    
    /**
     Initialize a UIViewController from Storyboard.
     
     
     - returns: UIViewController.
     */
    fileprivate class func instantiateViewController<T>() -> T? where T : UIViewController  {
        return UIViewController().myStoryBoard.instantiateViewController(withIdentifier: String(describing: self)) as? T
    }
    
}

extension UIViewController {
    
    ///Show activity indicator with or without animation.
    open func showActivityIndicator(withAnimation isEnable: Bool = true) {
        MBActivityIndicator.shared.showActivityIndicator(withAnimation: isEnable)
    }
    
    ///Hide activity indicator with or without animation.
    open func hideActivityIndicator(withAnimation isEnable: Bool = true) {
        MBActivityIndicator.shared.hideActivityIndicator(withAnimation: isEnable)
    }
    
    ///Hide all activity indicators..
    open func removeAllActivityIndicator(){
        MBActivityIndicator.shared.removeAllActivityIndicator()
    }
}

extension UIViewController {
    
    /**
     Add ViewController as sub view UIView.
     
     - parameter childController: ViewController to add as sub view.
     - parameter onView: Parent view in which you want to add sub view.
     
     - returns: void.
     */
    func addChildViewController(childController: UIViewController, onView holderView: UIView)  {
        
        self.addChild(childController)
        holderView.addSubview(childController.view)
        childController.didMove(toParent: self)
        
        childController.view.snp.makeConstraints { make in
            make.top.equalTo(holderView.snp.top)
            make.right.equalTo(holderView.snp.right)
            make.left.equalTo(holderView.snp.left)
            make.bottom.equalTo(holderView.snp.bottom)
        }
    }
    
    /**
     Remove ViewController from sub view.
     
     - parameter childController: ViewController to remove from sub view.
     
     - returns: void.
     */
    func removeChildViewController(childController: UIViewController)  {
        
        self.children.forEach { (aViewController) in
            
            if aViewController.isKind(of: childController.classForCoder) {
                
                // Notify the child that it's about to be moved away from its parent
                aViewController.willMove(toParent: nil)
                // Remove the child
                aViewController.removeFromParent()
                // Remove the child view controller's view from its parent
                aViewController.view.removeFromSuperview()
                
            }
        }
    }
    
    /**
     Initialize a call.
     
     - parameter number: Number on which call need to initialized.
     
     - returns: void.
     */
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number.trimmWhiteSpace)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        }
    }
    
    /**
     Opens a URL in Safari.
     
     - parameter urlString: URL to open.
     
     - returns: void.
     */
    func openURLInSafari(urlString : String?) {
        
        // Not Valid URL string
        if urlString == nil {
            self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
        } else {
            
            var newURLString = urlString
            if (urlString?.containsSubString(subString: "http://") ?? false) == false &&
                (urlString?.containsSubString(subString: "https://") ?? false) == false{
                
                newURLString = "http://\(urlString ?? "")"
            }
            
            if let requestUrl = URL(string: newURLString?.trimmWhiteSpace ?? "") {
                
                if UIApplication.shared.canOpenURL(requestUrl) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(requestUrl, options: [:], completionHandler:nil)
                    } else {
                        UIApplication.shared.openURL(requestUrl)
                    }
                }  else {
                    self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
                }
                
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }
    }
    
    
    ///Check whether notification are enabled or not.
    func isUserEnabledNotification(completionHandler: @escaping (Bool) -> Void) {
        
        
        if #available(iOS 10.0, *) {
            
            var isEnabled : Bool = false
            let current = UNUserNotificationCenter.current()
            
            current.getNotificationSettings(completionHandler: { (settings) in
                
                if settings.authorizationStatus == .authorized {
                    // Notification permission was already granted
                    isEnabled = true
                    
                } else if settings.authorizationStatus == .notDetermined {
                    // Notification permission has not been asked yet, go for it!
                    isEnabled = false
                    
                } else if settings.authorizationStatus == .denied {
                    // Notification permission was previously denied, go to settings & privacy to re-enable
                    isEnabled = false
                    
                }
                completionHandler(isEnabled)
                
            })
            
        } else {
            
            var isEnabled : Bool = false
            // Fallback on earlier versions
            if let notificationType = UIApplication.shared.currentUserNotificationSettings?.types {
                if notificationType == [] {
                    
                    isEnabled = false
                } else {
                    isEnabled = true
                }
            } else {
                isEnabled = false
            }
            
            completionHandler(isEnabled)
        }
    }
    
    ///Show share Popup.
    func showShareAlert( _ shareActionBlock : @escaping MBButtonCompletionHandler = {})  {
        
        if let shareAlertVC : ShareAlertVC = ShareAlertVC.fromNib() {
            
            shareAlertVC.setShareAlertAction(shareActionBlock: shareActionBlock)
            self.presentPOPUP(shareAlertVC, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
            
        }
    }
    
    /**
     Show confirmation popup. All Parameters are optional.
     
     - parameter title: Popup titile. Bydefualt its 'Confirmation'.
     - parameter message: Message text that need to dispalyed.
     - parameter balanceAmount: Balance amount in popup. Bydefualt its nill.
     - parameter okBtnTitle: Ok button title. Bydefault its 'OK'.
     - parameter cancelBtnTitle: cancel button title. Bydefault its 'Cancel'
     - parameter attributedText: attirbuted text. Bydefault its empty.
     - parameter alertType: Popup type could be 'logout, groupSelection or other'.
     - parameter okActionBlock: ok button action.
     - parameter cancelActionBlock: cancel button action.
     
     - returns: void.
     */
    func showConfirmationAlert(title:String = Localized("Title_Confirmation"), message: String?, balanceAmount:String? = nil, okBtnTitle:String = Localized("BtnTitle_OK"), cancelBtnTitle:String = Localized("BtnTitle_NO"), alertType : ConfirmationAlertVC.ConfirmationLayoutType? = .other, _ okActionBlock : @escaping MBButtonCompletionHandler = {}, _ cancelActionBlock : @escaping MBButtonCompletionHandler = {}) {
        
        self.showConfirmationAlert(title: title, attributedMessage:  MBUtilities.createAttributedText(message, textColor: UIColor.mbTextGray, withManatSign: false), balanceAmount: balanceAmount, okBtnTitle: okBtnTitle, cancelBtnTitle: cancelBtnTitle, alertType: alertType, okActionBlock, cancelActionBlock)
    }
    
    /**
     Show confirmation popup. All Parameters are optional.
     
     - parameter title: Popup titile. Bydefualt its 'Confirmation'.
     - parameter message: Message text that need to dispalyed.
     - parameter balanceAmount: Balance amount in popup. Bydefualt its nill.
     - parameter okBtnTitle: Ok button title. Bydefault its 'OK'.
     - parameter cancelBtnTitle: cancel button title. Bydefault its 'Cancel'
     - parameter attributedText: attirbuted text. Bydefault its empty.
     - parameter alertType: Popup type could be 'logout, groupSelection or other'.
     - parameter okActionBlock: ok button action.
     - parameter cancelActionBlock: cancel button action.
     
     - returns: void.
     */
    func showConfirmationAlert(title:String = Localized("Title_Confirmation"), attributedMessage: NSAttributedString? = NSAttributedString.init(string: ""), balanceAmount:String? = nil, okBtnTitle:String = Localized("BtnTitle_OK"), cancelBtnTitle:String = Localized("BtnTitle_NO"), alertType : ConfirmationAlertVC.ConfirmationLayoutType? = .other, _ okActionBlock : @escaping MBButtonCompletionHandler = {}, _ cancelActionBlock : @escaping MBButtonCompletionHandler = {}) {
        
        if let alert : ConfirmationAlertVC = ConfirmationAlertVC.fromNib() {
            
            alert.setAlertWith(title: title, balanceAmount: balanceAmount, popupType: alertType, message: attributedMessage, okBtnTitle: okBtnTitle, caneclBtnTitle: cancelBtnTitle, okBlock: okActionBlock, cancelBlock: cancelActionBlock)
            
            //            alert.setAlertWith(title: title , balanceAmount: balanceAmount, message: message, okBtnTitle: okBtnTitle, caneclBtnTitle: cancelBtnTitle, okBlock: okActionBlock, cancelBlock: cancelActionBlock)
            self.presentPOPUP(alert, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
            
        } else {
            let alertViewController = UIAlertController(title: title, message: attributedMessage?.string ?? "", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: okBtnTitle, style: .default) { (action) in
                okActionBlock()
            }
            let cancelAction = UIAlertAction(title: cancelBtnTitle, style: .default) { (action) in
                cancelActionBlock()
            }
            
            alertViewController.addAction(okAction)
            alertViewController.addAction(cancelAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    ///Show logout popup.
    func showLogOutAlert(_ logOutActionBlock : @escaping MBButtonCompletionHandler = {}, _ cancelActionBlock : @escaping MBButtonCompletionHandler = {}) {
        
        if let alert : ConfirmationAlertVC = ConfirmationAlertVC.fromNib() {
            
            alert.setLogoutAlert({
                //Logout
                logOutActionBlock()
            }, cancelBlock: {
                //cancel
                cancelActionBlock()
            })
            self.presentPOPUP(alert, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
            
        } else {
            
            let alertViewController = UIAlertController(title: Localized("Title_Logout"), message: Localized("Message_LogoutMessage"), preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: Localized("BtnTitle_LOGOUT"), style: .default) { (action) in
                logOutActionBlock()
            }
            let cancelAction = UIAlertAction(title: Localized("BtnTitle_CANCEL"), style: .default) { (action) in
                cancelActionBlock()
            }
            
            alertViewController.addAction(okAction)
            alertViewController.addAction(cancelAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    /**
     Show popup with message and amount. All Parameters are optional.
     
     - parameter message: Message text that need to dispalyed.
     - parameter amountString: Balance amount in popup. Bydefualt its nill.
     - parameter actionBlock: ok button action.
     
     
     - returns: void.
     */
    func showAlertWithMessage(message: String?,amountString : String? = nil, actionBlock : @escaping MBButtonCompletionHandler = {}) {
        
        self.showAlert(title: Localized("Title_Alert"),message: message, balanceAmount:amountString, actionBlock)
    }
    
    /**
     Show error popup with message.
     
     - parameter message: Message text that need to dispalyed.
     - parameter actionBlock: ok button action.
     
     
     - returns: void.
     */
    func showErrorAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = {}) {
        self.showAlert(title: Localized("Title_Error"), message: message, actionBlock)
    }
    
    /**
     Show success popup with message.
     
     - parameter message: Message text that need to dispalyed.
     - parameter actionBlock: ok button action.
     
     
     - returns: void.
     */
    func showSuccessAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = {}) {
        self.showAlert(title: Localized("Title_successful"), message: message, actionBlock)
    }
    
    /**
     Show popup with parameters.
     
     - parameter title: Popup titile.
     - parameter message: Message text that need to dispalyed.
     - parameter balanceAmount: Balance amount in popup. Bydefualt its nill.
     - parameter btnTitle: Ok button title. Bydefault its 'OK'.
     
     - returns: void.
     */
    func showAlert(title:String?, message: String?, balanceAmount:String? = nil, btnTitle:String = Localized("BtnTitle_OK"), _ actionBlock : @escaping MBButtonCompletionHandler = {}) {
        
        self.showAlert(title: title, attributedMessage: MBUtilities.createAttributedText(message, textColor: UIColor.mbTextGray, withManatSign: false), balanceAmount: balanceAmount, btnTitle: btnTitle, actionBlock)
    }
    
    /**
     Show popup with parameters.
     
     - parameter title: Popup titile.
     - parameter attributedMessage: Message text that need to dispalyed.
     - parameter balanceAmount: Balance amount in popup. Bydefualt its nill.
     - parameter btnTitle: Ok button title. Bydefault its 'OK'.
     
     - returns: void.
     */
    func showAlert(title:String?, attributedMessage : NSAttributedString?, balanceAmount:String? = nil, btnTitle:String = Localized("BtnTitle_OK"), _ actionBlock : @escaping MBButtonCompletionHandler = {}) {
        
        if let alert:AlertMessageVC = AlertMessageVC.fromNib() {
            
            alert.setAlertWith(title: title ?? "", balanceAmount: balanceAmount, attributedMessage: attributedMessage, btnTitle: btnTitle , cancelBlock: actionBlock)
            self.presentPOPUP(alert, animated: true, modalTransitionStyle: .crossDissolve, completion: nil)
            
        } else {
            let alertViewController = UIAlertController(title: title ?? "", message: attributedMessage?.string ?? "", preferredStyle: .alert)
            
            
            let okAction = UIAlertAction(title: btnTitle, style: .default) { (action) in
                actionBlock()
            }
            
            alertViewController.addAction(okAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    /**
     Show success popup with message.
     
     - parameter message: Message text that need to dispalyed.
     
     - returns: void.
     */
    func showAccessAlert(message: String?) {
        
        let alertViewController = UIAlertController(title: Localized("Title_Alert"), message: message ?? "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: Localized("BtnTitle_CANCEL"), style: .cancel) { (action) in }
        
        let settingAction = UIAlertAction(title: Localized("BtnTitle_SETTING"), style: .default) { (action) in
            
            if let url = URL(string: UIApplication.openSettingsURLString ), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
            }
        }
        
        alertViewController.addAction(cancelAction)
        alertViewController.addAction(settingAction)
        
        self.present(alertViewController, animated: true, completion: nil)
        
    }
    
    /**
     Show date picker as popup.
     
     - parameter minDate: Minimum date.
     - parameter maxDate: Maximum date.
     - parameter currentDate: Current date.
     
     - returns: didSelectDate(Selected date).
     */
    func showNewDatePicker(minDate : Date , maxDate : Date, currentDate : Date, didSelectDate : @escaping MBDatePickerVC.MBDatePickerCompletionHandler) {
        if let datePickerVC : MBDatePickerVC = MBDatePickerVC.fromNib() {
            datePickerVC.setDatePicker(minDate: minDate, maxDate: maxDate, currentDate: currentDate, didSelectDateBlock: didSelectDate)
            self.presentPOPUP(datePickerVC, animated: true)
        }
    }
    
    
    /**
     Redirect user to AppStore
     - returns: void
     */
    func redirectUserToAppStore() {
        if let storeURL = URL(string: MBUtilities.getAppStoreURL()),
            UIApplication.shared.canOpenURL(storeURL) {
            
            if #available(iOS 10, *) {
                UIApplication.shared.open(storeURL, options: [:], completionHandler: { (success: Bool) in
                    
                    // If failded to open URL
                    if success == false {
                        UIApplication.shared.openURL(storeURL)
                    }
                })
            } else {
                UIApplication.shared.openURL(storeURL)
            }
            
        } else {
            self.showErrorAlertWithMessage(message: Localized("Message_CannotAbleToRedirect"))
        }
    }
}


