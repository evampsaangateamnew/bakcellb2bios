//
//  Array+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 12/19/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import ObjectMapper

extension Array where Element: Copyable {
    
    /**
     Copy an arry objects.
   
     
     - returns: Coppied array.
     */
    func copy() -> Array {
        var copiedArray = Array<Element>()
        for element in self {
            copiedArray.append(element.copy())
        }
        return copiedArray
    }
    
    /**
     Converts arry to json string.
     
     
     - returns: Json String.
     */
    func toJSONString() -> String {
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: [])
            return String(data: jsonData, encoding: .utf8) ?? ""
            
        } catch {
            return ""
        }
        
    }
}


public extension Array where Element: Mappable {
    
    /// Save JSON string into user defaults
    func saveInUserDefaults (key : String) {
        
        UserDefaults.standard.set(self.toJSONString(), forKey: key)
        UserDefaults.standard.synchronize()
    }
}
