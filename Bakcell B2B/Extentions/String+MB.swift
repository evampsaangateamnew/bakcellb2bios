//
//  String+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 05/30/18.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    //MARK: - Properites
    
    var length: Int {
        return self.count
    }
    
    var countWithOutSpace: Int {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newString.count
    }
    
    var trimmWhiteSpace: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var toInt: Int {
        
        if self != "" {
            
            let newString = self.trimmWhiteSpace
            
            if let cValue = Int(newString) {
                return cValue
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    var lengthWithOutSpace: Int {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newString.count
    }
    
    var toDouble: Double {
        
        if self != "" {
            
            let newString = self.trimmWhiteSpace
            // Try to convert into double
            if let cValue = Double(newString) {
                return cValue
            } else {
                return 0
            }
        } else {
            return 0.0
        }
    }
    
    var toFloat: Float {
        
        if self != "" {
            
            let newString = self.trimmWhiteSpace
            // Try to convert into double
            if let cValue = Float(newString) {
                return cValue
            } else {
                return 0
            }
        } else {
            return 0.0
        }
    }
    
    
    ///To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    /// Check that is string contains only numbers or not
    var isNumeric: Bool {
        return !isEmpty && range(of: "[^0-9]", options: .regularExpression) == nil
    }
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    /// Return first character of string
    var firstCharacter : String {
        let inputString : [String] = self.map { String($0) }
        return inputString.first!
    }
    
    
    var urlEncode : String {
        if self.count <= 0 {
            return ""
        }
        let escapedString : String = self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        return escapedString
    }
    
    var urlDecode : String {
        if self.count <= 0 {
            return ""
        }
        let decodeString : String = self.removingPercentEncoding!
        return decodeString
    }
    
    var removeHyphen : String {
        return self.replacingOccurrences(of: "-", with: "")
    }
    
    var replaceSpaceWithHyphen : String {
        return self.replacingOccurrences(of: " ", with: "_")
    }
    
    //MARK: - Functions
    
    ///Remove null from string
    func removeNullValues() -> String {
        
        if self.isBlank {
            return ""
        }
        
        if self.lowercased() == "null" || self.lowercased() == "nil" {
            return ""
        } else {
            return self
        }
    }
    
    /// Check that the string text is a valid email type
    func isValidEmail() -> Bool {
        
        if self.isBlank {
            return false
        }
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        
        let result =  emailPredicate.evaluate(with: self)
        
        return result
        
    }
    
    
    
    /// Check that the string text is a valid email type
    func isValidUserName() -> Bool {
        if self.isBlank {
            return false
        }
        
        if self.length >= Constants.minUserLength && self.length <= Constants.maxUserLength {
            
            // Allow charactors check
            let allowedCharactersString = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharactersForPIC
            let allowedCharacters = CharacterSet(charactersIn: allowedCharactersString)
            let newCharacterSet = CharacterSet(charactersIn: self)
            
            let result =  allowedCharacters.isSuperset(of: newCharacterSet)
            
            return result
            
        }
        
            return false
    }
    
    
    /**
     Check whether MSISDN is valid or not.
     
     - returns: MSISDN status (Bool).
     */
    func isValidMSISDN() -> Bool {
        
        if self.length == Constants.MSISDNLength {
            
            if self.isNumeric {
                return true
                
            } else {
                return false
            }
            
        } else {
            return false
        }
    }
    
    /**
     Check whether Card number is valid or not.
     
     - returns: Card number status (Bool).
     */
    func isValidCardNumber() -> Bool {
        
        if self.length == Constants.ValidCardNumberLength,
            self.isNumeric {
            
            return true
            
        } else {
            return false
        }
    }
    
    ///Check if sting is equal to 'Active' or not.
    func isActive() -> Bool {
        
        if self.isBlank {
            return false
        }
        return self.isEqual("Active", ignorCase: true)
    }
    
    
    ///Convet base64 to String
    func fromBase64() -> String {
        if let data = Data(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0)) {
            return String(data: data, encoding: String.Encoding.utf8) ?? ""
        }
        return ""
    }
    
    ///Convet String to base64
    func toBase64() -> String {
        if let data = self.data(using: String.Encoding.utf8) {
            return data.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        }
        return ""
    }
    
    ///Check if sting is equal to 'true' or not.
    func isTrue() -> Bool {
        
        if self.isBlank {
            return false
        }
        
        if self.lowercased() == "true" {
            return true
        } else {
            return false
        }
    }
    
    /**
     Check if string contain sub string or not.
     
     - parameter subString: Substring to check in string.
     
     - returns: Substring existance status(Bool).
     */
    func containsSubString(subString : String?) -> Bool {
        
        let mainLowerString : String = self.lowercased()
        
        if let subString = subString {
            let subLowerString = subString.lowercased()
            
            return mainLowerString.contains(subLowerString)
            
        } else {
            return false
        }
    }
    
    /**
     Check if two string are equal or not.
     
     - parameter toString: string to compare with.
     - parameter ignorCase: ignorecase  Bool.
     
     - returns: Equal status(Bool).
     */
    func isEqual(_ toString : String?, ignorCase :Bool = false) -> Bool {
        
        // Check that if self is Empty
        if self.isBlank {
            return false
        }
        
        // Check that if self is Empty
        if toString?.isBlank ?? true {
            return false
        }
        
        let mainLowerString : String = ignorCase ? self.lowercased() : self
        
        if let otherString = toString {
            let otherLowerString = ignorCase ? otherString.lowercased() : otherString
            
            return mainLowerString == otherLowerString
            
        } else {
            return false
        }
    }
    
    /// Check whether a string contain alphabets or not.
    func containsAlphabets() -> Bool {
        
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedAlphbeats)
        
        if self.rangeOfCharacter(from: allowedCharacters) != nil {
            return true
        } else {
            return false
        }
    }
    
    /// Check whether a string contain small alphabets or not.
    func constainsSmallAlphabets() -> Bool{
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedSmallAlphabets)
        if self.rangeOfCharacter(from: allowedCharacters) != nil {
            return true
        }else{
            return false
        }
        
    }
    
    /// Check whether a string contain Capital alphabets or not.
    func constainsCapitalAlphabets() -> Bool{
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedCapitalAlphabets)
        if  self.rangeOfCharacter(from: allowedCharacters) != nil {
            return true
        }else{
            return false
        }
        
    }
    
    /// Check whether a string contain Digits or not.
    func containsDigits() -> Bool {
        
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        let allowedCharacters = CharacterSet(charactersIn: Constants.allowedNumbers)
        
        if self.rangeOfCharacter(from: allowedCharacters) != nil {
            return true
        } else {
            return false
        }
    }
    
    /// Check whether a string contain Special Characters or not.
    func containsSpecialCharacters() -> Bool {
        
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        let disallowedCharacters = CharacterSet(charactersIn: Constants.allowedSpecialCharacters)
        
        if self.rangeOfCharacter(from: disallowedCharacters) != nil {
            return true
        } else {
            return false
        }
    }
    
    /// Check whether a string is number or not.
    func isStringAnNumber() -> Bool {
        
        let valueString = self.trimmWhiteSpace
        //First try to convert into Int
        if let _ = Int(valueString) {
            
            return true
        } else {
            
            // If cannot convert to Int then try to convert in Double
            if let _ = Double(valueString) {
                return true
                
            } else {
                return false
            }
        }
        
    }
    
    /// Check button enabled or based on string value if '1' mean its active otherwise its inactive.
    func isButtonEnabled() -> Bool {
        if self.isBlank {
            return false
        }
        
        if self == "1" {
            return true
        } else {
            return false
        }
    }
    
    func offSetFirstValue() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        //        "2017-10-03 12:00:00.0"
        formatter.calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        //    formatter.locale = Locale(identifier: "PST")
        
        if let date = formatter.date(from: self) {
            
            var calender = Calendar.current
            switch MBLanguageManager.userSelectedLanguage() {
                
            case .russian:
                calender.locale = Locale(identifier: "ru")
            case .english:
                calender.locale = Locale(identifier: "en")
            case .azeri:
                calender.locale = Locale(identifier: "az-Cyrl")
            }
            
            let dateComponentsFormatter = DateComponentsFormatter()
            dateComponentsFormatter.calendar = calender
            dateComponentsFormatter.allowedUnits = [.year,.month,.day,.hour,.minute,.second]
            dateComponentsFormatter.unitsStyle = .short
            dateComponentsFormatter.collapsesLargestUnit = false
            let componetArray : [String] = (dateComponentsFormatter.string(from: date, to: Date()) ?? "").components(separatedBy: ",")
            
            //  print("\n\n\n\(dateComponentsFormatter.string(from: date, to: Date()) ?? "") \n\n")
            //  “9h 41m 30s”
            
            return componetArray.first ?? ""
            
        } else {
            return ""
        }
    }
    
    /// Check whether a string has 'Free' or 'Unlimited' text.
    ///  - returns: Bool.
    
    func isHasFreeOrUnlimitedText() -> Bool {
        
        if self.isBlank {
            return false
        }
        if self.isHasFreeText() ||
            self.isEqual("Unlimited", ignorCase: true) ||
            self.isEqual("Безлимитный", ignorCase: true) ||
            self.isEqual("Limitsiz", ignorCase: true) {
            
            return true
        } else {
            return false
        }
        
    }
    
    /// Check whether a string has 'Free' text or not.
    ///  - returns: Bool.
    func isHasFreeText() -> Bool {
        
        if self.isBlank {
            return false
        }
        if self.isEqual("Free", ignorCase: true) ||
            self.isEqual("БЕСПЛАТНО", ignorCase: true) ||
            self.isEqual("PULSUZ", ignorCase: true) {
            
            return true
        } else {
            return false
        }
        
    }
    
    /// Check whether a string has 'Get' text or not.
    ///  - returns: Bool.
    func isHasGetText() -> Bool {
        
        if self.isBlank {
            return false
        }
        if self.isEqual("Get", ignorCase: true) ||
            self.isEqual("Предоставляется", ignorCase: true) ||
            self.isEqual("Təqdim edilir ", ignorCase: true) {
            
            return true
        } else {
            return false
        }
        
    }
    
    func localizedAPIKey() -> String {
        
        switch MBLanguageManager.userSelectedLanguage() {
        case .english:
            return self + "_Englist"
        case .azeri:
            return self + "_Azeri"
        case .russian:
            return self + "_Russian"
        }
    }
    
    /// Check whether a string has 'Free' or 'Unlimited' text.
    ///  - returns: Bool.
    
    func isGroupPayBySUBs() -> Bool {
        
        if self.isBlank {
            return false
        }
        if self.isEqual(Constants.K_PayBySUBs, ignorCase: true) ||
            self.isEqual(Constants.K_PayBySUB, ignorCase: true){
            
            return true
        } else {
            return false
        }
        
    }
    
    
    
    
    /**
     Create and returns attributed text with custom color.
     
     - parameter stringsListForAttributed: Array of strings that need to be attributed.
     - parameter stringColor: Color of Attributed strings.
     
     - returns: NSMutableAttributedString.
     */
    func createAttributedString(stringsListForAttributed: [String], stringColor: UIColor) ->  NSMutableAttributedString {
        
        
        let newStr = NSMutableAttributedString(string: self)
        for  text in stringsListForAttributed {
            newStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont.mbArialBold(fontSize: 14), range: (self as NSString).range(of: text))
            newStr.addAttribute(NSAttributedString.Key.foregroundColor, value: stringColor, range: (self as NSString).range(of: text))
        }
        
        return newStr
    }
    
    ///Return wheter string cotain characters othere than allowed or not.
    func containsOtherThenAllowedCharactersForFreeSMSInEnglish() -> Bool {
        
        // Check that if self is Empty
        if self.isBlank{
            return false
        }
        
        // Allow charactors check
        let allowedCharactors = Constants.allowedAlphbeats + Constants.allowedNumbers + Constants.allowedSpecialCharacters + " \n"
        let allowedCharacters = CharacterSet(charactersIn: allowedCharactors)
        let characterSet = CharacterSet(charactersIn: self)
        
        if allowedCharacters.isSuperset(of: characterSet) == true {
            return false
        } else {
            return true
        }
    }
}
