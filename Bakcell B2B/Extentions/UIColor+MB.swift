//
//  UIColor+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 8/15/17.
//  Copyright © 2018 evampsaanga. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    public class var mbBrandRed : UIColor {
        return UIColor(hexString: "#E00034")
    }
    public class var mbStatusBarRed : UIColor {
        //  return UIColor.init(red: 230.0/255.0, green: 40.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        return UIColor.mbBrandRed
    }
    
    public class var mbTextGray: UIColor{
        return UIColor(hexString: "#808080")
    }
    public class var mbTextFieldTextGray: UIColor{
        return UIColor(hexString: "#7F7F7F")
    }
    public class var mbPlaceholderGray: UIColor{
        return UIColor.mbTextFieldTextGray
    }
    public class var mbPlaceholderGrayDisable: UIColor{
        return UIColor(hexString: "#CCCCCC")
    }
    
    public class var mbTextBlack: UIColor{
        return UIColor (hexString: "#363636")
    }
    
    public class var mbTextLighGray : UIColor{
        return UIColor (hexString: "#BEBEBE")
    }
    
    public class var mbButtonBackgroundGray: UIColor{
        return UIColor(hexString: "#BEBEBE")
    }
    
    public class var mbGreen: UIColor{
        return UIColor (hexString: "#6DBC36")
    }
    
    public class var mbBorderGray: UIColor{
        return UIColor (hexString: "#ECECEC")
    }
    public class var mbBackgroundGray: UIColor{
        return UIColor.mbBorderGray
    }
    public class var mbBackgroundLightGray: UIColor{
        return UIColor.mbSeperator
    }
    
    public class var mbBarRed: UIColor{
        return UIColor(hexString: "#FF3939")
        
    }
    public class var mbBarOrange: UIColor{
        return UIColor(hexString: "#FF8A3B")
    }
    public class var mbBarGreen: UIColor{
        return UIColor(hexString: "#73F473")
    }
    public class var mbSeperator : UIColor{
        return UIColor(hexString: "#EBEBEB")
    }
    public class var mbDarkSeperator : UIColor{
        return UIColor(hexString: "#BEBEBE")
    }
    public class var mbDottedBorder : UIColor {
        return UIColor(hexString: "#D3D2D2")
    }
    public class var mbArrowTintGray : UIColor{
        return UIColor(hexString: "#BFBFBF")
    }
    
}

// Based on GradientProgressBar Library
extension UIColor {
    /// Create color from RGB
    convenience init(absoluteRed: Int, green: Int, blue: Int) {
        self.init(
            absoluteRed: absoluteRed,
            green: green,
            blue: blue,
            alpha: 1.0
        )
    }
    
    /// Create color from RGBA
    convenience init(absoluteRed: Int, green: Int, blue: Int, alpha: CGFloat) {
        let normalizedRed = CGFloat(absoluteRed) / 255
        let normalizedGreen = CGFloat(green) / 255
        let normalizedBlue = CGFloat(blue) / 255
        
        self.init(
            red: normalizedRed,
            green: normalizedGreen,
            blue: normalizedBlue,
            alpha: alpha
        )
    }
    
    // Color from HEX-Value
    // Based on: http://stackoverflow.com/a/24263296
    convenience init(hexValue:Int) {
        self.init(
            absoluteRed: (hexValue >> 16) & 0xff,
            green: (hexValue >> 8) & 0xff,
            blue: hexValue & 0xff
        )
    }
    
    // Color from HEX-String
    // Based on: http://stackoverflow.com/a/27203691
    convenience init(hexString:String) {
        var normalizedHexString = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (normalizedHexString.hasPrefix("#")) {
            normalizedHexString.remove(at: normalizedHexString.startIndex)
        }
        //       normalizedHexString = normalizedHexString.replacingOccurrences(of: "#", with: "")
        
        // Convert to hexadecimal integer
        var hexValue:UInt32 = 0
        Scanner(string: normalizedHexString).scanHexInt32(&hexValue)
        
        self.init(
            hexValue:Int(hexValue)
        )
    }
}
