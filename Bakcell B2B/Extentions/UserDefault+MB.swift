//
//  UserDefault+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 10/9/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
extension UserDefaults {

    /**
     Save string in user default.
     
     - parameter stringValue: string to save.
     - parameter forKey: key against which you want to save value in userdefault.
     
     - returns: void.
     */
    public class func saveStringForKey(stringValue: String, forKey key: String) {
        UserDefaults.standard.set(stringValue, forKey: key)
        UserDefaults.standard.synchronize()
    }

    /**
     Save Bool in user default.
     
     - parameter boolValue: bool value to save.
     - parameter forKey: key against which you want to save value in userdefault.
     
     - returns: void.
     */
    public class func saveBoolForKey(boolValue: Bool, forKey key: String) {
        UserDefaults.standard.set(boolValue, forKey: key)
        UserDefaults.standard.synchronize()
    }
}
