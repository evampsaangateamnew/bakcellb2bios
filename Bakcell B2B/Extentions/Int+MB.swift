//
//  Int+MB.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 8/8/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation

extension Int {
    
    ///Convert Int to string
    func toString() -> String {
        return "\(self)"
    }
     ///Convert Int to double
    func toDouble() -> Double {
        return Double(self)
    }
}
