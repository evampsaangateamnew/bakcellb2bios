//
//  TimeZone+MB.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 10/3/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation
extension TimeZone{
    static func appTimeZone() -> TimeZone {
        return self.init(abbreviation: "UTC") ??  self.current
    }
}
