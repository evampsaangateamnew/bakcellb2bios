//
//  NSError+MB.swift
//  Bakcell B2B
//
//  Created by Waqar Ahmed on 8/7/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import Foundation

let MBErrorDomain = "com.errordomain.bakcell"
let MBFormErrorCode = 40001

// typealias MBAlertCompletionHandler = ( retry: Bool,  cancel: Bool) -> Void

extension NSError {
    
    /**
     Show alert controller with title.
     
     - parameter title: title of alert controller.
     
     - returns: UIAlertController.
     */
    func alertControllerWithTitle(_ title: String) -> UIAlertController {
        return UIAlertController(title: title, message: localizedDescription, preferredStyle: .alert)
    }
    
    /**
     Show server error inside UIViewController.
     
     - parameter viewController: View controller in which you want to show error..
     
     - returns: void.
     */
    func showServerErrorInViewController(_ viewController: UIViewController, _ actionBlock : @escaping MBButtonCompletionHandler = {}) {
        
        if (viewController.isViewLoaded && (viewController.view.window != nil)) {
            viewController.showAlert(title: Localized("Title_Error"), message: localizedDescription, balanceAmount: nil, btnTitle: Localized("BtnTitle_OK"), actionBlock)
        }
    }
}
