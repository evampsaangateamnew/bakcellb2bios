//
//  UITextField+MB.swift
//  Bakcell
//
//  Created by AbdulRehman Warraich on 9/18/17.
//  Copyright © 2017 evampsaanga. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    /**
     Check whether MSISDN is valid or not.
     
     - returns: MSISDN status (Bool).
     */
    func isValidMSISDN() -> Bool {
        
        if let text = self.text {
            
            return text.isValidMSISDN()
        } else {
            return false
        }
    }
    
    /**
     Check whether Card number is valid or not.
     
     - returns: Card number status (Bool).
     */
    func isValidCardNumber() -> Bool {
        
        if let text = self.text {
            
            return text.isValidCardNumber()
        } else {
            return false
        }
    }
    
    ///Removes '-' from String.
    func plainText() -> String {
        
        if let text = self.text {
            
            let charsToRemove: Set = Set("-")
            let newNumberCharacters = String( text.filter { !charsToRemove.contains($0) })
            
            if newNumberCharacters.isBlank {
                return ""
            } else {
                return newNumberCharacters
            }
        }
        
        return ""
    }
    
    func adjustsFontSizeToFitDevice() {
        
        switch UIDevice().screenType {
            
        case .iPhone4, .iPhone5:
            self.font = UIFont.mbArial(fontSize: 14)
            break
            
        case .iPhone6, .iPhone6Plus, .iPhoneX:
            self.font = UIFont.mbArial(fontSize: 16)
            break
            
        default:
            self.font = UIFont.mbArial(fontSize: 14)
        }
    }
    
}
