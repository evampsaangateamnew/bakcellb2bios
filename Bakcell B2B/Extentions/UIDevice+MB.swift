//
//  UIDevice+MB.swift
//  Bakcell B2B
//
//  Created by AbdulRehman Warraich on 5/23/18.
//  Copyright © 2018 Evamp&Saanga. All rights reserved.
//

import UIKit

public extension UIDevice {
    enum ScreenType: String {
        case iPhone4
        case iPhone5
        case iPhone6
        case iPhone6Plus
        case iPhoneX
        case Unknown
    }
    
    //MARK: - Properties
    var iPhone: Bool {
        return UIDevice().userInterfaceIdiom == .phone
    }
    var screenType: ScreenType {
        guard iPhone else { return .Unknown}
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone6
        case 2208:
            return .iPhone6Plus
        case 2436:
            return .iPhoneX
        default:
            return .Unknown
        }
    }
    var additionalFontSizeAccourdingToScreen: CGFloat {
        switch UIDevice().screenType {
            
        case .iPhone4, .iPhone5:
            return 0
        case .iPhone6:
            return 2
        case .iPhone6Plus, .iPhoneX:
            return 2
        default:
            return 0
        }
    }
    
    //MARK: - Functions
    
    /**
     Get UUID of Vendor and remove "_" from uuidString
     
     - returns: UUID string of current vendor
     */
    static func deviceID() -> String {
        if let UDID : String = UIDevice.current.identifierForVendor?.uuidString {
            return UDID.removeHyphen
        } else {
            return ""
        }
    }
    
    /**
     Returns app version.
     
     - returns: Version number string of current app.
     */
    static func appVersion() -> String {
        guard let dictionary = Bundle.main.infoDictionary else {
            return ""
        }
        if let version : String = dictionary["CFBundleShortVersionString"] as? String {
            return version
        } else {
            return ""
        }
    }
   
    
}
